/* Multi-start metaheuristic
 * Marcelo Friske
 * 10/08/2018
 *  
 */ 
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>
//~ #include <ilcplex/ilocplex.h>
#include "../include/util.h"
#include "../include/solution.h"
#include "../include/model.h"

//~ #define NDEBUG
#include <assert.h>
ILOSTLBEGIN
#define NCSMA
#define NFixAndOptimize
//~ #define NLNS

using namespace std;
using mirp::Solution;
using mirp::Instance;
using mirp::Model;

default_random_engine engine;
int seed1;

/* Multistart algorithm with MIP embeded in a CSMA algorithm
 * Params:
 * @ file - path to the instance file
 * @ output - file to print the final results (time, best obj, etc)
 * @ timeLimit - of the whole algorithm/MIP iterations
 * @ numSolutions - number of solutions generated at each iteration of 
 * CSMA
 * @ probSelectBestVessel - probability [0,1] of selecting the first 
 * vessel of the list in the selected criteria
 * @ nRoutes - number of best routes of each vessel addded to the MIP
 * @ nSolutions - number of best solutions added to the MIP
 * @ ageMax - max age of the variables in the CSMA
 * @ r [0.0,100.0] - percentage of the variables with age>ageMax with worst reduced 
 * cost which are removed from the MIP 
 */ 
void  mirp::multiStart(string file, ofstream& output, const double& 
timeLimit, const unsigned int& numSolutions, const float& 
probSelectBestVessel, const float& probRandomSelectCounterPartNode, const unsigned int& nRoutes, const unsigned int& 
nSolutions, const unsigned int& ageMax, const float& r, const float& probRandomSelect2ndPort){
	///Time parameters
	Timer<chrono::milliseconds> timer_global(timeLimit*1000);
	Timer<chrono::milliseconds> timer_cplex;
	Timer<chrono::milliseconds> timer_model;
	float global_time {0};
	float cplex_time {0};
	float model_time {0};
	float modelObj;
	
	size_t pos = file.find("LR");
	string str = file.substr(pos);
	str.erase(str.end()-1);
	stringstream ss,ss1;
	//Log file
	ss << "log_" << str << ".txt";
	ofstream out(ss.str());
    streambuf *coutbuf = cout.rdbuf(); //save old buf
    cout.rdbuf(out.rdbuf()); //redirect std::cout to out
    //~ cout.rdbuf(0); //redirect std::cout to null
	//Solution obj file
	ss1 << "s_" << str << ".txt";
	ofstream outS(ss1.str());
	
	IloEnv env;
    Instance inst(env);
    inst.readInstance(env, file);
    timer_global.start();
    
    vector<Solution> solutions;
    //Used for sorting the solutions in ascending order
    priority_queue< pair<unsigned int, float>, vector<pair<unsigned int,float>>, ComparePairMinorFloatFirst> solutionsObj; 
    //For each vessel, store the route in ascending order of objective function - [v][pq<id sol,route obj>]
    vector< priority_queue< pair<unsigned int, float>, vector<pair<unsigned int,float>>, ComparePairMinorFloatFirst > > vesselRoutes(inst.V);
    //Vessel avaiable for perform voyages (restricted when using LNS)
    vector<unsigned int> avVessels;
    for(unsigned int v=0;v<inst.V;v++){
		avVessels.push_back(v);
	}
    
    unsigned int idSol = 0,bestSol=0, it;
    float bestSolValue =  1E10;
          
    unsigned int iteration = 1;
    double bestObj = 1E10;   
    
    //Randomness 
    int seed1 = 1;
	engine.seed(seed1);
    try{
		///Build the instance graph and set model parameters
		timer_model.start();		
		Model model(env);		
		#ifdef NFixAndOptimize
		model.buildNetwork(env,inst);				
		#endif
		#ifndef NFixAndOptimize
		model.buildFixAndRelaxModel(env,inst,1,0,true,false,false,false,true,false); //For using MIPStart
		#endif
		model.setParameters(env, timeLimit);
		model_time += timer_model.total();
		
		#ifndef NCSMA
		while(!timer_global.reachedTimeLimit()){   //CMSA loop 		
		#endif
			for(it=0;it<numSolutions;it++){ //Multi-start loop 
				//~ inst.generator.seed(it+1);
				idSol = it + ((iteration-1)*numSolutions);
				Solution s;
				s.buildSolution(inst, false, avVessels, probSelectBestVessel, probRandomSelectCounterPartNode, probRandomSelect2ndPort);
				solutions.push_back(s);
												
				for (unsigned int v=0;v<inst.V;v++){				
					vesselRoutes[v].push(make_pair(idSol,solutions[idSol].vesselRouteCostSpot[v])); //Add the cost of the route of veesel v in solution idSol
				}
				solutionsObj.push(make_pair(idSol,solutions[idSol].objectiveValue));  			
				cout << "\n Solution " << idSol << ": " << solutions[idSol].objectiveValue << endl;
				//~ s.print(inst);
				outS << "Solution " << idSol << ": " << 
				solutions[idSol].objectiveValue;
				
				if(solutionsObj.top().second < bestSolValue){
					bestSol = solutionsObj.top().first;
					bestSolValue = solutionsObj.top().second;					
				}						
				outS << " - Best (" << bestSol << ") : " << bestSolValue 
				<< endl;
			}						
			goto noMIP;
			///Define from the best routes and solutions the variables to be added to the model
			#ifdef NFixAndOptimize
			model.setRoute(inst, vesselRoutes, solutions,nRoutes,false); 
			model.setSolutions(inst,solutions,solutionsObj,nSolutions,true);			
			timer_model.start();
			if(iteration == 1){
				model.buildReducedModel(env,inst);		
			}else{				
				model.addComponentsToModel(env,inst);
			}
			model_time += timer_model.total();
			//~ model.cplex.exportModel("reducedMIP.lp");			
			///Optimize
			model.cplex.setParam(IloCplex::TiLim, max(0.0,timeLimit-timer_global.total()/1000));
			timer_cplex.start();
			if(model.cplex.solve()){
				modelObj = model.cplex.getObjValue();
				if(modelObj < bestObj){
					bestObj = modelObj;					
				}				
			}else{
				cout <<  "No solution by the model in iteration " << iteration << endl;
			}
			cplex_time += timer_cplex.total();
			#endif
			//F&O
			#ifndef NFixAndOptimize
			model.setMIPStart(env, inst,solutions[bestSol]);
			model.cplex.setParam(IloCplex::TiLim, 10);
			timer_cplex.start();
			if(model.cplex.solve()){
				modelObj = model.cplex.getObjValue();
				if(modelObj < bestObj){
					bestObj = modelObj;					
				}				
			}else{
				cout <<  "No solution by the model \n";
			}
			cplex_time += timer_cplex.total();			
			model.fixAllSolution(env, inst);
			float elapsed_time=0;
			unsigned int stopsByGap=0,stopsByTime=0;
			model.improvementPhaseVND_vessels(env, inst, 20, 0, bestObj, timer_cplex, cplex_time, 300, elapsed_time, stopsByGap, stopsByTime);
			#endif
			cout << "Iteration " << iteration << " objective value " << modelObj << endl;
			outS << "Iteration " << iteration << " objective value " << modelObj 
			<< " Best bound " << model.cplex.getBestObjValue() << "(" 
			<< model.cplex.getMIPRelativeGap()*100 << "%)" 
			<< " - Best obj " << bestObj << endl;
			#ifndef NCSMA
			///Remove r% of the variables with the largest reduced cost that achieved the maxAge
			timer_model.start();
			model.adapt(env, inst, r, ageMax);
			model_time += timer_model.total();
							
			iteration++;
			#endif
		#ifndef NCSMA
		}
		#endif
		//~ cout << "MIP Solution: \n";
		//~ model.printSolution(env, inst, inst.t);
	}catch (IloException& e) {
		cerr << "Concert exception caught: " << e << endl;		
		e.end();
	}
	catch (...) {
		cerr << "Unknown exception caught" << endl;
	}
	noMIP:    
    //~ solutions[bestSol].print(inst);
    Solution best(solutions[bestSol]);
    int itLNS=0;
    #ifndef NLNS
    Solution current(solutions[bestSol]);    
    Solution candidate;
	//LNS
	cout << "Starting LNS\n";		
	while(!timer_global.reachedTimeLimit()){	
		candidate = Solution(current);
		avVessels = candidate.destroy(inst,2);
		
		candidate.rebuild(inst, 1, avVessels, probSelectBestVessel, probRandomSelectCounterPartNode, probRandomSelect2ndPort);
		cout << endl << itLNS << " - ";
		cout << "Candidate objective " << candidate.objectiveValue << endl;
		cout << "Current objective " << current.objectiveValue << endl;
		cout << "Best objective " << best.objectiveValue << endl;
		if(candidate.objectiveValue < current.objectiveValue){
			current = Solution(candidate);
		}
		if(candidate.objectiveValue < best.objectiveValue){
			best = Solution(candidate);
		}
		itLNS++;		
		//~ candidate.print(inst);
	}
	#endif
	global_time += timer_global.total();
	//~ cout << "Re-builded solution\n";
	//~ candidate.print(inst);
	best.print(inst);
	///Print header <file> <idBestSol> <bestObj> <bestObjLNS> <itLNS> <cplexObl> <totalTime> <modelTime> <cplexTime> <ifInfeasible> <ifInfeasibleLNS>
	
    output << str << "\t" << bestSol << "\t" << bestSolValue;
    output << "\t" << best.objectiveValue;    
    output << "\t" << itLNS;
	output << "\t" << bestObj;
    output << "\t " << global_time/1000;
    output << "\t " << model_time/1000;
    output << "\t " << cplex_time/1000;
    if(solutions[bestSol].costPenalization > 0.0){
        output << "\t infeasible ";
	 }else{
		output << "\t feasible ";
	}
    if(best.costPenalization > 0.0){
        output << "\t infeasible ";
	 }else{
		output << "\t feasible ";
	}
     output << endl;
     //~ cout << bestSolValue << endl;
	env.end();
}

