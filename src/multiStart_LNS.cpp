/* Multi-start metaheuristic - with embeeded LNS for each solution
 * Marcelo Friske
 * 08/10/2019
 *  
 */ 
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include "../include/util.h"
#include "../include/solution.h"

//~ #define NDEBUG
#include <assert.h>


using namespace std;
using mirp::Solution;
using mirp::Instance;


//~ default_random_engine engine;
//~ int seed1;

/* Multistart algorithm with MIP embeded in a CSMA algorithm
 * Params:
 * @ file - path to the instance file
 * @ output - file to print the final results (time, best obj, etc)
 * @ timeLimit - of the whole algorithm/MIP iterations
 * @ numSolutions - number of solutions generated at each iteration of 
 * CSMA
 * @ probSelectBestVessel - probability [0,1] of selecting the first 
 * vessel of the list in the selected criteria
 * @ nRoutes - number of best routes of each vessel addded to the MIP
 * @ nSolutions - number of best solutions added to the MIP
 * @ ageMax - max age of the variables in the CSMA
 * @ r [0.0,100.0] - percentage of the variables with age>ageMax with worst reduced 
 * cost which are removed from the MIP 
 */ 
void  mirp::multiStartLNS(string file, ofstream& output, const double& 
timeLimit, const unsigned int& numSolutions, const float& 
probSelectBestVessel, const float& probRandomSelectCounterPartNode, const unsigned int& nRoutes, const unsigned int& 
nSolutions, const unsigned int& ageMax, const float& r, const float& probRandomSelect2ndPort){
	///Time parameters
	Timer<chrono::milliseconds> timer_global(timeLimit*1000);
	float global_time {0};	
	
	size_t pos = file.find("LR");
	string str = file.substr(pos);
	str.erase(str.end()-1);
	stringstream ss,ss1;
	//Log file
	ss << "log_" << str << ".txt";
	ofstream out(ss.str());
    streambuf *coutbuf = cout.rdbuf(); //save old buf
    cout.rdbuf(out.rdbuf()); //redirect std::cout to out
    //~ cout.rdbuf(0); //redirect std::cout to null
	//Solution obj file
	ss1 << "s_" << str << ".txt";
	ofstream outS(ss1.str());
	
	IloEnv env;
    Instance inst(env);
    inst.readInstance(env, file);
    timer_global.start();
    
    vector<Solution> solutions;    
    //Used for sorting the solutions in ascending order
    priority_queue< pair<unsigned int, float>, vector<pair<unsigned int,float>>, ComparePairMinorFloatFirst> solutionsObj; 
    
    //Vessels avaiable for perform voyages (restricted when using LNS)
    vector<unsigned int> avVessels;
    for(unsigned int v=0;v<inst.V;v++){
		avVessels.push_back(v);
	}
    
    unsigned int idSol = 0,bestSol=0, it;
    float bestSolValue =  1E10;
          
    unsigned int iteration = 1;
    double bestObj = 1E10;   
    
    //Randomness 
    int seed1 = 1;
	engine.seed(seed1);
	for(it=0;it<numSolutions;it++){ //Multi-start loop 
		idSol = it + ((iteration-1)*numSolutions);
		Solution s;
		s.buildSolution(inst, false, avVessels, probSelectBestVessel, probRandomSelectCounterPartNode, probRandomSelect2ndPort);
		//LNS		
		int itLNS=0;
		Solution current(s);    
		Solution candidate;	
		while(itLNS <= 4000){	
			candidate = Solution(current);
			avVessels = candidate.destroy(inst,2);
			
			candidate.rebuild(inst, 1, avVessels, probSelectBestVessel, probRandomSelectCounterPartNode, probRandomSelect2ndPort);
			cout << endl << itLNS << " - ";
			cerr << endl << itLNS;
			cout << "Candidate objective " << candidate.objectiveValue << endl;
			cout << "Current objective " << current.objectiveValue << endl;			
			//Accepting  improving solutions - or accept any solution if it is infeasible
			if(candidate.objectiveValue < current.objectiveValue || current.costPenalization > 0.0){
				current = Solution(candidate);
			}			
			itLNS++;
		}		
		solutions.push_back(current);
		solutionsObj.push(make_pair(idSol,solutions[idSol].objectiveValue));
		
		cout << "\n Solution " << idSol << ": " << solutions[idSol].objectiveValue << endl;
		//~ s.print(inst);
		outS << "Solution " << idSol << ": " << solutions[idSol].objectiveValue;
		
		if(solutionsObj.top().second < bestSolValue){
			bestSol = solutionsObj.top().first;
			bestSolValue = solutionsObj.top().second;					
		}						
		outS << " - Best (" << bestSol << ") : " << bestSolValue 
		<< endl;
	}						

    solutions[bestSol].print(inst);
    
	global_time += timer_global.total();
	///Print header <file> <idBestSol> <bestObj> <bestObjLNS> <itLNS> <cplexObl> <totalTime> <modelTime> <cplexTime> <ifInfeasible> <ifInfeasibleLNS>
	
    output << str << "\t" << bestSol << "\t" << bestSolValue;
    output << "\t" << bestObj;
    output << "\t " << global_time/1000;    
    if(solutions[bestSol].costPenalization > 0.0){
        output << "\t infeasible ";
	 }else{
		output << "\t feasible ";
	}    
    output << endl;
     
	env.end();
}

