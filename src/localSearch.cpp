#include "../include/localSearch.h"
//~ #define NDEBUG
#include <assert.h>

using namespace std;
using mirp::Solution;
using mirp::Instance;

Solution mirp::LS_reduceBetaPenalization(Instance inst, const Solution& solution){
	Solution s(solution);
	int i,j,t,id,v,v1;
	int t_vj; //time vessel v operates at j
	float profit = 0;
	pair<int, int> nodeJ; //Node visited by vessel v time after using spotMarket
	pair<int, int> nodeI; //Node (with port != nodeJ) visited by vessel before nodeJ
	pair<int, int> nodeI1;
	pair<int, int> nodeJ1;
	for(j=0;j<inst.J;j++){
		for(t=0;t<inst.t;t++){
			if(s.portBeta[j][t] > 0.0){
				v = s.getNextVisitingVessel(j,t,t_vj); 
				nodeJ = make_pair(j,t_vj);
				if(v != -1){ //If there is a vessel supplying i after time t
					nodeI = s.getPreviosNodeVisit(v,nodeJ.second); 
					if(nodeI.first == -1){
						cout << "No visit of vessel " << v << " before nodeJ " << nodeJ.first << "," << nodeJ.second << endl;
					}else{
						if(inst.delta[nodeJ.first] != inst.delta[nodeI.first]){ //Only applies for inter regional arcs
							for(v1=0;v1<inst.V;v1++){ 
								if(v1!=v){
									//Get the visiting node j1 with t_vj1 < t
									nodeJ1 = s.getPreviosNodeVisit(v1,t);
									//If there is a visit (is not the first node) and is not the same of nodeJ
									if(nodeJ1.first != -1 && nodeJ1.first != nodeJ.first) {
										///Case nodeJ and nodeJ1 are of the same type
										if(inst.delta[nodeJ1.first] == inst.delta[nodeJ.first]){
											nodeI1 = s.getPreviosNodeVisit(v1,nodeJ1.second);
											//if there is a visit of v1 before j1 and i1 is of the same type of i
											if(nodeI1.first != -1 && inst.delta[nodeI1.first] == inst.delta[nodeI.first]){ 
												//S' = S  //generate other solution by copy struct
												//S' = getProfitExchange
												//if (f(S') < f(S))
												// S = S'
												//
												Solution s1(s);
												//~ profit = s1.getProfitExchange(inst,v,v1,nodeI,nodeJ,nodeI1,nodeJ1);
												cout << "Profit " << profit << endl;
												//~ if(profit < 0){ //If minimizes the FO 
													//~ exchangeArcs(v,v1,nodeI,nodeJ,nodeI1,nodeJ1);
												//~ }
											}
										}else{ ///Case nodeJ and nodeJ1 are of different type
											
										}
									}
								}
							}
						}
					}
				}
				
				/* Verify vessel that supplyed (i,t1), t1>t 
				 *  Remove the arc (x,x) -> (i,t1)
				 *  Look for a vessel v1, arriving at (j,t2), with t2 < t, and j of same type of i
				 *  Verify if v1 can reach (i,t) at time, and v can reach (j,t2) at time - OR 
				 * 		 re-calculate the route cost (including the addition of beta in other times)
				 *  If improved the solution apply. (First or best improvement) - i=0,t=0 - reset the search
				 *  Until no improvement can be made
				 * 
				 */ 
			}
		}
	}
	
}
