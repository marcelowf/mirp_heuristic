#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
//~ #include <ilcplex/ilocplex.h>
#include "../include/util.h"
#include "../include/solution.h"

//~ #define NDEBUG
#include <assert.h>
#define THETA_PENALIZATION 1000
#define BETA_PENALIZATION 1000
ILOSTLBEGIN

using namespace std;
using mirp::Solution;
using mirp::Instance;

/*
 * Init a struct Solution and defines the initial visiting port of each vessel (including a possible 2nd visit)	
 */
void Solution::init(Instance inst, const vector<unsigned int>& avVessels){
	unsigned int i,j,t,v,k;
    costTravel = 0.0;
    costOperation = 0.0;
    costAlpha = 0.0;
    profitRevenue = 0.0;
    costPenalization = 0.0;
    
    vesselRoute = vector< vector<tuple<unsigned int, unsigned int,float, float> > >(inst.V);
    vesselRouteCost = vector<float>(inst.V);        
    vesselRouteCostSpot = vector<float>(inst.V);        
    
    portBerthOcupation = vector<vector<unsigned int> >(inst.J);
    portInventory = vector<vector<float> > (inst.J);
    maxF_it = vector<vector<float> > (inst.J);
    maxF_itCopy = vector<vector<float> > (inst.J);
    portInventoryCopy = vector<vector<float> >(inst.J);
    portAlpha = vector<vector<float> >(inst.J);
    portOperationTime = vector< priority_queue<pair<unsigned int,unsigned int>,vector<pair<unsigned int,unsigned int> >, ComparePairHigherFirst > >(inst.J); 
	portOperations = vector<vector<float> >(inst.J);
    portVisits = vector< vector<pair<unsigned int,unsigned int> > >(inst.J); 
    dPortsStockout = priority_queue<pair<unsigned int, unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst >();
    initFirstVesselArriving = vector<int>(inst.J);
    portCosts = vector<float>(inst.J);
    
    //Revenue in non-increasing order
    dPortHighRevenue = vector< pair<unsigned int, float> >();
    
    //Consumption/capacity non-increasing order
    lPortsHighCosumptionCapacity = vector< pair<unsigned int, float> >();
    dPortsHighCosumptionCapacity = vector< pair<unsigned int, float> >();
    
    pPortsOverflow = priority_queue<pair<unsigned int, unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst >();
    hasInventoryViolation = vector<bool>(inst.J);
    cummulativeAlpha = vector<float>(inst.J);
    inventoryViolation = vector<unsigned int>(inst.J);
    //Auxiliary variables
    portBeta = vector<vector<float> >(inst.J);
    portTheta = vector<vector<float> >(inst.J);
    infeasibilitiesBeta = priority_queue<tuple<unsigned int, unsigned int, float>,vector<tuple<unsigned int,unsigned int, float>>,compareTupleHigherFirst>();
    infeasibilitiesTheta = priority_queue<tuple<unsigned int, unsigned int, float>,vector<tuple<unsigned int,unsigned int, float>>,compareTupleHigherFirst>();
    cummulativeBeta = vector<float>(inst.J);
    cummulativeTheta = vector<float>(inst.J);
    
    resetAuxliaryStructureForSelectingVessel(inst);

	//Port inveroty infos
    for(i=0;i<inst.J;i++){
        portBerthOcupation[i] = vector<unsigned int>(inst.t);
        portInventory[i] = vector<float>(inst.t);
        portInventoryCopy[i] = vector<float>(inst.t);
        maxF_it[i] = vector<float>(inst.t);
        maxF_itCopy[i] = vector<float>(inst.t);
        portAlpha[i] = vector<float>(inst.t);        
        portBeta[i] = vector<float>(inst.t);
        portTheta[i] = vector<float>(inst.t);
        portOperations[i] = vector<float>(inst.t);
        bool violated = false;        
        for(t=0;t<inst.t;t++){
            portBerthOcupation[i][t] = 0;
            portAlpha[i][t] = 0;
            portBeta[i][t] = 0;
            portTheta[i][t] = 0;
            if(t==0){                
                portInventory[i][t] = inst.s_j0[i]+(inst.delta[i]*inst.d_jt[i][t]);
            }else{
                portInventory[i][t] = portInventory[i][t-1]+(inst.delta[i]*inst.d_jt[i][t]);
            }
            portInventoryCopy[i][t] = portInventory[i][t];
            //Manual updating as no operations took place yet
            if(inst.delta[i] == 1){
				maxF_it[i][t] = max(0.0,(double)portInventory[i][t]);
			}else{
				maxF_it[i][t] = max(0.0, (double)inst.sMax_jt[i][0]-portInventory[i][t]);
			}
			maxF_itCopy[i][t] = maxF_it[i][t];
            if(!violated){ //Set the first time a violation occurs (based on initial inventory, consumption and capacity)
                if(inst.delta[i] == 1){
                    if(portInventory[i][t] > inst.sMax_jt[i][0]){
                        inventoryViolation[i] = t;
                        pPortsOverflow.push(make_pair(i,t));
                        violated = true;     
                        hasInventoryViolation[i] = true;                   
                    }
                }else if(portInventory[i][t] < inst.sMin_jt[i][0]){
                    inventoryViolation[i] = t;
                    dPortsStockout.push(make_pair(i,t));
                    violated = true;
                    hasInventoryViolation[i] = true;
                }
            }
        }
        int bestV = -1;
        unsigned int bestTime = inst.t;
        //Defines the firts vessel arriving at each port (according to the initial port j0 information)
        for(k=0;k<inst.startingVessel[i].getSize();k++){ //For each vessel starting at port i
            v = inst.startingVessel[i][k];
            if(inst.firstTimeAv[v] < bestTime){
                bestV = v;
                bestTime = inst.firstTimeAv[v];
            }
        }
        initFirstVesselArriving[i] = bestV;
    }
	///Init the vessels voyage according to the firstTimeAvaliable
    //~ for(v=0;v<inst.V;v++){
        //~ unsigned int vId = inst.vesselFirstTimeAv.top().first; 
        //~ inst.vesselFirstTimeAv.pop();
        //~ setFirstVoygeVessel(inst, avVessels, vId);
    //~ }
    ///Init the vessels voyage in random order
    vector<int> vessels;
    for(v=0;v<inst.V;v++){
		vessels.push_back(v);
	}
	shuffle(vessels.begin(),vessels.end(),engine);
	//~ //~ cout << "Vessels order: " ;
    for(v=0;v<vessels.size();v++){
		int vId = vessels[v];
		//~ //~ cout << vId << " ";
		setFirstVoygeVessel(inst, avVessels, vId);
	}
	//~ //~ cout << endl;
    
    
    //Build priority queues for the port selection criteirias 
    //~ for(i=0;i<inst.J;i++){
        //~ if(inst.delta[i] == 1){ //LOADING PORT
            //~ lPortsHighCosumptionCapacity.push_back(make_pair(i,inst.d_jt[i][0]/inst.sMax_jt[i][0]));
            //~ for(t=0;t<inst.t;t++){
                //~ if(portInventory[i][t]>inst.sMax_jt[i][0]){
                    //~ pPortsOverflow.push(make_pair(i,t));
                    //~ inventoryViolation[i] = t;
                    //~ hasInventoryViolation[i] = true;
                    //~ break;
                //~ }
            //~ }            
        //~ }else{  //DISCHARGING PORT
            //~ dPortHighRevenue.push_back(make_pair(i,inst.r_jt[i][0]));
            //~ dPortsHighCosumptionCapacity.push_back(make_pair(i,inst.d_jt[i][0]/inst.sMax_jt[i][0]));
            //~ for(t=0;t<inst.t;t++){
                //~ if(portInventory[i][t]<inst.sMin_jt[i][0]){
                    //~ dPortsStockout.push(make_pair(i,t));
                    //~ inventoryViolation[i] = t;
                    //~ hasInventoryViolation[i] = true;
                    //~ break;
                //~ }
            //~ }       
        //~ }
    //~ }    
    //Sort vectors
    //~ ComparePairHigherFloatFirst comparePairHigherFloatFirst;
    //~ sort(dPortHighRevenue.begin(),dPortHighRevenue.end(),comparePairHigherFloatFirst);
    //~ sort(lPortsHighCosumptionCapacity.begin(), lPortsHighCosumptionCapacity.end(), comparePairHigherFloatFirst);
    //~ sort(dPortsHighCosumptionCapacity.begin(), dPortsHighCosumptionCapacity.end(), comparePairHigherFloatFirst);

}

/*
 *Clear the structures used for selecting a vessel to perform a voyage
 * Also used on the init() method
 */  
void Solution::resetAuxliaryStructureForSelectingVessel(Instance inst){
    vesselMinNumberOperations = priority_queue< pair<unsigned int,unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst >();
    vesselCostVoyage = priority_queue< pair<unsigned int,float>, vector<pair<unsigned int, float >>, ComparePairMinorFloatFirst >();
    vesselHigherCostVoyage = priority_queue< pair<unsigned int,float>, vector<pair<unsigned int, float>>, ComparePairHighestFloatFirst >();    
    vesselEarliestArrivalPort = priority_queue< pair<unsigned int,unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst >();
    vesselMinArrivalDifference = priority_queue< pair<unsigned int,unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst > ();
    vesselMinWaitTimes = priority_queue< pair<unsigned int,unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst > ();
    vesselCostQuantity = priority_queue< pair<unsigned int,float>, vector<pair<unsigned int, float >>, ComparePairMinorFloatFirst >();
    
    lateVesselMinNumberOperations = priority_queue< pair<unsigned int,unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst >();
    lateVesselCostVoyage = priority_queue< pair<unsigned int,float>, vector<pair<unsigned int, float >>, ComparePairMinorFloatFirst >();
    lateVesselHigherCostVoyage = priority_queue< pair<unsigned int,float>, vector<pair<unsigned int, float>>, ComparePairHighestFloatFirst >();    
    lateVesselEarliestArrivalPort = priority_queue< pair<unsigned int,unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst >();
    lateVesselMinArrivalDifference = priority_queue< pair<unsigned int,unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst > ();
    lateVesselMinWaitTimes = priority_queue< pair<unsigned int,unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst > ();
    lateVesselArrivalTimes = vector<pair<unsigned int, unsigned int> >(inst.V);
    lateVesselMinimumAlphaBeta = priority_queue< pair<unsigned int,float>, vector<pair<unsigned int, float>>, ComparePairMinorFloatFirst >();
    lateVesselCostQuantity = priority_queue< pair<unsigned int,float>, vector<pair<unsigned int, float >>, ComparePairMinorFloatFirst >();
    totalAlphaBeta = 0;
    
    vesselOperationOrders = vector< priority_queue< tuple<unsigned int,unsigned int,unsigned int,float> , 
            vector<tuple<unsigned int,unsigned int,unsigned int,float>>, compare4TupleMinorFirst > >(inst.V);
    portInventoryCopy = vector<vector<float> >(inst.J);
    for(unsigned int i=0;i<inst.J;i++){
        portInventoryCopy[i] = vector<float>(inst.t);
    }
    
}

/*
 * Set the first voyage of vessel v, starting at port i, time t .
 * Assumes that vessels are sorted in incresing order of their firstTime available parameter
 * Randomly defines a second port of same region of j0 to visit
 * Use spot markets if inventory breach occurs in port before the vessel(s) arrival
 */ 
void Solution::setFirstVoygeVessel(Instance inst, const vector<unsigned int>& avVessels, const unsigned int& v){
	unsigned int i = inst.initialPort[v];
	unsigned int timeInI = inst.firstTimeAv[v];
	unsigned int j=i; //Second port to be (or not) visited - set initially equals to i
	vesselRoute[v] =  vector<tuple<unsigned int, unsigned int,float, float> >();

	//Init the route of vessels considering the initial port
	vesselRoute[v].push_back(make_tuple(inst.initialPort[v],inst.firstTimeAv[v],0,inst.s_v0[v]));
	costTravel += inst.getSourceArcCost(v);         
	vesselRouteCost[v] += inst.getSourceArcCost(v);         
	vesselRouteCostSpot[v] += inst.getSourceArcCost(v);         

	//~ cout<< "Setting first voyage of vessel " << v << " starting at (" << i << "," << timeInI << ")\n";
	//~ cout<< "Inventory " << portInventory[i][timeInI] << " InventoryCopy " << portInventoryCopy[i][timeInI] << endl;
	float fI=inst.q_v[v],fJ=0; //Amount to be operated in i and j
	///Only if there is other ports in the region of the initial port i
	if(inst.portsRegion[inst.regionId[i]].getSize() > 1){
		unsigned int x;
		j = get2ndRegionPort(inst,avVessels,true,i,timeInI,v,inst.q_v[v],fI,fJ,x);	
	}
	
	//~ cout<< "fI = " << fI << " fJ(" << j << ") = " << fJ << endl;	
	
	///New implementation
	portInventoryCopy[i] = vector<float>(portInventory[i]);
	//Use spot market from time 0 to init time of vessel if needed
	if(timeInI > 0 && (portInventoryCopy[i][timeInI-1] > inst.sMax_jt[i][0] 
		|| portInventoryCopy[i][timeInI-1] < inst.sMin_jt[i][0])){
		useSpotMarketCopy(inst,v,make_pair(i,0),timeInI,portInventoryCopy[i],maxF_itCopy[i]);
	}
	int minOpI, minOpJ;
	unsigned int numOp;
	//Operate at i
	minOpI = getMinOperationTime(inst,v,fI,i,timeInI,numOp,true);
	if(minOpI == -1){
		//~ cout << "Vessel " << v << " cannot operate at initial port " << i << endl;
		print(inst);
	}
	if(j != i){
		portInventoryCopy[j] = vector<float>(portInventory[j]);
		//Travels to j
		vesselOperationOrders[v].push(make_tuple(timeInI+minOpI,TRAVEL,j,0));
		int timeAtJ = timeInI + minOpI + inst.travelTime[v][i][j];
		if(portInventoryCopy[j][timeAtJ-1] > inst.sMax_jt[j][0] || portInventoryCopy[j][timeAtJ-1] < inst.sMin_jt[j][0]){
			useSpotMarketCopy(inst,v,make_pair(j,0),timeAtJ,portInventoryCopy[j],maxF_itCopy[j]);
		}
		//Operate at j
		minOpJ = getMinOperationTime(inst,v,fJ,j,timeAtJ,numOp,true);	
	}
	//Perform the operations at i, and if j!=i, travels to j and operate at j
	performActions(inst, v);
	//~ print(inst);
	///Old implementation
	//~ //Perform the operation at port i
	//~ unsigned int timeAfterOper = operate(inst,v,i,timeInI,fI,true); //Direct 
	//~ //~ cout<< "Time after operated " << timeAfterOper << endl;
	//~ //If voyage to j will be performed 
	//~ if(j!=i){
		//~ //Travel to j
		//~ performVoayge(inst,v,j);
		//~ //Verify if port j needs spot market before arrival of vessel v
		//~ for(unsigned int t=0;t < get<1>(vesselRoute[v].back());t++){
			//~ if(portInventory[j][t] > inst.sMax_jt[j][t] || portInventory[j][t] < inst.sMin_jt[j][t]){
				//~ useSpotMarket(inst,make_pair(j,t),get<1>(vesselRoute[v].back()),true); 
			//~ }		
		//~ }
		//~ //Operate at j
		//~ operate(inst,v,j,get<1>(vesselRoute[v].back()),fJ,true);
	//~ }
}
/*
 * Return the best port j of the same region to be visited by vessel v after visiting port i at time t
 * Considers as priority no inventory violation, and minimum waiting time of vessel 
 * Defines the quantity to be operated in i (fI) and j (fJ) which sums up f
 * If the best choice is not visiting other port (or there is no other port), return j=i
 * If cannot visit/operate any port - return <0,0>
  */
pair<unsigned int,unsigned int> Solution::get2ndRegionPort(Instance inst, const vector<unsigned int>& avVessels, const bool& isInit, const unsigned int& i, const unsigned int& t, const unsigned int& v, const float& f, float& fI, float& fJ, unsigned int& lessDelayedTime, float probRandomSelect2ndPort){
	/*Evaluate for each port j of the  same region of i:
		- a) if the port can be only visited by v before inventory violation
		- b) the finishing time of v after operating in i and j
		- c) the cost of travelling (and operating) to port j
	*/	
	//~ cout<< "Evaluating a visit to a second port j after visiting " << "(" << i << "," << t  << ") with vessel " << v << endl;
	
	//Store the actions considering the visit o v to each port of the region of i
	vector< vector< tuple<unsigned int,unsigned int,unsigned int,float> > > vesselPortActions(inst.J); 
    //Number of operations needed for operating at each j (including i)
    vector<unsigned int > numbOperations(inst.J); 
	unsigned int idJ,j,l,u;
	vector<float> FI(inst.J); //Store the amount operated in port i when also operate in port j ([j]->fI)
	vector<float> FJ(inst.J); //Store the amount operated in port j if is visited ([j] -> fJ);
	vector<int> numVesselArriving(inst.J); //Store total of vessesl that can arrive port j before inventory violation
	
	vector<unsigned int> delayedTime(inst.J); //Store the less delayed time in which a vessel u != v reaches port j
	priority_queue<tuple<unsigned int, unsigned int, unsigned int, float>,vector<tuple<unsigned int,unsigned int, unsigned int, float>>,compareSecondPortList> candidatesPorts;
	float costI;
	unsigned int waitTimeI;
	//For each port j of the same region of i
	for(idJ=0;idJ<inst.portsRegion[inst.regionId[i]].getSize();idJ++){		
		j = inst.portsRegion[inst.regionId[i]][idJ];    
		bool haveTimeToOperate = false;	
		if(j != i){
			//Copy port j inventory     
			portInventoryCopy[j] = vector<float>(portInventory[j]);
			///Evaluates arrival of other vessels to j
			if(isInit){///Init -> Check if has a vessel which starts before inventory violation 
				// TODO - Consider vessel starting at a different port and travelling to j
				for(l=0;l<inst.startingVessel[j].getSize();l++){ //For each vessel u that starts at port j
					u = inst.startingVessel[j][l];
					if(inst.firstTimeAv[u] <= inventoryViolation[j]){ 
						haveTimeToOperate = true; 
						numVesselArriving[j]++;
					}
				}
			}else{///Iterations - checks if other vessel can reach j before inv violation, or it is at the port j and not operated yet
				for(unsigned int uId=0;uId<avVessels.size();uId++){ //For each vessel different from v
					u = avVessels[uId];
					if (u != v){
						unsigned int currentPortU = get<0>(vesselRoute[u].back());
						unsigned int currentTimeU = get<1>(vesselRoute[u].back());
						float currentLoad = get<3>(vesselRoute[u].back());
						unsigned int earliestTimeJ=0;
						//If vessel u is at a port of different type from j, it can travels directly to port j						
						if(inst.delta[currentPortU] != inst.delta[j]){
							earliestTimeJ = currentTimeU + inst.travelTime[u][currentPortU][j];
						}else{ // u is at a port of same type of j
							//If u is at j and not operated, just consider the time of the current vessel route							
							if(j == currentPortU && (inst.delta[j] == 1 && currentLoad == 0 || inst.delta[j] == -1 && currentLoad == inst.q_v[v])){
								earliestTimeJ = currentTimeU;
							}else{ //Else needs to travel to a port of diffenrt type before go to j
								//Assuming travelling to the nearest port of different type of currentPort
								unsigned int nearPort = inst.closestPortOtherType[currentPortU][0].first;
								earliestTimeJ = currentTimeU + inst.travelTime[u][currentPortU][nearPort] + inst.travelTime[u][nearPort][j];
							}
						}
						if(earliestTimeJ <= inventoryViolation[j]){ //If at least one vessel u reaches port j at time
							haveTimeToOperate = true;
							lessDelayedTime = inst.t;
							numVesselArriving[j]++;
						}else{ //Store the less delayed time of the arrival - for comparing with a possible dalay on the port2 visit
							if(earliestTimeJ < lessDelayedTime){
								lessDelayedTime = earliestTimeJ;
							}
						}
					}
					//Number of time periods delayed for the 'best vessel'
					delayedTime[j] = lessDelayedTime - inventoryViolation[j]; 
				}
			}
			//Check if v can visits j and calculate the associated cost
			float cost;
			int endTimeOperations;
			if(vesselCanOperatePorts2(inst,v,i,j,t,FI[j],FJ[j],
				inst.q_v[v],vesselPortActions[j],endTimeOperations,numbOperations[j])){//It is possible to hande Q_v at the two ports
				cost = inst.travelCost[v][i][j];
				if(inst.delta[j] == -1){ //Revenue by discharging at port i and j
					cost -= inst.r_jt[i][0]*FI[j];
					cost -= inst.r_jt[j][0]*FJ[j];
				}
				if(!haveTimeToOperate){ //When v is the only tha visit j before inventory violation					
					//Put on the list - port with inventory violation
					candidatesPorts.push(make_tuple(j,1,endTimeOperations,cost));				
				}else{
					//Put on the list - no inventory violation
					candidatesPorts.push(make_tuple(j,0,endTimeOperations,cost));
				}
				
				
				
				
				//TODO - Remove above
				/*
				//Time to reach port j after operating fI in port i
				int operationTimeI = getMinOperationTime(inst,v,FI[j],i,t,numOp,vesselPortActions[j]);
				if(operationTimeI != -1){ //If the operation can be performed
					int timeToJ = t;
					timeToJ += operationTimeI + inst.travelTime[v][i][j];
					//~ cout<< "Time to j(" << j << ") " << timeToJ << " - inventory violation j " << inventoryViolation[j] << endl;
					if(!haveTimeToOperate){ //When no vessel visit j or no vessel reaches it before inventory violation - Visit J
						//If can arrive before inventory violation, calculate the cost - otherwise does not rank this port
						if(timeToJ <= inventoryViolation[j]){
							int operationTimeJ = getMinOperationTime(inst,v,FJ[j],j,timeToJ,numOp);
							//~ cout<< "operationTimeJ(" << FJ[j] << ")" << operationTimeJ << endl;
							if(operationTimeJ != -1){
								int endTimeOperations = timeToJ + operationTimeJ;
								cost = inst.travelCost[v][i][j];
								if(inst.delta[j] == -1){ //Revenue by discharging at port i and j
									cost -= inst.r_jt[i][0]*FI[j];
									cost -= inst.r_jt[j][0]*FJ[j];
								}
								//Put on the list - port with inventory violation
								candidatesPorts.push(make_tuple(j,1,endTimeOperations,cost));
							}
						}
					}else{ //When j is selected just for reducing the time to v end its operation
						int operationTimeJ = getMinOperationTime(inst,v,FJ[j],j,timeToJ,numOp);
						if(operationTimeJ != -1){
							int endTimeOperations = timeToJ + operationTimeJ;
							cost = inst.travelCost[v][i][j];
							if(inst.delta[j] == -1){ //Revenue by discharging at port i and j
								cost -= inst.r_jt[i][0]*FI[j];
								cost -= inst.r_jt[j][0]*FJ[j];
							}
							//Put on the list 
							candidatesPorts.push(make_tuple(j,0,endTimeOperations,cost));
						}
					}
				}
				*/
				
				
			}//No need else if cannot operate at j, just does not feed candidatePorts
		}else{ //Evaluate only visiting i (j=i)
			FI[j] = inst.q_v[v];
			FJ[j] = 0;			
			int endTimeOperations = getMinOperationTime(inst,v,FI[j],i,t,numbOperations[j],vesselPortActions[j]);
			//~ cout<< "-End time operation " << i << " " << t << ": " << endTimeOperations << endl;
			if(endTimeOperations != -1){
				float cost=0;
				if(inst.delta[j] == -1){ //Revenue by discharging port j
					cost -= inst.r_jt[j][0]*FI[j];
				}
				endTimeOperations += t;
				candidatesPorts.push(make_tuple(j,0,endTimeOperations,cost));
				costI = cost;
				waitTimeI = endTimeOperations;
			}
		}
		////~ cout<< "FJ[" << j << "] = " << FJ[j] << " / FI = " << FI[j] << endl;
	}
	//Get the best ranked
	unsigned int endTimeJ=0;
	if(!candidatesPorts.empty()){
		j = get<0>(candidatesPorts.top());
		endTimeJ = get<2>(candidatesPorts.top());
	}else{//No visit can be performed at i (and j) by vessel v
		return make_pair(0,0);
	}
	///Additional - Evaluate if worths travel to j considering other vessels arrive	
	if(i != j){
		while(!candidatesPorts.empty()){
			unsigned int portJ = get<0>(candidatesPorts.top());
			unsigned int timeJ = get<2>(candidatesPorts.top());
			if(portJ != i){				
				if(numVesselArriving[j] > ceil(inst.V * 0.25)){ // a percentage of vessels can visit j before inventory violation
					candidatesPorts.pop();
				}else{ //use portJ as j (different of i)
					j = portJ;
					endTimeJ = timeJ;
					break;
				}
			}else{ //Reached I - choose it
				j = i;
				endTimeJ = timeJ;
				break;
			}
		}
	}		
	fI = FI[j];
	fJ = FJ[j];
	//~ cout<< "Candidates of second port after " << i << endl;
	mergeActions(vesselOperationOrders[v],vesselPortActions[j]);
	numOp += numbOperations[j];
	#ifndef NDEBUG
	while(!candidatesPorts.empty()){
		//~ cout<< "Port " << get<0>(candidatesPorts.top()) << " " << get<1>(candidatesPorts.top()) << " times for operating " << get<2>(candidatesPorts.top()) << " cost " << get<3>(candidatesPorts.top()) << endl;
		candidatesPorts.pop();
	}
	#endif
	lessDelayedTime = delayedTime[j];
	return make_pair(j,endTimeJ);	
}

/*
 * Load  vessel v in port i from time t the cargo f - return true if the operation was susscefull,
 * If the vessel is already full, return false
 */
bool Solution::loadVessel(Instance inst, const unsigned int& v, const unsigned int& i, const unsigned int& t, const float f){     
    #ifndef NDEBUG
    if(get<0>(vesselRoute[v].back()) != i || get<1>(vesselRoute[v].back()) != t){
		cerr << "LOADING - Last route register differs from node informed: \n" ;
		cerr << "Route end = (" << get<0>(vesselRoute[v].back()) << "," << get<1>(vesselRoute[v].back()) << "," << get<2>(vesselRoute[v].back()) << "," << get<3>(vesselRoute[v].back()) << ") \n";
		cerr << "Informed node: (" << i << "," << t << ")\n";
		//~ cout << "LOADING - Last route register differs from node informed: \n" ;
		//~ cout << "Route end = (" << get<0>(vesselRoute[v].back()) << "," << get<1>(vesselRoute[v].back()) << "," << get<2>(vesselRoute[v].back()) << "," << get<3>(vesselRoute[v].back()) << ") \n";
		//~ cout << "Informed node: (" << i << "," << t << ")\n";
		exit(1);
	}
	#endif
    if(get<3>(vesselRoute[v].back()) + f <= inst.q_v[v] && portBerthOcupation[i][t] < inst.b_j[i]){ //If vessel has capacity for the load
        get<2>(vesselRoute[v].back()) = f;        
        //Update vessel inventory 
        get<3>(vesselRoute[v].back()) += f;
                
        //Update port information
        portBerthOcupation[i][t] += 1; 
        portOperations[i][t] = f;
        portOperationTime[i].push(make_pair(v,t));
        //Search for the port i in pPortsOverflow and store the elements in the auxiliary vector            
		vector<pair<unsigned int, unsigned int>> auxVector;
		if(hasInventoryViolation[i]){                
			while(!pPortsOverflow.empty()){
				if(pPortsOverflow.top().first != i){ 
					auxVector.push_back(make_pair(pPortsOverflow.top().first,pPortsOverflow.top().second));            
					pPortsOverflow.pop();
				}else{ //when i is found
					pPortsOverflow.pop(); // remove the pair corresponding to port i
					break;
				}
			}
		}
		
		bool pPortsOverflowIsUpdated=false;        //Flag for informing if the value t associated with i in priorityQueue is updated             
		for(unsigned int t1=t;t1<inst.t;t1++){
			portInventory[i][t1] -= f;
			portInventoryCopy[i][t1] -= f;
			
			//~ if (hasInventoryViolation[i]){
				//Verify if i is updated on priority queue
				if(!pPortsOverflowIsUpdated){
					if(portInventory[i][t1]>inst.sMax_jt[i][0]){
						//~ //~ cout<< "Overflow in " << t1 << endl;
						hasInventoryViolation[i] = true;
						pPortsOverflow.push(make_pair(i,t1));
						inventoryViolation[i] = t1;
						while(!auxVector.empty()){ //Re-insert elements of the auxiliary vector
							pPortsOverflow.push(make_pair(auxVector.back().first,auxVector.back().second));
							auxVector.pop_back();
						}
						pPortsOverflowIsUpdated = true;
					}else if (t1+1 == inst.t){ //Case there is no overflow on port i just re-insert the elements of the auxiliary vector                                                
						while(!auxVector.empty()){ 
							pPortsOverflow.push(make_pair(auxVector.back().first,auxVector.back().second));
							auxVector.pop_back();                            
						}
						pPortsOverflowIsUpdated = true;
						hasInventoryViolation[i] = false;
						inventoryViolation[i] = inst.t;
					}else if(portInventory[i][t1] < inst.sMin_jt[i][0]){
						cerr << "Error: Negative inventory on loading port \n";                            
						//~ cout<< "Error: Negative inventory on loading port (" << i << "," << t1 << ")\n";
					}
				}
			//~ }
		}             
   
        
        //Costs
        costOperation += t*inst.attemptCost;
        vesselRouteCost[v] += t*inst.attemptCost;
        vesselRouteCostSpot[v] += t*inst.attemptCost;        
    }else if(get<3>(vesselRoute[v].back()) == inst.q_v[v]) {
        //~ cout<< "Vessel is already loaded \n";
        return false;
    }else{        
        cerr << "ERROR: Vessel " << v << " with no capacity to load cargo " << f << " (" << get<3>(vesselRoute[v].back())  << ") OR Berth limit is violated" <<  endl;
        //~ print(inst);
        exit(1);
    }
    return true;
}
/*
 * Unload from vessel v in port i from time t the cargo f - return true if the operation was susscefull,
 * If the vessel is already empty, return false
 */
bool Solution::unloadVessel(Instance inst, const unsigned int& v, const unsigned int& i, const unsigned int& t, const float f){
    #ifndef NDEBUG
    assert(inst.delta[i]==-1);    
    if(get<0>(vesselRoute[v].back()) != i || get<1>(vesselRoute[v].back()) != t){
			cerr << "DISCHARGING - Last route register differs from node informed: \n" ;
			cerr << "Route end = (" << get<0>(vesselRoute[v].back()) << "," << get<1>(vesselRoute[v].back()) << "," << get<2>(vesselRoute[v].back()) << "," << get<3>(vesselRoute[v].back()) << ") \n";
			cerr << "Informed node: (" << i << "," << t << ")\n";
			//~ cout << "DISCHARGING - Last route register differs from node informed: \n" ;
			//~ cout << "Route end = (" << get<0>(vesselRoute[v].back()) << "," << get<1>(vesselRoute[v].back()) << "," << get<2>(vesselRoute[v].back()) << "," << get<3>(vesselRoute[v].back()) << ") \n";
			//~ cout << "Informed node: (" << i << "," << t << ")\n";
		exit(1);
	}
	#endif
    if(get<3>(vesselRoute[v].back()) >= f && portBerthOcupation[i][t] < inst.b_j[i]){ //If vessel has the load f to unload 
        get<2>(vesselRoute[v].back()) = f;        
        profitRevenue += f*inst.r_jt[i][t];
        vesselRouteCost[v] -= f*inst.r_jt[i][t];
        vesselRouteCostSpot[v] -= f*inst.r_jt[i][t];
        
        //Update vessel inventory 
        get<3>(vesselRoute[v].back()) -= f;       
        
        //Update port information
        portBerthOcupation[i][t] += 1;
        portOperationTime[i].push(make_pair(v,t));
        portOperations[i][t] = f;
        
		vector<pair<unsigned int, unsigned int>> auxVector;            
		if(hasInventoryViolation[i]){                
			while(!dPortsStockout.empty()){
				if(dPortsStockout.top().first != i){ 
					auxVector.push_back(make_pair(dPortsStockout.top().first,dPortsStockout.top().second));            
					dPortsStockout.pop();
				}else{ //when i is found
					//~ cout<< "Replace " << dPortsStockout.top().first << "," << dPortsStockout.top().second;
					dPortsStockout.pop(); // remove the pair corresponding to port i
					break;
				}
			}
		}
		
		bool dPortsStockoutIsUptade=false;        //Flag for informing if the value t associated with i in priorityQueue is updated             
		for(unsigned int t1=t;t1<inst.t;t1++){
			portInventory[i][t1] += f;
			portInventoryCopy[i][t1] += f;
			//~ if(hasInventoryViolation[i]){
				//Verify if i is updated on priority queue
				if(!dPortsStockoutIsUptade){
					if(portInventory[i][t1]<inst.sMin_jt[i][0]){                        
						hasInventoryViolation[i] = true;
						dPortsStockout.push(make_pair(i,t1));
						inventoryViolation[i] = t1;
						//~ cout<< " by " << t1 << endl;
						while(!auxVector.empty()){ //Re-insert elements of the auxiliary vector
							dPortsStockout.push(make_pair(auxVector.back().first,auxVector.back().second));
							auxVector.pop_back();
						}
						dPortsStockoutIsUptade = true;
					}else if (t1+1 == inst.t){ //Case there is no stockout on port i just re-insert the elements of the auxiliary vector                                                
						while(!auxVector.empty()){                             
							dPortsStockout.push(make_pair(auxVector.back().first,auxVector.back().second));
							auxVector.pop_back();                            
						}
						dPortsStockoutIsUptade = true;                    
						hasInventoryViolation[i] = false;
						inventoryViolation[i] = inst.t;
					}else if(portInventory[i][t1] > inst.sMax_jt[i][0]){
						cerr << "Error: Inventory on discharging port (" <<  portInventory[i][t1] << ") superior to its capacity " << inst.sMax_jt[i][0] << endl;
						//~ cout<< "Error: Inventory on discharging port (" <<  portInventory[i][t1] << ") superior to its capacity " << inst.sMax_jt[i][0] << endl;
					}
				}                
			//~ }
		}             
              
        //Costs
        costOperation += t*inst.attemptCost;
        vesselRouteCost[v] += t*inst.attemptCost;
        vesselRouteCostSpot[v] += t*inst.attemptCost;
    }else if(get<3>(vesselRoute[v].back()) == 0){ //No cargo to load
        return false;
    }else{
        cerr << "ERROR: Vessel " << v << " with less cargo to unload " << f << " (" << get<3>(vesselRoute[v].back()) << ")" <<  " or bethlimit of " << inst.b_j[i] << " reached (" << portBerthOcupation[i][t] << ")\n";
        //~ print(inst);
        exit(1);
    }
    return true;
}
/*
 * Makes vessel v waits in port i at time t
 */ 
void Solution::wait(Instance inst, const unsigned int& v, const unsigned int& i, const unsigned int& t){
    if(get<0>(vesselRoute[v].back()) == i && t < inst.t-1){
        float s = get<3>(vesselRoute[v].back());
        vesselRoute[v].push_back(make_tuple(i,t+1,0,s));
        portVisits[i].push_back(make_pair(v,t+1));
    }else if (get<0>(vesselRoute[v].back()) != i){
        cerr << "Vessel " << v << " is not at port " << i << " to wait\n";        
        //~ cout << "Vessel " << v << " is not at port " << i << " time " << t << " to wait\n";        
        //~ cout << "Current location is (" << get<0>(vesselRoute[v].back()) << "," << get<1>(vesselRoute[v].back()) << ")\n";
        //~ print(inst);
        exit(1);
    }else if (t >= inst.t-1){
        cerr << "Vessel " << v << " cannot wait at port " << i << " in time " << t << " because there is no time due the end of planning horizon.\n";
        exit(1);
    }
    
}
/*
 * Prints and show the costs/profits
 * Cost variables with sufix '1' are re-calculated from the instance information
 */ 
void Solution::print(Instance inst){
    unsigned int v,i,j,t,r,op = 0;
    float f,s,costTravel1=0.0, costOperation1=0.0, costAlpha1=0.0, profitRevenue1=0.0, costPenalization1=0.0;
    for (v=0;v<inst.V;v++){		
        float sV = inst.s_v0[v];
        cout << "Vessel " << v << " capacity = " << inst.q_v[v] << " - route (size = " << vesselRoute[v].size()    << ") : \n";
        for (r=0;r<vesselRoute[v].size();r++){
            i = get<0>(vesselRoute[v][r]);
            t = get<1>(vesselRoute[v][r]);
            f = get<2>(vesselRoute[v][r]);
            s = get<3>(vesselRoute[v][r]);
            cout << "(" << i << "," << t << ") - Load on board " << s;
            if(f>0){
				if (inst.delta[i] == 1){
					cout << " - Loaded " << f << " ";
				}else{
					cout << " - Discharged " << f << " ";
					profitRevenue1 += f*inst.r_jt[i][t];
				}
				costOperation1 += t*inst.attemptCost;
				if(f > inst.f_max_jt[i][0]){
					cout << " Error: Operated a value greater than F^Max";
				}else if(f < inst.f_min_jt[i][0]){
					cout << " Error: Operated a value lesser than F^Min";
				}				
				//Inventory of vessel
				sV += inst.delta[i]*f;
			}
			if(sV != s){
				cout << "Error : Vessel inventory s differs from the calculated sV " << s << "/" << sV;
			}
            if(r==0){   //Initial port
                costTravel1 += inst.portFee[i];
            }else if(i != get<0>(vesselRoute[v][r-1])){   //Traveling arc
                costTravel1 += inst.travelCost[v][get<0>(vesselRoute[v][r-1])][i];
                //Check travel time
                if(t - get<1>(vesselRoute[v][r-1]) != inst.travelTime[v][get<0>(vesselRoute[v][r-1])][i]){
					cout << "Error: travel time between " << get<0>(vesselRoute[v][r-1]) << " and " << i << " wrong. ";
					cout << "Correct is " << inst.travelTime[v][get<0>(vesselRoute[v][r-1])][i] << " informed " << t - get<1>(vesselRoute[v][r-1]);
				}
				//Check travel at cap/empty
				if((inst.delta[get<0>(vesselRoute[v][r-1])] == 1 && inst.delta[i] == -1 && get<3>(vesselRoute[v][r-1]) != inst.q_v[v]) ||
					(inst.delta[get<0>(vesselRoute[v][r-1])] == -1 && inst.delta[i] == 1 && get<3>(vesselRoute[v][r-1]) != 0.0)){
					cout << "Error: Travel at capacity/empty not respected ";
				}
            }	
            if(r == vesselRoute[v].size()-1){ //Last port 
                costTravel1 += inst.getSinkArcCost(t);
            }          
            
			cout << endl;            
        }
    }
    cout << "PORT INFO: \n";
    for(i=0;i<inst.J;i++){		
        cout << "S_0 " << inst.s_j0[i] << " D_i0 " << inst.d_jt[i][0] << " S^Max " << inst.sMax_jt[i][0];
        cout << " F^Min " << inst.f_min_jt[i][0] << " F^Max " << inst.f_max_jt[i][0] << " Max alpha " << inst.alp_max_j[i];
        cout << " Max alpha/t " << inst.alp_max_jt[i][0] << " Total alpha used " << cummulativeAlpha[i] << " Total beta used " <<  cummulativeBeta[i] << endl;
        
        if(cummulativeAlpha[i] < inst.alp_max_j[i] && cummulativeBeta[i] > 0){
			cout<< "Error: using beta while alpha is avaliable \n";
		}
        cout<< "Operations: \n";
        vector<pair<unsigned int,unsigned int>> auxVector;
        while(!portOperationTime[i].empty()){
            cout << "v= " << portOperationTime[i].top().first << " t= " << portOperationTime[i].top().second << " f " << portOperations[i][portOperationTime[i].top().second] <<  endl;
            auxVector.push_back(make_pair(portOperationTime[i].top().first, portOperationTime[i].top().second));
            portOperationTime[i].pop();
        }
        while(!auxVector.empty()){
			portOperationTime[i].push(make_pair(auxVector.back().first,auxVector.back().second));
			auxVector.pop_back();
		}
        float cumAlpha = 0;
        vector<float> inventory2(inst.t);
        for(t=0;t<inst.t;t++){
            if(t==0){
				inventory2[t] += inst.s_j0[i] + inst.delta[i]*(inst.d_jt[i][t] - portOperations[i][t] - portAlpha[i][t] - portBeta[i][t]);
			}else{//t>1
				inventory2[t] = inventory2[t-1] + inst.delta[i]*(inst.d_jt[i][t] - portOperations[i][t] - portAlpha[i][t] - portBeta[i][t]);
			}
            cout << "Node (" << i << "," << t << ") = " << portInventory[i][t] << " " << inventory2[t] << " ";
            if(t>0 && (portInventory[i][t] - (portInventory[i][t-1]+inst.delta[i]*inst.d_jt[i][t]) > inst.f_max_jt[i][0])){
                cout << "ERROR: Value operated in the previous time period was greater than FMAX " << inst.f_max_jt[i][0] << " ";
            }
            if(portInventory[i][t] != inventory2[t]){
				cout << "Error: Inventory presented differs from the operations presented and instance info ";
			}
            if (portAlpha[i][t] > 0.0){
                cout << "Alpha = " << portAlpha[i][t] << " ";
                if(portAlpha[i][t] > inst.alp_max_jt[i][0])
                    cout << " Excedded the limit per time period!";
                costAlpha1 += portAlpha[i][t]*inst.p_jt[i][t];
            }
            if (portBeta[i][t] > 0.0){
                cout << "Beta = " << portBeta[i][t] << " ";
                costPenalization1 += portBeta[i][t]*BETA_PENALIZATION;
            }
            if (portTheta[i][t] > 0.0){
                cout << "Theta = " << portTheta[i][t] << " ";
                costPenalization1 += portTheta[i][t]*THETA_PENALIZATION;
            }
            if(portInventory[i][t] > inst.sMax_jt[i][0] || portInventory[i][t] < inst.sMin_jt[i][0])
                cout << "ERROR: Inventory out of the bounds (" << inst.sMin_jt[i][0] << "," << inst.sMax_jt[i][0] << ")";
            if(portBerthOcupation[i][t]>inst.b_j[i]){
                cout << "ERROR: More than one vessel operating at the same time"; 
			}
            cout << endl;
        }
    }
    cout << "Travelling costs = " << costTravel << "/" << costTravel1 << endl;
    cout << "Operation costs = " << costOperation << "/" << costOperation1 << endl; 
    cout << "Alpha = " << costAlpha << "/" << costAlpha1 << endl;
    cout << "Revenue = " << profitRevenue << "/" << profitRevenue1 << endl;
    //~ if(costPenalization > 0){
        cout << "Beta and theta cost = " << costPenalization << "/" << costPenalization1 << endl;
    //~ }
    cout << "Final cost = " << costTravel + costOperation + costAlpha - profitRevenue + costPenalization << "/" ;
    cout <<  costTravel1 + costOperation1 + costAlpha1 - profitRevenue1 + costPenalization1 << endl;
    
    setObjectiveValue();
}
void Solution::setObjectiveValue(){
    objectiveValue = costTravel + costOperation + costAlpha - profitRevenue + costPenalization;
}

/* Calculate the travelling time and operating time at counterPartNode (to operate fCP) and urgentNode (for operate fU),
 * If needed visit a port1_1 after counterPartNode and a port2_1 after urgentNode
 * Calculate the total cost/profit of the voyage
 * Return FALSE if vessel v cannot visit counterpart and urgent nodes at time before reaching the end of horizon 
 * Return TRUE if vessel v can visit before the estipulated time
 * Return FALSE otherwise  
 * Feed priority queues for selecting the best vessel
 * PQs with prefix 'late' store also the vessels that are late 
 * Exception: In the first iteration if a vessel is loaded, it does not visit loading port, but is allowed to supply the L,D pair 
  */

bool Solution::vesselCanVisitNodes(Instance inst, unsigned int v, const vector<unsigned int>& avVessels, pair<unsigned int, unsigned int> urgentNode,  
    pair<unsigned int, unsigned int> counterPartNode, bool& cpNodeCanVisited, bool& uNodeCanVisited, const float& probRandomSelect2ndPort, float fCP, float fU){
    unsigned int currentI = get<0>(vesselRoute[v].back());
    unsigned int currentT = get<1>(vesselRoute[v].back());
    unsigned int port1 = counterPartNode.first;
    unsigned int port2 = urgentNode.first;
    unsigned int regP1 = inst.regionId[port1];    
    unsigned int regP2 = inst.regionId[port2];
    int timeDiff = 0;
    int totalWaitTimes = 0; //Counts the total waiting times to finish the operation 
    
    bool jumpPort1Operation=false, canVisit = true, isVisitedPort1_1 = false, isDPort2Visited=false;
    numOp = 0, 
    unsigned int aux=0, numVisitedPorts=2;
    costVoyage = 0;
    int earliestTimeP1 = currentT;
    int earliestTimeP2 = 0;
    cpNodeCanVisited = false;    //informs if have time to arrive and operate at counterPartNode even late
    uNodeCanVisited = false;    //informs if have time to arrive and operate at urgentNode even late
    float vLoad = get<3>(vesselRoute[v].back());
    int minOpTimeP1_partCargo = 0;
    int minOpTimeP1_allCargo = 0;
    float fCurrent=0.0, fUrgent = 0.0; //For using in special cases
    //Assumes default values if fCP and fU are not informed
    if(fCP == -1)
        fCP = inst.q_v[v];
    if(fU == -1) 
        fU = inst.q_v[v];

    /*When counterpart node does not need to be visited: 
		- port1 is of the same type of the current position of v and has already operated, OR
		- v is at the urgentPort and not operated yet */ 
    if( (inst.delta[port1] == inst.delta[currentI] && 
		 ((inst.delta[port1]==1 && vLoad == inst.q_v[v]) || (inst.delta[port1]==-1 && vLoad == 0))) ||
		 (port2 == currentI && (inst.delta[port2] == -1 && vLoad == inst.q_v[v] || inst.delta[port2] == 1 && vLoad == 0))){    
        
        port1 = currentI;
        jumpPort1Operation = true;        
    // When v is at a port of same type of urgent port and not operated yet    
    }else if(inst.delta[port2] == inst.delta[currentI] && 
		 ((inst.delta[currentI] == 1 && get<3>(vesselRoute[v].back()) == 0.0) ||
		 (inst.delta[currentI] == -1 && get<3>(vesselRoute[v].back()) == inst.q_v[v]))){
		portInventoryCopy[currentI] = vector<float>(portInventory[currentI]);
        maxF_itCopy[currentI] = vector<float>(maxF_it[currentI]);
		float endTimeP2;
		vector< tuple<unsigned int,unsigned int,unsigned int,float> > actionsNode1_2;
		// If port2 and currentI are of the same region
		// Tries to operate at port currentI and after at port2 (currentI equivalent to port1)
		if(inst.idRegion[port2] == inst.idRegion[currentI] && 
		  vesselCanOperatePorts2(inst,v,currentI,port2,currentT,fCurrent,fUrgent,fU,actionsNode1_2,endTimeP2)){
			int timeOpCurrent = getMinOperationTime(inst, v, fCurrent, currentI,currentT,numOp,true);
			if(timeOpCurrent != -1){
				earliestTimeP1 += timeOpCurrent;
			}else{ //Cannot operate at the current port anyway, 
				cout << "False 1\n";
				return false;
			}
			port1 = currentI;
			jumpPort1Operation = true;
			cout << "Special case: Divides load between currentI and port2 " << currentI << " " << port2 << endl;
		}else{ //Otherwise, operates all at currentI and after travel to port1			
			int timeOpCurrent = getMinOperationTime(inst, v, fU, currentI,currentT,numOp,true);
			if(timeOpCurrent != -1){
				earliestTimeP1 += timeOpCurrent;
			}else{ //Cannot operate at the current port anyway, 
				cout << "False 2\n";
				return false;
			}
			cout << "Special case: operate at currentI and after travels to port 1 " << currentI << " " << port1 << endl;
		}	
	}
    //Special case - if counterpart node have time > EOH (avoid to use spot market)
    if(counterPartNode.second >= inst.t){
		cpNodeCanVisited = true;
	}
	if(jumpPort1Operation){
		cout<< "Evaluating vessel " << v << " to supply node (" << port2 << "," << urgentNode.second << ") from its current port " << port1 << endl;
		numVisitedPorts--;
	}else{
		cout<< "Evaluating vessel " << v << " to supply nodes (" << port1 << "," << counterPartNode.second << ") and (" << port2 << "," << urgentNode.second << ")\n";
	}
	unsigned int port1_1, port2_1;
    if(!jumpPort1Operation){       
        portInventoryCopy[port1] = vector<float>(portInventory[port1]);
        maxF_itCopy[port1] = vector<float>(maxF_it[port1]);
        ///ACTION 1 - Voyage to port1 (only if it is not at the port yet)
        if(currentI != port1){
			vesselOperationOrders[v].push(make_tuple(earliestTimeP1,TRAVEL,port1,0));
		}
        earliestTimeP1 += inst.travelTime[v][currentI][port1]; //(0 if currentI == counterPartNode)
        timeDiff += abs(earliestTimeP1 - (int)counterPartNode.second);
        //Travelling costs to port1
        costVoyage += inst.travelCost[v][currentI][port1]; // 0 if ports are the same

        if(earliestTimeP1 >= inst.t){//Impossible to visit the port
            cout << "False 3\n";
            return false;
        }        
        //Check if vessel arrives after inv violation
        if(earliestTimeP1 > counterPartNode.second){
            vector<tuple<unsigned int,unsigned int,unsigned int,float>> actionsP1;
            useSpotMarketCopy(inst,v,counterPartNode,earliestTimeP1,portInventoryCopy[port1],maxF_itCopy[port1],actionsP1);
            mergeActions(vesselOperationOrders[v], actionsP1);            
        } 

        float f1, f1_1; 
        float minOpTimeP1_1;
        //~ unsigned int lessDelayedTime; // Stores the less delayed time in which other vessels !=v can reach j
		//Identify a second counterpart port port1_1 to be visited
		pair<unsigned int, unsigned int> node1 = get2ndRegionPort(inst,avVessels,false,port1,earliestTimeP1,v,inst.q_v[v],f1,f1_1,lessDelayedTime);
		if(node1.first == 0 && node1.second == 0){ //Cannot visit port1 (and port 1_1)			
			return false;
		}
		port1_1 = node1.first;	
		unsigned int endTimeNode1 = node1.second;
		cpNodeCanVisited = true;
		///Action - Travel to port2
		vesselOperationOrders[v].push(make_tuple(endTimeNode1,TRAVEL,port2,0));
		costVoyage += inst.travelCost[v][port1_1][port2];
		earliestTimeP2 = endTimeNode1 + inst.travelTime[v][port1_1][port2];
		timeDiff += abs(earliestTimeP2 - (int)urgentNode.second);
		

		/*If visiting port1_1 causes delayed arrival at port2 (no delay only visiting port1)
	     * do not visit port1_1 if delay(port2) >= delay(port1_1) 
	     * - This only occurs when port1_1 is visited due inventory violation */ 
		//If a port1_1 is selected - check compatibility time to reach port2
		/*
		if(port1_1 != port1){
			numVisitedPorts++;
			portInventoryCopy[port1_1] = vector<float>(portInventory[port1_1]);
			maxF_itCopy[port1_1] = vector<float>(maxF_it[port1_1]);
			minOpTimeP1_partCargo = getMinOperationTime(inst, v, f1, port1,earliestTimeP1,aux);
			minOpTimeP1_allCargo = getMinOperationTime(inst, v, inst.q_v[v], port1,earliestTimeP1,aux);
			if(minOpTimeP1_partCargo != -1){
				unsigned int earliestTime1_1 = earliestTimeP1 + minOpTimeP1_partCargo + inst.travelTime[v][port1][port1_1];
				if(earliestTime1_1 <= inst.t-1 && earliestTime1_1 <= inventoryViolation[port1_1]){ //If can reach port1_1 before inventory violation of this port
					minOpTimeP1_1 = getMinOperationTime(inst, v, f1_1, port1_1, earliestTime1_1,aux); 
					int timeToPort2 = earliestTime1_1 + minOpTimeP1_1 + inst.travelTime[v][port1_1][port2];
					if(minOpTimeP1_1 == -1 || timeToPort2 >= inst.t){ //Cannot operate at port1_1 or arrive in time at port2
						isVisitedPort1_1 = false;
						//~ //There is no option for vessel to operate at port1
						//~ if(minOpTimeP1_allCargo == -1){
							//~ return -1;
						//~ }
					}else{						
						float timeToPort2Direct = earliestTimeP1 + minOpTimeP1_allCargo + inst.travelTime[v][port1][port2]; 
						if(timeToPort2 <= urgentNode.second){//No delay caused in port2 by supply port1_1
							isVisitedPort1_1 = true;
						}else if(timeToPort2Direct <= urgentNode.second){ //No delay direct to p2, (delay passing first in port1_1)
							if(timeToPort2-timeToPort2Direct < lessDelayedTime + (timeToPort2 - urgentNode.second)){  //If the delay in port1_1 is reduced more than the delay caused  by travelling p1_1 -> p2
								canVisit = false;
								isVisitedPort1_1 = true;
							}else{
								isVisitedPort1_1 = false;
							}
						}else{ //Delay in port2 for all cases - perform voyage to port1_1 anyway
							isVisitedPort1_1 = true;
							canVisit = false;
						}
					}
				}
			}else{
				cout<< "Operation cannot be performed at node (" << port1 << "," << counterPartNode.second << ")\n";
			}
		}
        if(isVisitedPort1_1){ //Perform operation in port1_1
			cout<< "Visit also port1_1 " << port1_1 << endl;
            ///ACTION - Operate/wait at port1 
            minOpTimeP1_partCargo = getMinOperationTime(inst,v,f1,port1,earliestTimeP1,numOp,true);
            //Case vessel has load < F_Min or it is not possible to complete the operation at pPort  
            if(minOpTimeP1_partCargo == -1 || earliestTimeP1 + minOpTimeP1_partCargo > inst.t-1){ 
                canVisit = false;
                cout << "False 3.5\n";
                return canVisit;
            }
            cpNodeCanVisited = true;
            costVoyage += inst.travelCost[v][port1][port1_1];
            totalWaitTimes += minOpTimeP1_partCargo;
            ///ACTION - TRAVEL TO port1_1
            vesselOperationOrders[v].push(make_tuple(earliestTimeP1+minOpTimeP1_partCargo,TRAVEL,port1_1,0));            
            unsigned int earliestTimeP1_1 = earliestTimeP1 + minOpTimeP1_partCargo + inst.travelTime[v][port1][port1_1];
            ///Action - Operate/wait at port1_1
            minOpTimeP1_1 = getMinOperationTime(inst,v,f1_1,port1_1,earliestTimeP1_1,aux,true);
            if(minOpTimeP1_1 != -1){
				totalWaitTimes += minOpTimeP1_1;
			}
            ///Action - Travel to port2
            vesselOperationOrders[v].push(make_tuple(earliestTimeP1_1+minOpTimeP1_1,TRAVEL,port2,0));
            costVoyage += inst.travelCost[v][port1_1][port2];
            earliestTimeP2 = earliestTimeP1_1 + minOpTimeP1_1 + inst.travelTime[v][port1_1][port2];
            timeDiff += abs(earliestTimeP2 - (int)urgentNode.second);
        }else{ //Operate at port 1 and go direct to port2
            //Minimum time to operate at loading node 
            ///ACTION - Operate/wait at port1
            minOpTimeP1_partCargo = getMinOperationTime(inst, v, fCP, port1,earliestTimeP1, numOp, true);
            if(minOpTimeP1_partCargo == -1 || earliestTimeP1 + minOpTimeP1_partCargo > inst.t-1){ //Case vessel has load < F_Min or it is not possible to complete the operation at pPort  
                canVisit = false;
                cout << "False 3.52\n";
                return canVisit;
            }
            cpNodeCanVisited = true;
            totalWaitTimes += minOpTimeP1_partCargo;            
            ///ACTION - Travel to port2
            vesselOperationOrders[v].push(make_tuple(earliestTimeP1 + minOpTimeP1_partCargo,TRAVEL,port2,0));
            costVoyage += inst.travelCost[v][port1][port2];
            earliestTimeP2 = earliestTimeP1 + minOpTimeP1_partCargo + inst.travelTime[v][port1][port2];
            timeDiff += abs(earliestTimeP2 - (int)urgentNode.second);
        }
        */
        
        
    }else{//If jumps the operation in port1
        costVoyage += inst.travelCost[v][currentI][port2];
        ///ACTION - Travel to port2
        if(currentI != port2){
			vesselOperationOrders[v].push(make_tuple(earliestTimeP1,TRAVEL,port2,0));
		}
        //Time to reach discharging port
        earliestTimeP2 = earliestTimeP1 + inst.travelTime[v][currentI][port2];
        timeDiff += abs(earliestTimeP2 - (int)urgentNode.second);
    }        
    ///SECOND PORT
    bool canVisitP2=true;
    if (earliestTimeP2 >= inst.t){
        canVisit = false;
        canVisitP2 = false;
        cout << "False 3.53\n";
        return canVisit;
    }
    //Copy port2 inventory     
    portInventoryCopy[port2] = vector<float>(portInventory[port2]);
    maxF_itCopy[port2] = vector<float>(maxF_it[port2]);
    if(earliestTimeP2 > urgentNode.second){ 
        canVisit = false;
        canVisitP2 = false;
        cout<< "USE SPOT ON " << urgentNode.first << " " << urgentNode.second << " until " << earliestTimeP2 << endl;
        vector<tuple<unsigned int,unsigned int,unsigned int,float>> actionsP2;
        useSpotMarketCopy(inst,v,urgentNode,earliestTimeP2,portInventoryCopy[port2],maxF_itCopy[port2],actionsP1);
        mergeActions(vesselOperationOrders[v], actionsP2);        
    }
    
    unsigned int earliestTime2_1;
    float f2, f2_1;
    int minOpTimeP2;
    unsigned int x;    
    pair<unsigned int, unsigned int> node2;
    //Means that port1 is a port of same type of port2 - just operate at port2
    if(fUrgent != 0.0){
		f2 = fUrgent;
		port2_1 = port2;
		///ACTION - Operate at port2
		vector<tuple<unsigned int,unsigned int,unsigned int,float>> actionsP2;
		minOpTimeP2 = getMinOperationTime(inst, v,f2, port2,earliestTimeP2,numOp,actionsP2);
		if(minOpTimeP2 == -1 || earliestTimeP2+minOpTimeP2 >= inst.t){
			canVisit = false;
			cout << "False 3.55\n";
			return canVisit;
		}
		mergeActions(vesselOperationOrders[v], actionsP2);
	}else{
		///Actions -- Operate as port2 and port2_1
		node2 = get2ndRegionPort(inst,avVessels, false,port2,earliestTimeP2,v,inst.q_v[v],f2,f2_1,x);
		if(node2.first == 0 && node2.second == 0){//Cannot operate as port2 (and port2_1)
			return false;
		}
	}
	/*
	///ACTION - Operate/wait at port2
    minOpTimeP2 = getMinOperationTime(inst, v,f2, port2,earliestTimeP2,numOp,true);
    if(minOpTimeP2 == -1 || earliestTimeP2+minOpTimeP2 >= inst.t){  //Case vessel has load < F_min or it is not possible to complete the operation at dPort
        canVisit = false;
        cout << "False 3.55\n";
        return canVisit;
    }        
    totalWaitTimes += minOpTimeP2;
    if(port2_1 != port2){
		portInventoryCopy[port2_1] = vector<float>(portInventory[port2_1]);
		maxF_itCopy[port2_1] = vector<float>(maxF_it[port2_1]);
		cout<< "Visit also port2_1 " << port2_1 << endl;
		numVisitedPorts++;
        ///ACTION - Travel to port2_1
        costVoyage += inst.travelTime[v][port2][port2_1];
        earliestTime2_1 = earliestTimeP2+minOpTimeP2;
        vesselOperationOrders[v].push(make_tuple(earliestTime2_1,TRAVEL,port2_1,0));
        earliestTime2_1 += inst.travelTime[v][port2][port2_1];
        if(earliestTime2_1 >= inst.t){
			cout << "False 4\n";
			return false;
		}
        ///Use spot market if needed (when the delay is lesser than the delay of other vessels)
        cout<< "Time to port2_1 (" << port2_1 << ") " <<  earliestTime2_1 << " inventory violation " <<  inventoryViolation[port2_1] << endl;
        if(earliestTime2_1 > inventoryViolation[port2_1]){			
			useSpotMarketCopy(inst,v,make_pair(port2_1,inventoryViolation[port2_1]),earliestTime2_1,portInventoryCopy[port2_1],maxF_itCopy[port2_1]);
		}
        ///ACTION - Operate/wait at port2_1
        unsigned int waitP2_1 = getMinOperationTime(inst, v,f2_1, port2_1,earliestTime2_1,aux,true);
        if(waitP2_1 == -1){
			cout << "False 5\n";
			return false;
		}
        totalWaitTimes += waitP2_1;
    }
    */
    uNodeCanVisited = true;   
    unsigned int composite = earliestTimeP1+ minOpTimeP1_partCargo + earliestTimeP2 + minOpTimeP2 + inst.travelTime[v][port1][port2];
    cout<< v << " - Finish at L " << earliestTimeP1 + minOpTimeP1_partCargo << " - Finish at D " << earliestTimeP2+minOpTimeP2 << " numOp " << numOp << " composite " << composite;
    if (isVisitedPort1_1){
        cout<< " 2nd L Port visited: " << port1_1;
        cout<< endl;
	}
    if (isDPort2Visited){
        cout<< " 2nd D Port visited: " << port2_1;
		cout<< endl;
	}
    
    //~ cout<< "Cost :" << costVoyage << endl;
    //~ cout<< "Number op.: " << numOp << endl;
    //~ cout<< "Arrival times : " << earliestTimeP1 << " + " <<  earliestTimeP2 << " composite " << composite << endl;
    //~ cout<< "Arrival difference: " << timeDiff << endl;
    //~ cout<< "Number wait times: " << totalWaitTimes << endl;
    //~ cout<< "Number of visited ports: " << numVisitedPorts << endl;
    //~ cout<< "Total Alpha/Beta " << totalAlphaBeta << endl;
    
    //Fill the PQs with only not delayed vessels to port2 (urgentPort)
    if(canVisit){   
        vesselCostVoyage.push(make_pair(v,costVoyage));
        vesselHigherCostVoyage.push(make_pair(v,costVoyage));
        vesselMinNumberOperations.push(make_pair(v,numOp));
        vesselEarliestArrivalPort.push(make_pair(v,composite)); //Use a combinantion of arrival time at loading and discharging port plus the travel time between them
        vesselMinArrivalDifference.push(make_pair(v,timeDiff));
        vesselMinWaitTimes.push(make_pair(v,totalWaitTimes));
        vesselCostQuantity.push(make_pair(v,costVoyage/inst.q_v[v]));
    }
    //Fill the PQs with delayed vessels too
    lateVesselCostVoyage.push(make_pair(v,costVoyage));
    lateVesselHigherCostVoyage.push(make_pair(v,costVoyage));
    lateVesselMinNumberOperations.push(make_pair(v,numOp));
    lateVesselEarliestArrivalPort.push(make_pair(v,composite));
    lateVesselArrivalTimes[v] = make_pair(earliestTimeP1,earliestTimeP2);
    lateVesselMinArrivalDifference.push(make_pair(v,timeDiff));
    lateVesselMinimumAlphaBeta.push(make_pair(v,totalAlphaBeta));
    lateVesselMinWaitTimes.push(make_pair(v,totalWaitTimes));
    lateVesselCostQuantity.push(make_pair(v,costVoyage/inst.q_v[v]));
    return canVisit;
}

/* Return the minimum number of time periods needed to vessel v operate quantity f at port i, from time t, 
 * considering the current port inventory (copy) and queue of port operations - does not change any inventory informatio (solution and copy)
 * Bool parameter isStored (default false) is true when the actions (operation and waiting) are stored in the vessel queue
 * default value = 0 (can full operate at the same time period in which arrives at port i) 
 * Parameter minOp by reference contabilize the number of operations needed (only in port1 and port2)
 * Return -1 when the operation cannot be performed
 * Requires: updated vector portInventoryCopy[i] and maxF_itCopy[i]
 */ 
int Solution::getMinOperationTime(Instance inst, const unsigned int& v, const float& f, const unsigned int& i, const unsigned int& t, unsigned int& numberOp, bool isStored, vector<tuple<unsigned int,unsigned int,unsigned int,float>>& actions){
	//~ cout<< "Getting minimum time of " << v << " to operate at (" << i << "," << t << ") quantity f = " << f << endl;
    /// Exception - No cargo will be operated (occurs when a vessel is at a loading port and already operated)
    if(f == 0){
        //~ cout<< 0 << endl;
        return 0;
    }
    if(t >= inst.t){ 
		//~ cout<< "-1" << endl;
		return -1;
	}
    unsigned int time = 0;    
    unsigned int u = t;
    float f1=0;
    //Vector copy (only for this scope)
    vector<float> portInvCP(inst.t);  
    vector<float> maxF_itCP(maxF_itCopy[i]);  
	if(!portOperationTime[i].empty()){
		//~ cout<< "Time arrive " << u << " last operation " << portOperationTime[i].top().second << endl;
	}
    ///If other vessel already operated at the port and t < last operation
    if(!portOperationTime[i].empty() && u <= portOperationTime[i].top().second){
		//~ cout<< "Vessel arrive before other opertaion \n";		
    }
	//Impossible to operate before another opertaion without violating inventory
	if(f > maxF_itCP[inst.t-1]){
		//~ cout<< " Cannot operate due maxF_it limit: " << maxF_itCP[inst.t-1] << endl;		
		return -1;
	}
	//Wait (if necessary)
	while(portBerthOcupation[i][u] >= inst.b_j[i]){
		if(isStored){
			vesselOperationOrders[v].push(make_tuple(u,WAIT,i,0));
		}
		u++;
		time++;
		//~ cout<< "Wait due berth occupation " << time << endl;
		if(u >= inst.t){ 
			return -1;
		}			
	}
    portInvCP[u] = portInventoryCopy[i][u];
        
    if(inst.sMin_jt[i][0] > f){// If minimum is greater than the quantity to load -infeasible
        cerr << "FIX: Vessel not able to load because sMin > f \n";
        //~ cout<< "-1" << endl;
        return -1;
    }
	vector<pair<int,float>> operations; //Stores the time and value operated (in increasing order)
    if(inst.delta[i]==1){ //LOADING PORT
		//~ cout<< "("<< i << "," << u << ") - maxF_itCP " << maxF_itCP[u] << ", portInvCP " << portInvCP[u] << endl;
		if(inst.f_max_jt[i][0] >= f && f-inst.sMax_jt[i][0] <= inst.d_jt[i][u]){//If can load in one time period (inclusively excedent of port)            
            if(maxF_itCP[u] >= f){     //Operation in just one time period
				//~ cout<< "Load all in one time period\n";
				portInvCP[u] -= f;
				if(isStored){
					vesselOperationOrders[v].push(make_tuple(u,OPERATE,i,f));
					costVoyage += u*inst.attemptCost;
					operations.push_back(make_pair(u,f));
					updateAuxiliaryVariables(inst,isStored,v,i,operations,portInventoryCopy[i]);
				}
				numberOp++;
				//~ cout<< 0+time << endl;
				return 0 + time;
            }else{ //Wait until can operate all cargo in one time period (min 1, max >= 2)
                //~ cout<< "Wait until can load all in one time period \n";
                unsigned int currentTimeAfterWait = u;
                float f1 = 0;
                while(f-f1 != 0){
					/*If inventory overflows or
					 * can discharge all*/
					if(portInvCP[currentTimeAfterWait] > inst.sMax_jt[i][0] ||
						f-f1 <= maxF_itCP[currentTimeAfterWait]-f1){
						//~ cout<< "Case 1 - sCP[" << currentTimeAfterWait << "] = " << portInvCP[currentTimeAfterWait] << " f1 = " << f1 << endl;
						while(portBerthOcupation[i][currentTimeAfterWait] >= inst.b_j[i]){
							if(portInvCP[currentTimeAfterWait] > inst.sMax_jt[i][0]){ 
								cerr << "Exception 1 - Vessel " << v << " is not able to operate at (" << i << "," << currentTimeAfterWait << ") due violation of inventory and berth limit - use alpha \n";
								//~ cout<< "Exception 1 - Vessel " << v << " is not able to operate at (" << i << "," << currentTimeAfterWait << ") due violation of inventory and berth limit   (use alpha) - Inventory= " << portInvCP[currentTimeAfterWait] << endl;
								useSpotMarketCopy(inst,v,make_pair(i,currentTimeAfterWait),currentTimeAfterWait+1,portInvCP,maxF_itCP);
							}
							//Waiting procedure
							if(isStored){
								vesselOperationOrders[v].push(make_tuple(currentTimeAfterWait,WAIT,i,0));
							}
							currentTimeAfterWait++;
							if(currentTimeAfterWait >= inst.t){
								//~ cout<< "\n-1A" << endl;
								return -1;
							}
							portInvCP[currentTimeAfterWait] = portInvCP[currentTimeAfterWait-1] + inst.delta[i]*(inst.d_jt[i][currentTimeAfterWait] - portOperations[i][currentTimeAfterWait] - portAlpha[i][currentTimeAfterWait] - portBeta[i][currentTimeAfterWait]);
						}
						//~ cout<< "Time " << currentTimeAfterWait << "inv " << portInvCP[currentTimeAfterWait] << endl;
						bool cancelOp = false;
						float f2;						
						if(f-f1 <= maxF_itCP[currentTimeAfterWait]-f1){//Loads maximum
							f2 = f-f1;
						}else if(portInvCP[currentTimeAfterWait] > inst.sMax_jt[i][0]){// Load a minimum
							f2 = 
							min((float)inst.f_min_jt[i][0],portInvCP[currentTimeAfterWait]-(float)inst.sMax_jt[i][0]);
							if(f2 > maxF_itCP[currentTimeAfterWait]-f1){//No solution
								cerr << "Exception 1.1\n";
								//~ cout<< "Exception 1.1\n"; 
								return -1;
							}
						}else{//Constraints not meet anymore (waited due berth occupation)
							cancelOp = true;
						}
						if(!cancelOp){
							//Adequate f2 if necessary
							if(f-f1-f2 < inst.f_min_jt[i][0] && f-f1-f2 > 0){//If a next op will be necessary
								f2 = f2 - (inst.f_min_jt[i][0] - (f-f1-f2));
							}
							if(f2<inst.f_min_jt[i][0]){
								//~ cout<< "\n-1B" << endl;
								return -1;
							}
							//~ cout<< f2 << endl;
							if(isStored){
								vesselOperationOrders[v].push(make_tuple(currentTimeAfterWait,OPERATE,i,f2));
								operations.push_back(make_pair(currentTimeAfterWait,f2));
								costVoyage += currentTimeAfterWait*inst.attemptCost;
							}
							numberOp++;
							portInvCP[currentTimeAfterWait] -= f2;
							f1 += f2;                    
						}
					}
					//Waiting procedure - only if still need operate
					if(f-f1 > 0.0){
						if(isStored){
							vesselOperationOrders[v].push(make_tuple(currentTimeAfterWait,WAIT,i,0));
						}
						currentTimeAfterWait++;
						if(currentTimeAfterWait >= inst.t){
							//~ cout<< "-1C" << endl;
							return -1;
						}
						portInvCP[currentTimeAfterWait] = portInvCP[currentTimeAfterWait-1] + inst.delta[i]*(inst.d_jt[i][currentTimeAfterWait] - portOperations[i][currentTimeAfterWait] - portAlpha[i][currentTimeAfterWait] - portBeta[i][currentTimeAfterWait]);
					}
					//~ cout<< "Time " << currentTimeAfterWait << "inv " << portInvCP[currentTimeAfterWait] << endl;					
				}
				if(isStored){
					updateAuxiliaryVariables(inst,isStored,v,i,operations,portInventoryCopy[i]);
				}
				//~ cout<< "ok " << currentTimeAfterWait - u + time << endl;				
				return currentTimeAfterWait - u + time;
			}
        }else{ // Fmax < f: Wait load the maximum value first, then wait until the remaining cargo 			can be loaded in one (or more) time period
            //~ cout<< "Wait until load maximum first, wait again for loading the remaining vessel cap \n";
            unsigned int currentTimeAfterWait = u;
            float f1=0;
            while(f-f1 != 0){
				/* a) a maximum can be loaded, loads maximum OR
				 * b) f-f1 [remaining] can be loaded OR 
				 * c) If inventory is greater than the capacity (loads min) */
                if(min(f-f1,maxF_itCP[currentTimeAfterWait]-f1) >= min((float)inst.f_max_jt[i][0],(float)inst.sMax_jt[i][0]) ||
					(f-f1 <= maxF_itCP[currentTimeAfterWait]-f1 && f-f1 <= inst.f_max_jt[i][0]) ||
					portInvCP[currentTimeAfterWait] > inst.sMax_jt[i][0] ){
                    //~ cout<< "Case 2 - ";
                    while(portBerthOcupation[i][currentTimeAfterWait] >= inst.b_j[i]){
						//~ cout<< "Wait due berth limit \n";
						if(portInvCP[currentTimeAfterWait] > inst.sMax_jt[i][0]){ 
							cerr << "Exception 2 - Vessel " << v << " is not able to operate at (" << i << "," << currentTimeAfterWait << ") due violation of inventory and berth limit - use alpha \n";
							//~ cout<< "Exception 2 - Vessel " << v << " is not able to operate at (" << i << "," << currentTimeAfterWait << ") due violation of inventory and berth limit  (use alpha) - Inventory= " << portInvCP[currentTimeAfterWait] << endl;
							useSpotMarketCopy(inst,v,make_pair(i,currentTimeAfterWait),currentTimeAfterWait+1,portInvCP,maxF_itCP);
						}
						//Waiting procedure
						if(isStored){
							vesselOperationOrders[v].push(make_tuple(currentTimeAfterWait,WAIT,i,0));
						}
						currentTimeAfterWait++;
						if(currentTimeAfterWait >= inst.t){
							//~ cout<< "-1" << endl;
							return -1;
						}
						portInvCP[currentTimeAfterWait] = portInvCP[currentTimeAfterWait-1] + inst.delta[i]*(inst.d_jt[i][currentTimeAfterWait] - portOperations[i][currentTimeAfterWait] - portAlpha[i][currentTimeAfterWait] - portBeta[i][currentTimeAfterWait]);
					}
					bool cancelOp = false;
					float f2;						
					if(min(f-f1,maxF_itCP[currentTimeAfterWait]-f1) >= min((float)inst.f_max_jt[i][0],(float)inst.sMax_jt[i][0])){ // Maximum
						f2 = min(min(f-f1,maxF_itCP[currentTimeAfterWait]-f1),(float)inst.f_max_jt[i][0]);
					}else if(f-f1 <= maxF_itCP[currentTimeAfterWait]-f1 && f-f1 <= inst.f_max_jt[i][0]){
						f2 = f-f1;
					}else if(portInvCP[currentTimeAfterWait] > inst.sMax_jt[i][0]){ // Minimum
						f2 = 
						min((float)inst.f_min_jt[i][0],portInvCP[currentTimeAfterWait]-(float)inst.sMax_jt[i][0]);
						if(f2 > maxF_itCP[currentTimeAfterWait]-f1){//No solution
							cerr << "Exception 2.1\n";
							//~ cout<< "Exception 2.1\n"; 
							return -1;
						}
					}else{// Constraints not meet anymore (waited due berth occupation)
						cancelOp = true;
					}					
					if(!cancelOp){
						//Adequate f2 if necessary
						if(f-f1-f2 < inst.f_min_jt[i][0] && f-f1-f2 > 0){
							f2 = f2 - (inst.f_min_jt[i][0] - (f-f1-f2));                        
						}
						if(f2<inst.f_min_jt[i][0]){
							//~ cout<< "\n-1" << endl;
							return -1;
						}
						//~ cout<< f2 << endl;
						if(isStored){
							vesselOperationOrders[v].push(make_tuple(currentTimeAfterWait,OPERATE,i,f2));
							operations.push_back(make_pair(currentTimeAfterWait,f2));
							costVoyage += currentTimeAfterWait*inst.attemptCost;
						}
						numberOp++;
						portInvCP[currentTimeAfterWait] -= f2;
						f1 += f2;
					}
                }
				//Waiting procedure - only if still needs operate
				if(f-f1 > 0.0){
					if(isStored){
						vesselOperationOrders[v].push(make_tuple(currentTimeAfterWait,WAIT,i,0));
					}
					currentTimeAfterWait++;
					if(currentTimeAfterWait >= inst.t){
						//~ cout<< "-1" << endl;
						return -1;
					}
					portInvCP[currentTimeAfterWait] = portInvCP[currentTimeAfterWait-1] + inst.delta[i]*(inst.d_jt[i][currentTimeAfterWait] - portOperations[i][currentTimeAfterWait] - portAlpha[i][currentTimeAfterWait] - portBeta[i][currentTimeAfterWait]);
				}
			}
			if(isStored){
				updateAuxiliaryVariables(inst,isStored,v,i,operations, portInventoryCopy[i]);
			}
            //~ cout<< "ok " << currentTimeAfterWait - u + time << endl;
            return currentTimeAfterWait - u + time; 
		}
    }else{ //DISCHARGING PORT    
		//~ cout<< "PORT INVENTORY (" << i << "," << u << ") pInvCp/pInvCopy/pInv= " <<  portInvCP[u] << "/" << portInventoryCopy[i][u] << "/" << portInventory[i][u] << endl;
		//~ cout<< "("<< i << "," << u << ") - maxF_itCP " << maxF_itCP[u] << ", portInvCP " << portInvCP[u] << " sMax-portInvCP " << inst.sMax_jt[i][0] - portInvCP[u] << " sMax " << inst.sMax_jt[i][0] << endl;
        if(inst.f_max_jt[i][u] >= f && f-inst.sMax_jt[i][0] <= inst.d_jt[i][u]){ //If has capacity to discharge all in one t.p. 
            if(maxF_itCP[u] >= f){  //Can operate in one time period
                //~ cout<< "Discharge all in one time period\n";
                if(isStored){
                    vesselOperationOrders[v].push(make_tuple(u,OPERATE,i,f));
                    costVoyage += u*inst.attemptCost - inst.r_jt[i][u]*f;
                    operations.push_back(make_pair(u,f));
                    updateAuxiliaryVariables(inst,isStored,v,i,operations,portInventoryCopy[i]);
                }
                numberOp++;
                portInvCP[u] += f;
                //~ cout<< 0+time << endl;
                return 0 + time;                
            }else{      //Wait until can operate all cargo in one time period (min 1, max >=2)
				//~ cout<< "Wait until can operate all cargo\n";
                unsigned int currentTimeAfterWait = u;
                float f1 = 0;                
                while(f-f1 != 0){
					/*If inventory stockouts
					 * can discharge all, discharge maximum */
					if(portInvCP[currentTimeAfterWait] < inst.sMin_jt[i][0] ||
						f-f1 <= maxF_itCP[currentTimeAfterWait]-f1){
						//~ cout<< "Case 3 - ";
						while(portBerthOcupation[i][currentTimeAfterWait] >= inst.b_j[i]){
							if(portInvCP[currentTimeAfterWait] < inst.sMin_jt[i][0]){ 
								cerr << "Exception 3 - Vessel " << v << " is not able to operate at (" << i << "," << currentTimeAfterWait << ") due violation of inventory and berth limit - use alpha \n";
								//~ cout<< "Exception 3 - Vessel " << v << " is not able to operate at (" << i << "," << currentTimeAfterWait << ") due violation of inventory and berth limit   (use alpha) - Inventory= " << portInvCP[currentTimeAfterWait] << endl;
								useSpotMarketCopy(inst,v,make_pair(i,currentTimeAfterWait),currentTimeAfterWait+1,portInvCP,maxF_itCP);
							}
							//Waiting procedure
							if(isStored){
								vesselOperationOrders[v].push(make_tuple(currentTimeAfterWait,WAIT,i,0));
							}
							currentTimeAfterWait++;
							if(currentTimeAfterWait >= inst.t){
								//~ cout<< "-1" << endl;
								return -1;
							}
							portInvCP[currentTimeAfterWait] = portInvCP[currentTimeAfterWait-1] + inst.delta[i]*(inst.d_jt[i][currentTimeAfterWait] - portOperations[i][currentTimeAfterWait] - portAlpha[i][currentTimeAfterWait] - portBeta[i][currentTimeAfterWait]);
						}
						bool cancelOp = false;
						float f2;						
						if(f-f1 <= maxF_itCP[currentTimeAfterWait]-f1){//Discharge maximum
							f2 = f-f1;
						}else if(portInvCP[currentTimeAfterWait] < inst.sMin_jt[i][0]){// Discharge minimum
							f2 = min((float)inst.f_min_jt[i][0],fabs(portInvCP[currentTimeAfterWait]));
							if(f2 > maxF_itCP[currentTimeAfterWait]-f1){//No solution
								cerr << "Exception 3.1\n";
								//~ cout<< "Exception 3.1\n"; 
								return -1;
							}
						}else{//Constraints not meet anymore (waited due berth occupation)
							cancelOp = true;
						}
						if(!cancelOp){
							//Adequate f2 if necessary
							if(f-f1-f2 < inst.f_min_jt[i][0] && f-f1-f2 > 0){//If a next op will be necessary
								f2 = f2 - (inst.f_min_jt[i][0] - (f-f1-f2));                        
							}
							if(f2<inst.f_min_jt[i][0]){
								//~ cout<< "\n-1" << endl;
								return -1;
							}
							//~ cout<< f2 << endl;
							if(isStored){
								vesselOperationOrders[v].push(make_tuple(currentTimeAfterWait,OPERATE,i,f2));
								operations.push_back(make_pair(currentTimeAfterWait,f2));
								costVoyage += currentTimeAfterWait*inst.attemptCost - inst.r_jt[i][currentTimeAfterWait]*f2;
							}
							numberOp++;
							portInvCP[currentTimeAfterWait] += f2;
							f1 += f2;                    
						}
					}
					//Waiting procedure - only if still need operate
					if(f-f1 > 0.0){
						if(isStored){
							vesselOperationOrders[v].push(make_tuple(currentTimeAfterWait,WAIT,i,0));
						}
						currentTimeAfterWait++;
						if(currentTimeAfterWait >= inst.t){
							//~ cout<< "-1" << endl;
							return -1;
						}
						portInvCP[currentTimeAfterWait] = portInvCP[currentTimeAfterWait-1] + inst.delta[i]*(inst.d_jt[i][currentTimeAfterWait] - portOperations[i][currentTimeAfterWait] - portAlpha[i][currentTimeAfterWait] - portBeta[i][currentTimeAfterWait]);
					}
				}
				if(isStored){
					updateAuxiliaryVariables(inst,isStored,v,i,operations,portInventoryCopy[i]);
				}
				//~ cout<< "ok " << currentTimeAfterWait - u + time << endl;
				return currentTimeAfterWait - u + time;
            }
        }else{ // Fmax < f: Wait to dicharge the maximum value first, then wait until the remaining cargo can be discharged in one (or more) time period
            //~ cout<< "Wait until discharging maximum first, wait again for discharging the remaining\n";
            unsigned int currentTimeAfterWait = u;
            float f1=0;
            while(f-f1 != 0){
				/* a) a maximum can be discharged, discharge maximum OR
				 * b) f-f1 [remaining] can be unloaded OR 
				 * c) If storage is lesser than minimum capacity (discharge min) */
                if(min(f-f1,maxF_itCP[currentTimeAfterWait]-f1) >= min((float)inst.f_max_jt[i][0],(float)inst.sMax_jt[i][0]) ||
					(f-f1 <= maxF_itCP[currentTimeAfterWait]-f1 && f-f1 <= inst.f_max_jt[i][0]) ||
					portInvCP[currentTimeAfterWait] < inst.sMin_jt[i][0] ){
                    //~ cout<< "Case 4 - ";
                    while(portBerthOcupation[i][currentTimeAfterWait] >= inst.b_j[i]){
						//~ cout<< "Wait due berth limit \n";
						if(portInvCP[currentTimeAfterWait] < inst.sMin_jt[i][0]){ 
							cerr << "Exception 4 - Vessel " << v << " is not able to operate at (" << i << "," << currentTimeAfterWait << ") due violation of inventory and berth limit - use alpha \n";
							//~ cout<< "Exception 4 - Vessel " << v << " is not able to operate at (" << i << "," << currentTimeAfterWait << ") due violation of inventory and berth limit  (use alpha) - Inventory= " << portInvCP[currentTimeAfterWait] << endl;
							useSpotMarketCopy(inst,v,make_pair(i,currentTimeAfterWait),currentTimeAfterWait+1,portInvCP,maxF_itCP);
						}
						//Waiting procedure
						if(isStored){
							vesselOperationOrders[v].push(make_tuple(currentTimeAfterWait,WAIT,i,0));
						}
						currentTimeAfterWait++;
						if(currentTimeAfterWait >= inst.t){
							//~ cout<< "-1" << endl;
							return -1;
						}
						portInvCP[currentTimeAfterWait] = portInvCP[currentTimeAfterWait-1] + inst.delta[i]*(inst.d_jt[i][currentTimeAfterWait] - portOperations[i][currentTimeAfterWait] - portAlpha[i][currentTimeAfterWait] - portBeta[i][currentTimeAfterWait]);
					}
					bool cancelOp = false;
					float f2;						
					if(min(f-f1,maxF_itCP[currentTimeAfterWait]-f1) >= min((float)inst.f_max_jt[i][0],(float)inst.sMax_jt[i][0])){ // Maximum
						f2 = min(min(f-f1,maxF_itCP[currentTimeAfterWait]-f1),(float)inst.f_max_jt[i][0]);
					}else if(f-f1 <= maxF_itCP[currentTimeAfterWait]-f1 && f-f1 <= inst.f_max_jt[i][0]){
						f2 = f-f1;
					}else if(portInvCP[currentTimeAfterWait] < inst.sMin_jt[i][0]){ // Minimum
						f2 = min((float)inst.f_min_jt[i][0],fabs(portInvCP[currentTimeAfterWait]));
						if(f2 > maxF_itCP[currentTimeAfterWait]-f1){//No solution
							cerr << "Exception 4.1\n";
							//~ cout<< "Exception 4.1\n"; 
							return -1;
						}
					}else{// Constraints not meet anymore (waited due berth occupation)
						cancelOp = true;
					}				
					
					if(!cancelOp){
						//Adequate f2 if necessary
						if(f-f1-f2 < inst.f_min_jt[i][0] && f-f1-f2 > 0){
							f2 = f2 - (inst.f_min_jt[i][0] - (f-f1-f2));                        
						}
						if(f2<inst.f_min_jt[i][0]){
							//~ cout<< "\n-1" << endl;
							return -1;
						}
						//~ cout<< f2 << endl;
						if(isStored){
							vesselOperationOrders[v].push(make_tuple(currentTimeAfterWait,OPERATE,i,f2));
							operations.push_back(make_pair(currentTimeAfterWait,f2));
							costVoyage += currentTimeAfterWait*inst.attemptCost - inst.r_jt[i][currentTimeAfterWait]*f2;							
						}
						numberOp++;
						portInvCP[currentTimeAfterWait] += f2;
						f1 += f2;
					}
                }
				//Waiting procedure - only if still needs operate
				if(f-f1 > 0.0){
					if(isStored){
						vesselOperationOrders[v].push(make_tuple(currentTimeAfterWait,WAIT,i,0));
					}
					currentTimeAfterWait++;
					if(currentTimeAfterWait >= inst.t){
						//~ cout<< "-1" << endl;
						return -1;
					}
					portInvCP[currentTimeAfterWait] = portInvCP[currentTimeAfterWait-1] + inst.delta[i]*(inst.d_jt[i][currentTimeAfterWait] - portOperations[i][currentTimeAfterWait] - portAlpha[i][currentTimeAfterWait] - portBeta[i][currentTimeAfterWait]);
				}
			}
			if(isStored){
				updateAuxiliaryVariables(inst,isStored,v,i,operations,portInventoryCopy[i]);
			}
            //~ cout<< "ok " << currentTimeAfterWait - u + time << endl;
            return currentTimeAfterWait - u + time;
        }
    }
}
//Make vessel v perform a voayge to port i 
void Solution::performVoayge(Instance inst,unsigned int v, unsigned int i){
    //Verify if vessel is actually at port i
    if(get<0>(vesselRoute[v].back()) == i){
        //~ cout<< "Vessel " << v << " is already in port " << i << endl;
        return;
    }else if(get<3>(vesselRoute[v].back()) == inst.q_v[v] && inst.delta[i] == 1){ ///Exception for the first iteration
        //~ cout<< "Vessel " << v << " is already loaded for loading at " << i << endl;
        return;        
    }else{
        unsigned int currentPort = get<0>(vesselRoute[v].back());
        unsigned int arriveTime = get<1>(vesselRoute[v].back()) + inst.travelTime[v][currentPort][i];
        #ifndef NDEBUG
        //~ cout<< currentPort << " " << i << " = " << inst.travelTime[v][currentPort][i] << endl;
        //~ cout<< arriveTime << endl;
        assert(arriveTime<inst.t);
        #endif
        costTravel += inst.travelCost[v][currentPort][i];
        //~ cout<< "Travelling from " << currentPort << " and " << i << " - arriving at time " << arriveTime << endl;
        vesselRoute[v].push_back(make_tuple(i,arriveTime,0,get<3>(vesselRoute[v].back())));
        vesselRouteCost[v] += inst.travelCost[v][currentPort][i];        
        vesselRouteCostSpot[v] += inst.travelCost[v][currentPort][i];        
        portVisits[i].push_back(make_pair(v,arriveTime));
    }
}

/*
 * Method for evaluate the cost of voyage of a vessel - it does not modify a solution 
 * Passing vector inventory and maxFit_cp as reference parameter
 * Use what is possible of spot market in node, until one time period before endTime
 * Use beta/theta variables when alpha reached the limit
  */
void Solution::useSpotMarketCopy(Instance inst, const int& v, const pair<unsigned int, unsigned int>& node, const unsigned int& endTime, vector<float>& portInventoryCP, vector<float>& maxF_itCP){
    bool sucess = true;
    unsigned int i = node.first;
    unsigned int t = node.second; 
    unsigned int firstTime = t;
    float cumAlpha = cummulativeAlpha[i];
    vector<float> addBeta (inst.t);
    vector<float> addAlpha(inst.t);     
    vector<float> cumAlphaT(inst.t);     //Accumulte the alpha used in the iteration in a same time period
    //~ cout<< "Using spot market on port " << i << " until time " << endTime << endl;
    while(t < endTime){
        float totalAlpha = 0;
        float totalBeta = 0;
        if(cumAlpha >= inst.alp_max_j[i]){ //No spot market can be more used
            sucess = false;
        }
        if(inst.delta[i] == -1){ ///DISCHARGING PORT            
            int u=t; //Auxiliar time index
            if (portInventoryCP[t] < inst.f_min_jt[i][0]){ //Need check if violates inventory
				if(sucess){ //If spot can be used                
					addAlpha[t] = min(fabs(portInventoryCP[t]), 
									min((float)inst.alp_max_jt[i][0]-portAlpha[i][t],
									(float)inst.alp_max_j[i]-cumAlpha)); //Assuming that the inventory is negative
					cumAlpha += addAlpha[t];
					totalAlpha += addAlpha[t];
					cumAlphaT[t] += addAlpha[t];
					portInventoryCP[t] += addAlpha[t];//portAlpha[i][t];
					vesselOperationOrders[v].push(make_tuple(t,ADDALPHA,i,addAlpha[t]));
					costVoyage += addAlpha[t]*inst.p_jt[i][0];            
					//~ cout<< "Using alpha (t)" << i << " " << t << ": " << addAlpha[t] << " Total alpha " << totalAlpha << endl;
					if(portInventoryCP[t] < inst.sMin_jt[i][t]){ //If alpha was not sufficient for that time period
						float quantityLackProduct = fabs(portInventoryCP[t]);
						//Try to use spot market in the previous time periods
						if(cumAlpha <= inst.alp_max_j[i]){ //If there is sufficient spot for using to cover the lack
							u = t-1;
							while(u>=0 && (quantityLackProduct > 0 && cumAlpha < inst.alp_max_j[i])){ //TODO rever último termo
								addAlpha[u] = min(quantityLackProduct, 
								  min((float)inst.alp_max_jt[i][0]-(portAlpha[i][u]+cumAlphaT[u]),(float)inst.alp_max_j[i]-cumAlpha));
								quantityLackProduct -= addAlpha[u];
								cumAlpha += addAlpha[u];
								cumAlphaT[u] += addAlpha[u];
								portInventoryCP[t] += addAlpha[u];
								vesselOperationOrders[v].push(make_tuple(u,ADDALPHA,i,addAlpha[u]));
								totalAlpha += addAlpha[u];
								costVoyage += addAlpha[u]*inst.p_jt[i][0];
								//~ cout<< "Using alpha (u)" << i << " " << u << ": " << addAlpha[u] << " Total alpha " << totalAlpha << endl;
								u--;
							}
							u++; //Increment in one iterator u for update after the inventories after
							//Put the remaining in beta
							if(quantityLackProduct > 0){
								sucess = false;
								addBeta[t] = quantityLackProduct;                            
								vesselOperationOrders[v].push(make_tuple(t,ADDBETA,i,addBeta[t]));
								totalBeta += addBeta[t];
								//~ cout<< "Using beta (t)" << i << " " << t << ": " << addBeta[t] << " total Beta " << totalBeta << endl;
								portInventoryCP[t] += addBeta[t];
								costVoyage += addBeta[t]*BETA_PENALIZATION;
								infeasibilitiesBeta.push(make_tuple(i,t,addBeta[t]));
							}
						}else{ //Complement time t with beta
							sucess = false;
							addBeta[t] = quantityLackProduct;
							vesselOperationOrders[v].push(make_tuple(t,ADDBETA,i,addBeta[t]));
							totalBeta += addBeta[t]; 
							portInventoryCP[t] += addBeta[t];
							costVoyage += addBeta[t]*BETA_PENALIZATION;
							infeasibilitiesBeta.push(make_tuple(i,t,addBeta[t]));
							//~ cout<< "Using beta (u)" << i << " " << t << ": " << addBeta[t] << " Total Beta " << totalBeta << endl;
						}
					}            
				}else{//USE BETAs
					if (t == firstTime){ //First iteration
						addBeta[t] = fabs(portInventoryCP[t]);
					}else{
						addBeta[t] = inst.d_jt[i][t];
					}                
					vesselOperationOrders[v].push(make_tuple(t,ADDBETA,i,addBeta[t]));
					totalBeta += addBeta[t];
					//~ cout<< "Using beta (t)" << i << " " << t << ": " << addBeta[t] << " Total Beta " << totalBeta << endl;
					portInventoryCP[t] += addBeta[t];
					costVoyage += addBeta[t]*BETA_PENALIZATION;
				}            
				//Update inventory and priority queue
				bool dPortsStockoutIsUptade=false;        //Flag for informing if the value t associated with i in priorityQueue is updated                 
				int w = u;
				for(u;u<inst.t;u++){                
					if(u!=t){ //Time t is already updated
						if(u<t){
							for(int y=u;y>=w;y--){
								//~ cout<< "Updating inventoryCopy timeY " << y << ": " << portInventoryCP[u] << " -> " << portInventoryCP[u] + addAlpha[y] << endl;
								portInventoryCP[u] += addAlpha[y];
							}   
						}else{ //Cases where u>t
							//~ //~ cout<< "Updating inventoryCopy time " << u << ": " << portInventoryCP[u] << " -> " << portInventoryCP[u] + totalAlpha + totalBeta << endl;
							portInventoryCP[u] += totalAlpha + totalBeta;
						}
					}
				}
            }
        }else{  ///LOADING PORT 
            int u=t; //Auxiliar time index
            //~ vector<float> addAlpha(inst.t);     //For each iteration t stores the value of alpha used (sublevel of alpha)
            if(portInventoryCP[t] > inst.sMax_jt[i][0]){ //Need to check each time period
				if(sucess){
					addAlpha[t] = min(portInventoryCP[t] - (float)inst.sMax_jt[i][0], 
									min((float)inst.alp_max_jt[i][0]-portAlpha[i][t],
									(float)inst.alp_max_j[i]-cumAlpha));
					cumAlpha += addAlpha[t];
					cumAlphaT[t] += addAlpha[t];
					totalAlpha = addAlpha[t];
					portInventoryCP[t] -= addAlpha[t];
					vesselOperationOrders[v].push(make_tuple(t,ADDALPHA,i,addAlpha[t]));
					costVoyage += addAlpha[t]*inst.p_jt[i][0];
					//~ cout<< "Using alpha (t)" << i << " " << t << ": " << addAlpha[t] << " Cum alpha " << cumAlpha << endl;
					if(portInventoryCP[t] > inst.sMax_jt[i][0]){//If alpha is not sufficient for that time period
						float quantitySurplusProduct = portInventoryCP[t]-inst.sMax_jt[i][0];
						//Try to use spot market in the previous time periods
						if(cumAlpha <= inst.alp_max_j[i]){ //If there is spot for using
							u = t-1;                    
							while(u>=0 && (quantitySurplusProduct > 0 && cumAlpha < inst.alp_max_j[i])){
								addAlpha[u] = min(quantitySurplusProduct, 
												min((float)inst.alp_max_jt[i][0]-(portAlpha[i][u]+cumAlphaT[u]),
												  (float)inst.alp_max_j[i]-cumAlpha)); 
								quantitySurplusProduct -= addAlpha[u];
								cumAlpha += addAlpha[u];
								cumAlphaT[u] += addAlpha[u];
								portInventoryCP[t] -= addAlpha[u];
								vesselOperationOrders[v].push(make_tuple(u,ADDALPHA,i,addAlpha[u]));
								totalAlpha += addAlpha[u];
								costVoyage += addAlpha[u]*inst.p_jt[i][0];
								//~ cout<< "Using alpha (u)" << i << " " << u << ": " << addAlpha[u] << " Cum alpha " << cumAlpha << " Surplus " << quantitySurplusProduct << endl;
								u--;
							}
							u++;
							//If alpha was not sufficient, complete with beta
							if(quantitySurplusProduct > 0){
								sucess = false;
								addBeta[t] = quantitySurplusProduct;
								vesselOperationOrders[v].push(make_tuple(t,ADDBETA,i,addBeta[t]));
								totalBeta += addBeta[t]; 
								//~ cout<< "Using beta (u)" << i << " " << t << ": " << addBeta[t] << " Total Beta " << totalBeta << endl;
								portInventoryCP[t] -= addBeta[t];
								costVoyage += addBeta[t]*BETA_PENALIZATION;
								infeasibilitiesBeta.push(make_tuple(i,t,addBeta[t]));                            
							}
						}else{ //Only beta is possible
							sucess = false;
							addBeta[t] = quantitySurplusProduct;
							vesselOperationOrders[v].push(make_tuple(t,ADDBETA,i,addBeta[t]));
							totalBeta += addBeta[t];
							//~ cout<< "Using beta (u1)" << i << " " << t << ": " << addBeta[t] << " Total Beta " << totalBeta << endl;
							portInventoryCP[t] -= addBeta[t];
							costVoyage += addBeta[t]*BETA_PENALIZATION;
							infeasibilitiesBeta.push(make_tuple(i,t,addBeta[t]));
						}
					}
				}else{ //Only beta can be used
					if (t == firstTime){ //First iteration
						addBeta[t] = portInventoryCP[t]-inst.sMax_jt[i][0];
					}else{
						addBeta[t] = inst.d_jt[i][t];
					}                
					totalBeta += addBeta[t];
					vesselOperationOrders[v].push(make_tuple(t,ADDBETA,i,addBeta[t]));
					//~ cout<< "Using beta (t)" << i << " " << t << ": " << addBeta[t] << " Total Beta " << totalBeta << endl;
					portInventoryCP[t] -= addBeta[t];
					costVoyage += addBeta[t]*BETA_PENALIZATION;
					infeasibilitiesBeta.push(make_tuple(i,t,addBeta[t]));
				}
				//Update inventory
				int w = u;
				for(u;u<inst.t;u++){
					if(u!=t){ //Current time is already updated
						if(u<t){
							for(int y=u;y>=w;y--){                                 
								portInventoryCP[u] -= addAlpha[y];
							}
						}else{ //Cases where u>t                        
							portInventoryCP[u] -= totalAlpha + totalBeta;
						}
					}
				}
			}
        }   
        t++;
        totalAlphaBeta += totalAlpha + totalBeta;
        //~ cout<< "Total Alpha Beta " <<  totalAlphaBeta << " a " << totalAlpha << " b " << totalBeta << endl;
    }
    //TODO portInventoryCP is updated - but the value of alpha/betas is not known
    updateMaxFCopy(inst,i,portInventoryCP,maxF_itCP);    
}
/*
 * Select at random a vessel to perform a voyage and operate at port(s)
 * Params: 
 * id of best vessel or best delayed vessel
 * prob - proabiblity [0,1] of the first best vessel of each criteria be selected
  */
void Solution::selectVesselRandomCriteria(Instance inst, int& bestVessel, int& bestDelayedVessel, float prob){
    unsigned int random = inst.iRand2(6);    
    //Print first vessel
    if(!vesselMinWaitTimes.empty()){
		//~ cout<< ">> Vessel First Vessel of List \n";
		//~ cout<<	"0 - Min Wait Times " << vesselMinWaitTimes.top().first << endl; 
		//~ cout<< "1 - Min Cost " << vesselCostVoyage.top().first << endl;
		//~ cout<< "2 - Min cost/capacity " << vesselCostQuantity.top().first << endl;
		//~ cout<< "3 - Earliest Arrival " << vesselEarliestArrivalPort.top().first << endl;
		//~ cout<< "4 - Closest arrival time " << vesselMinArrivalDifference.top().first << endl;
		//~ cout<<	"5 - Min number of operation " << vesselMinNumberOperations.top().first << endl;
		//~ cout<< "6 - Max cost " << vesselHigherCostVoyage.top().first << endl;
	}
	//~ cout<< "Selected best vessel of list " << random << endl;
    switch (random){
		case 0: //By minor number of waiting times to operate
			if(!vesselMinWaitTimes.empty()){                
				bestVessel = vesselMinWaitTimes.top().first;
				break;
			}else if(!lateVesselMinimumAlphaBeta.empty()){             
				bestDelayedVessel = lateVesselMinimumAlphaBeta.top().first;
				break;
            }
            break;
        case 1: //By minor cost        
            if(!vesselCostVoyage.empty()){
				bestVessel = vesselCostVoyage.top().first;
				break;
            }else if(!lateVesselMinimumAlphaBeta.empty()){             
				bestDelayedVessel = lateVesselMinimumAlphaBeta.top().first;
				break;
			}
            break;
        case 5: //By minor number of operations 
            if(!vesselMinNumberOperations.empty()){
				bestVessel = vesselMinNumberOperations.top().first;
				break;
            }else if(!lateVesselMinimumAlphaBeta.empty()){             
				bestDelayedVessel = lateVesselMinimumAlphaBeta.top().first;
				break;
            }
            break;
        case 3: //By earliest arrival time 
            if(!vesselEarliestArrivalPort.empty()){
				bestVessel = vesselEarliestArrivalPort.top().first;
				break;
           }else if(!lateVesselMinimumAlphaBeta.empty()){             
				bestDelayedVessel = lateVesselMinimumAlphaBeta.top().first;
				break;				
            }
            break;
        case 4: //By clossest arrival time to the time of the inventory violation (DP)
            if(!vesselMinArrivalDifference.empty()){
				bestVessel = vesselMinArrivalDifference.top().first;
				break;				
           }else if(!lateVesselMinimumAlphaBeta.empty()){             
				bestDelayedVessel = lateVesselMinimumAlphaBeta.top().first;
				break;
            }
            break;
		case 2: //By minor cost/capacity
			if(!vesselCostQuantity.empty()){
				bestVessel = vesselCostQuantity.top().first;
				break;
            }else if(!lateVesselMinimumAlphaBeta.empty()){             
				bestDelayedVessel = lateVesselMinimumAlphaBeta.top().first;
					break;
            }
            break;
        case 6: //By highest cost
            if(!vesselHigherCostVoyage.empty()){
				bestVessel = vesselHigherCostVoyage.top().first;
				break;
            }else if(!lateVesselMinimumAlphaBeta.empty()){             
				bestDelayedVessel = lateVesselMinimumAlphaBeta.top().first;
				break;
            }
            break;
		case 7: //By minor alpha (only if late, otherwise consider the minor cost), -- TODO remove
			 if(!vesselCostVoyage.empty()){
				bestVessel = vesselCostVoyage.top().first;
				break;
            }else if(!lateVesselMinimumAlphaBeta.empty()){             
				bestDelayedVessel = lateVesselMinimumAlphaBeta.top().first;
				break;
            }
            break;
        default: 
			cerr << "No existing option for selectin vessel rank (" << random << ")\n";
            exit(0);
    }    
}


/*
 * Sstipulate a value to be operated by vessel v in ports i and j, starting to operate at i in time t
 * minimizing the total time operating at i
 * Return false if vessel cannot supply the ports at the same voyage due amount f or time
 */
bool Solution::vesselCanOperatePorts2(Instance inst,const unsigned int& v, const unsigned int& i,const unsigned int& j,
		const unsigned int& t,float& operateInI, float& operateInJ, const float& f, vector< tuple<unsigned int,unsigned int,unsigned int,float> >& vesselPortActions,
		int& endTimeOperation, unsigned int& numOperations){
    if(inst.f_min_jt[i][0] + inst.f_min_jt[j][0] > f || t + inst.travelTime[v][i][j] >= inst.t)
        return false;        
    operateInI = min(f, min((float)inst.f_max_jt[i][0], maxF_itCopy[i][t])); // As much as possible in I for operating in 1 t.p.
    operateInJ = f - operateInI;
    if(operateInI < inst.f_min_jt[i][0]){
        float dif = inst.f_min_jt[i][0] - operateInI;
        operateInI += dif;
        operateInJ -= dif;
    }else if (operateInJ < inst.f_min_jt[j][0]){
        float dif = inst.f_min_jt[j][0] - operateInJ;
        operateInJ += dif;
        operateInI -= dif;
    }
    //Operate at i
    int timeOpI = getMinOperationTime(inst,v,operateInI,i,t,numOperations,vesselPortActions);
    if(timeOpI == -1){
		return false;
	}
    //Travel to j
    vesselPortActions.push_back(make_tuple(t+timeOpI,TRAVEL,j,0));
    costVoyage += inst.costVoyage[v][i][j];
    int timeAtJ = t+timeOpI+inst.travelTime[v][i][j];
    //Spot on j
    if(timeAtJ > inventoryViolation[j]){
		useSpotMarketCopy(inst,v,make_pair<j,inventoryViolation[j]>,timeAtJ,portInventoryCopy,maxF_itCopy,vesselPortActions);
	}
    //Operate at j    
    int timeOpJ = getMinOperationTime(inst,v,operateInJ,j,t+inst.travelTime[v][i][j],numOperations,vesselPortActions);
    if(timeOpJ == -1){
		return false;
	}
	endTimeOperation = timeAtJ + timeOpJ;
    //~ cout<< "OperateInI (" << i << ") " << operateInI << "/ OperateInJ (" << j << ") " << operateInJ << endl;    
    return true;
}
/*
 * Return the maximum value that can be operated in port i at time t considering the F_Max parameter and port inventory
 * OBS: Do not guarantee that f >= FMin_jt
 */ 
float Solution::getMaxOperationInOneTimePeriod(Instance inst,const unsigned int& i, const unsigned int& t){
	float f;
	if(inst.delta[i] == 1){ //Loading port
		f = min((double)inst.f_max_jt[i][0], (double)portInventory[i][t]);
	}else{ //Discharging port
		f = min((double)inst.f_max_jt[i][0], (double)inst.sMax_jt[i][0] - portInventory[i][t]);
	}
	return f;
}

/*
 * Add spot market variables alpha,beta or theta directly to the solutioin (also updates the portInventoryCopy)
 * @ node: corresponding port time to add the spot market
 * @ endTime: last time period where spot makert should be used
 * @ init = inform if it is used in the init method (no updates priority queues), default false
 */
bool Solution::useSpotMarket(Instance inst, const pair<unsigned int, unsigned int>& node, const unsigned int& endTime, bool init){
    bool sucess = true;
    unsigned int i = node.first;
    unsigned int t = node.second; 
    unsigned int firstTime = t;
    cout<< "Using spot market at (" << i << "," << t << ") until time " << endTime << endl;
    while(t < endTime){
        float totalAlpha = 0;
        float totalBeta = 0;
        if(cummulativeAlpha[i] >= inst.alp_max_j[i]){ //No spot market can be used more
            sucess = false;
        }
        cout << "S_it " << portInventory[i][t] << endl;
        if(inst.delta[i] == -1){ ///DISCHARGING PORT            
            if(portInventory[i][t] < inst.sMin_jt[i][0]){//Only if an inventory violation occured
				int u=t; //Auxiliar time index
				vector<float> addAlpha(inst.t);     //For each iteration t stores the value of alpha used (sublevel of alpha)
				if(sucess){ //If spot can be used                
					addAlpha[t] = min(fabs(portInventory[i][t]), 
									min((float)inst.alp_max_jt[i][0]-portAlpha[i][t],
									(float)inst.alp_max_j[i]-cummulativeAlpha[i])); //Assuming that the inventory is negative
					//~ portAlpha[i][t] += min(fabs(portInventory[i][t]), min((float)inst.alp_max_jt[i][0],(float)inst.alp_max_j[i]-cummulativeAlpha[i])); //Assuming that the inventory is negative
					cummulativeAlpha[i] += addAlpha[t];
					totalAlpha = addAlpha[t];
					portInventory[i][t] += addAlpha[t];//portAlpha[i][t];
					portInventoryCopy[i][t] += addAlpha[t];
					costAlpha += addAlpha[t]*inst.p_jt[i][t];
					portCosts[i] += addAlpha[t]*inst.p_jt[i][t];
					cout<< "Using alpha (t)" << i << " " << t << ": " << addAlpha[t] << " Total alpha " << totalAlpha << endl;
					if(portInventory[i][t] < 0){ //If alpha was not sufficient for that time period
						float quantityLackProduct = fabs(portInventory[i][t]);
						//Try to use spot market in the previous time periods
						if(cummulativeAlpha[i] <= inst.alp_max_j[i]){ //If there is sufficient spot for using to cover the lack
							u = t-1;
							while(u>=0 && (quantityLackProduct > 0 && cummulativeAlpha[i] < inst.alp_max_j[i])){ //TODO rever último termo
								addAlpha[u] = min(quantityLackProduct, min((float)inst.alp_max_jt[i][0]-portAlpha[i][u],(float)inst.alp_max_j[i]-cummulativeAlpha[i]));
								quantityLackProduct -= addAlpha[u];
								cummulativeAlpha[i] += addAlpha[u];
								portInventory[i][t] += addAlpha[u];
								portInventoryCopy[i][t] += addAlpha[u];
								totalAlpha += addAlpha[u];
								costAlpha += addAlpha[u]*inst.p_jt[i][u];
								portCosts[i] += addAlpha[u]*inst.p_jt[i][u];
								cout<< "Using alpha (u)" << i << " " << u << ": " << addAlpha[u] << " Total alpha " << totalAlpha << endl;
								u--;
								
							}                        
							u++; //Increment in one iterator u for update after the inventories after
							//Put the remaining in beta
							if(quantityLackProduct > 0){
								sucess = false;
								portBeta[i][t] = quantityLackProduct;
								
								totalBeta += portBeta[i][t]; 
								portInventory[i][t] += portBeta[i][t];
								portInventoryCopy[i][t] += portBeta[i][t];
								costPenalization += portBeta[i][t]*BETA_PENALIZATION;
								portCosts[i] += portBeta[i][t]*BETA_PENALIZATION;
								cummulativeBeta[i] += portBeta[i][t];
								infeasibilitiesBeta.push(make_tuple(i,t,portBeta[i][t]));
								cout<< "Using beta (u)" << i << " " << t << ": " << portBeta[i][t] << " Cum Beta " << cummulativeBeta[i] << " Cost " << costPenalization <<  endl;
								
							}
						}else{ //Complement time t with beta
							sucess = false;
							portBeta[i][t] = quantityLackProduct;
							
							totalBeta += portBeta[i][t]; 
							portInventory[i][t] += portBeta[i][t];
							portInventoryCopy[i][t] += portBeta[i][t];
							costPenalization += portBeta[i][t]*BETA_PENALIZATION;
							portCosts[i] += portBeta[i][t]*BETA_PENALIZATION;
							cummulativeBeta[i] += portBeta[i][t];
							infeasibilitiesBeta.push(make_tuple(i,t,portBeta[i][t]));
							cout<< "Using beta (u)" << i << " " << t << ": " << portBeta[i][t] << " Cum Beta " << cummulativeBeta[i] << " Cost " << costPenalization <<  endl;
						}
					}            
				}else{//USE BETAs
					if (t == firstTime){ //First iteration
						portBeta[i][t] = fabs(portInventory[i][t]);
					}else{
						portBeta[i][t] = inst.d_jt[i][t];
					}                
					totalBeta += portBeta[i][t];
					cummulativeBeta[i] += portBeta[i][t];              
					portInventory[i][t] += portBeta[i][t];
					portInventoryCopy[i][t] += portBeta[i][t];
					costPenalization += portBeta[i][t]*BETA_PENALIZATION;                
					portCosts[i] += portBeta[i][t]*BETA_PENALIZATION;
					infeasibilitiesBeta.push(make_tuple(i,t,portBeta[i][t]));
					cout<< "Using beta (t)" << i << " " << t << ": " << portBeta[i][t] << " Cum Beta " << cummulativeBeta[i] << " Cost " << costPenalization << endl;
				}
				vector<pair<unsigned int, unsigned int>> auxVector;
				if(!init){				
					if(hasInventoryViolation[i]){
						//Remove the element corresponding to the port i from the PQ                            
						while(!dPortsStockout.empty()){
							cout << "dPQ " << dPortsStockout.top().first << "," << dPortsStockout.top().second << endl;
							if(dPortsStockout.top().first != i){ 
								auxVector.push_back(make_pair(dPortsStockout.top().first,dPortsStockout.top().second));            
								dPortsStockout.pop();
							}else{ //when i is found                        
								dPortsStockout.pop(); // remove the pair corresponding to port i
								break;
							}
						}
						cout << endl;
					}
				}
				//Update inventory and priority queue
				bool dPortsStockoutIsUptade=false;        //Flag for informing if the value t associated with i in priorityQueue is updated                 
				unsigned int w = u;
				for(u;u<inst.t;u++){                
					if(u!=t){ //Time t is already updated
						if(u<t){
							for(unsigned int y=u;y>=w;y--){
								portInventory[i][u] += addAlpha[y];
								portInventoryCopy[i][u] += addAlpha[y];
							}   
						}else{ //Cases where u>t
							portInventory[i][u] += totalAlpha + totalBeta;
							portInventoryCopy[i][u] += totalAlpha + totalBeta;
						}
					}
					//Update alpha variables 
					portAlpha[i][u] += addAlpha[u];
					
					//Verify if i is updated on priority queue
					if(!init){
						if(!dPortsStockoutIsUptade){
							if(portInventory[i][u]<inst.sMin_jt[i][0]){                        
								cout << "(D) New violation at (" << i << "," << u << ") =" << portInventory[i][u] << endl;
								hasInventoryViolation[i] = true;
								dPortsStockout.push(make_pair(i,u));
								inventoryViolation[i] = u;
								while(!auxVector.empty()){ //Re-insert elements of the auxiliary vector
									dPortsStockout.push(make_pair(auxVector.back().first,auxVector.back().second));
									auxVector.pop_back();
								}
								dPortsStockoutIsUptade = true;
							}else if (u+1 == inst.t){ //Case there is no stockout on port i just re-insert the elements of the auxiliary vector                                                
								while(!auxVector.empty()){                             
									dPortsStockout.push(make_pair(auxVector.back().first,auxVector.back().second));
									auxVector.pop_back();                            
								}
								dPortsStockoutIsUptade = true;                    
								hasInventoryViolation[i] = false;
								inventoryViolation[i] = inst.t;
							}else if(portInventory[i][u] > inst.sMax_jt[i][0]){
								cout<< "Error: Inventory on discharging port superior to their capacity\n";
							}
						}   
					}
				}
			}
        }else{  //LOADING PORT 
            int u=t; //Auxiliar time index
            if(portInventory[i][t] > inst.sMax_jt[i][0]){//Only if an inventory violation occured
				vector<float> addAlpha(inst.t);     //For each iteration t stores the value of alpha used (sublevel of alpha)
				if(sucess){
					addAlpha[t] = min(portInventory[i][t] - (float)inst.sMax_jt[i][0], 
									min((float)inst.alp_max_jt[i][0]-portAlpha[i][t],
									(float)inst.alp_max_j[i]-cummulativeAlpha[i]));
					cummulativeAlpha[i] += addAlpha[t];
					totalAlpha = addAlpha[t];
					portInventory[i][t] -= addAlpha[t];
					portInventoryCopy[i][t] -= addAlpha[t];
					costAlpha += addAlpha[t]*inst.p_jt[i][t];
					portCosts[i] += addAlpha[t]*inst.p_jt[i][t];
					cout<< "Using alpha (t)" << i << " " << t << ": " << addAlpha[t] << " Total alpha " << totalAlpha << endl;
					if(portInventory[i][t] > inst.sMax_jt[i][0]){//If alpha is not sufficient for that time period
						float quantitySurplusProduct = portInventory[i][t]-inst.sMax_jt[i][0];
						//Try to use spot market in the previous time periods
						if(cummulativeAlpha[i] <= inst.alp_max_j[i]){ //If there is sufficient spot for using
							u = t-1;                    
							while(u>=0 && (quantitySurplusProduct > 0 && cummulativeAlpha[i] < inst.alp_max_j[i])){
								addAlpha[u] = min(quantitySurplusProduct, 
												min((float)inst.alp_max_jt[i][0]-portAlpha[i][u],
												  (float)inst.alp_max_j[i]-cummulativeAlpha[i])); //Assuming that the inventory is negative
								quantitySurplusProduct -= addAlpha[u];
								cummulativeAlpha[i] += addAlpha[u];
								portInventory[i][t] -= addAlpha[u];
								portInventoryCopy[i][t] -= addAlpha[u];
								totalAlpha += addAlpha[u];
								costAlpha += addAlpha[u]*inst.p_jt[i][u];
								portCosts[i] += addAlpha[u]*inst.p_jt[i][u];
								cout<< "Using alpha (u)" << i << " " << u << ": " << addAlpha[u] << " Total alpha " << totalAlpha << endl;
								u--;
							}
							u++;
							//If alpha was not sufficient, complete with beta
							if(quantitySurplusProduct > 0){
								sucess = false;
								portBeta[i][t] = quantitySurplusProduct;
								totalBeta += portBeta[i][t];                             
								portInventory[i][t] -= portBeta[i][t];
								portInventoryCopy[i][t] -= portBeta[i][t];
								costPenalization += portBeta[i][t]*BETA_PENALIZATION;
								portCosts[i] += portBeta[i][t]*BETA_PENALIZATION;
								cummulativeBeta[i] += portBeta[i][t];
								cout<< "Using beta (u)" << i << " " << t << ": " << portBeta[i][t] << " Cum Beta " << cummulativeBeta[i] << " Cost " << costPenalization << endl;
								infeasibilitiesBeta.push(make_tuple(i,t,portBeta[i][t]));                            
							}
						}else{
							sucess = false;
							portBeta[i][t] = quantitySurplusProduct;
							totalBeta += portBeta[i][t]; 
							cout<< "Using beta (u1)" << i << " " << t << ": " << portBeta[i][t] << " Cum Beta " << cummulativeBeta[i] << endl;
							portInventory[i][t] -= portBeta[i][t];
							portInventoryCopy[i][t] -= portBeta[i][t];
							costPenalization += portBeta[i][t]*BETA_PENALIZATION;
							portCosts[i] += portBeta[i][t]*BETA_PENALIZATION;
							cummulativeBeta[i] += portBeta[i][t];
							infeasibilitiesBeta.push(make_tuple(i,t,portBeta[i][t]));
						}
					}
				}else{ //Only beta can be used
					if (t == firstTime){ //First iteration
						portBeta[i][t] = portInventory[i][t]-inst.sMax_jt[i][0];
					}else{
						portBeta[i][t] = inst.d_jt[i][t];
					}                
					totalBeta += portBeta[i][t];
					cout<< "Using beta (t)" << i << " " << t << ": " << portBeta[i][t] << " Cum Beta " << cummulativeBeta[i] << endl;
					portInventory[i][t] -= portBeta[i][t];
					portInventoryCopy[i][t] -= portBeta[i][t];
					costPenalization += portBeta[i][t]*BETA_PENALIZATION;
					portCosts[i] += portBeta[i][t]*BETA_PENALIZATION;
					cummulativeBeta[i] += portBeta[i][t];
					infeasibilitiesBeta.push(make_tuple(i,t,portBeta[i][t]));
				}
				//Remove the element corresponding to port i from the PQ
				vector<pair<unsigned int, unsigned int>> auxVector;
				if(!init){
					if(hasInventoryViolation[i]){                
						while(!pPortsOverflow.empty()){
							cout << "pPort " << pPortsOverflow.top().first << "," << pPortsOverflow.top().second << endl;
							if(pPortsOverflow.top().first != i){ 
								auxVector.push_back(make_pair(pPortsOverflow.top().first,pPortsOverflow.top().second));            
								pPortsOverflow.pop();
							}else{ //when i is found
								pPortsOverflow.pop(); // remove the pair corresponding to port i
								break;
							}
						}
						cout << endl;
					}
				}
				//Update inventory and priority queue
				bool pPortsOverflowIsUpdated=false;        //Flag for informing if the value t associated with i in priorityQueue is updated             
				int w = u;
				for(u;u<inst.t;u++){
					if(u!=t){ //Current time is already updated
						if(u<t){
							for(int y=u;y>=w && y>=0;y--){
								portInventory[i][u] -= addAlpha[y];
								portInventoryCopy[i][u] -= addAlpha[y];
							}
						}else{ //Cases where u>t                        
							portInventory[i][u] -= totalAlpha + totalBeta;
							portInventoryCopy[i][u] -= totalAlpha + totalBeta;
						}
					}
					//Update alpha variables 
					portAlpha[i][u] += addAlpha[u];
					
					//Verify if i is updated on priority queue
					if(!init){
						if(!pPortsOverflowIsUpdated){
							if(portInventory[i][u]>inst.sMax_jt[i][0]){                        
								cout << "(L) New violation at (" << i << "," << u << ") =" << portInventory[i][u] << endl;
								hasInventoryViolation[i] = true;
								pPortsOverflow.push(make_pair(i,u));
								inventoryViolation[i] = u;
								while(!auxVector.empty()){ //Re-insert elements of the auxiliary vector
									pPortsOverflow.push(make_pair(auxVector.back().first,auxVector.back().second));
									auxVector.pop_back();
								}
								pPortsOverflowIsUpdated = true;
							}else if (u+1 == inst.t){ //Case there is no overflow on port i just re-insert the elements of the auxiliary vector                                                
								while(!auxVector.empty()){ 
									pPortsOverflow.push(make_pair(auxVector.back().first,auxVector.back().second));
									auxVector.pop_back();                            
								}
								pPortsOverflowIsUpdated = true;
								hasInventoryViolation[i] = false;
								inventoryViolation[i] = inst.t;
							}else if(portInventory[i][u] < inst.sMin_jt[i][0]){
								cout<< "Error: Negative inventory on loading port \n";
							}
						}
					}
				}
			}
        }   
        t++;     
    }
	updateMaxF(inst,i);
    return sucess;
}
/* Perform the actions for vessel v stored in the vesselOperationOrders queue  */
void Solution::performActions(Instance inst, const unsigned int& v){
    vector<int> portsVisited; //Stores the ports where the actions were performed
    vector<bool> isPortVisited(inst.J); //Identify if a port is already visited
    while(!vesselOperationOrders[v].empty()){
        tuple<unsigned int,unsigned int,unsigned int,float> action = vesselOperationOrders[v].top();
        unsigned int i = get<2>(action);
        unsigned int t = get<0>(action);
        float f = get<3>(action);
        
        if(!isPortVisited[i]){
			isPortVisited[i] = true;
			portsVisited.push_back(i);
		}
        //~ cout<< "Action vessel " << v << " time " << t << " port " << i << " value " << f << ": ";
        vesselOperationOrders[v].pop();
        switch(get<1>(action)){			
            case OPERATE:
                if(inst.delta[i] == 1){ //Loading port
					//~ cout<< "Load \n";
                    if(!loadVessel(inst,v,i,t,f)){
						cerr << "Vessel " << v << " cannot load at (" << i << "," << t << ") the amount " << f << endl;
						//~ cout << "Vessel " << v << " cannot load at (" << i << "," << t << ") the amount " << f << endl;
					}
                }else{ //Discharging port
                    //~ cout<< "Discharge \n";
                    if(!unloadVessel(inst,v,i,t,f)){
						cerr << "Vessel " << v << " cannot unload at (" << i << "," << t << ") the amount " << f << endl;
						//~ cout << "Vessel " << v << " cannot unload at (" << i << "," << t << ") the amount " << f << endl;
					}
                }
                break;
            case WAIT:
				//~ cout<< "Wait\n";
                wait(inst,v,i,t);
                break;
            case TRAVEL:
				//~ cout<< "Travel to " << i << endl;
                performVoayge(inst,v,i); 
                break;            
            case ADDALPHA:
				//~ cout<< "Add alpha\n";
                addAlphaBetaTheta(inst,i,t,f,0.0,0.0); 
                vesselRouteCostSpot[v] += f*inst.p_jt[i][t];
                break;
            case ADDBETA:
                //~ cout<< "Add beta\n";
                addAlphaBetaTheta(inst,i,t,0.0, f, 0.0);
                vesselRouteCostSpot[v] += f*BETA_PENALIZATION;
                break;
            case ADDTHETA:
				//~ cout<< "Add theta\n";
                addAlphaBetaTheta(inst,i,t,0.0, 0.0, f);
                vesselRouteCostSpot[v] += f*THETA_PENALIZATION;
                break;
        }                 
    }
    //Updates maxF of visited ports
	for(int j = 0; j<portsVisited.size();j++){			
		updateMaxF(inst,portsVisited[j]);
	}
}
/* Value alpha,beta,theta is added to the port i in time period t
 * If value is positive,  auxiliary variables are added, otherwise they are removed
 * Modify portInventory and update the priorityQueue of inventory violation */
void Solution::addAlphaBetaTheta(Instance inst,const unsigned int& i, const unsigned int& t,const float& alpha, const float& beta, const float& theta){
    portAlpha[i][t] += alpha;
    portBeta[i][t] += beta;
    portTheta[i][t] += theta;
    
    costAlpha += alpha*inst.p_jt[i][t];
    costPenalization += beta*BETA_PENALIZATION + theta*THETA_PENALIZATION;
    portCosts[i] += alpha*inst.p_jt[i][t] + beta*BETA_PENALIZATION + theta*THETA_PENALIZATION;
    cummulativeAlpha[i] += alpha;
    cummulativeBeta[i] += beta;
    cummulativeTheta[i] += theta;
    vector<pair<unsigned int, unsigned int>> auxVector;
    if(inst.delta[i]==-1){ ///DISCHARGING PORTS 
        if(hasInventoryViolation[i]){
            //Remove the element corresponding to the port i from the PQ
            while(!dPortsStockout.empty()){
                if(dPortsStockout.top().first != i){ 
                    auxVector.push_back(make_pair(dPortsStockout.top().first,dPortsStockout.top().second));            
                    dPortsStockout.pop();
                }else{ //when i is found                        
                    dPortsStockout.pop(); // remove the pair corresponding to port i
                    break;
                }
            }
        }
        //Update inventory and priority queue
        bool dPortsStockoutIsUptade=false;        //Flag for informing if the value t associated with i in priorityQueue is updated                 
        for(unsigned int t1=t;t1<inst.t;t1++){                
            portInventory[i][t1] += alpha + beta - theta;
            portInventoryCopy[i][t1] = portInventory[i][t1];
             //Verify if i is updated on priority queue            
            if(!dPortsStockoutIsUptade){
                if(portInventory[i][t1]<inst.sMin_jt[i][0]){                        
                    hasInventoryViolation[i] = true;
                    dPortsStockout.push(make_pair(i,t1));
                    inventoryViolation[i] = t1;
                    while(!auxVector.empty()){ //Re-insert elements of the auxiliary vector
                        dPortsStockout.push(make_pair(auxVector.back().first,auxVector.back().second));
                        auxVector.pop_back();
                    }
                    dPortsStockoutIsUptade = true;
                }else if (t1+1 == inst.t){ //Case there is no stockout on port i just re-insert the elements of the auxiliary vector                                                
                    while(!auxVector.empty()){                             
                        dPortsStockout.push(make_pair(auxVector.back().first,auxVector.back().second));
                        auxVector.pop_back();                            
                    }
                    dPortsStockoutIsUptade = true;                    
                    hasInventoryViolation[i] = false;
                    inventoryViolation[i] = inst.t;
                }else if(portInventory[i][t1] > inst.sMax_jt[i][0]){
                    //~ cout<< "Error: Inventory on discharging port superior to their capacity\n";
                }
            }
        }
    }else{ ///LOADING PORTS 
        //Remove the element corresponding to port i from the PQ
        vector<pair<unsigned int, unsigned int>> auxVector;
        if(hasInventoryViolation[i]){                
            while(!pPortsOverflow.empty()){
                if(pPortsOverflow.top().first != i){ 
                    auxVector.push_back(make_pair(pPortsOverflow.top().first,pPortsOverflow.top().second));            
                    pPortsOverflow.pop();
                }else{ //when i is found
                    pPortsOverflow.pop(); // remove the pair corresponding to port i
                    break;
                }
            }
        }
        //Update inventory and priority queue
        bool pPortsOverflowIsUpdated=false;        //Flag for informing if the value t associated with i in priorityQueue is updated             
        for(unsigned int t1=t;t1<inst.t;t1++){
            portInventory[i][t1] -= alpha + beta - theta;
            portInventoryCopy[i][t1] = portInventory[i][t1];
            //Verify if i is updated on priority queue
            if(!pPortsOverflowIsUpdated){
                if(portInventory[i][t1]>inst.sMax_jt[i][0]){
                    hasInventoryViolation[i] = true;
                    pPortsOverflow.push(make_pair(i,t1));
                    inventoryViolation[i] = t1;
                    while(!auxVector.empty()){ //Re-insert elements of the auxiliary vector
                        pPortsOverflow.push(make_pair(auxVector.back().first,auxVector.back().second));
                        auxVector.pop_back();
                    }
                    pPortsOverflowIsUpdated = true;
                }else if (t1+1 == inst.t){ //Case there is no overflow on port i just re-insert the elements of the auxiliary vector                                                
                    while(!auxVector.empty()){ 
                        pPortsOverflow.push(make_pair(auxVector.back().first,auxVector.back().second));
                        auxVector.pop_back();                            
                    }
                    pPortsOverflowIsUpdated = true;
                    hasInventoryViolation[i] = false;
                    inventoryViolation[i] = inst.t;
                }else if(portInventory[i][t1] < inst.sMin_jt[i][0]){
                    //~ cout<< "Error: Negative inventory on loading port (" << i << "," << t1 << ") = " << portInventory[i][t1] << endl;
                }
            }
        }
    }
}
unsigned int Solution::getNearestPortOtherType(Instance inst,const unsigned int& v){
    unsigned int i =  get<0>(vesselRoute[v].back()); //Current position
    unsigned int j,nearestPort,minorDistance=99;
    for(j=0;j<inst.J;j++){
        if(inst.delta[i] != inst.delta[j]){
            if(inst.travelTime[v][i][j] < minorDistance){
                nearestPort = j;
                minorDistance = inst.travelTime[v][i][j];
            }
        }
    }
    return nearestPort;    
}

/*
 * Builds a solution for the problem
 * @isPartial - False if wants to build a solution from the scratch, True  if only needs to 'complete' a partial solution
 * @probSelectBestVessel - [0,1] - Probability of getting the best vessel of the selected criteria
 * 
 */ 
void Solution::buildSolution(Instance inst, const bool& isPartial, const vector<unsigned int>& avVessels, const float& probSelectBestVessel, const float& probRandomSelectCounterPartNode, const float& probRandomSelect2ndPort){
	if(!isPartial){
		init(inst,avVessels);
		//~ print(inst);
	}	
	pair<unsigned int, unsigned int> dischargingNode;
	pair<unsigned int, unsigned int> loadingNode;
	
	pair<unsigned int, unsigned int> urgentNode;
	pair<unsigned int, unsigned int> counterpartNode;
	
	unsigned int dPort2, pPort2; // Used to identify possible ports of the same region to be visited in the same voyage
	unsigned int i,j,v,t;    
	priority_queue<pair<unsigned int, unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst > aux;
	int pairDP=0,onlyP=0,onlyD=0;
	
	while(!dPortsStockout.empty() || !pPortsOverflow.empty()){
		int bestVessel=-1;
		int bestDelayedVessel=-1;
		bool cpNodeCanVisited = false, uNodeCanVisited = false;
		bool counterPartNodeCanBeVisited = false, urgentNodeCanBeVisited = false;
		bool noStockout=false, noOverflow=false;
		if(!dPortsStockout.empty()){
			dischargingNode = dPortsStockout.top();
		}else{
			noStockout = true;
		}
		if(!pPortsOverflow.empty()){
			loadingNode = pPortsOverflow.top();
		}else{
			noOverflow = true;
		}
		if(!noStockout && !noOverflow){ //When L & D ports have inventory violation
			pairDP++;
			if(dischargingNode.second <= loadingNode.second){
				urgentNode = dischargingNode;				
			}else{
				urgentNode = loadingNode;				
			}
		}else if(noOverflow){ // Only D ports with inventory violation
			urgentNode = dischargingNode;
			onlyD++;
		}else{ // Only P ports with inventory violation
			urgentNode = loadingNode;
			onlyP++;
		}		
		counterpartNode = getBestCounterpartNode(inst,urgentNode.first, probRandomSelectCounterPartNode);
		//~ cout<< "Urgent node (" << urgentNode.first << "," << urgentNode.second << ") - Counterpart node (" << counterpartNode.first << "," << counterpartNode.second << ")\n";
		///Evaluating vessels
		for(unsigned int vId=0;vId < avVessels.size();vId++){
			v = avVessels[vId];
			vesselCanVisitNodes(inst,v, avVessels, urgentNode, counterpartNode, cpNodeCanVisited, uNodeCanVisited, probRandomSelect2ndPort);
			totalAlphaBeta = 0;					
			//~ cout << "cpNodeCanVisited = " << cpNodeCanVisited << " uNodeCanVisited " << uNodeCanVisited << endl;
			if(cpNodeCanVisited){
				counterPartNodeCanBeVisited = true;                        
			}
			if(uNodeCanVisited){
				urgentNodeCanBeVisited = true;
			}
		}
		//Select the vessel at time according to the strategy (return -1 if no vessel is available)
		selectVesselRandomCriteria(inst,bestVessel, bestDelayedVessel, probSelectBestVessel);		
		if(bestVessel!= -1){ //If a voyage can be performed without delay
			cout<< "Vessel " << bestVessel << " was selected to perform a voyage between port " << counterpartNode.first << " and " << urgentNode.first << endl;
			performActions(inst, bestVessel);
			
		}else if(bestDelayedVessel != -1){ //If a late vessel can reach the port
			cout<< "Vessel " << bestDelayedVessel << " was selected to perform a voyage between port " << counterpartNode.first << " and " << urgentNode.first << " -- Using spot market " << endl;
			performActions(inst, bestDelayedVessel);                  
		}else{
			//Use spot market on the ports
			cout<< "Only spot market can be used \n";                                        
			cout<< "counterPartNodeCanBeVisited " << counterPartNodeCanBeVisited << endl;
			cout<< "urgentNodeCanBeVisited " << urgentNodeCanBeVisited << endl;
			if(!counterPartNodeCanBeVisited){ 
				useSpotMarket(inst,counterpartNode,inst.t);				
			}else if(!urgentNodeCanBeVisited){
				useSpotMarket(inst,urgentNode,inst.t);
			}
		}
		//~ vector<pair<unsigned int, unsigned int>> auxVector;
		//~ while(!dPortsStockout.empty()){
			//~ cout << "dPQ " << dPortsStockout.top().first << "," << dPortsStockout.top().second << endl;
			//~ auxVector.push_back(make_pair(dPortsStockout.top().first,dPortsStockout.top().second));            
			//~ dPortsStockout.pop();
		//~ }
		//~ while(!auxVector.empty()){
			//~ dPortsStockout.push(make_pair(auxVector.back().first,auxVector.back().second));
			//~ auxVector.pop_back();
		//~ }
			
		resetAuxliaryStructureForSelectingVessel(inst);
		//~ print(inst);
	}
	///Verify if some discharging port can be visited by a vessel for operating - even if no inventory violation takes place
	//~ cout<< ">>>> Trying profit by discharging at D ports without inventory violation \n";
	int residual=0;
	for(i=0;i<inst.J;i++){
		if(inst.delta[i] == -1){
			urgentNode = make_pair(i,inst.t-1);
			counterpartNode = getBestCounterpartNode(inst,urgentNode.first, probRandomSelectCounterPartNode);
			int bestVessel=-1;
			int bestDelayedVessel=-1;
			bool cpNodeCanVisited = false, uNodeCanVisited = false;
			for(unsigned int vId=0;vId < avVessels.size();vId++){
				v = avVessels[vId];
				vesselCanVisitNodes(inst,v, avVessels, urgentNode, counterpartNode, cpNodeCanVisited, uNodeCanVisited, probRandomSelect2ndPort);
				totalAlphaBeta = 0;
			}
			//Select a vessel by its voyage cost (only if a profit is provided)
			if(!vesselCostVoyage.empty()){
				if (vesselCostVoyage.top().second < 0.0)
					bestVessel = vesselCostVoyage.top().first;                            
			}else if(!lateVesselCostVoyage.empty()){
				if (lateVesselCostVoyage.top().second < 0.0)
					bestDelayedVessel = lateVesselCostVoyage.top().first;                            
			}
			
			if(bestVessel!= -1){ //If a voyage can be performed 
				//~ cout<< "1 Add voyage of vessel " << bestVessel << " to port " << i << endl;
				performActions(inst, bestVessel);
				residual++;
			}else if(bestDelayedVessel != -1){ //If a delayed vessel can reach the port
				//~ cout<< "2 Add voyage of vessel " << bestDelayedVessel << " to port " << i << endl;
				performActions(inst, bestDelayedVessel);
				residual++;
			}
			resetAuxliaryStructureForSelectingVessel(inst);
		}
	}
	///			
	//Place the cost of sink arcs
	for(unsigned int vId=0;vId < avVessels.size();vId++){
		v = avVessels[vId];
		costTravel += inst.getSinkArcCost(get<1>(vesselRoute[v].back()));
		vesselRouteCost[v]+= inst.getSinkArcCost(get<1>(vesselRoute[v].back()));		
		vesselRouteCostSpot[v]+= inst.getSinkArcCost(get<1>(vesselRoute[v].back()));		
	}
	
	setObjectiveValue();        
	//~ cout<< "Inventory breachs at Pairs " << pairDP << " OnlyD " << onlyD << " onlyP " << onlyP << " residual " << residual << " Total iterations = " << pairDP + onlyD + onlyP << endl;
}

/*
 * Return the first vessel which arrives at j in or after time t
 * If no vessel arrives, return -1
 * Param by reference t_v is feed by the time in which vessel visit the node
 */ 
int Solution::getNextVisitingVessel(const int& j, const int& t, int& t_vj){
	int v = -1;
	for(unsigned int i=0;i<portVisits[j].size();i++){
		if(portVisits[j][i].second >= t){
			v = portVisits[j][i].first;
			t_vj = portVisits[j][i].second;
			break;
		}
	}
	return v;
}
/*
 * Return the previous visited node by vessel v before time t
 * If no exists node, return <-1,-1>
 */ 
pair<int,int> Solution::getPreviosNodeVisit(const unsigned int& v, const unsigned int& t){	
	vector<unsigned int, unsigned int> auxVec();
	unsigned int i, timeNode;
	for(i=0;i<vesselRoute[v].size();i++){
		timeNode = get<1>(vesselRoute[v][i]);
		if(timeNode >= t && i>0){
			return make_pair(get<0>(vesselRoute[v][i-1]),get<1>(vesselRoute[v][i-1]));
		}
	}	
	return make_pair(-1,-1);	
}
/*
 * Gets the cost/profit associated with the route of vessel v, from nodeI until the end of planning horizon
 * Does not consider operating costs/revenue at nodeI 
 * It considers the alpha/beta penalization if vessel arrived before an inventory violation.
 * TODO - Instead calculate, get the value stored during building the solution
 */
float Solution::getVesselRouteCost(Instance inst, const unsigned int& v, const pair<unsigned int,unsigned int>& nodeI){
	//Get the current cost of each route - vessel v from port i, vessel v1 from port i1
	unsigned int r,j,t, idRV=0;
	unsigned int vRouteSize = vesselRoute[v].size();
	float currentCost = 0;
	//Iterate until achieve nodeI
	while(get<0>(vesselRoute[v][idRV]) != nodeI.first && 
			get<1>(vesselRoute[v][idRV]) != nodeI.second){
		idRV++;
		#ifndef NDEBUG
		assert(idRV < vRouteSize);	
		#endif
	}
	//Arcs and operation cost, revenue profit
	for(r=idRV;r<vRouteSize;r++){		
		if(r+1 < vRouteSize){			
			j = get<0>(vesselRoute[v][r+1]);
			t = get<1>(vesselRoute[v][r+1])-1;
			if(get<0>(vesselRoute[v][r]) != get<0>(vesselRoute[v][r+1])){  //Travelling
				currentCost += inst.travelCost[v][get<0>(vesselRoute[v][r])][get<0>(vesselRoute[v][r+1])];
				
				//~ cout<< "Vessel " << v << " travels from (" << get<0>(vesselRoute[v][r]) << "," << get<1>(vesselRoute[v][r])<< ") to (" << j << "," << t << ")\n";
				//Alpha/beta costs
				while(portBeta[j][t] > 0.0 || portAlpha[j][t] > 0.0){
					//~ cout<< j << " " << t <<  " - Beta " << portBeta[j][t] << " Alpha " << portAlpha[j][t] << endl;
					currentCost += portBeta[j][t]*BETA_PENALIZATION + portAlpha[j][t]*inst.p_jt[j][t];
					t--;
				}
			}		
		}else{ //exiting system arc
			currentCost += inst.getSinkArcCost(get<1>(vesselRoute[v][r]));
		}
		if(r>idRV){ //Not consider operating costs at nodeI
			currentCost += get<1>(vesselRoute[v][r])*inst.attemptCost;
			if(inst.delta[get<0>(vesselRoute[v][r])] == -1){
				currentCost -= inst.r_jt[get<0>(vesselRoute[v][r])][t] * get<2>(vesselRoute[v][r]);
			}
		}		
	}	
	return currentCost;
}

/*
* Ranks the counterpart nodes by earliest inventory violation and closestenss to urgentPort
* Returning the best port (minor sum of the port in the two ranks)
* Considers all ports, independently of inventory violation or not
*/ 
pair<unsigned int,unsigned int> Solution::getBestCounterpartNode(Instance inst,const unsigned int& urgentPort, float probRandomSelect){
	unsigned int counterpartPort;
	//Random selection -> random counterpart or ranked counterpart
	//~ if(probRandomSelect >= fRand(0.0,1.0)){
		//~ int i = inst.iRand2(inst.closestPortOtherType[urgentPort].size()-1);
		//~ counterpartPort = inst.closestPortOtherType[urgentPort][i].first;
	//~ }else{		
		//~ cout<< "Evaluating counterpart ports of urgentPort " << urgentPort << endl;
		vector<unsigned int> rankUrgency(inst.J);
		vector<unsigned int> rankDistance(inst.J);
		vector <pair<unsigned int, unsigned int> > rankSum; // <port,sumRanks>
		unsigned int i,j,count=0;
		//Distance rank
		for(i=0;i<inst.closestPortOtherType[urgentPort].size();i++){
			j = inst.closestPortOtherType[urgentPort][i].first;
			rankDistance[j] += count;
			count++;
		}
		count = 0;
		//Urgency rank - considering only ports with inventory violation
		vector<pair<unsigned int, unsigned int>> auxVector;		
		if(inst.delta[urgentPort] == 1){ //Counterpart port is a discharging port
			while(!dPortsStockout.empty()){
				j = dPortsStockout.top().first;
				rankUrgency[j] += count;
				count++;
				auxVector.push_back(make_pair(dPortsStockout.top().first,dPortsStockout.top().second));
				dPortsStockout.pop();
			}
			while(!auxVector.empty()){ //Re-insert elements of the auxiliary vector
				dPortsStockout.push(make_pair(auxVector.back().first,auxVector.back().second));
				auxVector.pop_back();
			}		
		}else{//Counterpart port is a loading port
			while(!pPortsOverflow.empty()){
				j = pPortsOverflow.top().first;
				rankUrgency[j] += count;
				count++;
				auxVector.push_back(make_pair(pPortsOverflow.top().first,pPortsOverflow.top().second));
				pPortsOverflow.pop();
			}		
			while(!auxVector.empty()){ //Re-insert elements of the auxiliary vector
				pPortsOverflow.push(make_pair(auxVector.back().first,auxVector.back().second));
				auxVector.pop_back();
			}		
		}
		//Accounts the ports with no inventory violation - their urgency rank is the same
		count++;
		for(j=0;j<inst.J;j++){
			if(!hasInventoryViolation[j]){ //Ports with no inventory violation
				rankUrgency[j] += count;
			}
		}
		//Getting and sorting the ranks
		for(i=0;i<inst.J;i++){
			unsigned int sumRank=0;
			if(inst.delta[i] != inst.delta[urgentPort]){ //Only accpets the ports of different type of urgentPort
				sumRank += rankUrgency[i]; 
				sumRank += rankDistance[i];
				//~ cout<< "-> Port " << i << " Urgency " <<  rankUrgency[i] << " Distance " << rankDistance[i] << " sumRank = " << sumRank << endl;
				rankSum.push_back(make_pair(i,sumRank));
			}
		}
		//Sorting
		sort(rankSum.begin(), rankSum.end(), sortbySecInt); 
		
		//Get the first element 
		counterpartPort = rankSum[0].first;
		if(rankSum.size()>1){ //Only if there is more than one option
			if(rankSum[0].second == rankSum[1].second){ // If tie, select the best ranked on inventory violation
				if(rankUrgency[rankSum[0].first] > rankUrgency[rankSum[1].first]){
					counterpartPort = rankSum[1].first;
				}
			}
		}
	//~ }
	return make_pair(counterpartPort,inventoryViolation[counterpartPort]);	
}
/*
 * Check if auxiliary variables alpha, beta,theta are used after time t
 * If they are, remove them - store the actions, update portInvCP.
 */ 
void Solution::removeAuxiliaryVariables(Instance inst, const unsigned int& v, const unsigned int& i, const unsigned int& t,vector<float>& portInvCP, const bool& isStored){
	unsigned int u;
	unsigned int T = portInvCP.size();
	float cumAlpha=0;
	float cumBeta=0;
	for(u=t;u<T;u++){
		if(portAlpha[i][u] > 0){
			portInvCP[u] += inst.delta[i]*(portAlpha[i][u]+cumAlpha); //Sum to LP, minus for DP
			cumAlpha += portAlpha[i][u];
			if(isStored){
				vesselOperationOrders[v].push(make_tuple(u,ADDALPHA,i,-portAlpha[i][u]));
			}			
		}
		if(portBeta[i][u] > 0){
			portInvCP[u] += inst.delta[i]*(portBeta[i][u]+cumBeta); //Sum to LP, minus for DP
			cumBeta += portBeta[i][u];
			if(isStored){
				vesselOperationOrders[v].push(make_tuple(u,ADDBETA,i,-portBeta[i][u]));
			}			
		}
	}	
}

/*
 * Partially destroy a solution given the destruction method type
 * Return a vector with the vessel indexes which route was removed
 */ 
vector<unsigned int> Solution::destroy(Instance inst, const unsigned int& type){	
	vector<unsigned int> avVessels;
	switch(type){
		case 1: //Vessels route - major number of waiting times
			avVessels = removeVesselRoutes(inst, type); 
			break;
		case 2: //Vessels route - random vessel selection
			avVessels = removeVesselRoutes(inst, type); 
			break;
		default:
			cerr << "No type of solution destruction selected \n";
			break;
	}	
	//~ cout<< "Best solution after removing vessels\n"; print(inst);	
	return avVessels;
}
/*
 * Remove vessel route from the current solution
 */ 
vector<unsigned int> Solution::removeVesselRoutes(Instance inst,const unsigned int& type){
	vector<unsigned int> vessels; //Vector storing the vessels ids selected to be deleted
	unsigned int numVessels=3; //Number of vessels that will be removed from the solution
	
	//For random choice 
	bool isSelected[inst.V];
	for(int v=0;v<inst.V;v++){
		isSelected[v] = false;
	}
	int count=0,v1;
	switch(type){
		case 1:
			vessels = getVesselsMajorWaitTimes(numVessels);
			break;
		case 2:		//Random	 
			while(count < numVessels){
				int v1 = inst.iRand2(inst.V-1);
				if(!isSelected[v1]){
					vessels.push_back(v1);
					count++;
					isSelected[v1] = true;
				}
			}
			break;
		default:
			cerr << "No type of vessel route removing selected \n";
			break;
	}
	unsigned int idV,v,i,j,t;
	float f;
	vector<vector <float> > portChangeInv(inst.J); // for <i,t> store the amount that must be added to (or removed from) the inventory of port
	for(i=0;i<inst.J;i++){
		portChangeInv[i] = vector<float>(inst.t);
	}
	//Removing vessels information
	for(idV=0;idV<vessels.size();idV++){
		v = vessels[idV];		
		//~ cout<< "Removing route vessel " << v << " size " << vesselRoute[v].size() << endl;
		vesselRouteCost[v] = inst.getSourceArcCost(v);         
		vesselRouteCostSpot[v] = inst.getSourceArcCost(v);
		unsigned int count = 0, nextI=99;
		while(vesselRoute[v].size()>1){ //Remove all registers of vessel route, keep only the travel from source node to initial node
			i = get<0>(vesselRoute[v].back());
			t = get<1>(vesselRoute[v].back());
			f = get<2>(vesselRoute[v].back());
			////~ cout << "(" << i << "," << t << ") - f = " << f << endl;
			portChangeInv[i][t] += f;
			if(f > 0.0){ //If is an operation
				--portBerthOcupation[i][t];
				costOperation -= t*inst.attemptCost;
				if(inst.delta[i] == -1){
					profitRevenue -= f*inst.r_jt[i][t];
				}
			}
			//Discounting travelling
			if(count==0){   //Sink arc
                costTravel -= inst.getSinkArcCost(t);  
            }else if(i != nextI){   //Traveling arc
                costTravel -= inst.travelCost[v][i][nextI];
            }            
			nextI = i;
			vesselRoute[v].pop_back();
			count++;
		}
		//Remove possible operation/travel at the first time
		f = get<2>(vesselRoute[v].back());
		if(f>0){
			i = get<0>(vesselRoute[v].back());
			t = get<1>(vesselRoute[v].back());
			portChangeInv[i][t] = f;
			--portBerthOcupation[i][t];
			get<2>(vesselRoute[v].back()) = 0;
			get<3>(vesselRoute[v].back()) = inst.s_v0[v];
			costOperation -= t*inst.attemptCost;
			if(inst.delta[i] == -1){
				profitRevenue -= f*inst.r_jt[i][t];
			}
			if(i != nextI && nextI != 99){   //Traveling arc
                costTravel -= inst.travelCost[v][i][nextI];
            }
		}
		for(i=0;i<inst.J;i++){
			//Remove visits 
			unsigned int it=0;
			while(it<portVisits[i].size()){
				if(portVisits[i][it].first == v){
					portVisits[i].erase(portVisits[i].begin()+it);					
					--it;
				}
				++it;
			}
			//Remove operations
			vector<pair<unsigned int, unsigned int>> auxVector;
			while(!portOperationTime[i].empty()){
				if(portOperationTime[i].top().first != v){ 
					auxVector.push_back(make_pair(portOperationTime[i].top().first,portOperationTime[i].top().second));            
					portOperationTime[i].pop();
				}else{ //when finds v, does not store at aux vector, remove operations value
					portOperations[i][portOperationTime[i].top().second] = 0;
					portOperationTime[i].pop(); 					
				}
			}	
			while(!auxVector.empty()){
				portOperationTime[i].push(make_pair(auxVector.back().first,auxVector.back().second));
				auxVector.pop_back();
			}
		}
	}
	costAlpha = 0;
	costPenalization = 0;
	
	for(i=0;i<inst.J;i++){
		bool pqUpdated=false;
		//Update port inventory - remove all alpha and beta variables
		float cumF=0, cumA=0, cumB=0;
		cummulativeAlpha[i] = 0;
		cummulativeBeta[i] = 0;
		portCosts[i] = 0;
		for(t=0;t<inst.t;t++){
			////~ cout << "portChange (" << i << "," << t << ") = " << portChangeInv[i][t] << endl;
			portInventory[i][t] +=     inst.delta[i]*(portChangeInv[i][t]+ cumF + portAlpha[i][t] + cumA + portBeta[i][t] + cumB);
			portInventoryCopy[i][t] += inst.delta[i]*(portChangeInv[i][t]+ cumF + portAlpha[i][t] + cumA + portBeta[i][t] + cumB);
			cumF += portChangeInv[i][t];
			cumA += portAlpha[i][t];
			cumB += portBeta[i][t];
			portAlpha[i][t] = 0;
			portBeta[i][t] = 0;
			portCosts[i] = 0;
			if(!pqUpdated){
				if(inst.delta[i] == 1 && portInventory[i][t] > inst.sMax_jt[i][0]){
					hasInventoryViolation[i] = true;
					inventoryViolation[i] = t;
					pqUpdated = true;
					pPortsOverflow.push(make_pair(i,t));						
				}else if(inst.delta[i] == -1 && portInventory[i][t] < inst.sMin_jt[i][0]){
					hasInventoryViolation[i] = true;
					inventoryViolation[i] = t;
					pqUpdated = true;
					dPortsStockout.push(make_pair(i,t));
				}
			}
		}
		updateMaxF(inst,i); //Update all value of MaxF_it
	}
	setObjectiveValue();	
	return vessels;
}
vector<unsigned int> Solution::getVesselsMajorWaitTimes(const unsigned int& numVessels){
	unsigned int r,i,v,prevI,prevT,waits;
	priority_queue< pair<unsigned int,unsigned int>,vector<pair<unsigned int,unsigned int>>, ComparePairHigherFirst > vesselsWaitTime;
	for(v=0;v<vesselRoute.size();v++){
		waits = 0;
		for(r=0;r<vesselRoute[v].size();r++){
			i = get<0>(vesselRoute[v][r]);
			if(r>0){  
				if(i == prevI){
					++waits;
				}
			}
			prevI = i; 
		}
		vesselsWaitTime.push(make_pair(v,waits));
	}
	vector<unsigned int> vessels;
	unsigned int count = 0;
	
	while(count < numVessels){
		vessels.push_back(vesselsWaitTime.top().first);
		vesselsWaitTime.pop();	
		count++;
	}
	return vessels;
}

void Solution::rebuild(Instance inst, const unsigned int& type, const vector<unsigned int>& avVessels, const float& probSelectBestVessel, const float& probRandomSelectCounterPartNode, const float& probRandomSelect2ndPort){
	switch(type){
		case 1:			
			buildSolution(inst, true, avVessels, probSelectBestVessel, probRandomSelectCounterPartNode, probRandomSelect2ndPort);
			break;
		default:
			cerr << "No selected option for rebuild operation\n";
			exit(0);
	}

}

/* Returns the total alpha and beta used at port i from time t until the next operation occured at the port
 * If no operation is performed after t, return 0 */
float Solution::getSumAlphaBetaUntilNextOperation(const unsigned int& i, const unsigned int& t){
	unsigned int time;
	float sum=0;
	if(!portOperationTime[i].empty()){
		time = portOperationTime[i].top().second;	
		if(time < t){ //There is no operation after t
			return 0;
		}	
		time = getPortNextOperation(i,t).second;		
		for(unsigned int t1=t;t1<=time;t1++){
			sum += portAlpha[i][t1] + portBeta[i][t1];
		}
	}
	//~ cout<< "Sum alpha " << i << " " << t  << " = " << sum << endl;
	return sum;
}
/* Returns the pair <vessel,time> corresponding to the operation occured after time t at port i
 * If no operation occurs after t, return a pair <999,0>
 */  
pair<unsigned int,unsigned int> Solution::getPortNextOperation(const unsigned int& i, const unsigned int& t){
	if(portOperationTime[i].empty()){
		return make_pair(999,0);
	}	
	unsigned int time = portOperationTime[i].top().second;
	unsigned int v = portOperationTime[i].top().first;
	vector<pair<unsigned int, unsigned int>> auxVector;
	if(time < t){ //No operation after t
		return make_pair(999,0);
	}
	while(time >= t && !portOperationTime[i].empty()){
		v = portOperationTime[i].top().first;
		time = portOperationTime[i].top().second;
		auxVector.push_back(make_pair(v,time));
		portOperationTime[i].pop();
	}
	while(!auxVector.empty()){
		portOperationTime[i].push(make_pair(auxVector.back().first,auxVector.back().second));
		auxVector.pop_back();
	}
	if (time < t){
		return make_pair(999,0);
	}
	return make_pair(v,time);
}
/*
 * Update the maximum value that can be operated in each time period
 * Of port i without violating the inventory (maximum at DP, minimum at LP) 
 * due possible operations occuring before others one.
 * Update starting from time t=0
 * O(t^2)
 */ 
void Solution::updateMaxF(Instance inst, const unsigned int& i){
	unsigned int t,t1;
	//~ cout<< "Updating MaxF of port " << i << endl;
	for(t=0;t<inst.t;t++){
		float minS; //Minimum inventory at LP or minimum avaiable space in DP
		unsigned int minTime; //Time in which minS is registred
		if(inst.delta[i] == 1){ // LP ports - minimum value of port inventory
			minS = portInventory[i][t];
			for(t1=t;t1<inst.t;t1++){
				if(portInventory[i][t1] < minS){
					minS = portInventory[i][t1];
					minTime = t1;
				}				
			}
			//~ minS = *min_element(begin(portInventory)+t,end(portInventory));
			//~ //~ cout<< "Min inventoryCopy from " << t << " = " << minS << endl;
		}else{ //DP maximum value of port inventory
			minS = inst.sMax_jt[i][0] - portInventory[i][t];
			for(t1=t;t1<inst.t;t1++){
				if(inst.sMax_jt[i][0] - portInventory[i][t1] < minS){
					minS = inst.sMax_jt[i][0] - portInventory[i][t1];
					minTime = t1;
				}
			}
			//~ minS = *max_element(begin(portInventory)+t,end(portInventory));
			//~ //~ cout<< "Max inventoryCopy from " << t << " = " << minS << endl;
		}			
		maxF_it[i][t] = max(0.0, (double)minS);
		//DEPRECATED
		//~ if(minTime == t){//Cannot remove alphas/betas (only of the current time)
			//~ maxF_it[i][t] = max(0.0, (double)minS + portAlpha[i][t] + portBeta[i][t]);
		//~ }else{ //minTime > t 	 
			//~ maxF_it[i][t] = max(0.0, (double)minS + getSumAlphaBetaUntilNextOperation(i,t));
		//~ }
		maxF_itCopy[i][t] = maxF_it[i][t];
		////~ cout<< "Max F_it " << t << " (" << minTime << ")" << maxF_it[i][t] << " - Port inv " << portInventory[i][t] << endl;
	}
}
/*
 * Used for uptading the vector maxF_itCP using the information ath portInventoryCP */
void Solution::updateMaxFCopy(Instance inst, const unsigned int& i, const vector<float>& portInventoryCP, vector<float>& maxF_itCP){
	unsigned int t,t1;
	for(t=0;t<inst.t;t++){
		float minS; //Minimum inventory at LP or minimum avaiable space in DP
		unsigned int minTime; //Time in which minS is registred
		if(inst.delta[i] == 1){ // LP ports - minimum value of port inventory
			minS = portInventoryCP[t];
			for(t1=t;t1<inst.t;t1++){
				if(portInventoryCP[t1] < minS){
					minS = portInventoryCP[t1];
					minTime = t1;
				}				
			}
			//~ minS = *min_element(begin(portInventoryCP)+t,end(portInventoryCP));
			//~ //~ cout<< "Min inventoryCopy from " << t << " = " << minS << endl;
		}else{ //DP maximum value of port inventory
			minS = inst.sMax_jt[i][0] - portInventoryCP[t];
			for(t1=t;t1<inst.t;t1++){
				if(inst.sMax_jt[i][0] - portInventoryCP[t1] < minS){
					minS = inst.sMax_jt[i][0] - portInventoryCP[t1];
					minTime = t1;
				}
			}
			//~ minS = *max_element(begin(portInventoryCP)+t,end(portInventoryCP));
			//~ //~ cout<< "Max inventoryCopy from " << t << " = " << minS << endl;
		}
		maxF_itCP[t] = max(0.0, (double)minS);
		//~ if(minTime == t){//Cannot remove alphas/betas (only of the current time)
			//~ maxF_itCP[t] = max(0.0, (double)minS + portAlpha[i][t] + portBeta[i][t]);
		//~ }else{ //minTime > t 	 
			//~ maxF_itCP[t] = max(0.0, (double)minS + getSumAlphaBetaUntilNextOperation(i,t));
		//~ }
		////~ cout<< "Max F_itCopy " << t << " (" << minTime << ")" << maxF_it[i][t] << " - Port inv " << portInventoryCP[t] << endl;
	}
}
/*
 * Given a port i and time t, remove possible alphas/beta used after t, until the next operation or end of horizon.
 * Removing is only possible if the amount operated f can cover the inventory breach
 * Index v is necessary for adding to the action list of vessel
 */ 
void Solution::updateAuxiliaryVariables(Instance inst,const bool& isStored, const unsigned int& v,const unsigned int& i, vector<pair<int, float>>& operations, const vector<float>& portInventoryCP){
	if(cummulativeAlpha[i] <= 0 && cummulativeBeta[i] <= 0){ //Does not need update - current solution does not have auxiliary vars
		return;
	}
	int t = operations.front().first; //Time of the first operation
	float f = operations.front().second; //Amount operated at first operation
	operations.erase(operations.begin());
	int t1;
	int maxTime;
	if(!portOperationTime[i].empty()){
		maxTime = portOperationTime[i].top().second; //inst.t; //getPortNextOperation(i,t).second; //TODO get the time of last use of alpha/beta
	}else{
		maxTime = inst.t; //No operation after t (but alpha/beta are still  possible)
	}	
	float totalRemoved = 0.0; // For accounting previous removed alpha/beta 
	float totalAlphaRemoved = 0.0;
	float totalBetaRemoved = 0.0;
	vector<float> removedBeta(inst.t);
	vector<float> removedAlpha(inst.t);
	//Reset in time t-1 until no alpha and beta
	if(t>0){
		for(t1=t-1; t1>=0; t1--){
			if(portAlpha[i][t1] > 0 || portBeta[i][t1] > 0){
				//~ cout<< "\nPortInventoryCP[" << t1 << "] = " << portInventoryCP[t1] << " Alpha " << portAlpha[i][t1] << " Beta " << portBeta[i][t1] << endl;
				if(inst.delta[i] == 1){ //LPorts
					if(portInventoryCP[t1] + portAlpha[i][t1] + portBeta[i][t1] <= inst.sMax_jt[i][0]){//Remove all
						if(isStored){
							//~ cout<< "Removing a " << portAlpha[i][t1] << " and b " << portBeta[i][t1] << endl;
							if(portAlpha[i][t1] > 0.0){
								vesselOperationOrders[v].push(make_tuple(t1,ADDALPHA,i,-portAlpha[i][t1]));
								removedAlpha[t1] += portAlpha[i][t1];
							}
							if(portBeta[i][t1] > 0.0){
								vesselOperationOrders[v].push(make_tuple(t1,ADDBETA,i,-portBeta[i][t1]));
								removedBeta[t1] += portBeta[i][t1];
							}							
							totalRemoved += portAlpha[i][t1] + portBeta[i][t1];
							totalAlphaRemoved += portAlpha[i][t1];
							totalBetaRemoved += portBeta[i][t1];
						}	
					}else if(portInventoryCP[t1] < inst.sMax_jt[i][0]){ //There is a difference that can be removed
						float difA,difB,rB=0.0,rA=0.0;
						if(portBeta[i][t1] > 0.0){
							difB = portInventoryCP[t1] + portAlpha[i][t1] + portBeta[i][t1] - inst.sMax_jt[i][0];
							rB = min(portBeta[i][t1]-difB,portBeta[i][t1]);
							if(isStored && rB > 0.0){
								//~ cout<< "Removing b " << rB << endl;
								vesselOperationOrders[v].push(make_tuple(t1,ADDBETA,i,-rB));
								totalRemoved += rB;
								totalBetaRemoved += rB;
								removedBeta[t1] += rB;
							}							
						}
						if(portAlpha[i][t1] > 0.0){ //If alpha is positive
							difA = portInventoryCP[t1] + portAlpha[i][t1] + portBeta[i][t1] - inst.sMax_jt[i][0] - rB;
							rA = min(portAlpha[i][t1]-difA,portAlpha[i][t1]);
							if(isStored && rA > 0.0){
								//~ cout<< "Removing a " << rA << endl;
								vesselOperationOrders[v].push(make_tuple(t1,ADDALPHA,i,-rA));
								totalRemoved += rA;
								totalAlphaRemoved += rA;
								removedAlpha[t1] += rA;
							}
						}
					}else{//All beta and alpha are needed
						break;
					}
				}else{ // DPorts
					if(portInventoryCP[t1] - portAlpha[i][t1] - portBeta[i][t1] >= inst.sMin_jt[i][0]){//Remove all
						if(isStored){
							//~ cout<< "Removing a " << portAlpha[i][t1] << " and b " << portBeta[i][t1] << endl;
							if(portAlpha[i][t1] > 0.0){
								vesselOperationOrders[v].push(make_tuple(t1,ADDALPHA,i,-portAlpha[i][t1]));
								removedAlpha[t1] += portAlpha[i][t1];
							}
							if(portBeta[i][t1] > 0.0){
								vesselOperationOrders[v].push(make_tuple(t1,ADDBETA,i,-portBeta[i][t1]));
								removedBeta[t1] += portBeta[i][t1];
							}
							totalRemoved += portAlpha[i][t1] + portBeta[i][t1];
							totalAlphaRemoved += portAlpha[i][t1];
							totalBetaRemoved += portBeta[i][t1];
						}	
					}else if(portInventoryCP[t1] > inst.sMin_jt[i][0]){ //There is a difference that can be removed
						float difA,difB,rB=0.0,rA=0.0;
						if(portBeta[i][t1] > 0.0){
							difB = abs(portInventoryCP[t1] - portAlpha[i][t1] - portBeta[i][t1] + inst.sMin_jt[i][0]);
							rB = min(portBeta[i][t1]-difB,portBeta[i][t1]);
							if(isStored && rB > 0.0){
								//~ cout<< "Removing b " << rB << endl;
								vesselOperationOrders[v].push(make_tuple(t1,ADDBETA,i,-rB));
								totalRemoved += rB;
								totalBetaRemoved += rB;
								removedBeta[t1] += rB;
							}							
						}
						if(portAlpha[i][t1] > 0.0){ //If alpha is positive and some value can be removed
							difA = abs(portInventoryCP[t1] - portAlpha[i][t1] - portBeta[i][t1] + inst.sMin_jt[i][0] + rB);
							rA = min(portAlpha[i][t1]-difA,portAlpha[i][t1]);
							if(isStored && rA > 0.0){
								//~ cout<< "Removing a " << rA << endl;
								vesselOperationOrders[v].push(make_tuple(t1,ADDALPHA,i,-rA));
								totalRemoved += rA;
								totalAlphaRemoved += rA;
								removedAlpha[t1] += rA;
							}
						}
					}else{ //All beta and alpha are needed
						break;
					}
				}											
			}else{ //No using of auxiliary vars at the time, stops
				break;
			}	
		}
	}
	//Times after t (consider the operated value f and totalRemoved)
	for(t1=t; t1<maxTime; t1++){
		//Check if must consider the next(s) operation(s) 
		if(!operations.empty()){
			if(operations.front().first >= t1){
				f += operations.front().second;
				operations.erase(operations.begin());
			}
		}
		if(portAlpha[i][t1] > 0 || portBeta[i][t1] >0){
			//~ cout<< "PortInventoryCP[" << t1 << "] = " << portInventoryCP[t1] << " Alpha " << portAlpha[i][t1] << " Beta " << portBeta[i][t1] << "f = " << f << endl;
			if(inst.delta[i] == 1){ //LPorts
				if(portInventoryCP[t1] + portAlpha[i][t1] + portBeta[i][t1] + totalRemoved - f < inst.sMax_jt[i][0]){//Remove all
					if(isStored){
						//~ cout<< "Removing a " << portAlpha[i][t1] << " and b " << portBeta[i][t1]<< endl;
						if(portAlpha[i][t1] > 0.0){
							vesselOperationOrders[v].push(make_tuple(t1,ADDALPHA,i,-portAlpha[i][t1]));
							removedAlpha[t1] += portAlpha[i][t1];
						}
						if(portBeta[i][t1] > 0.0){
							vesselOperationOrders[v].push(make_tuple(t1,ADDBETA,i,-portBeta[i][t1]));
							removedBeta[t1] += portBeta[i][t1];
						}
						totalRemoved += portAlpha[i][t1] + portBeta[i][t1];
						totalAlphaRemoved += portAlpha[i][t1];
						totalBetaRemoved += portBeta[i][t1];
					}	
				}else if(portInventoryCP[t1] + portAlpha[i][t1] + portBeta[i][t1] + totalRemoved - f> inst.sMax_jt[i][0]){ //There is a difference that can be removed
					float difA,difB,rB=0.0,rA=0.0;
					if(portBeta[i][t1] > 0.0){
						difB = portInventoryCP[t1] + portAlpha[i][t1] + portBeta[i][t1] - inst.sMax_jt[i][0] + totalRemoved - f;
						rB = min(portBeta[i][t1]-difB,portBeta[i][t1]);
						if(isStored && rB > 0.0){
							//~ cout<< "Removing b " << rB << endl;
							vesselOperationOrders[v].push(make_tuple(t1,ADDBETA,i,-rB));
							totalRemoved += rB;
							totalBetaRemoved += rB;
							removedBeta[t1] += rB;
						}							
					}
					if(portAlpha[i][t1] > 0.0){ //If alpha is positive
						difA = portInventoryCP[t1] + portAlpha[i][t1] + portBeta[i][t1] - inst.sMax_jt[i][0] + totalRemoved - f - rB;
						rA = min(portAlpha[i][t1]-difA,portAlpha[i][t1]);
						if(isStored && rA > 0.0){
							//~ cout<< "Removing a " << rA << endl;
							vesselOperationOrders[v].push(make_tuple(t1,ADDALPHA,i,-rA));
							totalRemoved += rA;
							totalAlphaRemoved += rA;
							removedAlpha[t1] += rA;
						}
					}
				}//Otherwise - sum equals sMax - Cannot remove anything
			}else{ // DPorts
				if(portInventoryCP[t1] - portAlpha[i][t1] - portBeta[i][t1] - totalRemoved + f > inst.sMin_jt[i][0]){//Remove all
					if(isStored){
						//~ cout<< "Removing a " << portAlpha[i][t1] << " and b " << portBeta[i][t1]<< endl;
						if(portAlpha[i][t1] > 0.0){
							vesselOperationOrders[v].push(make_tuple(t1,ADDALPHA,i,-portAlpha[i][t1]));
							removedAlpha[t1] += portAlpha[i][t1];
						}
						if(portBeta[i][t1] > 0.0){
							vesselOperationOrders[v].push(make_tuple(t1,ADDBETA,i,-portBeta[i][t1]));
							removedBeta[t1] += portBeta[i][t1];
						}
						totalRemoved += portAlpha[i][t1] + portBeta[i][t1];
						totalAlphaRemoved += portAlpha[i][t1];
						totalBetaRemoved += portBeta[i][t1];						
					}	
				}else if(portInventoryCP[t1] - portAlpha[i][t1] - portBeta[i][t1] - totalRemoved + f < inst.sMin_jt[i][0]){ //There is a difference that can be removed
					float difA,difB,rB=0.0,rA=0.0;
					if(portBeta[i][t1] > 0.0){
						difB = abs(portInventoryCP[t1] - portAlpha[i][t1] - portBeta[i][t1]  - totalRemoved + f + inst.sMin_jt[i][0]);
						rB = min(portBeta[i][t1]-difB,portBeta[i][t1]);
						if(isStored && rB > 0.0){
							//~ cout<< "Removing b " << rB << endl;
							vesselOperationOrders[v].push(make_tuple(t1,ADDBETA,i,-rB));
							totalRemoved += rB;
							totalBetaRemoved += rB;
							removedBeta[t1] += rB;
						}
					}
					if(portAlpha[i][t1] > 0.0){ 
						difA = abs(portInventoryCP[t1] - portAlpha[i][t1] - portBeta[i][t1] + inst.sMin_jt[i][0] - totalRemoved + f + rB);
						rA = min(portAlpha[i][t1]-difA,portAlpha[i][t1]);
						if(isStored && rA > 0.0){
							//~ cout<< "Removing a " << rA << endl;
							vesselOperationOrders[v].push(make_tuple(t1,ADDALPHA,i,-rA));
							totalRemoved += rA;
							totalAlphaRemoved += rA;
							removedAlpha[t1] += rA;
						}
					}
				}
			}			
		}
	}
	//~ cout<< "Total alpha/beta removed after (" << i << "," << t << ") " << totalRemoved << endl;
	//Replace beta by alpha if it is possible	
	if(cummulativeAlpha[i] - totalAlphaRemoved < inst.alp_max_j[i] && 
	  cummulativeBeta[i] - totalBetaRemoved > 0.0){		
		float avaliableAlpha = inst.alp_max_j[i] - (cummulativeAlpha[i] - totalAlphaRemoved);
		//~ cout<< "Av Alpha " << avaliableAlpha;
		for(t1=0;t1<maxTime;t1++){
			if(avaliableAlpha == 0.0){
				break;
			}
			float beta = portBeta[i][t1] - removedBeta[t1];
			//~ cout<< " beta[" << t1 << "] " << beta << " " << endl;
			//Verify only if still have beta and some alpha that can be used at the time
			if(beta > 0.0 && avaliableAlpha > 0.0 && 
			    inst.alp_max_jt[i][0]-portAlpha[i][t1]+removedAlpha[t1] > 0.0){ 
				float removeBeta = min(beta, min(avaliableAlpha,
					(float)inst.alp_max_jt[i][0]-portAlpha[i][t1]+removedAlpha[t1]));
				if(isStored && removeBeta > 0.0){
					//~ cout<< "Time " << t1 << ": Replacing " << beta << " beta by " << removeBeta << " alpha \n" << endl;
					vesselOperationOrders[v].push(make_tuple(t1,ADDBETA,i,-removeBeta));
					vesselOperationOrders[v].push(make_tuple(t1,ADDALPHA,i,removeBeta));
					avaliableAlpha -= removeBeta;
				}				
			}
		}
		//~ cout<< endl;	
	}
}
/*
 * Merge the content of vector actionP1 to the priorityQueue vesselOperationOrders
 */
void Solution::mergeActions(priority_queue< tuple<unsigned int,unsigned int,unsigned int,float>, 
                vector<tuple<unsigned int,unsigned int,unsigned int,float>>, compare4TupleMinorFirst>& vesselOperationOrders, 
                vector<tuple<unsigned int,unsigned int,unsigned int,float>>& actionsP1);
	while(!actionsP1.empty()){
		vesselOperationOrders[v].push(actionsP1.back());
		actionsP1.pop_back();
	}
}
