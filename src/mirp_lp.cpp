/* Fixed charge network flow for a single-product MIRP: Version for using sub-instances
 * Consider that vessels can wait after operated in a port
 * Index i and j of ports J starts in 1
 * 0 is the source node, and J+1 is the sink node
 * Index t for time period start in 1
 * t=0 is the time in which vessesl starts its voyage from source node
 * Index v for vessels starts in 0
 *  
 */ 
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <ilcplex/ilocplex.h>
#include "../include/util.h"
#include "../include/model.h"
#include <assert.h>
//~ #define NSpotMarket
//~ #define NTravelAtCapacity
//~ #define NTravelEmpty
//~ #define NBerthLimit
//~ #define NBetas
//~ #define NThetas
#define NBranching
#define NRelaxation
#define NKnapsackInequalities
#define PENALIZATION 1000
#define WaitAfterOperate
/* 
 * TODOs: 
 * - Check constraints/valid inequalities where all variables are removed
 */


ILOSTLBEGIN

typedef IloArray<IloNumVarArray> NumVarMatrix;
typedef IloArray<IloIntVarArray> IntVarMatrix;
typedef IloArray<IloNumArray> NumNumMatrix;

using namespace std;
using mirp::Model;

void Model::buildFixAndRelaxModel(IloEnv& env, Instance inst, const double& nIntervals, const int& endBlock, 
		const bool& validIneq, const bool& addConstr, const bool& tightenInvConstr, const bool& proportionalAlpha,
		const bool& reduceArcs, const bool& tightenFlow){	
	int i, j,t,v,a;
	int timePerIntervalId = inst.t/nIntervals; //Number of time periods in each interval
	int J = inst.J;
	int T = inst.t+1;
	double intPart;
	int tOEB = inst.t - (timePerIntervalId*max(0.0,endBlock-modf(nIntervals, &intPart))) ; //Time periods out of End Block (index tOEB+1 is the first in the end block)
	int V = inst.speed.getSize(); //# of vessels
    int N = J + 2;
	//~ //~ cout "Building model...\n Integer Block: [0," << timePerIntervalId << "]\n Relaxed block: [" << timePerIntervalId+1 << "," << tOEB << "]\n End block: [" << tOEB+1 << "," << T-1 << "]\n";
	
    ///Variables, converters and arrays to storage the values
    #ifndef NAlpha
    alpha = NumVarMatrix(env, N);    
    #endif
	#ifndef NBetas
	beta = NumVarMatrix(env, N);
	#endif
    #ifndef NThetas
    theta = NumVarMatrix(env, N);
	#endif
    
    sP = NumVarMatrix(env,N);
	f = IloArray<NumVarMatrix> (env, V);
	fX = IloArray<IloArray<NumVarMatrix> >(env, V);	
	
	fO = IloArray<NumVarMatrix> (env, V);
	#ifndef WaitAfterOperate    
    fOB = IloArray<NumVarMatrix> (env, V);
	#endif
	fW = IloArray<NumVarMatrix> (env, V);

	hasArc = IloArray<IloArray<IloArray<IloIntArray> > >(env, V);
	arcCost = IloArray<IloArray<IloArray<IloNumArray> > >(env, V);
	hasEnteringArc1st = IloArray<IloArray<IloIntArray> >(env, V);
	
	x = IloArray<IloArray<IloArray<IloBoolVarArray> > >(env, V);    
	o = IloArray<IloArray<IloBoolVarArray> >(env,V);
    xValue = IloArray<IloArray<IloArray<IloNumArray> > >(env,V);
    oValue = IloArray<IloArray<IloNumArray> >(env,V);
    
	#ifndef WaitAfterOperate
    oA = IloArray<IloArray<IloBoolVarArray> >(env,V);
	oB = IloArray<IloArray<IloBoolVarArray> >(env,V);
    oAValue = IloArray<IloArray<IloNumArray> >(env,V);
    oBValue = IloArray<IloArray<IloNumArray> >(env,V);
	#endif
	w = IloArray<IloArray<IloBoolVarArray> >(env,V);
    wValue = IloArray<IloArray<IloNumArray> >(env,V);
	
	#ifdef WaitAfterOperate    
	fWB = IloArray<NumVarMatrix> (env, V);
	wB = IloArray<IloArray<IloBoolVarArray> >(env,V);
    wBValue = IloArray<IloArray<IloNumArray> >(env,V);
	#endif
	IloExpr expr_opd(env);
    if(addConstr){
		operateAndDepart = IloArray<IloArray<IloRangeArray> >(env,V);
	}
    
    #ifndef NWWCCReformulation
	wwcc_relaxation = IloArray<IloRangeArray>(env, N-1);
    #endif
    
    #ifndef NBranching
    //~ sumX = IntVarMatrix(env, V);
    //~ priorityX = IloArray<IloRangeArray>(env, V);
    sumOA = IntVarMatrix(env, V) ;
    priorityOA = IloArray<IloRangeArray>(env, V);
    #endif

    for(v=0;v<V;v++){
		f[v] = NumVarMatrix(env,N);
		fX[v] = IloArray<NumVarMatrix>(env,N);
		fO[v] = NumVarMatrix(env,N);
		#ifndef WaitAfterOperate
		fOB[v] = NumVarMatrix(env,N);
		#endif
		fW[v] = NumVarMatrix(env,N);
		
		o[v] = IloArray<IloBoolVarArray>(env,N);
        oValue[v] = IloArray<IloNumArray>(env,N);
		#ifndef WaitAfterOperate
		oA[v] = IloArray<IloBoolVarArray>(env,N);
		oB[v] = IloArray<IloBoolVarArray>(env,N);
        oAValue[v] = IloArray<IloNumArray>(env,N);
        oBValue[v] = IloArray<IloNumArray>(env,N);
		#endif
		w[v] = IloArray<IloBoolVarArray>(env,N);
		x[v] = IloArray<IloArray<IloBoolVarArray> >(env, N);        

        wValue[v] = IloArray<IloNumArray>(env,N);
        xValue[v] = IloArray<IloArray<IloNumArray> >(env,N);
					
		hasArc[v] = IloArray<IloArray<IloIntArray> >(env, N);
		arcCost[v] = IloArray<IloArray<IloNumArray> >(env, N);
		hasEnteringArc1st[v] = IloArray<IloIntArray>(env, N);
		
		#ifdef WaitAfterOperate
		fWB[v] = NumVarMatrix(env,N);
		wB[v] = IloArray<IloBoolVarArray>(env,N);
        wBValue[v] = IloArray<IloNumArray>(env,N);
		#endif
        
        if(addConstr){
			operateAndDepart[v] = IloArray<IloRangeArray> (env,N);
        }
        
        #ifndef NBranching
        //~ sumX[v] = IloIntVarArray(env, J+1,0,IloInfinity);
        //~ priorityX[v] = IloRangeArray(env, J+1);
        sumOA[v] = IloIntVarArray(env, J+1, 0,IloInfinity);
        priorityOA[v] = IloRangeArray(env, J+1);
        #endif

		for(j=0;j<N;j++){
			x[v][j] = IloArray<IloBoolVarArray>(env,N);

            xValue[v][j] = IloArray<IloNumArray>(env,N);
			hasArc[v][j] = IloArray<IloIntArray>(env,N);
			arcCost[v][j] = IloArray<IloNumArray>(env,N);
			fX[v][j] = IloArray<IloNumVarArray>(env, N);
            
            for(i=0;i<N;i++){ 
				x[v][j][i] = IloBoolVarArray(env,T);

                xValue[v][j][i] = IloNumArray(env, T);
				hasArc[v][j][i] = IloIntArray(env,T);
				arcCost[v][j][i] = IloNumArray(env,T);
				fX[v][j][i] = IloNumVarArray(env, T);                
				for(t=0;t<T;t++){
					stringstream ss;
					ss << "x_"<< v <<","<< j <<","<< i <<","<<t;
					x[v][j][i][t].setName(ss.str().c_str());

					ss.str(string());
					ss << "fX_"<<v<<","<<j<<","<<i<<","<<t;
					fX[v][j][i][t] = IloNumVar(env, 0, inst.q_v[v], ss.str().c_str());                    
				}
			}

			if(j>0 && j<=J){ //Only considering Ports (v,j)
				f[v][j] = IloNumVarArray(env, T);
				fO[v][j] = IloNumVarArray(env, T);
				#ifndef WaitAfterOperate
				fOB[v][j] = IloNumVarArray(env, T);
				#endif
				fW[v][j] = IloNumVarArray(env, T);
				
				o[v][j] = IloBoolVarArray(env, T);
                oValue[v][j] = IloNumArray(env,T);
				#ifndef WaitAfterOperate
				oA[v][j] = IloBoolVarArray(env, T);
				oB[v][j] = IloBoolVarArray(env, T);
                oAValue[v][j] = IloNumArray(env,T);
                oBValue[v][j] = IloNumArray(env,T);
				#endif
				w[v][j] = IloBoolVarArray(env, T);
				wValue[v][j] = IloNumArray(env,T);
				hasEnteringArc1st[v][j] = IloIntArray(env,T);
				#ifdef WaitAfterOperate
				wB[v][j] = IloBoolVarArray(env, T);
                wBValue[v][j] = IloNumArray(env,T);
				fWB[v][j] = IloNumVarArray(env, T);
				#endif
                
                if(addConstr){
					operateAndDepart[v][j] = IloRangeArray (env,T);
                }
				
				for(t=1;t<T;t++){
					stringstream ss;
					ss << "f_(" << j << "," << t << ")," << v;
					int maxAmountOperation = min(inst.q_v[v], inst.f_max_jt[j-1][t-1]);
					f[v][j][t] = IloNumVar(env, 0, maxAmountOperation, ss.str().c_str());
					ss.str(string());
					ss << "fO_(" << j << "," << t << ")," << v;
					fO[v][j][t] = IloNumVar(env, 0, IloInfinity, ss.str().c_str());
					//~ fO[v][j][t] = IloNumVar(env, 0, inst.q_v[v], ss.str().c_str());
					ss.str(string());
					ss << "z_(" << j << "," << t << ")," << v;
					o[v][j][t].setName(ss.str().c_str());
                                  
                    
					#ifndef WaitAfterOperate
					ss.str(string());
					ss << "oA_(" << j << "," << t << ")," << v;
					oA[v][j][t].setName(ss.str().c_str());
					#endif
					if(t<T-1){ //Variables that haven't last time index
						ss.str(string());
						ss << "fW_(" << j << "," << t << ")," << v;
						fW[v][j][t] = IloNumVar(env, 0, IloInfinity, ss.str().c_str());
						//~ fW[v][j][t] = IloNumVar(env, 0, inst.q_v[v], ss.str().c_str());
                        
                        ss.str(string());
						ss << "w_(" << j << "," << t << ")," << v;
						w[v][j][t].setName(ss.str().c_str());
                        						
						#ifndef WaitAfterOperate
                        ss.str(string());
						ss << "fOB_(" << j << "," << t << ")," << v;
						fOB[v][j][t] = IloNumVar(env, 0, IloInfinity, ss.str().c_str());
						//~ fOB[v][j][t] = IloNumVar(env, 0, inst.q_v[v], ss.str().c_str());
                        
						ss.str(string());
						ss << "oB_(" << j << "," << t << ")," << v;
						oB[v][j][t].setName(ss.str().c_str());
						#endif
						
						#ifdef WaitAfterOperate
                        ss.str(string());
                        ss << "wB_(" << j << "," << t << ")," << v;
                        wB[v][j][t].setName(ss.str().c_str());
                        
                        ss.str(string());
                        ss << "fWB_(" << j << "," << t << ")," << v;
                        fWB[v][j][t] = IloNumVar(env, 0, IloInfinity, ss.str().c_str());
                        //~ fWB[v][j][t] = IloNumVar(env, 0, inst.q_v[v], ss.str().c_str());
                        #endif
					}
				}
			}
		}
	}
    //Port time (i,t)
	for(i=1;i<N-1;i++){
		#ifndef NAlpha
		alpha[i] = IloNumVarArray(env,T);
		#endif
		sP[i] = IloNumVarArray(env, T);	
        #ifndef NBetas
        beta[i] = IloNumVarArray(env,T);
        #endif
        #ifndef NThetas
        theta[i] = IloNumVarArray(env,T);
        #endif
        #ifndef NWWCCReformulation
        //Common for loading and discharging ports        
        int num_combinations = (t-1)*t/2; //Number of combinations for k <= j, k,j \in T
        wwcc_relaxation[i] = IloRangeArray(env, num_combinations);        
        #endif        
		for(t=0;t<T;t++){
			stringstream ss;
			if (t == 0){
				ss << "sP_(" << i << ",0)";
				sP[i][t] = IloNumVar(env,inst.s_j0[i-1], inst.s_j0[i-1], ss.str().c_str()); //Initial inventory                
			}else{
				ss.str(string());
				ss << "sP_(" << i << "," << t << ")";
				sP[i][t] = IloNumVar(env, inst.sMin_jt[i-1][0], inst.sMax_jt[i-1][0], ss.str().c_str()); //As the port capacity is fixed, always used the data from index 0
				//Unlimited port capacities in on leg
                //~ if(inst.typePort[i-1]==0)
                    //~ sP[i][t] = IloNumVar(env, -IloInfinity, inst.sMax_jt[i-1][0], ss.str().c_str());
                //~ else
                    //~ sP[i][t] = IloNumVar(env, inst.sMin_jt[i-1][0], IloInfinity, ss.str().c_str());
                //Unlimited port capacity at all
                //~ sP[i][t] = IloNumVar(env, -IloInfinity, IloInfinity, ss.str().c_str());
                
				#ifndef NAlpha
				ss.str(string());
				ss << "alpha_(" << i << "," << t << ")";
				alpha[i][t] = IloNumVar(env, 0, inst.alp_max_jt[i-1][t-1], ss.str().c_str());
				#endif
                #ifndef NBetas
                ss.str(string());
				ss << "beta_(" << i << "," << t << ")";
				//~ beta[i][t] = IloNumVar(env, 0, inst.d_jt[i-1][t-1], ss.str().c_str());
				beta[i][t] = IloNumVar(env, 0, IloInfinity, ss.str().c_str());
                #endif 
                #ifndef NThetas
                ss.str(string());
				ss << "theta_(" << i << "," << t << ")";
				//~ theta[i][t] = IloNumVar(env, 0, inst.d_jt[i-1][t-1], ss.str().c_str());
				theta[i][t] = IloNumVar(env, 0, IloInfinity, ss.str().c_str());
                #endif                
			}
		}
	}
	
	///Objective function
	IloExpr expr(env);
	IloExpr expr1(env);	
	for(v=0;v<V;v++){
		///Builds the arcs between nodes. Only arcs in the integer and relaxed intervals are added							//2nd term
		//Source arcs -- all are included, assuming that the first time available is in the integer or relaxed block
		j = inst.initialPort[v]+1;
		hasArc[v][0][j][0] = 1;			//Time between source node and initial ports is equal to first time available
		arcCost[v][0][j][0] = inst.portFee[j-1];
        expr1 += arcCost[v][0][j][0]*x[v][0][j][0];
		hasEnteringArc1st[v][j][inst.firstTimeAv[v]+1] = 1;
		x[v][0][j][0].setBounds(1,1); // Fixing initial port position        
		//Travel and sink arcs
		i = inst.initialPort[v]+1;					//Arcs from initial port i
		for (t=inst.firstTimeAv[v]+1;t<T;t++){		//and initial time available t
			for(j=1;j<N-1;j++){						//Not necessary to account sink node
				bool addArcIJ = false;
				if(i != j && 
					((inst.delta[i-1] == inst.delta[j-1] && //if i and j are of the same type 
                    inst.f_min_jt[i-1][t-1] + inst.f_min_jt[j-1][t-1] <= inst.q_v[v]) || // and min operation sum is lesser than capacity op vessel
					inst.delta[i-1] != inst.delta[j-1])){ //or ports are of different types
						addArcIJ = true;
					}
				if(reduceArcs && inst.delta[i-1] == inst.delta[j-1] && inst.idRegion[i-1] != inst.idRegion[j-1]){
					addArcIJ = false;
				}
				int t2;				
				double arc_cost;
				if(addArcIJ){ ///Builds arcs between (i,t) to (j,t2) - implicit hasEnteringArc (j,t2). Builds a mirror (j,t)->(i,t2) when possible
					t2 = t + inst.travelTime[v][i-1][j-1]; 
					if (t2<T){ 	//If exists time to reach port j 
						if(hasArc[v][i][j][t] != 1){ //If not added yet
							if (inst.typePort[i-1]==1 && inst.typePort[j-1]==0){ //If port i is consuming and j is a producer port
								arc_cost = inst.costKm[v]*inst.distanceMatrix[i-1][j-1]*(1-inst.trav_empt[v]) + inst.portFee[j-1];
							}else{
								arc_cost = inst.costKm[v]*inst.distanceMatrix[i-1][j-1] + inst.portFee[j-1];
							}
							hasArc[v][i][j][t] = 1;	
							arcCost[v][i][j][t] = arc_cost;                        
							if(t2 <= tOEB)      //If arriving node is in the model                            
								expr1 += arcCost[v][i][j][t]*x[v][i][j][t];


							//Check if not set yet
							if(hasEnteringArc1st[v][j][t2] != 1){
								hasEnteringArc1st[v][j][t2] = 1;
								
							}						
						}						
						//when time t reach a time that can be built a mirror between j and i
						if(hasArc[v][j][i][t] != 1){
							if (t >= inst.firstTimeAv[v]+1 + inst.travelTime[v][i-1][j-1]){ 
								if (inst.typePort[j-1]==1 && inst.typePort[i-1]==0){ 		  //If port j is consuming and i is a producer port
									arc_cost = inst.costKm[v]*inst.distanceMatrix[j-1][i-1]*(1-inst.trav_empt[v]) + inst.portFee[i-1];		
								}else{
									arc_cost = inst.costKm[v]*inst.distanceMatrix[j-1][i-1] + inst.portFee[i-1];
								}
								hasArc[v][j][i][t] = 1;
								arcCost[v][j][i][t] = arc_cost;
								
								if(t2 <= tOEB)      //If arriving node is in the model
									expr1 += arcCost[v][j][i][t]*x[v][j][i][t];
								//Relaxing 
								#ifdef NRelaxation
								if(t2 > timePerIntervalId){ 
									if(hasEnteringArc1st[v][i][t2] != 1){
										hasEnteringArc1st[v][i][t2] = 1; 										
									}
								}
								#endif
							}
						}
					}
				}		
				///For cases where i and j are not compatible, but j is can be compatible with other j2
				///Builds waitJ, sinkJ, (j,t2)->(j2,t3), enteringJ2, 
				if(addArcIJ || hasEnteringArc1st[v][j][t] == 1){
					if(!addArcIJ){ //When i and j are not compatible
						t2 = t;
					}
					if(t2<T){					
						if (t2+1<T-1){ 			//Waiting arc of j
							if(hasEnteringArc1st[v][j][t2+1] != 1){
								hasEnteringArc1st[v][j][t2+1] = 1;
							}
						}
						//Sink arc from port j 
						if (hasArc[v][j][N-1][t2] != 1){
							hasArc[v][j][N-1][t2] = 1;
							arcCost[v][j][N-1][t2] = -(T-t2-1)*inst.perPeriodRewardForFinishingEarly;
							if(t2 <= tOEB)      //If arriving node is in the model
								expr1 += arcCost[v][j][N-1][t2]*x[v][j][N-1][t2];							
						}						
						
						//Create arc from j,t2 to others ports (j2) in time t3
						for(int j2=1;j2<=J;j2++){
							bool addArcJJ2 = false;
							if(j2 != i && j2 != j && 
								((inst.delta[j-1] == inst.delta[j2-1] && //if j and j2 are of the same type 
								inst.f_min_jt[j-1][0] + inst.f_min_jt[j2-1][0] <= inst.q_v[v]) || // and min operation sum is lesser than capacity op vessel
								inst.delta[j-1] != inst.delta[j2-1])){ //or ports are of different types
									addArcJJ2 = true;
								}
							if(reduceArcs && inst.delta[j-1] == inst.delta[j2-1] && inst.idRegion[j-1] != inst.idRegion[j2-1]){ //If they are of the same type but from different regions, no add the arc
								addArcJJ2 = false;
							}			
							
							if(addArcJJ2){
								int t3 = t2+inst.travelTime[v][j-1][j2-1];  
								if(hasArc[v][j][j2][t2] != 1){
									if(t3<T){
										if (inst.typePort[j-1]==1 && inst.typePort[j2-1]==0){
											arc_cost = inst.costKm[v]*inst.distanceMatrix[j-1][j2-1]*(1-inst.trav_empt[v]) + inst.portFee[j2-1];		
										}else{
											arc_cost = inst.costKm[v]*inst.distanceMatrix[j-1][j2-1] + inst.portFee[j2-1];
										}
										hasArc[v][j][j2][t2] = 1;
										arcCost[v][j][j2][t2] = arc_cost;
										if(t3 <= tOEB)      //Ir arriving node is in the model
											expr1 += arcCost[v][j][j2][t2]*x[v][j][j2][t2];

										if(hasEnteringArc1st[v][j2][t3] != 1){
											hasEnteringArc1st[v][j2][t3] = 1;																				
										}
									}
								}
							}
						}
					}
				}
			}
			if(t+1<T){ //Waitinng i
				if(hasEnteringArc1st[v][i][t+1] != 1){
					hasEnteringArc1st[v][i][t+1] = 1;
				}
			}
			//Sink arc from port i
			if(hasArc[v][i][N-1][t] != 1){
				hasArc[v][i][N-1][t] = 1;
				arcCost[v][i][N-1][t] = -(T-t-1)*inst.perPeriodRewardForFinishingEarly;
				if(t <= tOEB)      //If arriving node is in the model
					expr1 += arcCost[v][i][N-1][t]*x[v][i][N-1][t];
			}
		}

		for(i=1;i<N-1;i++){		//Only considering ports
			for(t=1;t<=tOEB;t++){
				if(hasEnteringArc1st[v][i][t]==1){
					expr += inst.r_jt[i-1][t-1]*f[v][i][t];									//1st term
					expr1 += (t-1)*inst.attemptCost*o[v][i][t];								//3rd term
				}
			}
		}
	}
	
	for(j=1;j<N-1;j++){
		for(t=1;t<=tOEB;t++){			
			#ifndef NAlpha
			expr1 += inst.p_jt[j-1][t-1]*alpha[j][t];									//4rd term
			#endif
            #ifndef NBetas
            expr1 += PENALIZATION*beta[j][t]; //100 may be enough
            #endif
            #ifndef NThetas
            expr1 += PENALIZATION*theta[j][t];
            #endif
		}
	}
	obj.setExpr(expr1-expr);
	model.add(obj);	
		
	///Constraints
	//Flow balance source and sink nodes
	IloExpr expr_sinkFlow(env);
	sinkNodeBalance = IloRangeArray(env,V,1,1); 
	sourceNodeBalance = IloRangeArray(env,V,1,1);
	//First level balance
	IloExpr expr_1stLevel(env);
	IloExpr expr_1stFlow(env);
	firstLevelBalance = IloArray<IloArray<IloRangeArray> > (env, V);
	firstLevelFlow = IloArray<IloArray<IloRangeArray> > (env, V);
	
	//Second level balance
	IloExpr expr_2ndLevel(env);
	IloExpr expr_2ndFlow(env);
	secondLevelBalance = IloArray<IloArray<IloRangeArray> > (env, V);
	secondLevelFlow = IloArray<IloArray<IloRangeArray> > (env, V);
	
	//Link betwenn 1st and 2nd level
	#ifndef WaitAfterOperate
	linkBalance = IloArray<IloArray<IloRangeArray> > (env, V);
	#endif
	
	//Berth Limit
	berthLimit = IloArray<IloRangeArray>(env, N-1);		//Index 0 ignored
	IloExpr expr_berth(env);
	
	//Travel at capacity and empty
	travelAtCapacity = IloArray<IloArray<IloArray<IloRangeArray> > > (env, V);
	travelEmpty = IloArray<IloArray<IloArray<IloRangeArray> > > (env, V);
	
	//Inventory balance at ports
	portInventory = IloArray<IloRangeArray> (env, N-1);
	IloExpr expr_invBalancePort(env);
	
	//Cumulative spot market
	#ifndef NAlpha
	cumSlack = IloRangeArray(env,N-1);
	IloExpr expr_cumSlack(env);
	#endif
	
	//Limits on operation values
	operationLowerLimit = IloArray<IloArray<IloRangeArray> >(env,V);
	operationUpperLimit = IloArray<IloArray<IloRangeArray> >(env,V);
	
	//Flow upper limits
	flowCapacityX = IloArray<IloArray<IloArray<IloRangeArray> > > (env,V);
	flowCapacityO = IloArray<IloArray<IloRangeArray> >(env,V);
	flowCapacityW = IloArray<IloArray<IloRangeArray> >(env,V);
	#ifndef WaitAfterOperate
	flowCapacityOB = IloArray<IloArray<IloRangeArray> >(env,V);
	#endif
	#ifdef WaitAfterOperate
	flowCapacityWB = IloArray<IloArray<IloRangeArray> >(env,V);
	#endif
    //Flow lower limits
    if(tightenFlow){
		flowMinCapacityX = IloArray<IloArray<IloArray<IloRangeArray> > > (env,V);
		flowMinCapacityO = IloArray<IloArray<IloRangeArray> >(env,V);
		flowMinCapacityW = IloArray<IloArray<IloRangeArray> >(env,V);
		#ifndef WaitAfterOperate	
		flowMinCapacityOB = IloArray<IloArray<IloRangeArray> >(env,V);
		#endif
		#ifdef WaitAfterOperate	
		flowMinCapacityWB = IloArray<IloArray<IloRangeArray> >(env,V);
		#endif
	}
    //WWCC reformulation
    #ifndef NWWCCReformulation
    IloExpr expr_sumF(env);
    IloExpr expr_sumO(env);
    IloExpr expr_wwcc(env);
    int it_kt;
    #endif
    
    #ifndef NStartUPValidInequalities
    startup_sumStartIfOperate = IloArray<IloRangeArray> (env, J+1);
    startup_sumForceStartup = IloArray<IloRangeArray> (env, J+1);
    startup_dlsccs = IloArray<IloRangeArray> (env, J);
    startup_validInequality = IloArray<IloRangeArray> (env, J+1);
    
    IloExpr expr_sum_vInV_OA;
    IloExpr expr_sum_vInV_O;
    IloExpr expr_sum_vInV_O_t_1;
    #endif
	
	
	if(validIneq){
		knapsack_P_1 = IloArray<IloRangeArray> (env, J+1);			//Altough created for all J ports, it is only considered for loading(production) or consumption(discharging) ports. We create J+1 as index j starts with 1
		knapsack_P_2 = IloArray<IloRangeArray> (env, J+1);	
		knapsack_D_1 = IloArray<IloRangeArray> (env, J+1);	
		knapsack_D_2 = IloArray<IloRangeArray> (env, J+1);	
		#ifndef WaitAfterOperate
		knapsack_D_3 = IloArray<IloRangeArray> (env, J+1);	
		#endif
	}
	unsigned int num_combinations = (T-3)*2;
	for(i=1;i<=J;i++){
        #ifndef NWWCCReformulation
        it_kt = 0;
        #endif
		stringstream ss1;
		berthLimit[i] = IloRangeArray(env,T);
		#ifndef NAlpha
		expr_cumSlack.clear();
		#endif
		portInventory[i] = IloRangeArray(env,T);
		if(validIneq){
			if (inst.typePort[i-1] == 0){ 	//Loading port
				knapsack_P_1[i] = IloRangeArray(env, num_combinations);		//(T-3)*2 = Number of combinations 1,...t + t,...,T for all t \in T. Using T-3 because de increment of T = inst.t+1
				knapsack_P_2[i] = IloRangeArray(env, num_combinations);
			}else{							//Discharging port
				knapsack_D_1[i] = IloRangeArray(env, num_combinations);
				knapsack_D_2[i] = IloRangeArray(env, num_combinations);
				#ifndef WaitAfterOperate
				knapsack_D_3[i] = IloRangeArray(env, num_combinations);
				#endif
			}        
			int it,it2=0,k,l;
			IloExpr expr_kP1_LHS(env), expr_kP2_LHS(env), expr_kD1_LHS(env), expr_kD2_LHS(env);		
			for(it=0;it< num_combinations-(T-1-tOEB);it++){	//For each valid inequality - limited to the constraint that uses the interval [tOEB-1, tOEB]
				double kP1_RHS=0, kP2_RHS=0, kD1_RHS=0, kD2_RHS=0, sum_alphaMax=0, alphaUB=0;
				expr_kP1_LHS.clear();
				expr_kP2_LHS.clear(); //Also used for the equivalent kD3
				expr_kD1_LHS.clear();
				expr_kD2_LHS.clear();
				//Definining the size of set T =[l,k] 
				l=1;
				k=tOEB;
				if(it<tOEB-2){
					k = it+2;
					it2++;		//It2 gets the same value of it until it reach the value tOEB-2
				}else if(it == tOEB-2){
					it = T-3;	//it jumps to half of array of the IloRangeArray (starting the constraints of range [t,...|T|], t \in T) - whent T=45, starts in 43
					l = it2+4-k;
					it2++;
				}
				else{ //After passed the half of array
					l = it2+4-k;
					it2++;
				}
				//~ if(i==1)
					//~ //~ //~ cout it << " [" << l << "," << k << "]\n";
				for(v=0;v<V;v++){
					#ifdef WaitAfterOperate
					if(k<tOEB && hasEnteringArc1st[v][i][k]){
						expr_kP1_LHS += wB[v][i][k];
					}
					if(l>1 && hasEnteringArc1st[v][i][l-1]==1)
						expr_kD1_LHS += w[v][i][l-1] + wB[v][i][l-1];
					#endif
					#ifndef WaitAfterOperate
					expr_kP1_LHS += oB[v][i][k];
					if(l>1 && hasEnteringArc1st[v][i][l-1]==1)
						expr_kD1_LHS += w[v][i][l-1] + oB[v][i][l-1];
					#endif
					#ifndef WaitAfterOperate
					if(l>1 && hasEnteringArc1st[v][i][l-1]==1)
						expr_kD2_LHS += oB[v][i][l-1];
					#endif
					for(t=l;t<=k;t++){
						if(hasEnteringArc1st[v][i][t]==1){
							expr_kP2_LHS += o[v][i][t];	
							#ifndef WaitAfterOperate
							expr_kD2_LHS += oA[v][i][t];
							#endif
							#ifdef WaitAfterOperate
							expr_kD2_LHS += o[v][i][t];
							#endif
						}
						for(j=0;j<N;j++){
							if(j==0 && inst.initialPort[v]+1 == i && t==inst.firstTimeAv[v]+1) 	//Source arc
								expr_kD1_LHS += x[v][j][i][0];
							else if (j == N-1 && hasArc[v][i][j][t] == 1)						//Sink arc
								expr_kP1_LHS += x[v][i][j][t];
							else if (j > 0 && j <= J){											//Port arcs	
								if(i != j){
									//~ if(hasArc[v][i][j][t] == 1)  //Ignoring the rule that the arriving node must be in the model
									if(hasArc[v][i][j][t] == 1 && t+inst.travelTime[v][i-1][j-1] <= tOEB)  //If arc exists and arrives at port j in the integer or relaxed block
										expr_kP1_LHS += x[v][i][j][t];
									if(t - inst.travelTime[v][j-1][i-1] > 0){					//Only if it is possible an entering arc due the travel time
										if(hasArc[v][j][i][t-inst.travelTime[v][j-1][i-1]] == 1)
											expr_kD1_LHS += x[v][j][i][t-inst.travelTime[v][j-1][i-1]];
									}
								}
							}
						}
						//Port time iterator (i,t)
						if (v==0){
							kP1_RHS += inst.d_jt[i-1][t-1];
							kD1_RHS += inst.d_jt[i-1][t-1];
							#ifndef NAlpha
							sum_alphaMax += inst.alp_max_jt[i-1][t-1];
							#endif
						}
					}
				}
				if(l==1){
					kP1_RHS += -inst.sMax_jt[i-1][0] + inst.s_j0[i-1];
					kD1_RHS += -inst.s_j0[i-1] + inst.sMin_jt[i-1][k-1];
					
				}else{
					kP1_RHS += -inst.sMax_jt[i-1][0] + inst.sMin_jt[i-1][0];
					kD1_RHS += -inst.sMax_jt[i-1][l-2] + inst.sMin_jt[i-1][k-1]; //equivalent 'l-1' and 'k' if index starts in 1                
				}
				///If considering alpha parameters, otherwise comment the above 2 lines
				#ifndef NAlpha
				kP1_RHS += - min(sum_alphaMax, inst.alp_max_j[i-1]);
				kD1_RHS += - min(sum_alphaMax, inst.alp_max_j[i-1]);
				#endif
				
				kP2_RHS = max(0.0,ceil(kP1_RHS/inst.f_max_jt[i-1][0]));
				kP1_RHS = max(0.0,ceil(kP1_RHS/inst.maxVesselCapacity));
				kD2_RHS = max(0.0,ceil(kD1_RHS/inst.f_max_jt[i-1][0]));
				kD1_RHS = max(0.0,ceil(kD1_RHS/inst.maxVesselCapacity));
				
				stringstream ss, ss1, ss2;
				if(inst.typePort[i-1] == 0){
					ss << "knpasackP1_" << i << "_(" << l << "," << k << ")";
					knapsack_P_1[i][it] = IloRange(env, kP1_RHS, expr_kP1_LHS, IloInfinity, ss.str().c_str());
					ss1 << "knapsackP2_" << i << "_(" << l << "," << k << ")";
					knapsack_P_2[i][it] = IloRange(env, kP2_RHS, expr_kP2_LHS, IloInfinity, ss1.str().c_str());
					model.add(knapsack_P_1[i][it]);
					model.add(knapsack_P_2[i][it]);
				}else{
					ss << "knapsackD1_" << i << "_(" << l << "," << k << ")";
					knapsack_D_1[i][it] = IloRange(env, kD1_RHS, expr_kD1_LHS, IloInfinity, ss.str().c_str());
					ss1 << "knapsackD2_" << i << "_(" << l << "," << k << ")";
					#ifndef WaitAfterOperate
					knapsack_D_2[i][it] = IloRange(env, kD1_RHS, expr_kD2_LHS, IloInfinity, ss1.str().c_str());
					#endif
					#ifdef WaitAfterOperate
					knapsack_D_2[i][it] = IloRange(env, kD2_RHS, expr_kD2_LHS, IloInfinity, ss1.str().c_str());
					#endif
					model.add(knapsack_D_1[i][it]);
					model.add(knapsack_D_2[i][it]);
					#ifndef WaitAfterOperate
					ss2 << "knapsackD3_" << i << "_(" << l << "," << k << ")";
					knapsack_D_3[i][it] = IloRange(env, kD2_RHS, expr_kP2_LHS, IloInfinity, ss2.str().c_str());
					model.add(knapsack_D_3[i][it]);
					#endif
				}
			}
		}
        
        #ifndef WaitAfterOperate
        #ifndef NStartUPValidInequalities
        startup_sumStartIfOperate[i] = IloRangeArray(env, T);
        startup_sumForceStartup[i] = IloRangeArray(env, T);
        startup_dlsccs[i] = IloRangeArray(env, T);
        startup_validInequality[i] = IloRangeArray(env);
        
        //TODO after tested, implement in optimized format
        for(t=1;t<=tOEB;t++){
            expr_sum_vInV_O.clear();
            expr_sum_vInV_O_t_1.clear();
            expr_sum_vInV_OA.clear();
            for(v=0;v<V;v++){
                expr_sum_vInV_O += o[v][i][t];
                expr_sum_vInV_OA += oA[v][i][t];
                if(t>1)
                    expr_sum_vInV_O_t_1 += o[v][i][t-1];
            }
            stringstream ss1,ss2,ss3;
            ss1 << "startUp_sumStartIfOperate_(" << i << "," << t << ")";
            ss2 << "startUp_sumForceStartUp_(" << i << "," << t << ")";
            ss3 << "startUp_DLSCCS_(" << i << "," << t << ")";
            startup_sumStartIfOperate[i][t] = IloRange(env, -IloInfinity, expr_sum_vInV_OA - expr_sum_vInV_O, 0, ss1.str().c_str());
            model.add(startup_sumStartIfOperate[i][t]);
            startup_sumForceStartup[i][t] = IloRange(env, -IloInfinity, expr_sum_vInV_O - expr_sum_vInV_O_t_1 - expr_sum_vInV_OA, 0, ss2.str().c_str());
            model.add(startup_sumForceStartup[i][t]);
            
            expr_sum_vInV_O.clear();
            for(int u=1;u<=t;u++){
                for(v=0;v<V;v++)
                    expr_sum_vInV_O += o[v][i][t];
            }
            startup_dlsccs[i][t] = IloRange(env, inst.lb_oper_jt[i-1][t-1], expr_sum_vInV_O, IloInfinity, ss3.str().c_str());
            model.add(startup_dlsccs[i][t]);
            
            //~ cplex.addUserCut();
        }
        #endif
        #endif
		float thighetInventoryValue = 0;
		for(t=1;t<=tOEB;t++){
			expr_berth.clear();
			expr_invBalancePort.clear();
			stringstream ss, ss2;
			ss << "berthLimit_(" << i << "," << t << ")";
			bool emptyExpr = true;
            #ifndef NWWCCReformulation
            expr_sumF.clear();
            expr_sumO.clear();
            #endif
			for(v=0;v<V;v++){
				if (hasEnteringArc1st[v][i][t]==1){ //Only if exists an entering arc in the node				
					expr_berth += o[v][i][t];
					emptyExpr = false;
                    #ifndef NWWCCReformulation                    
                    expr_sumF += f[v][i][t];
                    expr_sumO += o[v][i][t];                    
                    #endif
					expr_invBalancePort += -f[v][i][t];
				}
			}
			if(!emptyExpr){ //Only if there are some Z variable in the expr
				berthLimit[i][t] = IloRange(env,-IloInfinity, expr_berth, inst.b_j[i-1], ss.str().c_str());
				model.add(berthLimit[i][t]);
			}
			
			#ifndef NAlpha
			expr_cumSlack += alpha[i][t];
			expr_invBalancePort += -alpha[i][t];
			#endif
			#ifndef NBetas
            expr_invBalancePort += beta[i][t];
            #endif
            #ifndef NThetas
            expr_invBalancePort += -theta[i][t];
            #endif
            
			ss2 << "invBalancePort_(" << i << "," << t << ")";
			portInventory[i][t] = IloRange(env, inst.delta[i-1]*inst.d_jt[i-1][t-1],
				sP[i][t]-sP[i][t-1]-inst.delta[i-1]*expr_invBalancePort,
				inst.delta[i-1]*inst.d_jt[i-1][t-1], ss2.str().c_str());
			model.add(portInventory[i][t]);
					
            #ifndef NWWCCReformulation            
            stringstream ss3;
            //Common for both loading and discharging ports
            for(int k=1;k<=t;k++){
                ss3.str(string());
                ss3 << it_kt << "_wwcc_relaxation_" << i << "(" << k << "," << t << ")";                    
                expr_wwcc.clear();
                int wwcc_rhs=0;
                bool hasExpr=false;
                for(int u=k;u<=t;u++){
                    for (v=0;v<V;v++){
                        if(hasEnteringArc1st[v][i][u]==1){
                            expr_wwcc += o[v][i][u];
                            hasExpr = true;
                        }
                    }
                    if (inst.typePort[i-1] == 1) //Discharging port
                        wwcc_rhs += inst.dM_jt[i-1][u];
                    else    //Loading port
                        wwcc_rhs += inst.d_jt[i-1][u-1];
                }
                expr_wwcc *= inst.f_max_jt[i-1][t-1];
                if(k==1){ //When k=1 and k-1=0, net inventory variable is 0
                    if(inst.typePort[i-1] == 0){ //Loading port
                        wwcc_rhs += inst.s_j0[i-1] - inst.sMax_jt[i-1][1];
                    }
                }else{ 
                    if(inst.typePort[i-1] == 1){ //Discharging port
                        expr_wwcc += sP[i][k-1];
                        wwcc_rhs += inst.sMinM_jt[i-1][k-1];
                    }else{  //Loading port
                        expr_wwcc -= sP[i][k-1];
                        wwcc_rhs -= inst.sMax_jt[i-1][1]; // == -\overline{S}_i
                    }                    
                }                
                if(hasExpr || k>1){                    
                    //~ //~ //~ cout " " <<  expr_wwcc << " >= " << wwcc_rhs << endl;
                    wwcc_relaxation[i][it_kt] = IloRange(env, wwcc_rhs, expr_wwcc, IloInfinity, ss3.str().c_str());                    
                    model.add(wwcc_relaxation[i][it_kt]);
                }
                //~ //~ //~ cout ss3.str() << endl;
                it_kt++;
            }
            #endif
		}
		#ifndef NAlpha
		ss1 << "cum_slack("<<i<<")";		
		if(proportionalAlpha && nIntervals > 1 && endBlock > 0){
			cumSlack[i] = IloRange(env, expr_cumSlack, inst.alp_max_j[i-1]/(T-1)*tOEB, ss1.str().c_str());
		}else{
			cumSlack[i] = IloRange(env, expr_cumSlack, inst.alp_max_j[i-1], ss1.str().c_str());
		}
		model.add(cumSlack[i]);  
		#endif
		if(tightenInvConstr && nIntervals > 1 && endBlock > 0){
			if(inst.delta[i-1] == 1){
				sP[i][tOEB].setUB(inst.sMax_jt[i-1][0]-inst.sMax_jt[i-1][0]*0.1);
			}else{
				sP[i][tOEB].setLB(inst.sMin_jt[i-1][0] + inst.sMax_jt[i-1][0]*0.1);
			}
		}    
	}
    //~ //~ //~ cout "it_kt = " << it_kt << endl;
	#ifndef NAlpha
	expr_cumSlack.end();
	#endif
	expr_berth.end();
	expr_invBalancePort.end();
	
	for(v=0;v<V;v++){
		expr_sinkFlow.clear();
		firstLevelBalance[v] = IloArray<IloRangeArray>(env,J+1); //Only for ports - id 0 not used
		firstLevelFlow[v] = IloArray<IloRangeArray>(env,J+1); 
		secondLevelBalance[v] = IloArray<IloRangeArray>(env,J+1); 
		secondLevelFlow[v] = IloArray<IloRangeArray>(env,J+1); 
		#ifndef WaitAfterOperate
		linkBalance[v] = IloArray<IloRangeArray>(env,J+1); 
		#endif
		
		travelAtCapacity[v] = IloArray<IloArray<IloRangeArray> > (env, N-1);
		travelEmpty[v] = IloArray<IloArray<IloRangeArray> > (env, N-1);
		
		operationLowerLimit[v] = IloArray<IloRangeArray> (env,N-1);
		operationUpperLimit[v] = IloArray<IloRangeArray> (env,N-1);
		
		flowCapacityX[v] = IloArray<IloArray<IloRangeArray> >(env,N);
		flowCapacityO[v] = IloArray<IloRangeArray> (env,N-1);
		#ifndef WaitAfterOperate
		flowCapacityOB[v] = IloArray<IloRangeArray> (env,N-1);
		#endif
		flowCapacityW[v] = IloArray<IloRangeArray> (env,N-1);
		#ifdef WaitAfterOperate
		flowCapacityWB[v] = IloArray<IloRangeArray> (env,N-1);
		#endif
		
        if(tightenFlow){
			flowMinCapacityX[v] = IloArray<IloArray<IloRangeArray> >(env,N);
			flowMinCapacityO[v] = IloArray<IloRangeArray> (env,N-1);
			#ifndef WaitAfterOperate
			flowMinCapacityOB[v] = IloArray<IloRangeArray> (env,N-1);
			#endif
			flowMinCapacityW[v] = IloArray<IloRangeArray> (env,N-1);
			#ifdef WaitAfterOperate
			flowMinCapacityWB[v] = IloArray<IloRangeArray> (env,N-1);
			#endif
		}
        
		for(i=0;i<N;i++){
			if(i>0 && i <= J){ //Only considering ports				
				firstLevelBalance[v][i] = IloRangeArray(env,T,0,0); //Id 0 is not used
				secondLevelBalance[v][i] = IloRangeArray(env,T,0,0); 
				firstLevelFlow[v][i] = IloRangeArray(env,T,0,0); 
				secondLevelFlow[v][i] = IloRangeArray(env,T,0,0); 
				#ifndef WaitAfterOperate
				linkBalance[v][i] = IloRangeArray(env,T,-IloInfinity,0); 
				#endif
				travelAtCapacity[v][i] = IloArray<IloRangeArray> (env, N);
				travelEmpty[v][i] = IloArray<IloRangeArray> (env, N);
				
				operationLowerLimit[v][i] = IloRangeArray (env,T);
				operationUpperLimit[v][i] = IloRangeArray (env,T);
				
				for(j=0;j<N;j++){
					if(j>0){ //Considering ports and sink node
						travelAtCapacity[v][i][j] = IloRangeArray(env, T, -IloInfinity, 0);
						travelEmpty[v][i][j] = IloRangeArray (env, T, -IloInfinity, inst.q_v[v]);
						for(t=1;t<=tOEB;t++){
							stringstream ss, ss1;
                            if (j<=J){				//When j is a port
                                if(hasArc[v][i][j][t] == 1 &&
									t + inst.travelTime[v][i-1][j-1] <= tOEB){ //Only if arc departs and arrives in the integer or relaxed block
                                    if(inst.typePort[i-1] == 0 && inst.typePort[j-1] == 1){
                                        ss << "travelAtCap_(" << i << "," << j << ")_" << t << "," << v;
                                        travelAtCapacity[v][i][j][t].setExpr(-fX[v][i][j][t] + inst.q_v[v]*x[v][i][j][t]);	
                                        travelAtCapacity[v][i][j][t].setName(ss.str().c_str());
                                        model.add(travelAtCapacity[v][i][j][t]);
                                    }else if (inst.typePort[i-1] == 1 && inst.typePort[j-1] == 0){
                                        ss1 << "travelEmpty_(" << i << "," << j << ")_" << t << "," << v;
                                        travelEmpty[v][i][j][t].setExpr(inst.q_v[v]*x[v][i][j][t] + fX[v][i][j][t]);
                                        travelEmpty[v][i][j][t].setName(ss1.str().c_str());
                                        model.add(travelEmpty[v][i][j][t]);
                                    }
                                }
							}else{ // when j is the sink node
								if(hasArc[v][i][j][t]==1){
									if(inst.typePort[i-1] == 0){
										ss << "travelAtCap_(" << i << ",snk)_" << t << "," << v;
										travelAtCapacity[v][i][j][t].setExpr(-fX[v][i][j][t] + inst.q_v[v]*x[v][i][j][t]);	
										travelAtCapacity[v][i][j][t].setName(ss.str().c_str());
										model.add(travelAtCapacity[v][i][j][t]);
									}else if (inst.typePort[i-1] == 1){ 
										ss1 << "travelEmpty_(" << i << ",snk)_" << t << "," << v;
										travelEmpty[v][i][j][t].setExpr(inst.q_v[v]*x[v][i][j][t] + fX[v][i][j][t]);
										travelEmpty[v][i][j][t].setName(ss1.str().c_str());
										model.add(travelEmpty[v][i][j][t]);
									}
								}
							}
						}
					}
				}
				flowCapacityO[v][i] = IloRangeArray(env,T);
				#ifndef WaitAfterOperate
				flowCapacityOB[v][i] = IloRangeArray(env,T);
				#endif
				flowCapacityW[v][i] = IloRangeArray(env,T);
				#ifdef WaitAfterOperate
				flowCapacityWB[v][i] = IloRangeArray(env,T);
				#endif
                
                if(tightenFlow){
					flowMinCapacityO[v][i] = IloRangeArray(env,T);
					#ifndef WaitAfterOperate
					flowMinCapacityOB[v][i] = IloRangeArray(env,T);
					#endif
					flowMinCapacityW[v][i] = IloRangeArray(env,T);
					#ifdef WaitAfterOperate
					flowMinCapacityWB[v][i] = IloRangeArray(env,T);
					#endif
                }
                #ifndef NBranching
                //~ IloExpr expr_sumEnteringX(env);
                IloExpr expr_sumOA(env);                    
                #endif
				
				for(t=1;t<=tOEB;t++){
					stringstream ss, ss1, ss2, ss3, ss4, ss5, ss6, ss7, ss8, ss9, ss10, ss11;
					ss << "First_level_balance_" << v << "(" << i << "," << t << ")";
					ss1 << "Second_level_balance_" << v << "(" << i << "," << t << ")";
					ss2 << "link_balance_" << v << "(" << i << "," << t << ")";
					ss3 << "First_level_flow_" << v << "(" << i << "," << t << ")";
					ss4 << "Second_level_flow_" << v << "(" << i << "," << t << ")";
                    ss11 << "operate_and_depart_" << v << "(" << i << "," << t << ")";
					
					if(hasArc[v][i][N-1][t]==1)
						expr_sinkFlow += x[v][i][N-1][t];
					
					expr_1stLevel.clear();
					expr_1stFlow.clear();
					expr_2ndLevel.clear();
					expr_2ndFlow.clear();

                    if(addConstr){
						expr_opd.clear();
					}
                    
					for(j=0;j<N;j++){
						if(j<N-1){ //No consider sink arc (first level balance)
							//If j is the source node and reach i in time t
							if(j==0 && inst.initialPort[v]+1 == i && t==inst.firstTimeAv[v]+1){
								expr_1stLevel += x[v][j][i][0];
								expr_1stFlow += fX[v][j][i][0];
								fX[v][j][i][0].setBounds(inst.s_v0[v], inst.s_v0[v]); //Fixing initial inventory 
                                #ifndef NBranching
                                //~ expr_sumEnteringX += x[v][j][i][0];
                                #endif
							}
							else if (j>0){ //When j is a port
								if (t - inst.travelTime[v][j-1][i-1] >= 0){ //If is possible to exist an arc from j to i
									if(hasArc[v][j][i][t-inst.travelTime[v][j-1][i-1]] == 1){ //If the arc exists
										expr_1stLevel += x[v][j][i][t-inst.travelTime[v][j-1][i-1]];
										expr_1stFlow += fX[v][j][i][t-inst.travelTime[v][j-1][i-1]];
                                        #ifndef NBranching
                                        //~ expr_sumEnteringX += x[v][j][i][t-inst.travelTime[v][j-1][i-1]];
                                        #endif
									}
								}                                
							}
						}
						if(j>0){ //No consider source arc (second level balance)
                            if (hasArc[v][i][j][t] == 1){
                                if(j == N-1){ //If j is the sink node
                                    expr_2ndLevel += x[v][i][j][t];
                                    expr_2ndFlow += fX[v][i][j][t];
                                    if(addConstr){
										if (inst.q_v[v] <= inst.f_max_jt[i-1][0]){ //Only if a vessel can load(unload) in the port in just 1 time period
											expr_opd += x[v][i][j][t];
										}
                                    }
                                }else if (t + inst.travelTime[v][i-1][j-1] <= tOEB){ //If j is a port, it is then necessary that the arrival is in the model
                                    expr_2ndLevel += x[v][i][j][t];
                                    expr_2ndFlow += fX[v][i][j][t];
                                    if(addConstr){
										if (inst.q_v[v] <= inst.f_max_jt[i-1][0] && inst.typePort[i-1] != inst.typePort[j-1]){ //Only if a vessel can load(unload) in the port in just 1 time period and i and j are ports of different types
											expr_opd += x[v][i][j][t];
										}
                                    }
                                }
							}
						}
					}
                    if(addConstr){
                        if(hasEnteringArc1st[v][i][t]==1 && inst.q_v[v] <= inst.f_max_jt[i-1][t-1]){
							#ifdef WaitAfterOperate                        
								expr_opd += -o[v][i][t];
							#endif
							#ifndef WaitAfterOperate                        
								expr_opd += -oA[v][i][t];
							#endif
							operateAndDepart[v][i][t] = IloRange(env, 0, expr_opd, 0,ss11.str().c_str());                    
							model.add(operateAndDepart[v][i][t]);
						}
                    }
                    
					IloExpr expr_link;
					#ifndef WaitAfterOperate
					if (t==1 || (t < tOEB && hasEnteringArc1st[v][i][t-1]==0)){ //First time period or not last time period nor entering arc in the previous time period
						expr_1stLevel += - w[v][i][t] - oA[v][i][t];
                        expr_2ndLevel += -oA[v][i][t] + oB[v][i][t];
                        expr_link = oA[v][i][t] - o[v][i][t];
                        expr_1stFlow+= -fW[v][i][t] - fO[v][i][t];
						expr_2ndFlow += -fO[v][i][t] - inst.delta[i-1]*f[v][i][t] + fOB[v][i][t];
					}else if (t==tOEB){ //Last time period
                        if (hasEnteringArc1st[v][i][t-1]==1){
                            expr_1stLevel += w[v][i][t-1] - oA[v][i][t];
                            expr_2ndLevel += -oA[v][i][t] -oB[v][i][t-1];
                            expr_link = oA[v][i][t] + oB[v][i][t-1] - o[v][i][t];   
                            expr_1stFlow += fW[v][i][t-1] - fO[v][i][t];
                            expr_2ndFlow += -fO[v][i][t] - fOB[v][i][t-1] - inst.delta[i-1]*f[v][i][t];
                        }else{
                            expr_1stLevel += - oA[v][i][t];
                            expr_2ndLevel += -oA[v][i][t];
                            expr_link = oA[v][i][t] - o[v][i][t];   
                            expr_1stFlow += - fO[v][i][t];
                            expr_2ndFlow += -fO[v][i][t] - inst.delta[i-1]*f[v][i][t];
                        }
					}else{ //Other times
						expr_1stLevel += w[v][i][t-1] - w[v][i][t] - oA[v][i][t];
						expr_2ndLevel += - oA[v][i][t] - oB[v][i][t-1] + oB[v][i][t];
						expr_link = oA[v][i][t] + oB[v][i][t-1] - o[v][i][t];
						expr_1stFlow += fW[v][i][t-1] - fW[v][i][t] - fO[v][i][t];
						expr_2ndFlow += -fO[v][i][t] - fOB[v][i][t-1] - inst.delta[i-1]*f[v][i][t] + fOB[v][i][t];	
					}
					#endif
					#ifdef WaitAfterOperate
					if (t==1 || (t < tOEB && hasEnteringArc1st[v][i][t-1]==0)){ //First time period or not last time period nor entering arc in the previous time period
						expr_1stLevel += - w[v][i][t] - o[v][i][t];
						expr_2ndLevel += -o[v][i][t] + wB[v][i][t];	
						expr_1stFlow+= -fW[v][i][t] - fO[v][i][t];	
						expr_2ndFlow += -fO[v][i][t] - inst.delta[i-1]*f[v][i][t] + fWB[v][i][t];
					}else if (t==tOEB){ //Last time period
						if (hasEnteringArc1st[v][i][t-1]==1){
                            expr_1stLevel += w[v][i][t-1] - o[v][i][t] + wB[v][i][t-1];                                
                            expr_1stFlow += fW[v][i][t-1] - fO[v][i][t] + fWB[v][i][t-1];                                
                        }else{
                            expr_1stLevel += - o[v][i][t];                                
                            expr_1stFlow += - fO[v][i][t];                                
                        }
                        expr_2ndLevel += -o[v][i][t];
                        expr_2ndFlow += -fO[v][i][t] - inst.delta[i-1]*f[v][i][t];
					}else{ //Other times
						expr_1stLevel += w[v][i][t-1] - w[v][i][t] - o[v][i][t] + wB[v][i][t-1];
						expr_2ndLevel += - o[v][i][t] + wB[v][i][t];	
						expr_1stFlow += fW[v][i][t-1] - fW[v][i][t] - fO[v][i][t] + fWB[v][i][t-1];
						expr_2ndFlow += -fO[v][i][t] - inst.delta[i-1]*f[v][i][t] + fWB[v][i][t];
					}
					#endif
					
					firstLevelBalance[v][i][t].setExpr(expr_1stLevel);
					secondLevelBalance[v][i][t].setExpr(expr_2ndLevel);
					#ifndef WaitAfterOperate
					linkBalance[v][i][t].setExpr(expr_link);
					#endif
					firstLevelFlow[v][i][t].setExpr(expr_1stFlow);
					secondLevelFlow[v][i][t].setExpr(expr_2ndFlow);
					
					firstLevelBalance[v][i][t].setName(ss.str().c_str());
					if(hasEnteringArc1st[v][i][t]==1){
						model.add(firstLevelBalance[v][i][t]);
											
						secondLevelBalance[v][i][t].setName(ss1.str().c_str());
						model.add(secondLevelBalance[v][i][t]);
					}
					
					
					
					#ifndef WaitAfterOperate                    
					linkBalance[v][i][t].setName(ss2.str().c_str());
					model.add(linkBalance[v][i][t]);
					#endif
					
					if(hasEnteringArc1st[v][i][t]==1){
					
						firstLevelFlow[v][i][t].setName(ss3.str().c_str());
						model.add(firstLevelFlow[v][i][t]);
						
						secondLevelFlow[v][i][t].setName(ss4.str().c_str());
						model.add(secondLevelFlow[v][i][t]);

						ss5 << "fjmin_(" << i << "," << t << ")," << v;
						ss6 << "fjmax_(" << i << "," << t << ")," << v;
						IloNum minfMax = min(inst.f_max_jt[i-1][t-1], inst.q_v[v]);
						operationLowerLimit[v][i][t] = IloRange(env, 0, f[v][i][t] - inst.f_min_jt[i-1][t-1]*o[v][i][t] , IloInfinity, ss5.str().c_str());
						model.add(operationLowerLimit[v][i][t]);
						
						operationUpperLimit[v][i][t] = IloRange(env, -IloInfinity, f[v][i][t] - minfMax*o[v][i][t],	0, ss6.str().c_str());
						model.add(operationUpperLimit[v][i][t]);
						
						ss7 << "flowLimitOA_"<<v<<","<<i<<","<<t;
						stringstream ss7_1;
						ss7_1 << "flowMinLimitOA_"<<v<<","<<i<<","<<t;
						#ifndef WaitAfterOperate
						flowCapacityO[v][i][t] = IloRange(env, -IloInfinity, fO[v][i][t]-oA[v][i][t]*inst.q_v[v], 0, ss7.str().c_str());
						if(tightenFlow){
							if(inst.typePort[i-1] == 1){ //Minimum flow in discharging ports
								flowMinCapacityO[v][i][t] = IloRange(env, -IloInfinity, oA[v][i][t]*inst.f_min_jt[i-1][t-1]-fO[v][i][t], 0, ss7_1.str().c_str());
								model.add(flowMinCapacityO[v][i][t]);
							}else{ //Change upper bound for loading ports
								flowCapacityO.setExpr(fO[v][i][t] - oA[v][i][t]*(inst.q_v[v]-inst.f_min_jt[i-1][t-1]));
							}
						}
						#endif
						#ifdef WaitAfterOperate
						flowCapacityO[v][i][t] = IloRange(env, -IloInfinity, fO[v][i][t]-o[v][i][t]*inst.q_v[v], 0, ss7.str().c_str());
						if(tightenFlow){
							if(inst.typePort[i-1] == 1){ //Minimum flow in discharging port
								flowMinCapacityO[v][i][t] = IloRange(env, -IloInfinity, o[v][i][t]*inst.f_min_jt[i-1][t-1]-fO[v][i][t], 0, ss7_1.str().c_str());
								model.add(flowMinCapacityO[v][i][t]);
							}else{ //Change upper bound for loading ports
								flowCapacityO[v][i][t].setExpr(fO[v][i][t] - o[v][i][t]*(inst.q_v[v]-inst.f_min_jt[i-1][t-1]));
							}
						}                    
						#endif
						model.add(flowCapacityO[v][i][t]);
						
						if(t<tOEB && hasEnteringArc1st[v][i][t]==1){ //Constraints with no last time index
							#ifndef WaitAfterOperate
							ss8 << "flowLimitOB_"<<v<<","<<i<<","<<t;
							stringstream ss8_1;
							ss8 << "flowMinLimitOB_"<<v<<","<<i<<","<<t;
							flowCapacityOB[v][i][t] = IloRange(env, -IloInfinity, fOB[v][i][t]-oB[v][i][t](inst.q_v[v]), 0, ss8.str().c_str());
							model.add(flowCapacityOB[v][i][t]);
							if(tightenFlow){
								//Equal for discharging and loading ports
								flowMinCapacityOB[v][i][t] = IloRange(env, -IloInfinity, oB[v][i][t]*inst.f_min_jt[i-1][t-1] - fOB[v][i][t], 0, ss8_1.str().c_str());
								model.add(flowMinCapacityOB[v][i][t]);
								flowCapacityOB[v][i][t].setExpr(oB[v][i][t](inst.q_v[v]-inst.f_min_jt[i-1][t-1]) - fOB[v][i][t]);
							}
							#endif

							#ifdef WaitAfterOperate
							ss10 << "flowLimitWB_"<<v<<","<<i<<","<<t;
							stringstream ss10_1;
							ss10_1 << "flowMinLimitWB_"<<v<<","<<i<<","<<t;
							
							flowCapacityWB[v][i][t] = IloRange(env, -IloInfinity, fWB[v][i][t]-inst.q_v[v]*wB[v][i][t], 0, ss10.str().c_str());
							model.add(flowCapacityWB[v][i][t]);
							if(tightenFlow){
								//Equal for discharging and loading ports
								flowMinCapacityWB[v][i][t] = IloRange(env, -IloInfinity, wB[v][i][t]*inst.f_min_jt[i-1][t-1]-fWB[v][i][t], 0, ss10_1.str().c_str());
								model.add(flowMinCapacityWB[v][i][t]);
								flowCapacityWB[v][i][t].setExpr(wB[v][i][t]*(inst.q_v[v]-inst.f_min_jt[i-1][t-1])-fWB[v][i][t]);							
							}
							#endif
							ss9 << "flowLimitW_"<<v<<","<<i<<","<<t;
							stringstream ss9_1;
							ss9_1 << "flowLimitW_"<<v<<","<<i<<","<<t;
							flowCapacityW[v][i][t] = IloRange(env, -IloInfinity, fW[v][i][t]-inst.q_v[v]*w[v][i][t], 0, ss9.str().c_str());
							model.add(flowCapacityW[v][i][t]);
							if(tightenFlow){
								if(inst.typePort[i-1] == 1){ //Lower bound on flow for discharging ports
									flowMinCapacityW[v][i][t] = IloRange(env, -IloInfinity, w[v][i][t]*inst.f_min_jt[i-1][t-1]-fW[v][i][t], 0, ss9_1.str().c_str());
									model.add(flowMinCapacityW[v][i][t]);
								}else{ //Upper bound on loading ports
									flowCapacityW[v][i][t].setExpr(fW[v][i][t]-w[v][i][t]*(inst.q_v[v]-inst.f_min_jt[i-1][t-1]));
								}
							}
						}
                    }
                    #ifndef NBranching                    
                        expr_sumOA += oA[v][i][t];
                    #endif
				}
                flowCapacityX[v][i] = IloArray<IloRangeArray>(env,N);
                if(tightenFlow){
					flowMinCapacityX[v][i] = IloArray<IloRangeArray>(env,N);
				}	
                
                #ifndef NBranching                
                //~ priorityX[v][i] = IloRange(env, 0, expr_sumEnteringX - sumX[v][i], 0);
                //~ model.add(priorityX[v][i]);
                
                priorityOA[v][i] = IloRange(env, 0, expr_sumOA - sumOA[v][i], 0);
                model.add(priorityOA[v][i]);   
                #endif 
				for(j=1;j<N;j++){ //No arriving arc in source arc j=0
					if(i != j && i < N-1){ //There is no departing arc from sink node
						flowCapacityX[v][i][j] = IloRangeArray(env,T);
						if(tightenFlow){
							flowMinCapacityX[v][i][j] = IloRangeArray(env,T);
                        }
						for(t=0;t<=tOEB;t++){
							if(hasArc[v][i][j][t]==1){
								stringstream ss,ss1;
								ss << "flowLimitX_" << v << "," << i << "," << j << "," << t;
								ss1 << "flowMinLimitX_" << v << "," << i << "," << j << "," << t;
								if(j == N-1){ //If j is sink node                            
									flowCapacityX[v][i][j][t] = IloRange(env, -IloInfinity, fX[v][i][j][t] - inst.q_v[v]*x[v][i][j][t], 0, ss.str().c_str());
									model.add(flowCapacityX[v][i][j][t]);
								}else if (i>0 && t + inst.travelTime[v][i-1][j-1] <= tOEB){ //If x variable reach j in the time period
									//Original flow upper limit
									flowCapacityX[v][i][j][t] = IloRange(env, -IloInfinity, fX[v][i][j][t] - inst.q_v[v]*x[v][i][j][t], 0, ss.str().c_str());
									model.add(flowCapacityX[v][i][j][t]);
									if(tightenFlow){ //Thighter upper bound if traveling between 2 loading ports
										if(inst.typePort[i-1] == inst.typePort[j-1]){ //Only for cases where ports are of the same type
											if (inst.typePort[i-1] == 0){   //Loading ports 
												flowCapacityX[v][i][j][t].setExpr(fX[v][i][j][t] - x[v][i][j][t]*(inst.q_v[v]-inst.f_min_jt[j-1][0]));
												flowMinCapacityX[v][i][j][t] = IloRange(env, -IloInfinity, x[v][i][j][t]*inst.f_min_jt[i-1][0] - fX[v][i][j][t] , 0, ss1.str().c_str());
											}else{
												flowCapacityX[v][i][j][t].setExpr(fX[v][i][j][t] - x[v][i][j][t]*(inst.q_v[v]-inst.f_min_jt[i-1][0]));
												flowMinCapacityX[v][i][j][t] = IloRange(env, -IloInfinity, x[v][i][j][t]*inst.f_min_jt[j-1][0] - fX[v][i][j][t] , 0, ss1.str().c_str());
											}
											model.add(flowMinCapacityX[v][i][j][t]);
										}
									}
								}
							}
						}
					}
				}
			}
		}
		stringstream ss;
		ss << "flowBalanceSink_" << v;
		sinkNodeBalance[v].setExpr(expr_sinkFlow);
		sinkNodeBalance[v].setName(ss.str().c_str());
		model.add(sinkNodeBalance[v]);
	}
	
	expr_1stLevel.end();
	expr_2ndLevel.end();
	expr_2ndLevel.end();
	expr_2ndFlow.end();
	expr_sinkFlow.end();
  
}


void Model::buildModel(IloEnv& env, Instance inst){
	int i,j,t,v,a;
	int J = inst.J;
	int T = inst.t + 1;			//Init time-periods in 1
	int V = inst.speed.getSize(); //# of vessels
	int N = J + 2; // Number of nodes including source and sink. (0,1...j,j+1)
		
	//Init Variables and info arrays
	#ifndef NSpotMarket
	alpha = NumVarMatrix(env, N);
	#endif
	#ifndef NBetas
	beta = NumVarMatrix(env, N);
	#endif
	#ifndef NThetas
	theta = NumVarMatrix(env, N);
	#endif
	sP = NumVarMatrix(env,N);
	f = IloArray<NumVarMatrix> (env, V);
	fX = IloArray<IloArray<NumVarMatrix> >(env, V);
	fO = IloArray<NumVarMatrix> (env, V);
	fW = IloArray<NumVarMatrix> (env, V);		
	fWB = IloArray<NumVarMatrix> (env, V);	
	
	hasArc = IloArray<IloArray<IloArray<IloIntArray> > >(env, V);
	arcCost = IloArray<IloArray<IloArray<IloNumArray> > >(env, V);
	hasEnteringArc1st = IloArray<IloArray<IloIntArray> >(env, V);
	
	x = IloArray<IloArray<IloArray<IloBoolVarArray> > >(env, V);
	o = IloArray<IloArray<IloBoolVarArray> >(env,V);
	w = IloArray<IloArray<IloBoolVarArray> >(env,V);
	wB = IloArray<IloArray<IloBoolVarArray> >(env,V);	
	
	//Additional variables for branching
	#ifndef NBranching		
	#endif
	
	for(v=0;v<V;v++){
		f[v] = NumVarMatrix(env,N);
		fX[v] = IloArray<NumVarMatrix>(env,N);
		fO[v] = NumVarMatrix(env,N);
		fW[v] = NumVarMatrix(env,N);				
		fWB[v] = NumVarMatrix(env,N);		
		
		o[v] = IloArray<IloBoolVarArray>(env,N);
		o[v] = IloArray<IloBoolVarArray>(env,N);
		w[v] = IloArray<IloBoolVarArray>(env,N);		
		x[v] = IloArray<IloArray<IloBoolVarArray> >(env, N);	
		wB[v] = IloArray<IloBoolVarArray>(env,N);
					
		hasArc[v] = IloArray<IloArray<IloIntArray> >(env, N);				
		arcCost[v] = IloArray<IloArray<IloNumArray> >(env, N);
		hasEnteringArc1st[v] = IloArray<IloIntArray>(env, N);
		
		for(j=0;j<N;j++){			
			x[v][j] = IloArray<IloBoolVarArray>(env,N);
			hasArc[v][j] = IloArray<IloIntArray>(env,N);
			arcCost[v][j] = IloArray<IloNumArray>(env,N);			
			fX[v][j] = IloArray<IloNumVarArray>(env, N);
			for(i=0;i<N;i++){				
				x[v][j][i] = IloBoolVarArray(env,T);				
				hasArc[v][j][i] = IloIntArray(env,T);
				arcCost[v][j][i] = IloNumArray(env,T);
				fX[v][j][i] = IloNumVarArray(env, T);
				for(t=0;t<T;t++){				
					stringstream ss;			
					ss << "x_"<<v<<","<<j<<","<<i<<","<<t;
					x[v][j][i][t].setName(ss.str().c_str());
					ss.str(string());
					ss << "fX_"<<v<<","<<j<<","<<i<<","<<t;
					fX[v][j][i][t] = IloNumVar(env, 0, IloInfinity, ss.str().c_str());
					#ifndef NRelaxation
					IloConversion conv(env, x[v][j][i][t],ILOFLOAT);
					model.add(conv);
					#endif
				}
			}
			if(j>0 && j<=J){ //Only considering Ports
				f[v][j] = IloNumVarArray(env, T);
				fO[v][j] = IloNumVarArray(env, T);
				fW[v][j] = IloNumVarArray(env, T);
				fWB[v][j] = IloNumVarArray(env, T);
				
				o[v][j] = IloBoolVarArray(env, T);				
				w[v][j] = IloBoolVarArray(env, T);
				wB[v][j] = IloBoolVarArray(env, T);
				
				hasEnteringArc1st[v][j] = IloIntArray(env,T);

				for(t=1;t<T;t++){				
					stringstream ss;
					ss << "f_(" << j << "," << t << ")," << v;
					int maxAmountOperation = min(inst.q_v[v], inst.f_max_jt[j-1][t-1]);
					f[v][j][t] = IloNumVar(env, 0, maxAmountOperation, ss.str().c_str());								
					ss.str(string());
					ss << "fO_(" << j << "," << t << ")," << v;
					fO[v][j][t] = IloNumVar(env, 0, IloInfinity, ss.str().c_str());
					ss.str(string());
					ss << "o_(" << j << "," << t << ")," << v;
					o[v][j][t].setName(ss.str().c_str());	
					#ifndef NRelaxation
					IloConversion convZ(env,o[v][j][t], ILOFLOAT);
					model.add(convZ);
					IloConversion convOA(env,o[v][j][t], ILOFLOAT);
					model.add(convOA);
					#endif 				
					
					if(t<T-1){ //Variables that haven't last time index
						ss.str(string());
						ss << "fW_(" << j << "," << t << ")," << v;
						fW[v][j][t] = IloNumVar(env, 0, IloInfinity, ss.str().c_str());
						ss.str(string());
						ss << "w_(" << j << "," << t << ")," << v;
						w[v][j][t].setName(ss.str().c_str());
						ss.str(string());
						ss << "wB_(" << j << "," << t << ")," << v;
						wB[v][j][t].setName(ss.str().c_str());					
						ss.str(string());
						ss << "fWB_(" << j << "," << t << ")," << v;
						fWB[v][j][t] = IloNumVar(env, 0, IloInfinity, ss.str().c_str());
						#ifndef NRelaxation
						IloConversion convW(env,w[v][j][t], ILOFLOAT);
						model.add(convW);
						IloConversion convWB(env,wB[v][j][t], ILOFLOAT);
						model.add(convWB);						
						#endif
					}									
				}		
			}	
		}
	}

	for(i=1;i<N-1;i++){
        #ifndef NSpotMarket
		alpha[i] = IloNumVarArray(env,T);		
        #endif
        #ifndef NBetas
        beta[i] = IloNumVarArray(env,T);		
        #endif
        #ifndef NThetas
        theta[i] = IloNumVarArray(env,T);		
        #endif
		sP[i] = IloNumVarArray(env, T);		
		for(t=0;t<T;t++){
			stringstream ss;						
			if (t == 0){								
				ss << "sP_(" << i << ",0)";
				sP[i][t] = IloNumVar(env,inst.s_j0[i-1], inst.s_j0[i-1], ss.str().c_str()); //Initial inventory		
			}else{
				ss.str(string());
				ss << "sP_(" << i << "," << t << ")";				
				sP[i][t] = IloNumVar(env,inst.sMin_jt[i-1][0], inst.sMax_jt[i-1][0], ss.str().c_str()); 
				//~ sP[i][t] = IloNumVar(env,-IloInfinity, IloInfinity, ss.str().c_str()); //For adding it as lazy constraints
				//~ cplex.addLazyConstraint(sP[i][t] <= inst.sMax_jt[i-1][0]);
				//~ cplex.addLazyConstraint(sP[i][t] >= inst.sMin_jt[i-1][0]);
				#ifndef NSpotMarket
				ss.str(string());
				ss << "alpha_(" << i << "," << t << ")";
				alpha[i][t] = IloNumVar(env, 0, inst.alp_max_jt[i-1][t-1], ss.str().c_str());
                #endif
                #ifndef NBetas
                ss.str(string());
				ss << "beta_(" << i << "," << t << ")";
				beta[i][t] = IloNumVar(env, 0, IloInfinity, ss.str().c_str());
				#endif
				#ifndef NThetas
				ss.str(string());
				ss << "theta_(" << i << "," << t << ")";
				theta[i][t] = IloNumVar(env, 0, IloInfinity, ss.str().c_str());
				#endif
			}			
		}		
	}
	
	///Objective function
	IloExpr expr(env);
	IloExpr expr1(env);	
	for(v=0;v<V;v++){
		///Builds the arcs between nodes 												//2nd term
		//Source arc	
		j = inst.initialPort[v]+1;
		hasArc[v][0][j][0] = 1;			//Time between source node and initial ports is equal to first time available
		arcCost[v][0][j][0] = inst.portFee[j-1];
		expr1 += arcCost[v][0][j][0]*x[v][0][j][0];
		hasEnteringArc1st[v][j][inst.firstTimeAv[v]+1] = 1;
		x[v][0][j][0].setBounds(1,1); // Fixing initial port position
				
		//Travel and sink arcs		
		i = inst.initialPort[v]+1;					//Arcs from initial port i
		for (t=inst.firstTimeAv[v]+1;t<T;t++){		//and initial time available t			
			for(j=1;j<N-1;j++){						//Not necessary to account sink node
				if (i != j){
					int t2 = t + inst.travelTime[v][i-1][j-1]; 
					if (t2<T){ 	//If exists time to reach port j 
						double arc_cost;
						if (inst.typePort[i-1]==1 && inst.typePort[j-1]==0){ //If port i is consuming and j is a producer port
							arc_cost = inst.costKm[v]*inst.distanceMatrix[i-1][j-1]*(1-inst.trav_empt[v]) + inst.portFee[j-1];		
						}else{
							arc_cost = inst.costKm[v]*inst.distanceMatrix[i-1][j-1] + inst.portFee[j-1];
						}
						hasArc[v][i][j][t] = 1;	
						//~ cout << v << " " << i << " " << j << " " << t << "=" << hasArc[v][i][j][t] << endl;
						arcCost[v][i][j][t] = arc_cost;
						hasEnteringArc1st[v][j][t2] = 1;
						if (t2+1<T-1) 			//Waiting arc 
							hasEnteringArc1st[v][j][t2+1] = 1;
							
						expr1 += arc_cost*x[v][i][j][t];
												
						//Sink arc from port j 
						hasArc[v][j][N-1][t2] = 1;
						arcCost[v][j][N-1][t2] = -(T-t2-1)*inst.perPeriodRewardForFinishingEarly;
						expr1 += arcCost[v][j][N-1][t2]*x[v][j][N-1][t2];						
						//when the time t reach a time that can be built a mirror between j and i
						if (t >= inst.firstTimeAv[v]+1 + inst.travelTime[v][i-1][j-1]){ //+1 for the data convert 
							if (inst.typePort[j-1]==1 && inst.typePort[i-1]==0){ 		  //If port j is consuming and i is a producer port
								arc_cost = inst.costKm[v]*inst.distanceMatrix[j-1][i-1]*(1-inst.trav_empt[v]) + inst.portFee[i-1];		
							}else{
								arc_cost = inst.costKm[v]*inst.distanceMatrix[j-1][i-1] + inst.portFee[i-1];
							}
							hasArc[v][j][i][t] = 1;
							arcCost[v][j][i][t] = arc_cost;
							hasEnteringArc1st[v][i][t2] = 1;
							expr1 += arc_cost*x[v][j][i][t];							
						}
						//~ //Create arc from j,t2 to others ports (j2) in time t3
						for(int j2=1;j2<=J;j2++){
							if(j2 != i && j2 != j){
								int t3 = t2+inst.travelTime[v][j-1][j2-1];  
								if(t3<T){
									if (inst.typePort[j-1]==1 && inst.typePort[j2-1]==0){
										arc_cost = inst.costKm[v]*inst.distanceMatrix[j-1][j2-1]*(1-inst.trav_empt[v]) + inst.portFee[j2-1];		
									}else{
										arc_cost = inst.costKm[v]*inst.distanceMatrix[j-1][j2-1] + inst.portFee[j2-1];
									}
									hasArc[v][j][j2][t2] = 1;
									arcCost[v][j][j2][t2] = arc_cost;
									expr1 += arc_cost*x[v][j][j2][t2];
									hasEnteringArc1st[v][j2][t3] = 1;
								}
							}
						}						
					}
				}
			}
			if(t+1<T)
				hasEnteringArc1st[v][i][t+1] = 1;
			//Sink arc from port i
			hasArc[v][i][N-1][t] = 1;
			arcCost[v][i][N-1][t] = -(T-t-1)*inst.perPeriodRewardForFinishingEarly;
			expr1 += arcCost[v][i][N-1][t]*x[v][i][N-1][t];			
		}		
		
		
		for(i=1;i<N-1;i++){		//Only considering ports
			for(t=1;t<T;t++){					
				if(hasEnteringArc1st[v][i][t]){
					expr += inst.r_jt[i-1][t-1]*f[v][i][t];									//1st term
					expr1 += (t-1)*inst.attemptCost*o[v][i][t];								//3rd term									   
				}
			}			
		}		
		
	}
    #ifndef NSpotMarket
	for(j=1;j<N-1;j++){
		for(t=1;t<T;t++){			
			expr1 += inst.p_jt[j-1][t-1]*alpha[j][t];									//4rd term
			#ifndef NBetas
			expr1 += PENALIZATION*beta[j][t];
			#endif
			#ifndef NThetas
			expr1 += PENALIZATION*theta[j][t];
			#endif 
		}
	}
    #endif
	obj.setExpr(expr1-expr);	
	model.add(obj);
	
	
	///Constraints
	//Flow balance source and sink nodes
	IloExpr expr_sinkFlow(env);
	//~ IloExpr expr_sourceFlow;
	sinkNodeBalance = IloRangeArray(env,V,1,1); 
	sourceNodeBalance = IloRangeArray(env,V,1,1);
	//First level balance
	IloExpr expr_1stLevel(env);
	IloExpr expr_1stFlow(env);
	firstLevelBalance = IloArray<IloArray<IloRangeArray> > (env, V);
	firstLevelFlow = IloArray<IloArray<IloRangeArray> > (env, V);
	
	//Second level balance
	IloExpr expr_2ndLevel(env);
	IloExpr expr_2ndFlow(env);
	secondLevelBalance = IloArray<IloArray<IloRangeArray> > (env, V);
	secondLevelFlow = IloArray<IloArray<IloRangeArray> > (env, V);
	
	//Berth Limit
	berthLimit = IloArray<IloRangeArray>(env, N-1);		//Index 0 ignored
	IloExpr expr_berth(env);
	
	//Travel at capacity and empty
	travelAtCapacity = IloArray<IloArray<IloArray<IloRangeArray> > > (env, V);
	travelEmpty = IloArray<IloArray<IloArray<IloRangeArray> > > (env, V);
	
	//Inventory balance at ports
	portInventory = IloArray<IloRangeArray> (env, N-1);
	IloExpr expr_invBalancePort(env);
	
	//Cumulative spot market
	cumSlack = IloRangeArray(env,N-1);
	IloExpr expr_cumSlack(env);
	
	//Limits on operation values
	operationLowerLimit = IloArray<IloArray<IloRangeArray> >(env,V);
	operationUpperLimit = IloArray<IloArray<IloRangeArray> >(env,V);
	
	//Flow limits
	flowCapacityX = IloArray<IloArray<IloArray<IloRangeArray> > > (env,V);
	flowCapacityO = IloArray<IloArray<IloRangeArray> >(env,V);
	flowCapacityWB = IloArray<IloArray<IloRangeArray> >(env,V);
	flowCapacityW = IloArray<IloArray<IloRangeArray> >(env,V);
			
	#ifndef NKnapsackInequalities
	knapsack_P_1 = IloArray<IloRangeArray> (env, J+1);			//Altough created for all J ports, it is only considered for loading(production) or consumption(discharging) ports
	knapsack_P_2 = IloArray<IloRangeArray> (env, J+1);	
	knapsack_D_1 = IloArray<IloRangeArray> (env, J+1);	
	knapsack_D_3 = IloArray<IloRangeArray> (env, J+1);	
	#endif
	
	for(i=1;i<N-1;i++){
		stringstream ss1;
		berthLimit[i] = IloRangeArray(env,T);
		expr_cumSlack.clear();
		portInventory[i] = IloRangeArray(env,T);				
		#ifndef NKnapsackInequalities
		if (inst.typePort[i-1] == 0){ 	//Loading port
			knapsack_P_1[i] = IloRangeArray(env, (T-3)*2);		//(T-2)*2 = Number of combinations 1,...t + t,...,T for all t \in T. Using T-3 because de increment
			knapsack_P_2[i] = IloRangeArray(env, (T-3)*2);
		}else{							//Discharging port
			knapsack_D_1[i] = IloRangeArray(env, (T-3)*2);			
			knapsack_D_3[i] = IloRangeArray(env, (T-3)*2);
		}
		int it,k,l;
		IloExpr expr_kP1_LHS(env), expr_kP2_LHS(env), expr_kD1_LHS(env); 
		double kP1_RHS, kP2_RHS, kD1_RHS, kD2_RHS;			
		for(it=0;it<(T-3)*2;it++){	//For each valid inequality
			expr_kP1_LHS.clear();
			expr_kP2_LHS.clear();
			expr_kD1_LHS.clear();
			//Definining the size of set T =[k,l]
			k=1;
			l=T-1;
			if(it<T-3)
				l = it+2;
			else
				k = it+4-l;
			for(v=0;v<V;v++){
				expr_kP1_LHS += wB[v][i][k];
				if(k>1 || hasEnteringArc1st[v][i][k-1]==1)
					expr_kD1_LHS += w[v][i][k-1] + wB[v][i][k-1] ;
				for(t=k;t<=l;t++){
					expr_kP2_LHS += o[v][i][t];	//It is used for both loading and discharging ports
					for(j=0;j<N;j++){						
						if(j==0 && inst.initialPort[v]+1 == i && t==inst.firstTimeAv[v]+1) 	//Source arc
							expr_kD1_LHS += x[v][j][i][0];								
						else if (j == N-1 && hasArc[v][i][j][t] == 1)						//Sink arc
							expr_kP1_LHS += x[v][i][j][t];
						else if (j > 0 && j<N-1){											//"Normal" arcs							 																
							if(i != j){
								if(hasArc[v][i][j][t] == 1)
									expr_kP1_LHS += x[v][i][j][t];
								if(t - inst.travelTime[v][j-1][i-1] >= 0){					//Only if it is possible an entering arc due the travel time
									if(hasArc[v][j][i][t-inst.travelTime[v][j-1][i-1]] == 1)
										expr_kD1_LHS += x[v][j][i][t-inst.travelTime[v][j-1][i-1]];										
								}
							}
						}
					}
				}
			}
			for(t=k;t<=l;t++){
				kP1_RHS += inst.d_jt[i-1][t-1];				
				kD1_RHS += inst.d_jt[i-1][t-1];
			}
			kP1_RHS += -inst.sMax_jt[i-1][0];
			kP2_RHS = ceil(kP1_RHS/inst.f_max_jt[i-1][0]);
			kP1_RHS = ceil(kP1_RHS/inst.maxVesselCapacity);
			
			if(k==1)
				kD1_RHS += -inst.s_j0[i-1] + inst.sMin_jt[i-1][0];
			else
				kD1_RHS += -inst.sMax_jt[i-1][k-1] + inst.sMin_jt[i-1][0];
			kD2_RHS = ceil(kD1_RHS/inst.f_max_jt[i-1][0]);
			kD1_RHS = ceil(kD1_RHS/inst.maxVesselCapacity);
			
			stringstream ss, ss1, ss2;
			if(inst.typePort[i-1] == 0){
				ss << "knpasackP1_" << i << "," << it;
				knapsack_P_1[i][it] = IloRange(env, kP1_RHS, expr_kP1_LHS, IloInfinity, ss.str().c_str());
				ss1 << "knapsackP2_" << i << "," << it;
				knapsack_P_2[i][it] = IloRange(env, kP2_RHS, expr_kP2_LHS, IloInfinity, ss1.str().c_str());
			}else{
				ss << "knpasackD1_" << i << "," << it;
				knapsack_D_1[i][it] = IloRange(env, kD1_RHS, expr_kD1_LHS, IloInfinity, ss.str().c_str());
				ss2 << "knpasackD3_" << i << "," << it;
				knapsack_D_3[i][it] = IloRange(env, kD2_RHS, expr_kP2_LHS, IloInfinity, ss2.str().c_str());
			}
		}
		if(inst.typePort[i-1] == 0){
			model.add(knapsack_P_1[i]);
			model.add(knapsack_P_2[i]);
		}else{
			model.add(knapsack_D_1[i]);
			model.add(knapsack_D_3[i]);
		}		
		#endif
		
		for(t=1;t<T;t++){
			expr_berth.clear();
			expr_invBalancePort.clear();
			stringstream ss, ss2;
			ss << "berthLimit_(" << i << "," << t << ")";
			bool emptyExpr = true;			
			for(v=0;v<V;v++){				
				if (hasEnteringArc1st[v][i][t]==1){ //Only if exists an entering arc in the node				
					expr_berth += o[v][i][t];
					emptyExpr = false;
				}
				expr_invBalancePort += -f[v][i][t];
			}			
			if(!emptyExpr){ //Only if there are some Z variable in the expr
				berthLimit[i][t] = IloRange(env,-IloInfinity, expr_berth, inst.b_j[i-1], ss.str().c_str());									
				model.add(berthLimit[i][t]);
			}
			#ifndef NSpotMarket
			expr_cumSlack += alpha[i][t];
			expr_invBalancePort += -alpha[i][t];
            #endif
            #ifndef NBetas
            expr_invBalancePort += -beta[i][t];
            #endif
            #ifndef NThetas
            expr_invBalancePort += theta[i][t];
            #endif
			ss2 << "invBalancePort_(" << i << "," << t << ")";
			portInventory[i][t] = IloRange(env, inst.delta[i-1]*inst.d_jt[i-1][t-1],
				sP[i][t]-sP[i][t-1]-inst.delta[i-1]*expr_invBalancePort,
				inst.delta[i-1]*inst.d_jt[i-1][t-1], ss2.str().c_str());
			model.add(portInventory[i][t]);
			
		}		
		#ifndef NSpotMarket
        ss1 << "cum_slack("<<i<<")";
		cumSlack[i] = IloRange(env, expr_cumSlack, inst.alp_max_j[i-1], ss1.str().c_str());
		model.add(cumSlack[i]);
        #endif
		
	}	
	
	expr_cumSlack.end();
	expr_berth.end();
	expr_invBalancePort.end();
	
	for(v=0;v<V;v++){
		expr_sinkFlow.clear();							
		firstLevelBalance[v] = IloArray<IloRangeArray>(env,J+1); //Only for ports - id 0 not used		
		firstLevelFlow[v] = IloArray<IloRangeArray>(env,J+1); 
		secondLevelBalance[v] = IloArray<IloRangeArray>(env,J+1); 
		secondLevelFlow[v] = IloArray<IloRangeArray>(env,J+1); 
				
		travelAtCapacity[v] = IloArray<IloArray<IloRangeArray> > (env, N-1);
		travelEmpty[v] = IloArray<IloArray<IloRangeArray> > (env, N-1);
		
		operationLowerLimit[v] = IloArray<IloRangeArray> (env,N-1);
		operationUpperLimit[v] = IloArray<IloRangeArray> (env,N-1);
		
		flowCapacityX[v] = IloArray<IloArray<IloRangeArray> >(env,N);
		flowCapacityO[v] = IloArray<IloRangeArray> (env,N-1);
		flowCapacityW[v] = IloArray<IloRangeArray> (env,N-1);
		flowCapacityWB[v] = IloArray<IloRangeArray> (env,N-1);
				
		for(i=0;i<N;i++){
			if(i>0 && i <= J){ //Only considering ports				
				firstLevelBalance[v][i] = IloRangeArray(env,T,0,0); //Id 0 is not used				
				secondLevelBalance[v][i] = IloRangeArray(env,T,0,0); 
				firstLevelFlow[v][i] = IloRangeArray(env,T,0,0); 
				secondLevelFlow[v][i] = IloRangeArray(env,T,0,0); 
				
				travelAtCapacity[v][i] = IloArray<IloRangeArray> (env, N);
				travelEmpty[v][i] = IloArray<IloRangeArray> (env, N);
				
				operationLowerLimit[v][i] = IloRangeArray (env,T);
				operationUpperLimit[v][i] = IloRangeArray (env,T);
				
				for(j=0;j<N;j++){
					if(j>0){ //Considering ports and sink node
						travelAtCapacity[v][i][j] = IloRangeArray(env, T, -IloInfinity, 0);
						travelEmpty[v][i][j] = IloRangeArray (env, T, -IloInfinity, inst.q_v[v]);
						for(t=1;t<T;t++){
							stringstream ss, ss1;
							if (j<=J){				//When j is a port
								if(inst.typePort[i-1] == 0 && inst.typePort[j-1] == 1){
									ss << "travelAtCap_(" << i << "," << j << ")_" << t << "," << v;
									travelAtCapacity[v][i][j][t].setExpr(-fX[v][i][j][t] + inst.q_v[v]*x[v][i][j][t]);	
									travelAtCapacity[v][i][j][t].setName(ss.str().c_str());
								}else if (inst.typePort[i-1] == 1 && inst.typePort[j-1] == 0){
									ss1 << "travelEmpty_(" << i << "," << j << ")_" << t << "," << v;
									travelEmpty[v][i][j][t].setExpr(inst.q_v[v]*x[v][i][j][t] + fX[v][i][j][t]);
									travelEmpty[v][i][j][t].setName(ss1.str().c_str());
								}
							}else{ // when j is the sink node
								if(inst.typePort[i-1] == 0){
									ss << "travelAtCap_(" << i << ",snk)_" << t << "," << v;
									travelAtCapacity[v][i][j][t].setExpr(-fX[v][i][j][t] + inst.q_v[v]*x[v][i][j][t]);	
									travelAtCapacity[v][i][j][t].setName(ss.str().c_str());
								}else if (inst.typePort[i-1] == 1){ 
									ss1 << "travelEmpty_(" << i << ",snk)_" << t << "," << v;
									travelEmpty[v][i][j][t].setExpr(inst.q_v[v]*x[v][i][j][t] + fX[v][i][j][t]);
									travelEmpty[v][i][j][t].setName(ss1.str().c_str());
								}
							}
							model.add(travelAtCapacity[v][i][j][t]);
							model.add(travelEmpty[v][i][j][t]);
						}						
					}
				}
				
				flowCapacityO[v][i] = IloRangeArray(env,T);
				flowCapacityW[v][i] = IloRangeArray(env,T);
				flowCapacityWB[v][i] = IloRangeArray(env,T);

				for(t=1;t<T;t++){
					stringstream ss, ss1, ss2, ss3, ss4, ss5, ss6, ss7, ss8, ss9, ss10;
					ss << "First_level_balance_" << v << "(" << i << "," << t << ")";
					ss1 << "Second_level_balance_" << v << "(" << i << "," << t << ")";
					ss2 << "link_balance_" << v << "(" << i << "," << t << ")";
					ss3 << "First_level_flow_" << v << "(" << i << "," << t << ")";
					ss4 << "Second_level_flow_" << v << "(" << i << "," << t << ")";
					
					if(hasArc[v][i][N-1][t]==1)
						expr_sinkFlow += x[v][i][N-1][t];
					
					expr_1stLevel.clear();
					expr_1stFlow.clear();
					expr_2ndLevel.clear();
					expr_2ndFlow.clear();
										
					//~ if(hasEnteringArc1st[v][i][t]==1){
						for(j=0;j<N;j++){
							if(j<N-1){ //No consider sink arc (first level balance)
								//If j is the source node and reach i in time t
								if(j==0 && inst.initialPort[v]+1 == i && t==inst.firstTimeAv[v]+1){
									expr_1stLevel += x[v][j][i][0];							
									expr_1stFlow += fX[v][j][i][0];
									fX[v][j][i][0].setBounds(inst.s_v0[v], inst.s_v0[v]); //Fixing initial inventory 
									//~ break; //If is the entering arc of vessel, there is no other arc entering port i for vessel v
								}
								else if (j>0){ //When j is a port
									if (t - inst.travelTime[v][j-1][i-1] >= 0){ //If is possible to exist an arc from j to i										
										if(hasArc[v][j][i][t-inst.travelTime[v][j-1][i-1]] == 1){ //If the arc exists
											expr_1stLevel += x[v][j][i][t-inst.travelTime[v][j-1][i-1]];									
											expr_1stFlow += fX[v][j][i][t-inst.travelTime[v][j-1][i-1]];
										}
									}
								}
							}
							if(j>0){ //No consider source arc (second level balance)
								//~ cout << v << " " << i << " " << j << " " << t << " " << hasArc[v][i][j][t] << endl;
								if (hasArc[v][i][j][t] == 1){
									//~ cout << v << " " << i << " " << j << " " << t << endl;
									expr_2ndLevel += x[v][i][j][t];
									expr_2ndFlow += fX[v][i][j][t];
								}
							}
						}						
						if (t==1 || hasEnteringArc1st[v][i][t-1]==0){ //First time period or not entering arc in the previous time period
							expr_1stLevel += - w[v][i][t] - o[v][i][t];
							expr_2ndLevel += -o[v][i][t] + wB[v][i][t];
							expr_1stFlow+= -fW[v][i][t] - fO[v][i][t];							
							expr_2ndFlow += -fO[v][i][t] - inst.delta[i-1]*f[v][i][t] + fWB[v][i][t];
						}else if (t==T-1){ //Last time period
							expr_1stLevel += w[v][i][t-1] - o[v][i][t] + wB[v][i][t-1];
							expr_2ndLevel += -o[v][i][t];
							expr_1stFlow += fW[v][i][t-1] + fWB[v][i][t-1]-fO[v][i][t];
							expr_2ndFlow += -fO[v][i][t] - inst.delta[i-1]*f[v][i][t];
						}else{ //Other times
							expr_1stLevel += w[v][i][t-1] - w[v][i][t] - o[v][i][t] + wB[v][i][t-1];
							expr_2ndLevel += - o[v][i][t] + wB[v][i][t];							
							expr_1stFlow += fW[v][i][t-1] - fW[v][i][t] - fO[v][i][t] + fWB[v][i][t-1];
							expr_2ndFlow += -fO[v][i][t] - inst.delta[i-1]*f[v][i][t] + fWB[v][i][t];
						}
						
						firstLevelBalance[v][i][t].setExpr(expr_1stLevel);
						secondLevelBalance[v][i][t].setExpr(expr_2ndLevel);

						firstLevelFlow[v][i][t].setExpr(expr_1stFlow);
						secondLevelFlow[v][i][t].setExpr(expr_2ndFlow);
						
						firstLevelBalance[v][i][t].setName(ss.str().c_str());
						model.add(firstLevelBalance[v][i][t]);
						
						secondLevelBalance[v][i][t].setName(ss1.str().c_str());
						model.add(secondLevelBalance[v][i][t]);
						
						firstLevelFlow[v][i][t].setName(ss3.str().c_str());
						model.add(firstLevelFlow[v][i][t]);
						
						secondLevelFlow[v][i][t].setName(ss4.str().c_str());						
						model.add(secondLevelFlow[v][i][t]);
					
						ss5 << "fjmin_(" << i << "," << t << ")," << v;
						ss6 << "fjmax_(" << i << "," << t << ")," << v;
						IloNum minfMax = min(inst.f_max_jt[i-1][t-1], inst.q_v[v]);
						operationLowerLimit[v][i][t] = IloRange(env, 0, f[v][i][t] - inst.f_min_jt[i-1][t-1]*o[v][i][t] , IloInfinity, ss5.str().c_str());
						model.add(operationLowerLimit[v][i][t]);
						
						operationUpperLimit[v][i][t] = IloRange(env, -IloInfinity, f[v][i][t] - minfMax*o[v][i][t],	0, ss6.str().c_str());				
						model.add(operationUpperLimit[v][i][t]);
						
						ss7 << "flowLimitO_"<<v<<","<<i<<","<<t;					
						flowCapacityO[v][i][t] = IloRange(env, -IloInfinity, fO[v][i][t]-inst.q_v[v]*o[v][i][t], 0, ss7.str().c_str());					
						model.add(flowCapacityO[v][i][t]);					
						
						if(t<T-1){ //Constraints with no last time index
							ss9 << "flowLimitW_"<<v<<","<<i<<","<<t;							
							flowCapacityW[v][i][t] = IloRange(env, -IloInfinity, fW[v][i][t]-inst.q_v[v]*w[v][i][t], 0, ss9.str().c_str());							
							model.add(flowCapacityW[v][i][t]);
							ss10 << "flowLimitWB_"<<v<<","<<i<<","<<t;
							flowCapacityWB[v][i][t] = IloRange(env, -IloInfinity, fWB[v][i][t]-inst.q_v[v]*wB[v][i][t], 0, ss10.str().c_str());
							model.add(flowCapacityWB[v][i][t]);
							
						}
					//~ }
				}				
			
			}			
			flowCapacityX[v][i] = IloArray<IloRangeArray>(env,N);
			for(j=0;j<N;j++){
				if(i != j){
					flowCapacityX[v][i][j] = IloRangeArray(env,T);
					for(t=0;t<T;t++){
						stringstream ss;
						ss << "flowLimitX_" << v << "," << i << "," << j << "," << t;
						flowCapacityX[v][i][j][t] = IloRange(env, -IloInfinity, fX[v][i][j][t] - inst.q_v[v]*x[v][i][j][t], 0, ss.str().c_str());
					}
					model.add(flowCapacityX[v][i][j]);
				}
			}
		
		}		
		stringstream ss;
		ss << "flowBalanceSink_" << v;
		sinkNodeBalance[v].setExpr(expr_sinkFlow);
		sinkNodeBalance[v].setName(ss.str().c_str());
		model.add(sinkNodeBalance);
	}
	expr_1stLevel.end();
	expr_2ndLevel.end();
	expr_2ndLevel.end();
	expr_2ndFlow.end();
	expr_sinkFlow.end();
	
	
	#ifndef NBranching
		
	#endif	
	
	#ifndef NValidInequalities
	
	#endif	
	
	#ifndef NOperateAndGo
	
	#endif	
	
	#ifndef N2PortNorevisit

	#endif
}

void Model::printSolution(IloEnv env, Instance inst, const int& tF){
	int i,j,t,v,a;
	int J = inst.J;
	int T = inst.t + 1;			//Init time-periods in 1
	int V = inst.speed.getSize(); //# of vessels
	int N = J + 2; // Number of nodes including source and sink. (0,1...j,j+1)
	
	double costArc, revenue, costSpot, costAtempt;
	
	//Store binary variables
	xValue = IloArray<IloArray<IloArray<IloNumArray> > >(env,V) ;
	oValue = IloArray<IloArray<IloNumArray> >(env,V);
	wValue = IloArray<IloArray<IloNumArray> >(env,V);
	wBValue = IloArray<IloArray<IloNumArray> >(env,V);
	//Store fractional variables
	fValue = IloArray<IloArray<IloNumArray> >(env,V);
	fXValue = IloArray<IloArray<IloArray<IloNumArray> > >(env,V);
	fOValue = IloArray<IloArray<IloNumArray> > (env,V);
	fWValue = IloArray<IloArray<IloNumArray> >(env,V);
	fWBValue = IloArray<IloArray<IloNumArray> >(env,V);
	sPValue = IloArray<IloNumArray>(env,N-1);
	alphaValue = IloArray<IloNumArray>(env,N-1);
	betaValue = IloArray<IloNumArray>(env,N-1);
	thetaValue = IloArray<IloNumArray>(env,N-1);
	
	for (v=0;v<V;v++){
		xValue[v] = IloArray<IloArray<IloNumArray>>(env, N);
		oValue[v] = IloArray<IloNumArray>(env,N-1);
		wValue[v] = IloArray<IloNumArray>(env,N-1);		
		wBValue[v] = IloArray<IloNumArray>(env,N-1);
		
		fXValue[v] = IloArray<IloArray<IloNumArray>>(env, N);
		fValue[v] = IloArray<IloNumArray>(env,N-1);
		fOValue[v] = IloArray<IloNumArray>(env,N-1);
		fWBValue[v] = IloArray<IloNumArray>(env,N-1);
		fWValue[v] = IloArray<IloNumArray>(env,N-1);		
		
		for (i=0;i<N;i++){
			if(i > 0 && i < N-1){ //Not consider sink node
				oValue[v][i] = IloNumArray(env,T);
				wValue[v][i] = IloNumArray(env,T);
				wBValue[v][i] = IloNumArray(env,T);
				
				fValue[v][i] = IloNumArray(env,T);
				fOValue[v][i] = IloNumArray(env,T);
				fWBValue[v][i] = IloNumArray(env,T);
				fWValue[v][i] = IloNumArray(env,T);
				
				for(t=1;t<T;t++){
					if(hasEnteringArc1st[v][i][t]){
						if(ageO[v][i][t]!= -1){
							oValue[v][i][t] = cplex.getValue(o[v][i][t]);
							fOValue[v][i][t] = cplex.getValue(fO[v][i][t]);
							if(oValue[v][i][t]>=0.1){
								costAtempt += (t-1)*inst.attemptCost;
								fValue[v][i][t] = cplex.getValue(f[v][i][t]);
								revenue += fValue[v][i][t]*inst.r_jt[i-1][t-1];
							}
						}						
						if(t<T-1){ //Variables not associated with last time index
							if(ageW[v][i][t]!=-1){
								wValue[v][i][t] = cplex.getValue(w[v][i][t]);
								fWValue[v][i][t] = cplex.getValue(fW[v][i][t]);
							}
							if(ageWB[v][i][t]!=-1){
								wBValue[v][i][t] = cplex.getValue(wB[v][i][t]);							
								fWBValue[v][i][t] = cplex.getValue(fWB[v][i][t]);	
							}
						}
					}
				}
			}
			xValue[v][i] = IloArray<IloNumArray>(env,N);
			fXValue[v][i] = IloArray<IloNumArray>(env,N);
			for(j=0;j<N;j++){
				xValue[v][i][j] = IloNumArray(env,T);
				fXValue[v][i][j] = IloNumArray(env,T);
				for(t=0;t<T;t++){					
					if(hasArc[v][i][j][t] == 1 && ageX[v][i][j][t]!=-1){ // Only if the arc exists
						xValue[v][i][j][t] = cplex.getValue(x[v][i][j][t]);
						fXValue[v][i][j][t] = cplex.getValue(fX[v][i][j][t]);
						if(xValue[v][i][j][t] >= 0.1){
							costArc += arcCost[v][i][j][t];
						}
					}					
				}
			}
		}	
	}

	//Printing solution vessel routes
	cout << "SOLUTION LOG: \n";
	for(v=0;v<V;v++){
		cout << "Vessel " << v << " capacity = " << inst.q_v[v] << " route: " << endl;
		//Starting point
		j = inst.initialPort[v]+1;
		if(xValue[v][0][j][0] >= 0.1)
			cout << "(" << j << "," << inst.firstTimeAv[v]+1 << ") - Load on board " << fXValue[v][0][j][0];
		else
			cout << "ERROR: Vessel not started at defined node. \n";
		i = j;
		t = inst.firstTimeAv[v]+1;
		//While not reach the sink node, Verify if wait,operate or wait after operate
		while(i != N-1){
			ifOperate:
			//~ cout << "[" << v << "," << i << "," << t << "]\n";
			//Verify if operates
			if(oValue[v][i][t] >= 0.1){
				if (inst.delta[i-1] == 1)
					cout << " - Loaded " << fValue[v][i][t];
				else
					cout << " - Discharged " << fValue[v][i][t];
				if(fValue[v][i][t] > inst.f_max_jt[i-1][0] || fValue[v][i][t] < inst.f_min_jt[i-1][0])
					cout << " Error: Operated a value greater than F^Max or lesser than F^Min";
				cout << endl;
				//if wait after operate
				if(wBValue[v][i][t] >= 0.1){
					cout << "(" << i << "," << t+1 << ") - Load onboard " << fWBValue[v][i][t];
					t++; 
					goto ifOperate;
				}
			}
			//Verify if waits
			if(wValue[v][i][t] >= 0.1){
				cout << endl << "(" << i << "," << t+1 << ") - Load onboard " << fWValue[v][i][t];
				t++;
				goto ifOperate;
			}			
			//If travel to another port
			for(j=1;j<N;j++){
				if(j!=i){
					if(xValue[v][i][j][t] >= 0.1){
						if(j<=inst.J){
							cout << "(" << j << "," << t+inst.travelTime[v][i-1][j-1] << ") - Load onboard " << fXValue[v][i][j][t];
							t = t + inst.travelTime[v][i-1][j-1];
						}
						i=j;						
						break;
					}				
				}
			}
		}
	}
	for(i=1;i<=J;i++){
		sPValue[i] = IloNumArray(env,T);
		alphaValue[i] = IloNumArray(env,T);
		betaValue[i] = IloNumArray(env,T);
		thetaValue[i] = IloNumArray(env,T);
		float sumAlpha = 0;
		cout << "Port inventory " << i << " S_0 " << inst.s_j0[i-1] << " D_i0 " << inst.d_jt[i-1][0] << " S^Max " << inst.sMax_jt[i-1][0] 
         << " F^Min " << inst.f_min_jt[i-1][0] << " F^Max " << inst.f_max_jt[i-1][0] << " Max cum alpha " << inst.alp_max_j[i-1] 
        << " Max alpha/t " << inst.alp_max_jt[i-1][0] << endl;
		for(t=0;t<T;t++){
			sPValue[i][t] = cplex.getValue(sP[i][t]);
			cout << "(" << i << "," << t << "): " << sPValue[i][t];
			if(t>0){
				alphaValue[i][t] = cplex.getValue(alpha[i][t]);
				#ifndef NBetas
				betaValue[i][t] = cplex.getValue(beta[i][t]);
				#endif
				#ifndef NThetas
				thetaValue[i][t] = cplex.getValue(theta[i][t]);
				#endif
				if (alphaValue[i][t] >= 0.01){
					costSpot += alphaValue[i][t]*inst.p_jt[i-1][t-1];
					cout << " Alpha: " << alphaValue[i][t];
					sumAlpha += alphaValue[i][t];
					if(sumAlpha > inst.alp_max_j[i-1]){
						cout << " ERROR! Violated the maximum cumulative alpha!";
					}
				}
				#ifndef NBetas
				if(betaValue[i][t] >= 0.01){
					costSpot += PENALIZATION*betaValue[i][t];
					cout << " Beta: " << betaValue[i][t];
				}
				#endif
				#ifndef NThetas
				if(thetaValue[i][t] >= 0.01){
					costSpot += PENALIZATION*thetaValue[i][t];
					cout << " Theta: " << thetaValue[i][t];
				}
				#endif
			}
			if(sPValue[i][t] > inst.sMax_jt[i-1][0] || sPValue[i][t] < inst.sMin_jt[i-1][0])
				cout << " - Inventory out of bounds!";
			cout << endl;
				
		}
	}
	
	cout << "Final value: " << costArc + costAtempt + costSpot - revenue << endl;
	cout << "Arcs: " << costArc << endl <<
			"Attempts: " << costAtempt << endl <<
			"Spots: " << costSpot << endl <<
			"Revenue: " << revenue << endl;

}
void Model::setParameters(IloEnv& env, const double& timeLimit, double gap){
	//~ cplex.exportModel("mip_original.lp");
	//~ env.setNormalizer(IloFalse);
	
	//Pressolve
	//~ cplex.setParam(IloCplex::PreInd,0);
	//~ cplex.setParam(IloCplex::RelaxPreInd,0);
	//~ cplex.setParam(IloCplex::PreslvNd,-1);
	
	//Cuts
	//~ cplex.setParam(IloCplex::Covers, 3);		//Max 3
	//~ cplex.setParam(IloCplex::GUBCovers, 2);		//Max 2
	//~ cplex.setParam(IloCplex::FlowCovers, 2);	//Max 2
	//~ cplex.setParam(IloCplex::Cliques, 3);		//Max 3
	//~ cplex.setParam(IloCplex::FracCuts, 2);		//Max 2
	//~ cplex.setParam(IloCplex::DisjCuts, 3);		//Max 3
	//~ cplex.setParam(IloCplex::FlowPaths, 2);		//Max 2
	//~ cplex.setParam(IloCplex::ImplBd, 2);		//Max 2
	//~ cplex.setParam(IloCplex::MIRCuts, 2);		//Max 2
	//~ cplex.setParam(IloCplex::MCFCuts, 2);		//Max 2
	//~ cplex.setParam(IloCplex::ZeroHalfCuts, 2);	//Max 2
	
	//~ cplex.setOut(env.getNullStream());
	//~ cplex.setWarning(env.getNullStream());
	cplex.setParam(IloCplex::Threads, 1);
	//~ cplex.setParam(IloCplex::ConflictDisplay, 2); 
	//~ cplex.setParam(IloCplex::MIPDisplay, 1); 
	//~ cplex.setParam(IloCplex::MIPEmphasis, 4); // RINS
	//~ cplex.setParam(IloCplex::DiveType, 3); // Guied dive
	cplex.setParam(IloCplex::WorkMem, 8192);
	cplex.setParam(IloCplex::NodeFileInd, 2);
	cplex.setParam(IloCplex::WorkDir, "workDir/");
	cplex.setParam(IloCplex::ClockType, 2);
	cplex.setParam(IloCplex::TiLim, timeLimit);
	//~ cplex.setParam(IloCplex::MIPEmphasis, 1); // 1 - Feasibility
	
	//~ cplex.setParam(IloCplex::NodeLim, 1);
	
	if (gap > 1e-04)
		cplex.setParam(IloCplex::EpGap, gap/100);
	//~ cplex.setParam(IloCplex::IntSolLim, 1); //Set number of solutions which must be found before stopping
	
	//Define high priority on the branching vairables
	#ifndef NBranching	
	for (int j=0;j<y.getSize();j++){
		cplex.setPriority(y[j],1);
	}	
	#endif
}

/*
 * Reads the n best routes of each vessel (with different cost) and identify the corresponding binary variable used
 * If age is equals to -1 (not used), set it to 0, meaning that it will be added to the model by the 
 * method addComponentsToModel
  */ 
void Model::setRoute(Instance inst, 
	    vector< priority_queue< pair<unsigned int, float>, vector<pair<unsigned int,float>>, ComparePairMinorFloatFirst> >& vesselRoutes, 
	    const vector<Solution>& solutions, const unsigned int& n, const bool& addAdjacents){
	unsigned int v;
	///Get the routes with the best cost and add them to the model
	for(v=0;v<inst.V;v++){		
		//~ cout << "Routes vessel " << v << endl;
		int rId = 0;
		float rCost = 0;
		int count = 0;
		while(!vesselRoutes[v].empty()){
			//Verify if the route has a different value from the previous added
			if(vesselRoutes[v].top().second != rCost){
				rId = vesselRoutes[v].top().first;
				rCost = vesselRoutes[v].top().second;				
				//~ cout << rId << ": " << rCost << endl;
				unsigned int i, t, prevI=999, prevT=999;
				bool prevOperated=false,operated=false;
				for(int r=0;r<solutions[rId].vesselRoute[v].size();r++){
					i = get<0>(solutions[rId].vesselRoute[v][r])+1;
					t = get<1>(solutions[rId].vesselRoute[v][r])+1;
					if (r==0){ //Fixed first port						
						if(ageX[v][0][inst.initialPort[v]+1][0]==-1){
							ageX[v][0][inst.initialPort[v]+1][0] = 0;
						}
						//~ cout << "Start (0,0) -> (" << i << "," << t << ")";
						if(addAdjacents){
							//Operating
							if(ageO[v][i][inst.firstTimeAv[v]+1] == -1){
								ageO[v][i][inst.firstTimeAv[v]+1] = 0;
							}
							//Waiting
							if(ageW[v][i][inst.firstTimeAv[v]+1] == -1){
								ageW[v][i][inst.firstTimeAv[v]+1] = 0;
							}
							//WaitAfterOperate
							if(ageWB[v][i][inst.firstTimeAv[v]+1] == -1){
								ageWB[v][i][inst.firstTimeAv[v]+1] = 0;
							}
							//Travelling arcs (only between different regions with delta !=
							for(unsigned int j=1;j<=inst.J;j++){
								if(j != i && (inst.delta[j-1] != inst.delta[i-1] || inst.idRegion[j-1] == inst.idRegion[i-1]) ){
									if(ageX[v][i][j][inst.firstTimeAv[v]+1] == -1){
										ageX[v][i][j][inst.firstTimeAv[v]+1] = 0;
									}
								}
							}
							//Sink node
							if(ageX[v][i][inst.J+1][inst.firstTimeAv[v]+1] == -1){
								ageX[v][i][inst.J+1][inst.firstTimeAv[v]+1] = 0;
							}
						}
						
					}else{
						if(prevI == i){ //Waiting
							if(addAdjacents){
								//Operating
								if(ageO[v][i][t] == -1){
									ageO[v][i][t] = 0;
								}
								
								if(t < inst.t){
									//Waiting
									if(ageW[v][i][t] == -1){
										ageW[v][i][t] = 0;
									}
									//WaitAfterOperate
									if(ageWB[v][i][t] == -1){
										ageWB[v][i][t] = 0;
									}
									//Travelling arcs (only between different regions with delta !=
									for(unsigned int j=1;j<=inst.J;j++){
										if(j != i && (inst.delta[j-1] != inst.delta[i-1] || inst.idRegion[j-1] == inst.idRegion[i-1]) ){
											if(inst.travelTime[v][i-1][j-1]+t <= inst.t){
												if(ageX[v][i][j][t] == -1){
													ageX[v][i][j][t] = 0;
												}
											}
										}
									}
								}
								//Sink node
								if(ageX[v][i][inst.J+1][t] == -1){
									ageX[v][i][inst.J+1][t] = 0;
								}
							}else{ //No adjacent variables
								if(prevOperated){
									if(ageWB[v][i][prevT] == -1){
										ageWB[v][i][prevT] = 0;
									}
									//~ cout << "Wait after op.: (" << prevI << "," << prevT << ") -> (" << i << "," << t << ")";	
								}else{
									if(ageW[v][i][prevT] == -1){
										ageW[v][i][prevT] = 0;
									}
									//~ cout << "Wait: (" << prevI << "," << prevT << ") -> (" << i << "," << t << ")";	
								}
							}
						}else{ //Traveling
							if(addAdjacents){ 
								//Travelling arcs from prevI (only between different regions with delta !=)
								for(unsigned int j=1;j<=inst.J;j++){
									if(j != prevI && (inst.delta[j-1] != inst.delta[prevI-1] || inst.idRegion[j-1] == inst.idRegion[prevI-1]) ){
										if(prevT + inst.travelTime[v][prevI-1][j-1] <= inst.t){
											if(ageX[v][prevI][j][prevT] == -1){
												ageX[v][prevI][j][prevT] = 0;
											}
										}
									}
								}
								//Sink node from prevI
								if(ageX[v][prevI][inst.J+1][prevT] == -1){
									ageX[v][prevI][inst.J+1][prevT] = 0;
								}
								/// Adjacent arcs of the current i
								//Operating
								if(ageO[v][i][t] == -1){
									ageO[v][i][t] = 0;
								}
								if(t < inst.t){
									//Waiting
									if(ageW[v][i][t] == -1){
										ageW[v][i][t] = 0;
									}
									//WaitAfterOperate
									if(ageWB[v][i][t] == -1){
										ageWB[v][i][t] = 0;
									}
									//Travelling arcs (only between different regions with delta !=
									for(unsigned int j=1;j<=inst.J;j++){
										if(j != i && (inst.delta[j-1] != inst.delta[i-1] || inst.idRegion[j-1] == inst.idRegion[i-1]) ){
											if(inst.travelTime[v][i-1][j-1]+t <= inst.t){
												if(ageX[v][i][j][t] == -1){
													ageX[v][i][j][t] = 0;
												}
											}
										}
									}
								}
								//Sink node
								if(ageX[v][i][inst.J+1][t] == -1){
									ageX[v][i][inst.J+1][t] = 0;
								}
							}else{ //Without adjacent arcs
								if(ageX[v][prevI][i][prevT] == -1){
									ageX[v][prevI][i][prevT] = 0;
								}
								//~ cout << "Travel: (" << prevI << "," << prevT << ") -> (" << i << "," << t << ")";	
							}
						}
					}
					//Operations - if f value is greater than 0
					if(!addAdjacents){
						if(get<2>(solutions[rId].vesselRoute[v][r]) > 0.01){
							if(ageO[v][i][t] == -1){
								ageO[v][i][t] = 0;
							}
							operated = true;
						}
						if(r==solutions[rId].vesselRoute[v].size()-1){
							//~ cout << endl << "End route (" << prevI << "," << prevT << ") -> (sink)";
							if(ageX[v][i][inst.J+1][t] == -1){
								ageX[v][i][inst.J+1][t] = 0;
							}
						}
						prevOperated = operated;										
					}
					prevI = i;
					prevT = t;					
					//~ cout << endl;
				}				
				count++;
			}
			if(count == n){ //number of routes added to the model
				break;
			}else{
				vesselRoutes[v].pop();
			}
		}
	}
}
/*
 * From the n best solutions (with different obj), set the variables that will be added to the model.
 * Change age to 0, if it is equal to -1 only (not previous added to the model)
 * If addAdjacents, all adjacents arcs of the current read arc are also added to the model
 * Obs: The corresponding indexes i and t starts in 1 in the FCNF model (at metaheuristic starts in 0)
 */
void Model::setSolutions(Instance inst, const std::vector<Solution>& solutions, priority_queue< pair<unsigned int, float>, vector<pair<unsigned int,float>>, ComparePairMinorFloatFirst>& solutionsObj, const unsigned int& n,const bool& addAdjacents){
	unsigned int v, s=0;
	unsigned int T=inst.t+1;
	unsigned int idSol;
	float obj, prevObj=0; 
	while(s<n && solutionsObj.size()>0){ //For each solution		
		unsigned int i, t, prevI=999, prevT=999;
		bool prevOperated=false;
		idSol = solutionsObj.top().first;
		obj = solutionsObj.top().second;
		solutionsObj.pop();
		if(obj != prevObj){ //For adding only different solutions
			s++;
			//~ cout << ">>>>>Adding solution " << idSol << " obj " << obj << " to the model\n";			
			for(v=0;v<inst.V;v++){
				for(int r=0;r<solutions[idSol].vesselRoute[v].size();r++){
					i = get<0>(solutions[idSol].vesselRoute[v][r])+1;
					t = get<1>(solutions[idSol].vesselRoute[v][r])+1;
					if (r==0){ //Fixed first port
						if(ageX[v][0][inst.initialPort[v]+1][0] == -1){
							ageX[v][0][inst.initialPort[v]+1][0] = 0;
						}
						//~ cout << "Start (0,0) -> (" << i << "," << t << ")";
						if(addAdjacents){
							//Operating
							if(ageO[v][i][inst.firstTimeAv[v]+1] == -1){
								ageO[v][i][inst.firstTimeAv[v]+1] = 0;
							}
							//Waiting
							if(ageW[v][i][inst.firstTimeAv[v]+1] == -1){
								ageW[v][i][inst.firstTimeAv[v]+1] = 0;
							}
							//WaitAfterOperate
							if(ageWB[v][i][inst.firstTimeAv[v]+1] == -1){
								ageWB[v][i][inst.firstTimeAv[v]+1] = 0;
							}
							//Travelling arcs (only between different regions with delta !=
							for(unsigned int j=1;j<=inst.J;j++){
								if(j != i && (inst.delta[j-1] != inst.delta[i-1] || inst.idRegion[j-1] == inst.idRegion[i-1]) ){
									if(ageX[v][i][j][inst.firstTimeAv[v]+1] == -1){
										ageX[v][i][j][inst.firstTimeAv[v]+1] = 0;
									}
								}
							}
							//Sink node
							if(ageX[v][i][inst.J+1][inst.firstTimeAv[v]+1] == -1){
								ageX[v][i][inst.J+1][inst.firstTimeAv[v]+1] = 0;
							}
						}
					}else{ //Not first node of vessel
						if(prevI == i){ //Waiting
							if(addAdjacents){
								//Operating
								if(ageO[v][i][t] == -1){
									ageO[v][i][t] = 0;
								}
								
								if(t < inst.t){
									//Waiting
									if(ageW[v][i][t] == -1){
										ageW[v][i][t] = 0;
									}
									//WaitAfterOperate
									if(ageWB[v][i][t] == -1){
										ageWB[v][i][t] = 0;
									}
									//Travelling arcs (only between different regions with delta !=
									for(unsigned int j=1;j<=inst.J;j++){
										if(j != i && (inst.delta[j-1] != inst.delta[i-1] || inst.idRegion[j-1] == inst.idRegion[i-1]) ){
											if(inst.travelTime[v][i-1][j-1]+t <= inst.t){
												if(ageX[v][i][j][t] == -1){
													ageX[v][i][j][t] = 0;
												}
											}
										}
									}
								}
								//Sink node
								if(ageX[v][i][inst.J+1][t] == -1){
									ageX[v][i][inst.J+1][t] = 0;
								}
							}else{ //No adjacent variables
								if(prevOperated){
									if(ageWB[v][i][prevT] == -1){
										ageWB[v][i][prevT] = 0;
									}
									//~ cout << "Wait after op.: (" << prevI << "," << prevT << ") -> (" << i << "," << t << ")";	
								}else{
									if(ageW[v][i][prevT] == -1){
										ageW[v][i][prevT] = 0;
									}
									//~ cout << "Wait: (" << prevI << "," << prevT << ") -> (" << i << "," << t << ")";	
								}
							}
						}else{ //Traveling
							if(addAdjacents){ 
								//Travelling arcs from prevI (only between different regions with delta !=)
								for(unsigned int j=1;j<=inst.J;j++){
									if(j != prevI && (inst.delta[j-1] != inst.delta[prevI-1] || inst.idRegion[j-1] == inst.idRegion[prevI-1]) ){
										if(prevT + inst.travelTime[v][prevI-1][j-1] <= inst.t){
											if(ageX[v][prevI][j][prevT] == -1){
												ageX[v][prevI][j][prevT] = 0;
											}
										}
									}
								}
								//Sink node from prevI
								if(ageX[v][prevI][inst.J+1][prevT] == -1){
									ageX[v][prevI][inst.J+1][prevT] = 0;
								}
								/// Adjacent arcs of the current i
								//Operating
								if(ageO[v][i][t] == -1){
									ageO[v][i][t] = 0;
								}
								if(t < inst.t){
									//Waiting
									if(ageW[v][i][t] == -1){
										ageW[v][i][t] = 0;
									}
									//WaitAfterOperate
									if(ageWB[v][i][t] == -1){
										ageWB[v][i][t] = 0;
									}
									//Travelling arcs (only between different regions with delta !=
									for(unsigned int j=1;j<=inst.J;j++){
										if(j != i && (inst.delta[j-1] != inst.delta[i-1] || inst.idRegion[j-1] == inst.idRegion[i-1]) ){
											if(inst.travelTime[v][i-1][j-1]+t <= inst.t){
												if(ageX[v][i][j][t] == -1){
													ageX[v][i][j][t] = 0;
												}
											}
										}
									}
								}
								//Sink node
								if(ageX[v][i][inst.J+1][t] == -1){
									ageX[v][i][inst.J+1][t] = 0;
								}

							}else{ //Without adjacent arcs
								if(ageX[v][prevI][i][prevT] == -1){
									ageX[v][prevI][i][prevT] = 0;
								}
							}
							//~ cout << "Travel: (" << prevI << "," << prevT << ") -> (" << i << "," << t << ")";	
						}
					}
					prevI = i;
					prevT = t;
					//Operations - if f value is greater than 0
					if(!addAdjacents){
						bool operated=false;
						if(get<2>(solutions[idSol].vesselRoute[v][r]) > 0.01){
							if(ageO[v][i][t] == -1){
								ageO[v][i][t] = 0;
							}
							operated=true;
						}
						if(r==solutions[idSol].vesselRoute[v].size()-1){
							//~ cout << endl << "End route (" << prevI << "," << prevT << ") -> (sink)";
							if(ageX[v][i][inst.J+1][t] == -1){
								ageX[v][i][inst.J+1][t] = 0;
							}
						}
						prevOperated = operated;
					}
					//~ cout << endl;
				}	
			}
			prevObj = obj;
		}			
	}
	
}
/* Get the solution values and set as a complete MIP-Start for the model */
void Model::setMIPStart(IloEnv& env, Instance inst, const Solution& solution){
	unsigned int v, s=0;
	unsigned int T=inst.t+1;	
	unsigned int i, j, t, prevI=999, prevT=999;
	bool prevOperated=false;
		
	//Getting variable values
	for(v=0;v<inst.V;v++){
		for(int r=0;r<solution.vesselRoute[v].size();r++){
			i = get<0>(solution.vesselRoute[v][r])+1;
			t = get<1>(solution.vesselRoute[v][r])+1;
			if (r==0){ //Fixed first port
				xValue[v][0][inst.initialPort[v]+1][0] = 1;				
			}else{ //Not first node of vessel
				if(prevI == i){ //Waiting
					if(prevOperated){
						wBValue[v][i][prevT] = 1;
					}else{
						wValue[v][i][prevT] = 1;
					}
				}else{ //Traveling
					xValue[v][prevI][i][prevT] = 1;
				}				
			}
			prevI = i;
			prevT = t;
			//Operations - if f value is greater than 0
			bool operated=false;
			if(get<2>(solution.vesselRoute[v][r]) > 0.01){
				oValue[v][i][t] = 1;
				operated=true;
			}
			if(r==solution.vesselRoute[v].size()-1){
				xValue[v][i][inst.J+1][t] = 1;
				//~ cout << endl << "End route (" << prevI << "," << prevT << ") -> (sink)";
			}
			prevOperated = operated;

		}	
	}
	//Setting the value of the variables
	IloNumVarArray startVar(env);
	IloNumArray startVal(env);
	for(v=0;v<inst.V;v++){
		for(i=0;i<=inst.J;i++){
			for(j=1;j<=inst.J+1;j++){
				for(t=0;t<T;t++){
					if(hasArc[v][i][j][t] == 1){
						//~ cout << x[v][i][j][t].getName() << " = " << xValue[v][i][j][t] << endl;
						startVar.add(x[v][i][j][t]);
						startVal.add(xValue[v][i][j][t]);						
					}
					if(j==1 && i >= 1){ //For <v,i,t> variables
						if(hasEnteringArc1st[v][i][t]==1){
							startVar.add(o[v][i][t]);
							startVal.add(oValue[v][i][t]);							
							//~ cout << o[v][i][t].getName() << " = " << oValue[v][i][t] << endl;
							if(t<T-1){
								startVar.add(w[v][i][t]);
								startVal.add(wValue[v][i][t]);
								//~ cout << w[v][i][t].getName() << " = " << wValue[v][i][t] << endl;
								
								startVar.add(wB[v][i][t]);
								startVal.add(wBValue[v][i][t]);
								//~ cout << wB[v][i][t].getName() << " = " << wBValue[v][i][t] << endl;
							}							
						}
					}
					
				}
			}
		}
	}
	cplex.addMIPStart(startVar,startVal);
	startVar.end();
	startVal.end();
}

/*
 * Use the instance information to create and store the arcs and nodes for after adding them to the model
 */
void Model::buildNetwork(IloEnv& env, Instance inst){
	int i,j,t,v,a;
	int J = inst.J;
	int T = inst.t + 1;			//Init time-periods in 1
	int V = inst.V; //# of vessels
	int N = J + 2; // Number of nodes including source and sink. (0,1...j,j+1)
	
	//Init Variables arrays and info arrays - not add them to the model (except conversion if using relaxation)
	#ifndef NSpotMarket
	alpha = NumVarMatrix(env, N);
	#endif
	#ifndef NBetas
	beta = NumVarMatrix(env, N);
	#endif
	#ifndef NThetas
	theta = NumVarMatrix(env, N);
	#endif
	sP = NumVarMatrix(env,N);
	f = IloArray<NumVarMatrix> (env, V);
	fX = IloArray<IloArray<NumVarMatrix> >(env, V);
	fO = IloArray<NumVarMatrix> (env, V);
	fW = IloArray<NumVarMatrix> (env, V);		
	fWB = IloArray<NumVarMatrix> (env, V);	
	
	hasArc = IloArray<IloArray<IloArray<IloIntArray> > >(env, V);
	arcCost = IloArray<IloArray<IloArray<IloNumArray> > >(env, V);
	hasEnteringArc1st = IloArray<IloArray<IloIntArray> >(env, V);
	
	x = IloArray<IloArray<IloArray<IloBoolVarArray> > >(env, V);
	o = IloArray<IloArray<IloBoolVarArray> >(env,V);
	w = IloArray<IloArray<IloBoolVarArray> >(env,V);
	wB = IloArray<IloArray<IloBoolVarArray> >(env,V);	
	
	//Age of variables for the CSMA algorithm - Initially = -1: not added to the model
	ageX = vector<vector<vector<vector<int>>>>(V) ; 
	ageO = vector<vector<vector<int>>>(V);
	ageW = vector<vector<vector<int>>>(V);
	ageWB = vector<vector<vector<int>>>(V);
	
	
	for(v=0;v<V;v++){
		f[v] = NumVarMatrix(env,N);
		fX[v] = IloArray<NumVarMatrix>(env,N);
		fO[v] = NumVarMatrix(env,N);
		fW[v] = NumVarMatrix(env,N);				
		fWB[v] = NumVarMatrix(env,N);		
		
		o[v] = IloArray<IloBoolVarArray>(env,N);
		w[v] = IloArray<IloBoolVarArray>(env,N);		
		x[v] = IloArray<IloArray<IloBoolVarArray> >(env, N);	
		wB[v] = IloArray<IloBoolVarArray>(env,N);
					
		hasArc[v] = IloArray<IloArray<IloIntArray> >(env, N);				
		arcCost[v] = IloArray<IloArray<IloNumArray> >(env, N);
		hasEnteringArc1st[v] = IloArray<IloIntArray>(env, N);
		
		ageX[v] = vector<vector<vector<int>>>(N);
		ageO[v] = vector<vector<int>>(N); 
		ageW[v] = vector<vector<int>>(N);
		ageWB[v] = vector<vector<int>>(N);
				
		for(j=0;j<N;j++){			
			x[v][j] = IloArray<IloBoolVarArray>(env,N);
			hasArc[v][j] = IloArray<IloIntArray>(env,N);
			arcCost[v][j] = IloArray<IloNumArray>(env,N);			
			fX[v][j] = IloArray<IloNumVarArray>(env, N);
			ageX[v][j] = vector<vector<int>>(N);
			for(i=0;i<N;i++){				
				x[v][j][i] = IloBoolVarArray(env,T);
				hasArc[v][j][i] = IloIntArray(env,T);
				arcCost[v][j][i] = IloNumArray(env,T);
				fX[v][j][i] = IloNumVarArray(env, T);
				ageX[v][j][i] = vector<int>(T);
				for(t=0;t<T;t++){				
					stringstream ss;			
					ss << "x_"<<v<<","<<j<<","<<i<<","<<t;
					x[v][j][i][t].setName(ss.str().c_str());
					ss.str(string());
					ss << "fX_"<<v<<","<<j<<","<<i<<","<<t;
					fX[v][j][i][t] = IloNumVar(env, 0, IloInfinity, ss.str().c_str());
					#ifndef NRelaxation
					IloConversion conv(env, x[v][j][i][t],ILOFLOAT);
					model.add(conv);
					#endif
					ageX[v][j][i][t] = -1;
					
				}
			}
			if(j>0 && j<=J){ //Only considering Ports
				f[v][j] = IloNumVarArray(env, T);
				fO[v][j] = IloNumVarArray(env, T);
				fW[v][j] = IloNumVarArray(env, T);
				fWB[v][j] = IloNumVarArray(env, T);
				
				o[v][j] = IloBoolVarArray(env, T);				
				w[v][j] = IloBoolVarArray(env, T);
				wB[v][j] = IloBoolVarArray(env, T);
				
				hasEnteringArc1st[v][j] = IloIntArray(env,T);

				ageO[v][j] = vector<int>(T); 
				ageW[v][j] = vector<int>(T);
				ageWB[v][j] = vector<int>(T);
								
				for(t=1;t<T;t++){				
					stringstream ss;
					ss << "f_(" << j << "," << t << ")," << v;
					int maxAmountOperation = min(inst.q_v[v], inst.f_max_jt[j-1][t-1]);
					f[v][j][t] = IloNumVar(env, 0, maxAmountOperation, ss.str().c_str());								
					ss.str(string());
					ss << "fO_(" << j << "," << t << ")," << v;
					fO[v][j][t] = IloNumVar(env, 0, IloInfinity, ss.str().c_str());
					ss.str(string());
					ss << "o_(" << j << "," << t << ")," << v;
					o[v][j][t].setName(ss.str().c_str());	
					#ifndef NRelaxation
					IloConversion convZ(env,o[v][j][t], ILOFLOAT);
					model.add(convZ);
					IloConversion convOA(env,o[v][j][t], ILOFLOAT);
					model.add(convOA);
					#endif 				
					
					ageO[v][j][t] = -1;
					ageW[v][j][t] = -1;
					ageWB[v][j][t] = -1;
					if(t<T-1){ //Variables that haven't last time index
						ss.str(string());
						ss << "fW_(" << j << "," << t << ")," << v;
						fW[v][j][t] = IloNumVar(env, 0, IloInfinity, ss.str().c_str());
						ss.str(string());
						ss << "w_(" << j << "," << t << ")," << v;
						w[v][j][t].setName(ss.str().c_str());
						ss.str(string());
						ss << "wB_(" << j << "," << t << ")," << v;
						wB[v][j][t].setName(ss.str().c_str());					
						ss.str(string());
						ss << "fWB_(" << j << "," << t << ")," << v;
						fWB[v][j][t] = IloNumVar(env, 0, IloInfinity, ss.str().c_str());
						#ifndef NRelaxation
						IloConversion convW(env,w[v][j][t], ILOFLOAT);
						model.add(convW);
						IloConversion convWB(env,wB[v][j][t], ILOFLOAT);
						model.add(convWB);						
						#endif
					}									
				}		
			}	
		}
	}

	for(i=1;i<N-1;i++){
        #ifndef NSpotMarket
		alpha[i] = IloNumVarArray(env,T);		
        #endif
        #ifndef NBetas
        beta[i] = IloNumVarArray(env,T);		
        #endif
        #ifndef NThetas
        theta[i] = IloNumVarArray(env,T);		
        #endif
		sP[i] = IloNumVarArray(env, T);		
		for(t=0;t<T;t++){
			stringstream ss;						
			if (t == 0){								
				ss << "sP_(" << i << ",0)";
				sP[i][t] = IloNumVar(env,inst.s_j0[i-1], inst.s_j0[i-1], ss.str().c_str()); //Initial inventory		
			}else{
				ss.str(string());
				ss << "sP_(" << i << "," << t << ")";				
				sP[i][t] = IloNumVar(env,inst.sMin_jt[i-1][0], inst.sMax_jt[i-1][0], ss.str().c_str()); 
				#ifndef NSpotMarket
				ss.str(string());
				ss << "alpha_(" << i << "," << t << ")";
				alpha[i][t] = IloNumVar(env, 0, inst.alp_max_jt[i-1][t-1], ss.str().c_str());
                #endif
                #ifndef NBetas
                ss.str(string());
				ss << "beta_(" << i << "," << t << ")";
				beta[i][t] = IloNumVar(env, 0, IloInfinity, ss.str().c_str());
				#endif
				#ifndef NThetas
				ss.str(string());
				ss << "theta_(" << i << "," << t << ")";
				theta[i][t] = IloNumVar(env, 0, IloInfinity, ss.str().c_str());
				#endif
			}			
		}		
	}
	
	for(v=0;v<V;v++){
		///Builds the arcs between nodes 												//2nd term
		//Source arc	
		j = inst.initialPort[v]+1;
		hasArc[v][0][j][0] = 1;			//Time between source node and initial ports is equal to first time available
		arcCost[v][0][j][0] = inst.portFee[j-1];
		hasEnteringArc1st[v][j][inst.firstTimeAv[v]+1] = 1;
						
		//Travel and sink arcs		
		i = inst.initialPort[v]+1;					//Arcs from initial port i
		for (t=inst.firstTimeAv[v]+1;t<T;t++){		//and initial time available t			
			for(j=1;j<N-1;j++){						//Not necessary to account sink node
				if (i != j){
					int t2 = t + inst.travelTime[v][i-1][j-1]; 
					if (t2<T){ 	//If exists time to reach port j 
						double arc_cost;
						if (inst.typePort[i-1]==1 && inst.typePort[j-1]==0){ //If port i is consuming and j is a producer port
							arc_cost = inst.costKm[v]*inst.distanceMatrix[i-1][j-1]*(1-inst.trav_empt[v]) + inst.portFee[j-1];		
						}else{
							arc_cost = inst.costKm[v]*inst.distanceMatrix[i-1][j-1] + inst.portFee[j-1];
						}
						hasArc[v][i][j][t] = 1;	
						arcCost[v][i][j][t] = arc_cost;
						hasEnteringArc1st[v][j][t2] = 1;
						if (t2+1<T-1) 			//Waiting arc 
							hasEnteringArc1st[v][j][t2+1] = 1;
																					
						//Sink arc from port j 
						hasArc[v][j][N-1][t2] = 1;
						arcCost[v][j][N-1][t2] = -(T-t2-1)*inst.perPeriodRewardForFinishingEarly;
						
						//when the time t reach a time that can be built a mirror between j and i
						if (t >= inst.firstTimeAv[v]+1 + inst.travelTime[v][i-1][j-1]){ //+1 for the data convert 
							if (inst.typePort[j-1]==1 && inst.typePort[i-1]==0){ 		  //If port j is consuming and i is a producer port
								arc_cost = inst.costKm[v]*inst.distanceMatrix[j-1][i-1]*(1-inst.trav_empt[v]) + inst.portFee[i-1];		
							}else{
								arc_cost = inst.costKm[v]*inst.distanceMatrix[j-1][i-1] + inst.portFee[i-1];
							}
							hasArc[v][j][i][t] = 1;
							arcCost[v][j][i][t] = arc_cost;
							hasEnteringArc1st[v][i][t2] = 1;
						}
						//~ //Create arc from j,t2 to others ports (j2) in time t3
						for(int j2=1;j2<=J;j2++){
							if(j2 != i && j2 != j){
								int t3 = t2+inst.travelTime[v][j-1][j2-1];  
								if(t3<T){
									if (inst.typePort[j-1]==1 && inst.typePort[j2-1]==0){
										arc_cost = inst.costKm[v]*inst.distanceMatrix[j-1][j2-1]*(1-inst.trav_empt[v]) + inst.portFee[j2-1];		
									}else{
										arc_cost = inst.costKm[v]*inst.distanceMatrix[j-1][j2-1] + inst.portFee[j2-1];
									}
									hasArc[v][j][j2][t2] = 1;
									arcCost[v][j][j2][t2] = arc_cost;
									hasEnteringArc1st[v][j2][t3] = 1;
								}
							}
						}						
					}
				}
			}
			if(t+1<T)
				hasEnteringArc1st[v][i][t+1] = 1;
			//Sink arc from port i
			hasArc[v][i][N-1][t] = 1;
			arcCost[v][i][N-1][t] = -(T-t-1)*inst.perPeriodRewardForFinishingEarly;
		}		
	}
}

void Model::buildReducedModel(IloEnv& env, Instance inst){
	int i,j,t,v,a;
	int J = inst.J;
	int T = inst.t + 1;			//Init time-periods in 1
	int V = inst.speed.getSize(); //# of vessels
	int N = J + 2; // Number of nodes including source and sink. (0,1...j,j+1)
		
	///Objective function
	IloExpr expr(env);
	IloExpr expr1(env);	
	for(v=0;v<V;v++){
		///Arcs between nodes											//2nd term
		//Source arc	
		j = inst.initialPort[v]+1;		
		expr1 += arcCost[v][0][j][0]*x[v][0][j][0];		
		x[v][0][j][0].setBounds(1,1); // Fixing initial port position
				
		for(i=1;i<=J;i++){
			for(j=1;j<N;j++){
				if(i!=j){
					for(t=1;t<T;t++){
						if(ageX[v][i][j][t]==0){
							expr1 += arcCost[v][i][j][t]*x[v][i][j][t];							
						}
					}
				}
			}
		}
		
		for(i=1;i<N-1;i++){		//[v][i][t] variables
			for(t=1;t<T;t++){					
				if(hasEnteringArc1st[v][i][t]){
					if(ageO[v][i][t]==0){
						expr += inst.r_jt[i-1][t-1]*f[v][i][t];									//1st term
						expr1 += (t-1)*inst.attemptCost*o[v][i][t];								//3rd term									   
					}
				}
				if(v==0){	//[i][t] variables
					#ifndef NSpotMarket
					expr1 += inst.p_jt[i-1][t-1]*alpha[i][t];									//4rd term
					#endif
					#ifndef NBetas
					expr1 += PENALIZATION*beta[i][t];
					#endif
					#ifndef NThetas
					expr1 += PENALIZATION*theta[i][t];
					#endif 
				}
			}			
		}
	}
	obj.setExpr(expr1-expr);	
	model.add(obj);
	
	
	///Constraints
	//Flow balance source and sink nodes
	IloExpr expr_sinkFlow(env);
	//~ IloExpr expr_sourceFlow;
	sinkNodeBalance = IloRangeArray(env,V,1,1); 
	sourceNodeBalance = IloRangeArray(env,V,1,1);
	//First level balance
	IloExpr expr_1stLevel(env);
	IloExpr expr_1stFlow(env);
	firstLevelBalance = IloArray<IloArray<IloRangeArray> > (env, V);
	firstLevelFlow = IloArray<IloArray<IloRangeArray> > (env, V);
	
	//Second level balance
	IloExpr expr_2ndLevel(env);
	IloExpr expr_2ndFlow(env);
	secondLevelBalance = IloArray<IloArray<IloRangeArray> > (env, V);
	secondLevelFlow = IloArray<IloArray<IloRangeArray> > (env, V);
	
	//Berth Limit
	berthLimit = IloArray<IloRangeArray>(env, N-1);		//Index 0 ignored
	IloExpr expr_berth(env);
	
	//Travel at capacity and empty
	travelAtCapacity = IloArray<IloArray<IloArray<IloRangeArray> > > (env, V);
	travelEmpty = IloArray<IloArray<IloArray<IloRangeArray> > > (env, V);
	
	//Inventory balance at ports
	portInventory = IloArray<IloRangeArray> (env, N-1);
	IloExpr expr_invBalancePort(env);
	
	//Cumulative spot market
	cumSlack = IloRangeArray(env,N-1);
	IloExpr expr_cumSlack(env);
	
	//Limits on operation values
	operationLowerLimit = IloArray<IloArray<IloRangeArray> >(env,V);
	operationUpperLimit = IloArray<IloArray<IloRangeArray> >(env,V);
	
	//Flow limits
	flowCapacityX = IloArray<IloArray<IloArray<IloRangeArray> > > (env,V);
	flowCapacityO = IloArray<IloArray<IloRangeArray> >(env,V);
	flowCapacityWB = IloArray<IloArray<IloRangeArray> >(env,V);
	flowCapacityW = IloArray<IloArray<IloRangeArray> >(env,V);
			
	#ifndef NKnapsackInequalities
	knapsack_P_1 = IloArray<IloRangeArray> (env, J+1);			//Altough created for all J ports, it is only considered for loading(production) or consumption(discharging) ports
	knapsack_P_2 = IloArray<IloRangeArray> (env, J+1);	
	knapsack_D_1 = IloArray<IloRangeArray> (env, J+1);	
	knapsack_D_3 = IloArray<IloRangeArray> (env, J+1);	
	#endif
	
	for(i=1;i<N-1;i++){
		stringstream ss1;
		berthLimit[i] = IloRangeArray(env,T);
		expr_cumSlack.clear();
		portInventory[i] = IloRangeArray(env,T);				
		#ifndef NKnapsackInequalities
		if (inst.typePort[i-1] == 0){ 	//Loading port
			knapsack_P_1[i] = IloRangeArray(env, (T-3)*2);		//(T-2)*2 = Number of combinations 1,...t + t,...,T for all t \in T. Using T-3 because de increment
			knapsack_P_2[i] = IloRangeArray(env, (T-3)*2);
		}else{							//Discharging port
			knapsack_D_1[i] = IloRangeArray(env, (T-3)*2);			
			knapsack_D_3[i] = IloRangeArray(env, (T-3)*2);
		}
		int it,k,l;
		IloExpr expr_kP1_LHS(env), expr_kP2_LHS(env), expr_kD1_LHS(env); 
		double kP1_RHS, kP2_RHS, kD1_RHS, kD2_RHS;			
		for(it=0;it<(T-3)*2;it++){	//For each valid inequality
			expr_kP1_LHS.clear();
			expr_kP2_LHS.clear();
			expr_kD1_LHS.clear();
			//Definining the size of set T =[k,l]
			k=1;
			l=T-1;
			if(it<T-3)
				l = it+2;
			else
				k = it+4-l;
			for(v=0;v<V;v++){
				if(ageWB[v][i][k]==0){
					expr_kP1_LHS += wB[v][i][k];
				}
				if(k>1 || hasEnteringArc1st[v][i][k-1]==1){
					if(ageW[v][i][k-1]==0){
						expr_kD1_LHS += w[v][i][k-1];
					}
					if(ageWB[v][i][k-1]==0){
						expr_kD1_LHS += wB[v][i][k-1];
					}
				}
				for(t=k;t<=l;t++){
					if(ageO[v][i][t]==0){
						expr_kP2_LHS += o[v][i][t];	//It is used for both loading and discharging ports
					}
					for(j=0;j<N;j++){						
						if(j==0 && inst.initialPort[v]+1 == i && t==inst.firstTimeAv[v]+1){ 	//Source arc
							if(ageX[v][j][i][0]==0){
								expr_kD1_LHS += x[v][j][i][0];								
							}
						}else if (j == N-1 && hasArc[v][i][j][t] == 1){						//Sink arc
							if(ageX[v][i][j][t]==0){
								expr_kP1_LHS += x[v][i][j][t];
							}
						}else if (j > 0 && j<N-1){											//"Normal" arcs							 																
							if(i != j){
								if(ageX[v][i][j][t]==0){
									expr_kP1_LHS += x[v][i][j][t];
								}
								if(t - inst.travelTime[v][j-1][i-1] >= 0){					//Only if it is possible an entering arc due the travel time
									if(ageX[v][j][i][t-inst.travelTime[v][j-1][i-1]]==0){
										expr_kD1_LHS += x[v][j][i][t-inst.travelTime[v][j-1][i-1]];										
									}
								}
							}
						}
					}
				}
			}
			for(t=k;t<=l;t++){
				kP1_RHS += inst.d_jt[i-1][t-1];				
				kD1_RHS += inst.d_jt[i-1][t-1];
			}
			kP1_RHS += -inst.sMax_jt[i-1][0];
			kP2_RHS = ceil(kP1_RHS/inst.f_max_jt[i-1][0]);
			kP1_RHS = ceil(kP1_RHS/inst.maxVesselCapacity);
			
			if(k==1){
				kD1_RHS += -inst.s_j0[i-1] + inst.sMin_jt[i-1][0];
			}else{
				kD1_RHS += -inst.sMax_jt[i-1][k-1] + inst.sMin_jt[i-1][0];
			}
			kD2_RHS = ceil(kD1_RHS/inst.f_max_jt[i-1][0]);
			kD1_RHS = ceil(kD1_RHS/inst.maxVesselCapacity);
			
			stringstream ss, ss1, ss2;
			unsigned int countTerm = 0;
			if(inst.typePort[i-1] == 0){				
				for(IloExpr::LinearIterator lit = expr_kP1_LHS.getLinearIterator(); lit.ok(); ++lit) {
					++countTerm;
				}
				if(countTerm >= kP1_RHS){
					ss << "knapsackP1_" << i << "," << it;
					knapsack_P_1[i][it] = IloRange(env, kP1_RHS, expr_kP1_LHS, IloInfinity, ss.str().c_str());
					model.add(knapsack_P_1[i][it]);
				}
				countTerm = 0;
				for(IloExpr::LinearIterator lit = expr_kP2_LHS.getLinearIterator(); lit.ok(); ++lit) {
					++countTerm;
				}
				if(countTerm >= kP2_RHS){				
					ss1 << "knapsackP2_" << i << "," << it;
					knapsack_P_2[i][it] = IloRange(env, kP2_RHS, expr_kP2_LHS, IloInfinity, ss1.str().c_str());
					model.add(knapsack_P_2[i][it]);
				}
			}else{
				for(IloExpr::LinearIterator lit = expr_kD1_LHS.getLinearIterator(); lit.ok(); ++lit) {
					++countTerm;
				}
				if(countTerm >= kD1_RHS){
					ss << "knapsackD1_" << i << "," << it;
					knapsack_D_1[i][it] = IloRange(env, kD1_RHS, expr_kD1_LHS, IloInfinity, ss.str().c_str());
					model.add(knapsack_D_1[i][it]);
				}
				countTerm = 0;
				for(IloExpr::LinearIterator lit = expr_kP2_LHS.getLinearIterator(); lit.ok(); ++lit){
					++countTerm;
				}
				if(countTerm >= kD2_RHS){
					ss2 << "knapsackD3_" << i << "," << it;
					knapsack_D_3[i][it] = IloRange(env, kD2_RHS, expr_kP2_LHS, IloInfinity, ss2.str().c_str());
					model.add(knapsack_D_3[i][it]);
				}
			}
		}		
		#endif
		
		for(t=1;t<T;t++){
			expr_berth.clear();
			expr_invBalancePort.clear();
			stringstream ss, ss2;
			ss << "berthLimit_(" << i << "," << t << ")";
			for(v=0;v<V;v++){				
				if (hasEnteringArc1st[v][i][t]==1 && ageO[v][i][t]==0){ //Only if exists an entering arc in the node				
					expr_berth += o[v][i][t];
				}
				if(ageO[v][i][t]==0){
					expr_invBalancePort += -f[v][i][t];
				}
			}
			berthLimit[i][t] = IloRange(env,-IloInfinity, expr_berth, inst.b_j[i-1], ss.str().c_str());									
			model.add(berthLimit[i][t]);

			#ifndef NSpotMarket
			expr_cumSlack += alpha[i][t];
			expr_invBalancePort += -alpha[i][t];
            #endif
            #ifndef NBetas
            expr_invBalancePort += -beta[i][t];
            #endif
            #ifndef NThetas
            expr_invBalancePort += theta[i][t];
            #endif
			ss2 << "invBalancePort_(" << i << "," << t << ")";
			portInventory[i][t] = IloRange(env, inst.delta[i-1]*inst.d_jt[i-1][t-1],
				sP[i][t]-sP[i][t-1]-inst.delta[i-1]*expr_invBalancePort,
				inst.delta[i-1]*inst.d_jt[i-1][t-1], ss2.str().c_str());
			model.add(portInventory[i][t]);
			
		}		
		#ifndef NSpotMarket
        ss1 << "cum_slack("<<i<<")";
		cumSlack[i] = IloRange(env, expr_cumSlack, inst.alp_max_j[i-1], ss1.str().c_str());
		model.add(cumSlack[i]);
        #endif
		
	}	
	
	expr_cumSlack.end();
	expr_berth.end();
	expr_invBalancePort.end();
	
	for(v=0;v<V;v++){
		expr_sinkFlow.clear();							
		firstLevelBalance[v] = IloArray<IloRangeArray>(env,J+1); //Only for ports - id 0 not used		
		firstLevelFlow[v] = IloArray<IloRangeArray>(env,J+1); 
		secondLevelBalance[v] = IloArray<IloRangeArray>(env,J+1); 
		secondLevelFlow[v] = IloArray<IloRangeArray>(env,J+1); 
				
		travelAtCapacity[v] = IloArray<IloArray<IloRangeArray> > (env, N-1);
		travelEmpty[v] = IloArray<IloArray<IloRangeArray> > (env, N-1);
		
		operationLowerLimit[v] = IloArray<IloRangeArray> (env,N-1);
		operationUpperLimit[v] = IloArray<IloRangeArray> (env,N-1);
		
		flowCapacityX[v] = IloArray<IloArray<IloRangeArray> >(env,N);
		flowCapacityO[v] = IloArray<IloRangeArray> (env,N-1);
		flowCapacityW[v] = IloArray<IloRangeArray> (env,N-1);
		flowCapacityWB[v] = IloArray<IloRangeArray> (env,N-1);
				
		for(i=0;i<N;i++){
			if(i>0 && i <= J){ //Only considering ports				
				firstLevelBalance[v][i] = IloRangeArray(env,T,0,0); //Id 0 is not used				
				secondLevelBalance[v][i] = IloRangeArray(env,T,0,0); 
				firstLevelFlow[v][i] = IloRangeArray(env,T,0,0); 
				secondLevelFlow[v][i] = IloRangeArray(env,T,0,0); 
				
				travelAtCapacity[v][i] = IloArray<IloRangeArray> (env, N);
				travelEmpty[v][i] = IloArray<IloRangeArray> (env, N);
				
				operationLowerLimit[v][i] = IloRangeArray (env,T);
				operationUpperLimit[v][i] = IloRangeArray (env,T);
				
				for(j=0;j<N;j++){
					if(j>0){ //Considering ports and sink node
						travelAtCapacity[v][i][j] = IloRangeArray(env, T, -IloInfinity, 0);
						travelEmpty[v][i][j] = IloRangeArray (env, T, -IloInfinity, inst.q_v[v]);
						for(t=1;t<T;t++){
							stringstream ss, ss1;
							if (j<=J){				//When j is a port
								if(inst.typePort[i-1] == 0 && inst.typePort[j-1] == 1){
									ss << "travelAtCap_(" << i << "," << j << ")_" << t << "," << v;
									if(ageX[v][i][j][t]==0){
										travelAtCapacity[v][i][j][t].setExpr(-fX[v][i][j][t] + inst.q_v[v]*x[v][i][j][t]);	
									}
									travelAtCapacity[v][i][j][t].setName(ss.str().c_str());
								}else if (inst.typePort[i-1] == 1 && inst.typePort[j-1] == 0){
									ss1 << "travelEmpty_(" << i << "," << j << ")_" << t << "," << v;
									if(ageX[v][i][j][t]==0){
										travelEmpty[v][i][j][t].setExpr(inst.q_v[v]*x[v][i][j][t] + fX[v][i][j][t]);
									}
									travelEmpty[v][i][j][t].setName(ss1.str().c_str());
								}
							}else{ // when j is the sink node
								if(inst.typePort[i-1] == 0){
									ss << "travelAtCap_(" << i << ",snk)_" << t << "," << v;
									if(ageX[v][i][j][t]==0){
										travelAtCapacity[v][i][j][t].setExpr(-fX[v][i][j][t] + inst.q_v[v]*x[v][i][j][t]);	
									}
									travelAtCapacity[v][i][j][t].setName(ss.str().c_str());
								}else if (inst.typePort[i-1] == 1){ 
									ss1 << "travelEmpty_(" << i << ",snk)_" << t << "," << v;
									if(ageX[v][i][j][t]==0){
										travelEmpty[v][i][j][t].setExpr(inst.q_v[v]*x[v][i][j][t] + fX[v][i][j][t]);
									}
									travelEmpty[v][i][j][t].setName(ss1.str().c_str());
								}
							}
							model.add(travelAtCapacity[v][i][j][t]);
							model.add(travelEmpty[v][i][j][t]);
						}						
					}
				}
				
				flowCapacityO[v][i] = IloRangeArray(env,T);
				flowCapacityW[v][i] = IloRangeArray(env,T);
				flowCapacityWB[v][i] = IloRangeArray(env,T);

				for(t=1;t<T;t++){
					stringstream ss, ss1, ss2, ss3, ss4, ss5, ss6, ss7, ss8, ss9, ss10;
					ss << "First_level_balance_" << v << "(" << i << "," << t << ")";
					ss1 << "Second_level_balance_" << v << "(" << i << "," << t << ")";
					ss2 << "link_balance_" << v << "(" << i << "," << t << ")";
					ss3 << "First_level_flow_" << v << "(" << i << "," << t << ")";
					ss4 << "Second_level_flow_" << v << "(" << i << "," << t << ")";
					
					if(ageX[v][i][N-1][t]==0){
						expr_sinkFlow += x[v][i][N-1][t];
					}
					
					expr_1stLevel.clear();
					expr_1stFlow.clear();
					expr_2ndLevel.clear();
					expr_2ndFlow.clear();
										
					//~ if(hasEnteringArc1st[v][i][t]==1){
						for(j=0;j<N;j++){
							if(j<N-1){ //No consider sink arc (first level balance)
								//If j is the source node and reach i in time t
								if(j==0 && inst.initialPort[v]+1 == i && t==inst.firstTimeAv[v]+1){
									expr_1stLevel += x[v][j][i][0];							
									expr_1stFlow += fX[v][j][i][0];
									fX[v][j][i][0].setBounds(inst.s_v0[v], inst.s_v0[v]); //Fixing initial inventory 								
								}
								else if (j>0){ //When j is a port
									if (t - inst.travelTime[v][j-1][i-1] >= 0){ //If is possible to exist an arc from j to i										
										if(ageX[v][j][i][t-inst.travelTime[v][j-1][i-1]]==0){
											expr_1stLevel += x[v][j][i][t-inst.travelTime[v][j-1][i-1]];									
											expr_1stFlow += fX[v][j][i][t-inst.travelTime[v][j-1][i-1]];
										}
									}
								}
							}
							if(j>0){ //No consider source arc (second level balance)
								if (ageX[v][i][j][t]==0){
									expr_2ndLevel += x[v][i][j][t];
									expr_2ndFlow += fX[v][i][j][t];
								}
							}
						}
						if (t==1 || hasEnteringArc1st[v][i][t-1]==0){ //First time period or not entering arc in the previous time period
							if(ageO[v][i][t]==0){
								expr_1stLevel += - o[v][i][t];
								expr_2ndLevel += -o[v][i][t];
								expr_1stFlow += - fO[v][i][t];
								expr_2ndFlow += -fO[v][i][t] - inst.delta[i-1]*f[v][i][t];
							}
							if(ageW[v][i][t]==0){
								expr_1stLevel += - w[v][i][t]; 
								expr_1stFlow+= -fW[v][i][t];
							}
							if(ageWB[v][i][t]==0){
								expr_2ndLevel += wB[v][i][t];
								expr_2ndFlow += fWB[v][i][t];
							}
						}else if (t==T-1){ //Last time period
							if(ageO[v][i][t]==0){
								expr_1stLevel += - o[v][i][t];
								expr_2ndLevel += -o[v][i][t];
								expr_1stFlow += -fO[v][i][t];
								expr_2ndFlow += -fO[v][i][t] - inst.delta[i-1]*f[v][i][t];
							}
							if(ageW[v][i][t-1]==0){
								expr_1stLevel += w[v][i][t-1];
								expr_1stFlow += fW[v][i][t-1];
							}
							if(ageWB[v][i][t-1]==0){
								expr_1stLevel += wB[v][i][t-1];								
								expr_1stFlow += fWB[v][i][t-1];
							}
						}else{ //Other times
							if(ageW[v][i][t-1]==0){
								expr_1stLevel += w[v][i][t-1];
								expr_1stFlow += fW[v][i][t-1]; 
							}
							if(ageW[v][i][t]==0){
								expr_1stLevel += - w[v][i][t]; 
								expr_1stFlow += -fW[v][i][t];
							}
							if(ageO[v][i][t]==0){
								expr_1stLevel += - o[v][i][t];
								expr_2ndLevel += - o[v][i][t];
								expr_1stFlow += - fO[v][i][t];
								expr_2ndFlow += -fO[v][i][t] - inst.delta[i-1]*f[v][i][t];
							}
							if(ageWB[v][i][t-1]==0){
								expr_1stLevel += wB[v][i][t-1];
								expr_1stFlow += fWB[v][i][t-1];								
							}
							if(ageWB[v][i][t]==0){
								expr_2ndLevel += wB[v][i][t];							
								expr_2ndFlow += fWB[v][i][t];
							}							
						}
						
						firstLevelBalance[v][i][t].setExpr(expr_1stLevel);
						secondLevelBalance[v][i][t].setExpr(expr_2ndLevel);

						firstLevelFlow[v][i][t].setExpr(expr_1stFlow);
						secondLevelFlow[v][i][t].setExpr(expr_2ndFlow);
						
						firstLevelBalance[v][i][t].setName(ss.str().c_str());
						model.add(firstLevelBalance[v][i][t]);
						
						secondLevelBalance[v][i][t].setName(ss1.str().c_str());
						model.add(secondLevelBalance[v][i][t]);
						
						firstLevelFlow[v][i][t].setName(ss3.str().c_str());
						model.add(firstLevelFlow[v][i][t]);
						
						secondLevelFlow[v][i][t].setName(ss4.str().c_str());						
						model.add(secondLevelFlow[v][i][t]);
					
						if(ageO[v][i][t]==0){
							ss5 << "fjmin_(" << i << "," << t << ")," << v;
							ss6 << "fjmax_(" << i << "," << t << ")," << v;
							IloNum minfMax = min(inst.f_max_jt[i-1][t-1], inst.q_v[v]);
							
							operationLowerLimit[v][i][t] = IloRange(env, 0, f[v][i][t] - inst.f_min_jt[i-1][t-1]*o[v][i][t] , IloInfinity, ss5.str().c_str());
							model.add(operationLowerLimit[v][i][t]);
							
							operationUpperLimit[v][i][t] = IloRange(env, -IloInfinity, f[v][i][t] - minfMax*o[v][i][t],	0, ss6.str().c_str());				
							model.add(operationUpperLimit[v][i][t]);
							
							ss7 << "flowLimitO_"<<v<<","<<i<<","<<t;					
							flowCapacityO[v][i][t] = IloRange(env, -IloInfinity, fO[v][i][t]-inst.q_v[v]*o[v][i][t], 0, ss7.str().c_str());					
							model.add(flowCapacityO[v][i][t]);					
						}
						
						if(t<T-1){ //Constraints with no last time index
							if(ageW[v][i][t]==0){
								ss9 << "flowLimitW_"<<v<<","<<i<<","<<t;							
								flowCapacityW[v][i][t] = IloRange(env, -IloInfinity, fW[v][i][t]-inst.q_v[v]*w[v][i][t], 0, ss9.str().c_str());							
								model.add(flowCapacityW[v][i][t]);
							}
							if(ageWB[v][i][t]==0){
								ss10 << "flowLimitWB_"<<v<<","<<i<<","<<t;
								flowCapacityWB[v][i][t] = IloRange(env, -IloInfinity, fWB[v][i][t]-inst.q_v[v]*wB[v][i][t], 0, ss10.str().c_str());
								model.add(flowCapacityWB[v][i][t]);
							}
						}
					//~ }
				}				
			
			}			
			flowCapacityX[v][i] = IloArray<IloRangeArray>(env,N);
			for(j=0;j<N;j++){
				if(i != j){
					flowCapacityX[v][i][j] = IloRangeArray(env,T);
					for(t=0;t<T;t++){
						if(ageX[v][i][j][t]==0){
							stringstream ss;
							ss << "flowLimitX_" << v << "," << i << "," << j << "," << t;
							flowCapacityX[v][i][j][t] = IloRange(env, -IloInfinity, fX[v][i][j][t] - inst.q_v[v]*x[v][i][j][t], 0, ss.str().c_str());
							model.add(flowCapacityX[v][i][j][t]);
						}
					}
				}
			}
		}		
		stringstream ss;
		ss << "flowBalanceSink_" << v;
		sinkNodeBalance[v].setExpr(expr_sinkFlow);
		sinkNodeBalance[v].setName(ss.str().c_str());
		model.add(sinkNodeBalance);
	}
	expr_1stLevel.end();
	expr_2ndLevel.end();
	expr_2ndLevel.end();
	expr_2ndFlow.end();
	expr_sinkFlow.end();
	
	
	#ifndef NOperateAndGo
	
	#endif
	
	//Update age to the status 'already added'
	for(v=0;v<V;v++){
		for(i=0;i<=J;i++){
			for(j=0;j<N;j++){
				if(i != j){					
					for(t=0;t<T;t++){
						if(ageX[v][i][j][t]==0){							
							ageX[v][i][j][t]=1; 
						}
						if(j==0 && i>0 && t>0){ // <v,i,t>
							if(ageO[v][i][t] == 0){
								ageO[v][i][t] = 1;
							}
							if(ageW[v][i][t] == 0){
								ageW[v][i][t] = 1;
							}
							if(ageWB[v][i][t]==0){
								ageWB[v][i][t]=1;
							}
						}
					}
				}
			}
		}
	}
}
/*
 * Can be used only after the initialization of the model
 * Add to the model the variables in which age = 0, after update them to the initial age value 1
 */
void Model::addComponentsToModel(IloEnv& env, Instance inst){
	int i,j,t,v,a;
	int J = inst.J;
	int T = inst.t + 1;			//Init time-periods in 1
	int V = inst.speed.getSize(); //# of vessels
	int N = J + 2; // Number of nodes including source and sink. (0,1...j,j+1)
		
	///Objective function
	IloExpr expr_obj(env);
	expr_obj = obj.getExpr();
	for(v=0;v<V;v++){
		///Arcs between nodes											//2nd term		
		for(i=1;i<=J;i++){
			for(j=1;j<N;j++){
				if(i!=j){
					for(t=1;t<T;t++){
						if(ageX[v][i][j][t]==0){
							expr_obj += arcCost[v][i][j][t]*x[v][i][j][t];
						}
					}
				}
			}
		}
		
		for(i=1;i<N-1;i++){		//[v][i][t] variables
			for(t=1;t<T;t++){					
				if(hasEnteringArc1st[v][i][t]){
					if(ageO[v][i][t]==0){
						expr_obj += - inst.r_jt[i-1][t-1]*f[v][i][t];							//1st term
						expr_obj += (t-1)*inst.attemptCost*o[v][i][t];							//3rd term
					}
				}				
			}			
		}
	}
	obj.setExpr(expr_obj);
	
	///Constraints
	IloExpr expr_sinkFlow(env);
	IloExpr expr_1stLevel(env);
	IloExpr expr_1stFlow(env);	
	IloExpr expr_2ndLevel(env);
	IloExpr expr_2ndFlow(env);
	IloExpr expr_berth(env);
	IloExpr expr_invBalancePort(env);

	for(i=1;i<N-1;i++){		
		#ifndef NKnapsackInequalities
		int it,k,l;
		IloExpr expr_kP1_LHS(env), expr_kP2_LHS(env), expr_kD1_LHS(env); 
		double kP1_RHS, kP2_RHS, kD1_RHS, kD2_RHS;			
		for(it=0;it<(T-3)*2;it++){	//For each valid inequality
			if(inst.delta[i-1] == 1){
				expr_kP1_LHS = knapsack_P_1[i][it].getExpr();
				expr_kP2_LHS = knapsack_P_2[i][it].getExpr();
			}else{
				expr_kD1_LHS = knapsack_D_1[i][it].getExpr();
				expr_kP2_LHS = knapsack_D_3[i][it].getExpr();
			}
			//Definining the size of set T =[k,l]
			k=1;
			l=T-1;
			if(it<T-3)
				l = it+2;
			else
				k = it+4-l;
			for(v=0;v<V;v++){
				if(ageWB[v][i][k]==0){
					expr_kP1_LHS += wB[v][i][k];
				}
				if(k>1 || hasEnteringArc1st[v][i][k-1]==1){
					if(ageW[v][i][k-1]==0){
						expr_kD1_LHS += w[v][i][k-1];
					}
					if(ageWB[v][i][k-1]==0){
						expr_kD1_LHS += wB[v][i][k-1];
					}
				}
				for(t=k;t<=l;t++){
					if(ageO[v][i][t]==0){
						expr_kP2_LHS += o[v][i][t];	//It is used for both loading and discharging ports
					}
					for(j=0;j<N;j++){						
						if(j==0 && inst.initialPort[v]+1 == i && t==inst.firstTimeAv[v]+1){ 	//Source arc
							if(ageX[v][j][i][0]==0){
								expr_kD1_LHS += x[v][j][i][0];								
							}
						}else if (j == N-1 && hasArc[v][i][j][t] == 1){						//Sink arc
							if(ageX[v][i][j][t]==0){
								expr_kP1_LHS += x[v][i][j][t];
							}
						}else if (j > 0 && j<N-1){											//"Normal" arcs							 																
							if(i != j){
								if(ageX[v][i][j][t]==0){
									expr_kP1_LHS += x[v][i][j][t];
								}
								if(t - inst.travelTime[v][j-1][i-1] >= 0){					//Only if it is possible an entering arc due the travel time
									if(ageX[v][j][i][t-inst.travelTime[v][j-1][i-1]]==0){
										expr_kD1_LHS += x[v][j][i][t-inst.travelTime[v][j-1][i-1]];										
									}
								}
							}
						}
					}
				}
			}
			for(t=k;t<=l;t++){
				kP1_RHS += inst.d_jt[i-1][t-1];				
				kD1_RHS += inst.d_jt[i-1][t-1];
			}
			kP1_RHS += -inst.sMax_jt[i-1][0];
			kP2_RHS = ceil(kP1_RHS/inst.f_max_jt[i-1][0]);
			kP1_RHS = ceil(kP1_RHS/inst.maxVesselCapacity);
			
			if(k==1){
				kD1_RHS += -inst.s_j0[i-1] + inst.sMin_jt[i-1][0];
			}else{
				kD1_RHS += -inst.sMax_jt[i-1][k-1] + inst.sMin_jt[i-1][0];
			}
			kD2_RHS = ceil(kD1_RHS/inst.f_max_jt[i-1][0]);
			kD1_RHS = ceil(kD1_RHS/inst.maxVesselCapacity);
			
			stringstream ss, ss1, ss2;
			unsigned int countTerm = 0;
			//TODO identify if a Range was added to the model to get/set the expr 
			if(inst.typePort[i-1] == 0){				
				for(IloExpr::LinearIterator lit = expr_kP1_LHS.getLinearIterator(); lit.ok(); ++lit) {
					++countTerm;
				}
				if(countTerm >= kP1_RHS){
					ss << "knapsackP1_" << i << "," << it;
					knapsack_P_1[i][it] = IloRange(env, kP1_RHS, expr_kP1_LHS, IloInfinity, ss.str().c_str());
					model.add(knapsack_P_1[i][it]);
				}
				countTerm = 0;
				for(IloExpr::LinearIterator lit = expr_kP2_LHS.getLinearIterator(); lit.ok(); ++lit) {
					++countTerm;
				}
				if(countTerm >= kP2_RHS){				
					ss1 << "knapsackP2_" << i << "," << it;
					knapsack_P_2[i][it] = IloRange(env, kP2_RHS, expr_kP2_LHS, IloInfinity, ss1.str().c_str());
					model.add(knapsack_P_2[i][it]);
				}
			}else{
				for(IloExpr::LinearIterator lit = expr_kD1_LHS.getLinearIterator(); lit.ok(); ++lit) {
					++countTerm;
				}
				if(countTerm >= kD1_RHS){
					ss << "knapsackD1_" << i << "," << it;
					knapsack_D_1[i][it] = IloRange(env, kD1_RHS, expr_kD1_LHS, IloInfinity, ss.str().c_str());
					model.add(knapsack_D_1[i][it]);
				}
				countTerm = 0;
				for(IloExpr::LinearIterator lit = expr_kP2_LHS.getLinearIterator(); lit.ok(); ++lit){
					++countTerm;
				}
				if(countTerm >= kD2_RHS){
					ss2 << "knapsackD3_" << i << "," << it;
					knapsack_D_3[i][it] = IloRange(env, kD2_RHS, expr_kP2_LHS, IloInfinity, ss2.str().c_str());
					model.add(knapsack_D_3[i][it]);
				}
			}
		}		
		#endif
		
		for(t=1;t<T;t++){
			expr_berth = berthLimit[i][t].getExpr();			
			expr_invBalancePort = portInventory[i][t].getExpr();
			for(v=0;v<V;v++){				
				if (hasEnteringArc1st[v][i][t]==1 && ageO[v][i][t]==0){ //Only if exists an entering arc in the node				
					expr_berth += o[v][i][t];					
				}
				if(ageO[v][i][t]==0){
					expr_invBalancePort += -inst.delta[i-1]*-f[v][i][t];
				}
			}
			berthLimit[i][t].setExpr(expr_berth);			
			portInventory[i][t].setExpr(expr_invBalancePort);			
		}		
	}	
	for(v=0;v<V;v++){
		expr_sinkFlow = sinkNodeBalance[v].getExpr();
		for(i=0;i<N;i++){
			if(i>0 && i <= J){ //Only considering ports				
				for(j=0;j<N;j++){
					if(j>0){ //Considering ports and sink node						
						for(t=1;t<T;t++){
							if(ageX[v][i][j][t]==0){
								if (j<=J){				//When j is a port
									if(inst.typePort[i-1] == 0 && inst.typePort[j-1] == 1){										
										travelAtCapacity[v][i][j][t].setExpr(-fX[v][i][j][t] + inst.q_v[v]*x[v][i][j][t]);	
									}else if (inst.typePort[i-1] == 1 && inst.typePort[j-1] == 0){
										travelEmpty[v][i][j][t].setExpr(inst.q_v[v]*x[v][i][j][t] + fX[v][i][j][t]);
									}
								}else{ // when j is the sink node
									if(inst.typePort[i-1] == 0){
										travelAtCapacity[v][i][j][t].setExpr(-fX[v][i][j][t] + inst.q_v[v]*x[v][i][j][t]);	
									}else if (inst.typePort[i-1] == 1){ 
										travelEmpty[v][i][j][t].setExpr(inst.q_v[v]*x[v][i][j][t] + fX[v][i][j][t]);
									}
								}
							}
						}						
					}
				}
				stringstream ss, ss1, ss2, ss3, ss4, ss5, ss6, ss7, ss8, ss9, ss10;
				for(t=1;t<T;t++){
					if(ageX[v][i][N-1][t]==0){
						expr_sinkFlow += x[v][i][N-1][t];
					}
					
					expr_1stLevel = firstLevelBalance[v][i][t].getExpr();
					expr_1stFlow = firstLevelFlow[v][i][t].getExpr();
					expr_2ndLevel = secondLevelBalance[v][i][t].getExpr();
					expr_2ndFlow = secondLevelFlow[v][i][t].getExpr();
										
					//~ if(hasEnteringArc1st[v][i][t]==1){
						for(j=0;j<N;j++){
							if(j<N-1){ //No consider sink arc (first level balance)
								if (j>0){ //When j is a port
									if (t - inst.travelTime[v][j-1][i-1] >= 0){ //If is possible to exist an arc from j to i										
										if(ageX[v][j][i][t-inst.travelTime[v][j-1][i-1]]==0){
											expr_1stLevel += x[v][j][i][t-inst.travelTime[v][j-1][i-1]];									
											expr_1stFlow += fX[v][j][i][t-inst.travelTime[v][j-1][i-1]];
										}
									}
								}
							}
							if(j>0){ //No consider source arc (second level balance)
								if (ageX[v][i][j][t] == 0){
									expr_2ndLevel += x[v][i][j][t];
									expr_2ndFlow += fX[v][i][j][t];
								}
							}
						}
						if (t==1 || hasEnteringArc1st[v][i][t-1]==0){ //First time period or not entering arc in the previous time period
							if(ageO[v][i][t]==0){
								expr_1stLevel += - o[v][i][t];
								expr_2ndLevel += -o[v][i][t];
								expr_1stFlow += - fO[v][i][t];
								expr_2ndFlow += -fO[v][i][t] - inst.delta[i-1]*f[v][i][t];
							}
							if(ageW[v][i][t]==0){
								expr_1stLevel += - w[v][i][t]; 
								expr_1stFlow+= -fW[v][i][t];
							}
							if(ageWB[v][i][t]==0){
								expr_2ndLevel += wB[v][i][t];
								expr_2ndFlow += fWB[v][i][t];
							}
						}else if (t==T-1){ //Last time period
							if(ageO[v][i][t]==0){
								expr_1stLevel += - o[v][i][t];
								expr_2ndLevel += -o[v][i][t];
								expr_1stFlow += -fO[v][i][t];
								expr_2ndFlow += -fO[v][i][t] - inst.delta[i-1]*f[v][i][t];
							}
							if(ageW[v][i][t-1]==0){
								expr_1stLevel += w[v][i][t-1];
								expr_1stFlow += fW[v][i][t-1];
							}
							if(ageWB[v][i][t-1]==0){
								expr_1stLevel += wB[v][i][t-1];								
								expr_1stFlow += fWB[v][i][t-1];
							}
						}else{ //Other times
							if(ageW[v][i][t-1]==0){
								expr_1stLevel += w[v][i][t-1];
								expr_1stFlow += fW[v][i][t-1]; 
							}
							if(ageW[v][i][t]==0){
								expr_1stLevel += - w[v][i][t]; 
								expr_1stFlow += -fW[v][i][t];
							}
							if(ageO[v][i][t]==0){
								expr_1stLevel += - o[v][i][t];
								expr_2ndLevel += - o[v][i][t];
								expr_1stFlow += - fO[v][i][t];
								expr_2ndFlow += -fO[v][i][t] - inst.delta[i-1]*f[v][i][t];
							}
							if(ageWB[v][i][t-1]==0){
								expr_1stLevel += wB[v][i][t-1];
								expr_1stFlow += fWB[v][i][t-1];								
							}
							if(ageWB[v][i][t]==0){
								expr_2ndLevel += wB[v][i][t];							
								expr_2ndFlow += fWB[v][i][t];
							}							
						}
						
						firstLevelBalance[v][i][t].setExpr(expr_1stLevel);
						secondLevelBalance[v][i][t].setExpr(expr_2ndLevel);

						firstLevelFlow[v][i][t].setExpr(expr_1stFlow);
						secondLevelFlow[v][i][t].setExpr(expr_2ndFlow);
					
						if(ageO[v][i][t]==0){
							ss5 << "fjmin_(" << i << "," << t << ")," << v;
							ss6 << "fjmax_(" << i << "," << t << ")," << v;
							IloNum minfMax = min(inst.f_max_jt[i-1][t-1], inst.q_v[v]);
							
							operationLowerLimit[v][i][t] = IloRange(env, 0, f[v][i][t] - inst.f_min_jt[i-1][t-1]*o[v][i][t] , IloInfinity, ss5.str().c_str());
							model.add(operationLowerLimit[v][i][t]);
							
							operationUpperLimit[v][i][t] = IloRange(env, -IloInfinity, f[v][i][t] - minfMax*o[v][i][t],	0, ss6.str().c_str());				
							model.add(operationUpperLimit[v][i][t]);
							
							ss7 << "flowLimitO_"<<v<<","<<i<<","<<t;					
							flowCapacityO[v][i][t] = IloRange(env, -IloInfinity, fO[v][i][t]-inst.q_v[v]*o[v][i][t], 0, ss7.str().c_str());					
							model.add(flowCapacityO[v][i][t]);					
						}
						
						if(t<T-1){ //Constraints with no last time index
							if(ageW[v][i][t]==0){
								ss9 << "flowLimitW_"<<v<<","<<i<<","<<t;							
								flowCapacityW[v][i][t] = IloRange(env, -IloInfinity, fW[v][i][t]-inst.q_v[v]*w[v][i][t], 0, ss9.str().c_str());							
								model.add(flowCapacityW[v][i][t]);
							}
							if(ageWB[v][i][t]==0){
								ss10 << "flowLimitWB_"<<v<<","<<i<<","<<t;
								flowCapacityWB[v][i][t] = IloRange(env, -IloInfinity, fWB[v][i][t]-inst.q_v[v]*wB[v][i][t], 0, ss10.str().c_str());
								model.add(flowCapacityWB[v][i][t]);
							}
						}
					//~ }
				}				
			
			}			
			for(j=0;j<N;j++){
				if(i != j){					
					for(t=0;t<T;t++){
						if(ageX[v][i][j][t]==0){
							stringstream ss;
							ss << "flowLimitX_" << v << "," << i << "," << j << "," << t;
							flowCapacityX[v][i][j][t] = IloRange(env, -IloInfinity, fX[v][i][j][t] - inst.q_v[v]*x[v][i][j][t], 0, ss.str().c_str());
							model.add(flowCapacityX[v][i][j][t]);
						}
					}
				}
			}
		}				
		sinkNodeBalance[v].setExpr(expr_sinkFlow);
	}
	//Update age
	for(v=0;v<V;v++){
		for(i=0;i<=J;i++){
			for(j=0;j<N;j++){
				if(i != j){					
					for(t=0;t<T;t++){
						if(ageX[v][i][j][t]==0){							
							ageX[v][i][j][t]=1; 
						}
						if(j==0 && i>0 && t>0){ // <v,i,t>
							if(ageO[v][i][t] == 0){
								ageO[v][i][t] = 1;
							}
							if(ageW[v][i][t] == 0){
								ageW[v][i][t] = 1;
							}
							if(ageWB[v][i][t]==0){
								ageWB[v][i][t]=1;
							}
						}
					}
				}
			}
		}
	}
}

/*
 * Read the MIP solution S
 * IF var \in S, age[var] = 1, ELSE age[var]++
 * Solve the LP relaxation
 * Get the reduced costs of var in which age[var] > ageMax and add to the priority queue
 * Remove from the model the r% variables with age > ageMax sorted by the worst(higher) reduced cost 
 */ 
void Model::adapt(IloEnv& env, Instance inst, const float& r,const unsigned int& ageMax){
	int i,j,t,v,a;
	int J = inst.J;
	int T = inst.t + 1;			//Init time-periods in 1
	int V = inst.speed.getSize(); //# of vessels
	int N = J + 2; // Number of nodes including source and sink. (0,1...j,j+1)
	
	///Generating a copy of model for solve the relaxation
	IloModel relax(env);
	relax.add(model);
	
	///Getting MIP solution, updating variables age, and relaxing the variables of relaxModel
	xValue = IloArray<IloArray<IloArray<IloNumArray> > >(env,V) ;
	oValue = IloArray<IloArray<IloNumArray> >(env,V);
	wValue = IloArray<IloArray<IloNumArray> >(env,V);
	wBValue = IloArray<IloArray<IloNumArray> >(env,V);
		
	for (v=0;v<V;v++){
		xValue[v] = IloArray<IloArray<IloNumArray>>(env, N);
		oValue[v] = IloArray<IloNumArray>(env,N-1);
		wValue[v] = IloArray<IloNumArray>(env,N-1);		
		wBValue[v] = IloArray<IloNumArray>(env,N-1);

		for (i=0;i<N;i++){
			if(i > 0 && i < N-1){ //Not consider sink node
				oValue[v][i] = IloNumArray(env,T);
				wValue[v][i] = IloNumArray(env,T);
				wBValue[v][i] = IloNumArray(env,T);
								
				for(t=1;t<T;t++){
					if(hasEnteringArc1st[v][i][t]){
						if(ageO[v][i][t]!=-1){
							oValue[v][i][t] = cplex.getValue(o[v][i][t]);							
							if(oValue[v][i][t]>=0.1){
								ageO[v][i][t] = 1;
							}else{
								ageO[v][i][t]++;
							}
							IloConversion conv(env, o[v][i][t],ILOFLOAT);
							relax.add(conv);
						}
						if(t<T-1){ //Variables not associated with last time index
							if(ageW[v][i][t]!=-1){
								wValue[v][i][t] = cplex.getValue(w[v][i][t]);
								if(wValue[v][i][t]>=0.1){
									ageW[v][i][t] = 1;
								}else{
									ageW[v][i][t]++;
								}
								IloConversion conv(env, w[v][i][t],ILOFLOAT);
								relax.add(conv);
							}
							if(ageWB[v][i][t]!=-1){
								wBValue[v][i][t] = cplex.getValue(wB[v][i][t]);
								if(wBValue[v][i][t] >= 0.1){
									ageWB[v][i][t] = 1;
								}else{
									ageWB[v][i][t]++;
								}
								IloConversion conv(env, wB[v][i][t],ILOFLOAT);
								relax.add(conv);
							}
						}
					}
				}
			}
			xValue[v][i] = IloArray<IloNumArray>(env,N);
			for(j=0;j<N;j++){
				xValue[v][i][j] = IloNumArray(env,T);
				for(t=0;t<T;t++){					
					if(hasArc[v][i][j][t] == 1 && ageX[v][i][j][t]!= -1){
						xValue[v][i][j][t] = cplex.getValue(x[v][i][j][t]);						
						if(xValue[v][i][j][t] >= 0.1){
							ageX[v][i][j][t] = 1;
						}else{
							ageX[v][i][j][t]++;
						}
						IloConversion conv(env, x[v][i][j][t],ILOFLOAT);
						relax.add(conv);
					}					
				}
			}
		}	
	}	
	///Solving the LP relaxation	
	IloCplex cpx(relax);	
	cpx.setOut(env.getNullStream());
	cpx.setParam(IloCplex::Threads, 1);		
	cpx.solve();
	
	///Getting the reduced costs
	variablesReducedCost = priority_queue< pair<std::string,float>, vector<pair<std::string, float>>, CompareSFPairHighestFloatFirst >();
	xReducedCost = IloArray<IloArray<IloArray<IloNumArray> > >(env,V) ;
	oReducedCost = IloArray<IloArray<IloNumArray> >(env,V);
	wReducedCost = IloArray<IloArray<IloNumArray> >(env,V);
	wBReducedCost = IloArray<IloArray<IloNumArray> >(env,V);
	stringstream ss;
	for (v=0;v<V;v++){
		xReducedCost[v] = IloArray<IloArray<IloNumArray>>(env, N);
		oReducedCost[v] = IloArray<IloNumArray>(env,N-1);
		wReducedCost[v] = IloArray<IloNumArray>(env,N-1);		
		wBReducedCost[v] = IloArray<IloNumArray>(env,N-1);
		for (i=0;i<N;i++){
			if(i > 0 && i < N-1){ //Not consider sink node
				oReducedCost[v][i] = IloNumArray(env,T);
				wReducedCost[v][i] = IloNumArray(env,T);
				wBReducedCost[v][i] = IloNumArray(env,T);
				for(t=1;t<T;t++){
					if(hasEnteringArc1st[v][i][t]){
						if(ageO[v][i][t] != -1 && ageO[v][i][t]>ageMax){
							oReducedCost[v][i][t] = cpx.getReducedCost(o[v][i][t]);
							ss.str(string());							
							ss << "1_" << v << "_" << i << "_" << t;	
							variablesReducedCost.push(make_pair(ss.str(),oReducedCost[v][i][t]));
						}
						if(t<T-1){ //Variables not associated with last time index
							if(ageW[v][i][t] != -1 && ageW[v][i][t]>ageMax){
								wReducedCost[v][i][t] = cpx.getReducedCost(w[v][i][t]);
								ss.str(string());							
								ss << "2_" << v << "_" << i << "_" << t;							
								variablesReducedCost.push(make_pair(ss.str(),wReducedCost[v][i][t]));
							}
							if(ageWB[v][i][t] != -1 && ageWB[v][i][t]>ageMax){
								wBReducedCost[v][i][t] = cpx.getReducedCost(wB[v][i][t]);
								ss.str(string());							
								ss << "3_" << v << "_" << i << "_" << t;							
								variablesReducedCost.push(make_pair(ss.str(),wBReducedCost[v][i][t]));
							}
						}
					}
				}
			}
			xReducedCost[v][i] = IloArray<IloNumArray>(env,N);
			for(j=0;j<N;j++){
				xReducedCost[v][i][j] = IloNumArray(env,T);
				for(t=0;t<T;t++){					
					if(hasArc[v][i][j][t] == 1 && ageX[v][i][j][t] != -1 && ageX[v][i][j][t]>ageMax){
						xReducedCost[v][i][j][t] = cpx.getReducedCost(x[v][i][j][t]);						
						ss.str(string());							
						ss << "0_" << v << "_" << i << "_" << j << "_" << t;							
						variablesReducedCost.push(make_pair(ss.str(),xReducedCost[v][i][j][t]));
					}					
				}
			}
		}	
	}
	///Remove from the model the r% of variables  in PQ
	unsigned int count=0, numRemovedVar = floor(variablesReducedCost.size()*r/100);
	//~ cout << "Removing " << numRemovedVar << " variables \n";
	int idV,idI,idJ,idT;
	while(count < numRemovedVar){
		stringstream ss(variablesReducedCost.top().first);
		string line1,line2;
		getline(ss,line1,'_'); 
		switch (stoi(line1)){// type variable (0=x,1=o,2=w,3=wb)
			case 0:
				getline(ss,line2,'_');
				idV=stoi(line2);
				getline(ss,line2,'_');
				idI=stoi(line2);
				getline(ss,line2,'_');
				idJ=stoi(line2);
				getline(ss,line2,'_');
				idT=stoi(line2);
				x[idV][idI][idJ][idT].removeFromAll();
				ageX[idV][idI][idJ][idT]=-1;
				break;
			case 1:
				getline(ss,line2,'_');
				idV=stoi(line2);
				getline(ss,line2,'_');
				idI=stoi(line2);
				getline(ss,line2,'_');
				idT=stoi(line2);
				o[idV][idI][idT].removeFromAll();
				ageO[idV][idI][idT]=-1;
				break;
			case 2:
				getline(ss,line2,'_');
				idV=stoi(line2);
				getline(ss,line2,'_');
				idI=stoi(line2);
				getline(ss,line2,'_');
				idT=stoi(line2);
				w[idV][idI][idT].removeFromAll();
				ageW[idV][idI][idT]=-1;
				break;
			case 3:
				getline(ss,line2,'_');
				idV=stoi(line2);
				getline(ss,line2,'_');
				idI=stoi(line2);
				getline(ss,line2,'_');
				idT=stoi(line2);
				wB[idV][idI][idT].removeFromAll();
				ageWB[idV][idI][idT]=-1;
				break;
			default:
				cerr << "No option of varaibles switched\n";
				exit(0);
		}
		
		variablesReducedCost.pop();
		count++;
	}	
	
}
void Model::getSolution(IloEnv& env, Instance inst){
	if((cplex.getStatus() == IloAlgorithm::Optimal) || (cplex.getStatus() == IloAlgorithm::Feasible)){
		int v, i, j, t, t0;
        int V = inst.speed.getSize();
        int J = inst.J;
        int N = J+1;
        for(v=0;v<V;v++){
            for(i=1;i<=J;i++){ //Not needed to consider source node either sink node
                for (j=1;j<=J;j++){ //Consider only ports, as the sink arcs are considered separately					
					for(t=1;t<=inst.t;t++){
						if(hasArc[v][i][j][t]==1){
							xValue[v][i][j][t] = cplex.getValue(x[v][i][j][t]); 
						}
						if (i == 1){ //(v,j,t) iterator
							if(hasEnteringArc1st[v][j][t] == 1) {
								oValue[v][j][t] = cplex.getValue(o[v][j][t]);
	
								if(t<inst.t){
									#ifdef WaitAfterOperate
									wBValue[v][j][t] = cplex.getValue(wB[v][j][t]);
									#endif     
									wValue[v][j][t] = cplex.getValue(w[v][j][t]);
								}				
		                         xValue[v][j][N][t] = cplex.getValue(x[v][j][N][t]);  //Gets the value of sink arc
							}
						}                                
					}                    
                }
            }
        }
	}else{
		cout << "Impossible to get feasible solution values!" << endl;
		exit(1);
	}
}
void Model::fixAllSolution(IloEnv& env, const Instance& inst){
    int v,i,j,t;
    int T = inst.t;
    int J = inst.J;
    int N = J+1;
    int V = inst.speed.getSize();
    //Get the solution values
    getSolution(env,inst);
    for(v=0;v<V;v++){
        for(i=1;i<=J;i++){
            for(j=1;j<=N;j++){
                for(t=1;t<=T;t++){
                    if(i != j && hasArc[v][i][j][t]==1){
                        x[v][i][j][t].setBounds(round(xValue[v][i][j][t]),round(xValue[v][i][j][t]));
                    }
                    if(j==1){ //(v,i,t) iterator
                        if(hasEnteringArc1st[v][j][t] == 1) {
							o[v][i][t].setBounds(round(oValue[v][i][t]),round(oValue[v][i][t]));
							if(t<T){
								w[v][i][t].setBounds(round(wValue[v][i][t]),round(wValue[v][i][t]));
								#ifdef WaitAfterOperate
								wB[v][i][t].setBounds(round(wBValue[v][i][t]),round(wBValue[v][i][t]));
								#endif								
							}							
						}
                    }
                }
            }
        }
    }
}
/*
 * Select itevatively a pair of vessel to be optimized.
 * If a subproblem improves the solution, the search is re-started from the first vessel pair
 */
void Model::improvementPhaseVND_vessels(IloEnv& env, Instance inst, const double& timePerIter, const double& gap, 
double& incumbent, Timer<chrono::milliseconds>& timer_cplex,float& opt_time, const double& timeLimit, float& elapsed_time,
	unsigned int& stopsByGap,unsigned int& stopsByTime){
	Timer<chrono::milliseconds> timer_LS;
	timer_LS.start();
	int v1, v2, V = inst.speed.getSize();
	if (gap > 1e-04)
		cplex.setParam(IloCplex::EpGap, gap/100);
		
	double prevObj = incumbent;    
    for(v1=0;v1<V;v1++){
		optimizeV1:
		cout << "Unfixing vessel v1 " << v1 << endl;
		unFixVessel(inst,v1);
		for(v2=v1+1;v2<V;v2++){
			cout << "unfixing vessel v2 " << v2 << endl;
			unFixVessel(inst,v2);
			
			optimizeV2:	
			if(elapsed_time/1000 >= timeLimit){				
				cout << "Stop by time\n";
				break;					
			}		
			cplex.setParam(IloCplex::TiLim,min(timePerIter, max(timeLimit-elapsed_time/1000,0.0)));
			timer_cplex.start();
			cplex.solve();
			if(cplex.getMIPRelativeGap() > 1e-04){
				++stopsByTime;
			}else{
				++stopsByGap;
			}
			opt_time += timer_cplex.total();
			elapsed_time += timer_LS.total();
			timer_LS.start();			
			incumbent = cplex.getObjValue();
			getSolutionVesselPair(env,inst,v1,v2);
			if(prevObj-incumbent >= 0.1){ //Improved
				cout << "Improved " << prevObj << " -> " << incumbent << endl;				
				prevObj = incumbent;					
				if(v1 == 0 && v2 == 1){ //Improvement on the first iteration
					cout << "Re-optimize\n";
					goto optimizeV2;
				}else if(v1 == 0){ //When v1 = 0 Only fixes v2
					cout << "Fixing v2 " << v2 << endl;
					fixVessel(env,inst,v2,false);
					v2=v1;						
				}else{ //Refix both vessels
					cout << "Fixing " << v1 << " and " << v2 << endl;
					fixVesselPair(env, inst, v1,v2,false); 
					v1 = 0;
					v2 = 0;	
					goto optimizeV1;				
				}					
			}else{					
				cout << "No improvement\n";	
				cout << "Fixing v2 " << v2 << endl;
				fixVessel(env,inst,v2,false);
			}
		}		
		if(elapsed_time/1000 >= timeLimit){
			break;					
		}
		cout << "Fixing v1 " << v1 << endl;
		fixVessel(env,inst,v1,false);		
	}
	//Use for verify if terminated by time (not change v1 and v2 index), or terminated normally (need change the index)
	if(v1==V) v1--;
	if(v2==V) v2--;
	cout << "Fixing vessel v1 and v2 " << v1 << " " << v2 << endl;
	fixVesselPair(env,inst,v1,v2,false);
}
void Model::unFixVessel(Instance inst, const int& v){
    int i,j,t;
    int T = inst.t;
    int J = inst.J;
    int N = J+1;
    for(i=1;i<=J;i++){
        for(j=1;j<=N;j++){
            for(t=1;t<=T;t++){
                if(i != j)
                    x[v][i][j][t].setBounds(0,1);
                
                if(j==1){ //(v,i,t) iterator
                    o[v][i][t].setBounds(0,1);
                    if(t<T){
                        w[v][i][t].setBounds(0,1);
                        #ifdef WaitAfterOperate
                        wB[v][i][t].setBounds(0,1);
                        #endif
                    }
                }
            }
        }
    }
}
void Model::fixVesselPair(IloEnv env, Instance inst, const int& v,const int& v1,const bool& getValues){
    int i,j,t;
    int T = inst.t;
    int J = inst.J;
    int N = J+1;
    //Get the values
    if(getValues){
		for(i=1;i<=J;i++){
			for(j=1;j<=N;j++){
				for(t=1;t<=T;t++){
					if(i != j){
						if(hasArc[v][i][j][t]==1)
							xValue[v][i][j][t] = cplex.getValue(x[v][i][j][t]);
						if(hasArc[v1][i][j][t]==1)
							xValue[v1][i][j][t] = cplex.getValue(x[v1][i][j][t]);
					}
					if(j==1){ //(v,i,t) iterator
						if(hasEnteringArc1st[v][i][t]==1){
							oValue[v][i][t] = cplex.getValue(o[v][i][t]);
							if(t<T){
								wValue[v][i][t] = cplex.getValue(w[v][i][t]);
								#ifdef WaitAfterOperate
								wBValue[v][i][t] = cplex.getValue(wB[v][i][t]);
								#endif
							}
						}
						if(hasEnteringArc1st[v1][i][t]==1){
							oValue[v1][i][t] = cplex.getValue(o[v1][i][t]);
							if(t<T){                            
								wValue[v1][i][t] = cplex.getValue(w[v1][i][t]);
								#ifdef WaitAfterOperate                            
								wBValue[v1][i][t] = cplex.getValue(wB[v1][i][t]);
								#endif
							}
						}
					}
				}
			}
		}
	}
    //Fix vessels
    for(i=1;i<=J;i++){
        for(j=1;j<=N;j++){
            for(t=1;t<=T;t++){
                if(i != j){
                    if(hasArc[v][i][j][t]==1)
                        x[v][i][j][t].setBounds(round(xValue[v][i][j][t]),round(xValue[v][i][j][t]));
                    if(hasArc[v1][i][j][t]==1)
                        x[v1][i][j][t].setBounds(round(xValue[v1][i][j][t]),round(xValue[v1][i][j][t]));
                }
                if(j==1){ //(v,i,t) iterator
                    //v
                    if(hasEnteringArc1st[v][i][t]==1){
                        o[v][i][t].setBounds(round(oValue[v][i][t]),round(oValue[v][i][t]));
                        if(t<T){
                            w[v][i][t].setBounds(round(wValue[v][i][t]),round(wValue[v][i][t]));
                            #ifndef WaitAfterOperate
                            oB[v][i][t].setBounds(round(oBValue[v][i][t]),round(oBValue[v][i][t]));
                            #endif
                        }
                    }
                    //v1
                    if(hasEnteringArc1st[v1][i][t]==1){
                        o[v1][i][t].setBounds(round(oValue[v1][i][t]),round(oValue[v1][i][t]));
                        if(t<T){
                            w[v1][i][t].setBounds(round(wValue[v1][i][t]),round(wValue[v1][i][t]));
                            #ifdef WaitAfterOperate
                            wB[v1][i][t].setBounds(round(wBValue[v1][i][t]),round(wBValue[v1][i][t]));
                            #endif
                        }
                    }
                }
            }
        }
    }
}
void Model::getSolutionVesselPair(IloEnv& env, Instance inst, const unsigned int& v1, const unsigned int& v2){
	if((cplex.getStatus() == IloAlgorithm::Optimal) || (cplex.getStatus() == IloAlgorithm::Feasible)){
		unsigned int v, idV, i, j, t, t0;
		array<unsigned int,2> VIDs = {v1,v2};
        
        int J = inst.J;
        int N = J+1;
        for(idV=0;idV<VIDs.size();idV++){
			v = VIDs[idV];
            for(i=1;i<=J;i++){ //Not needed to consider source node either sink node
                for (j=1;j<=J;j++){ //Consider only ports, as the sink arcs are considered separately					
					for(t=1;t<=inst.t;t++){
						if(hasArc[v][i][j][t]==1){
							xValue[v][i][j][t] = cplex.getValue(x[v][i][j][t]); 
						}
						if (i == 1){ //(v,j,t) iterator
							if(hasEnteringArc1st[v][j][t] == 1) {
								oValue[v][j][t] = cplex.getValue(o[v][j][t]);
								if(t<inst.t){
									#ifdef WaitAfterOperate
									wBValue[v][j][t] = cplex.getValue(wB[v][j][t]);
									#endif                            
									wValue[v][j][t] = cplex.getValue(w[v][j][t]);
								}								
                                xValue[v][j][N][t] = cplex.getValue(x[v][j][N][t]);  //Gets the value of sink arc
							}
						}                                
					}                    
                }
            }
        }
	}else{
		cout << "Impossible to get feasible solution values!" << endl;
		exit(1);
	}
}
void Model::fixVessel(IloEnv env, Instance inst, const int& v,const bool& getValues){
    int i,j,t;
    int T = inst.t;
    int J = inst.J;
    int N = J+1;
    //Get the values
    if(getValues){
		for(i=1;i<=J;i++){
			for(j=1;j<=N;j++){
				for(t=1;t<=T;t++){
					if(i != j){
						if(hasArc[v][i][j][t]==1){
							xValue[v][i][j][t] = cplex.getValue(x[v][i][j][t]);
						}						
					}
					if(j==1){ //(v,i,t) iterator
						if(hasEnteringArc1st[v][i][t]==1){
							oValue[v][i][t] = cplex.getValue(o[v][i][t]);
							if(t<T){
								wValue[v][i][t] = cplex.getValue(w[v][i][t]);
								#ifdef WaitAfterOperate
								wBValue[v][i][t] = cplex.getValue(wB[v][i][t]);
								#endif
							}
						}						
					}
				}
			}
		}
	}
    //Fix vessels
    for(i=1;i<=J;i++){
        for(j=1;j<=N;j++){
            for(t=1;t<=T;t++){
                if(i != j){
                    if(hasArc[v][i][j][t]==1){
                        x[v][i][j][t].setBounds(round(xValue[v][i][j][t]),round(xValue[v][i][j][t]));
					}
                }
                if(j==1){ //(v,i,t) iterator
                    //v
                    if(hasEnteringArc1st[v][i][t]==1){
                        o[v][i][t].setBounds(round(oValue[v][i][t]),round(oValue[v][i][t]));
                        if(t<T){
                            w[v][i][t].setBounds(round(wValue[v][i][t]),round(wValue[v][i][t]));
                            #ifdef WaitAfterOperate
                            wB[v][i][t].setBounds(round(wBValue[v][i][t]),round(wBValue[v][i][t]));
                            #endif
                        }
                    }                    
                }
            }
        }
    }
}
