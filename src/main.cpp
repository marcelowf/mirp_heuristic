#include <iostream>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "../include/util.h"
#include "../include/solution.h"

#define NTestInstanceSet
using namespace std;

int main(const int argc, char** argv) {
stringstream ss;
ss << "Usuage is \n " 
	<< "-p : instance Path \n " 
	<< "-v : Solution approach: 1 - multistart; 2 - Multistart with embeeded LNS;\n"
	<< "-t : Time limit in seconds \n " 
	<< "-n : number of solutions generated (at each iteration of CSMA) \n " 
	<< "-a : (range 0.0-> 1.0; default 1.0) probability of the best vessel be selected (acording to the criteria) to perform the voyage \n"
	<< "-c : (range 0.0-> 1.0; default 0.0) probability of selecting randomly a counterpart node along the iterations \n"
	<< "-j : (range 0.0-> 1.0; default -0.0) probability of selecting randomly a second port to be visited \n"
	<< "-s : number of the best so far solutions added to the MIP at each iteration  \n" 
	<< "-m : number of the best so far routes of each vessel added to the MIP at each iteration  \n";
	
	
 if (argc < 6) {
	cerr << "Error! At least 5 parameters must be defined" << endl;
	cerr << ss.str() << endl;
	exit(0);		
 }

int modelId;
unsigned int numSolutions, nSolutions, nRoutes, ageMax;
float r;
stringstream file;
string optstr;
double timeLimit;
float probA, probRandomSelectCounterPartNode, probRandomSelect2ndPort;
int opt, endBlock, outVessel, timePerIterFirst, timePerIterSecond, f ;
vector <string> instances;
instances.push_back("../Group1_data_format_only_files/LR1_1_DR1_3_VC1_V7a/");
instances.push_back("../Group1_data_format_only_files/LR1_1_DR1_4_VC3_V11a/");
instances.push_back("../Group1_data_format_only_files/LR1_1_DR1_4_VC3_V12a/");
instances.push_back("../Group1_data_format_only_files/LR1_1_DR1_4_VC3_V12b/");
instances.push_back("../Group1_data_format_only_files/LR1_1_DR1_4_VC3_V8a/");
instances.push_back("../Group1_data_format_only_files/LR1_1_DR1_4_VC3_V9a/");
instances.push_back("../Group1_data_format_only_files/LR1_2_DR1_3_VC2_V6a/");
instances.push_back("../Group1_data_format_only_files/LR1_2_DR1_3_VC3_V8a/");
instances.push_back("../Group1_data_format_only_files/LR2_11_DR2_22_VC3_V6a/");
instances.push_back("../Group1_data_format_only_files/LR2_11_DR2_33_VC4_V11a/");
instances.push_back("../Group1_data_format_only_files/LR2_11_DR2_33_VC5_V12a/");
instances.push_back("../Group1_data_format_only_files/LR2_22_DR2_22_VC3_V10a/");
instances.push_back("../Group1_data_format_only_files/LR2_22_DR3_333_VC4_V14a/");
//~ instances.push_back("../Group1_data_format_only_files/LR2_22_DR3_333_VC4_V17a/");
//~ instances.push_back("../Group1_data_format_only_files/t60/LR1_1_DR1_3_VC1_V7a/");
//~ instances.push_back("../Group1_data_format_only_files/t60/LR1_1_DR1_4_VC3_V11a/");
//~ instances.push_back("../Group1_data_format_only_files/t60/LR1_1_DR1_4_VC3_V12a/");
//~ instances.push_back("../Group1_data_format_only_files/t60/LR1_1_DR1_4_VC3_V12b/");
//~ instances.push_back("../Group1_data_format_only_files/t60/LR1_1_DR1_4_VC3_V8a/");
//~ instances.push_back("../Group1_data_format_only_files/t60/LR1_1_DR1_4_VC3_V9a/");
//~ instances.push_back("../Group1_data_format_only_files/t60/LR1_2_DR1_3_VC2_V6a/");
//~ instances.push_back("../Group1_data_format_only_files/t60/LR1_2_DR1_3_VC3_V8a/");
//~ instances.push_back("../Group1_data_format_only_files/t60/LR2_11_DR2_22_VC3_V6a/");
//~ instances.push_back("../Group1_data_format_only_files/t60/LR2_11_DR2_33_VC4_V11a/");
//~ instances.push_back("../Group1_data_format_only_files/t60/LR2_11_DR2_33_VC5_V12a/");
//~ instances.push_back("../Group1_data_format_only_files/t60/LR2_22_DR2_22_VC3_V10a/");
//~ instances.push_back("../Group1_data_format_only_files/t60/LR2_22_DR3_333_VC4_V14a/");
//~ instances.push_back("../Group1_data_format_only_files/t60/LR2_22_DR3_333_VC4_V17a/");
while ((opt = getopt(argc,argv,"p:v:t:n:a:c:j:s:m:r:g:")) != EOF)
	switch(opt)
	{
		case 'p':
			file << optarg;
			break;
		case 'v': 
			 modelId = stod(optarg);			 
			break;		
		case 't': 
			 timeLimit = stod(optarg);			 
			break;		
		case 'n':
			numSolutions = stod(optarg);
			break;
		case 'a':
			probA = stod(optarg);
			break;		
		case 'c':
			probRandomSelectCounterPartNode = stod(optarg);
			break;		
		case 'j':
			probRandomSelect2ndPort = stod(optarg);
			break;		
		case 's':
			nSolutions = stod(optarg);
			break;		
		case 'm':
			nRoutes = stod(optarg);
			break;		
		case 'r':
			r = stod(optarg);
			break;		
		case 'g':
			ageMax = stod(optarg);
			break;		
		default: cout<<endl; abort();
	}
	ofstream output;	
	output.open ("output.txt");	
 switch (modelId){
	case 1:
		output << "Inst \t idBestSol \t bestObj \t LNSObj \t itLNS \t cplexObj \t totalTime \t modelTime \t cplexTime \t Feasibility1 \t Feasibility2 \n";
		#ifdef NTestInstanceSet
		mirp::multiStart(file.str(), output, timeLimit, numSolutions, probA, probRandomSelectCounterPartNode, nRoutes, nSolutions, ageMax, r, probRandomSelect2ndPort);
		break;
		#endif
		
		#ifndef NTestInstanceSet
		for(int i=0;i<instances.size();i++){
			mirp::multiStart(instances[i], output, timeLimit, numSolutions, probA, probRandomSelectCounterPartNode, nRoutes, nSolutions, ageMax, r, probRandomSelect2ndPort);
		}
		break;	
		#endif
	case 2:
		output << "Inst \t idBestSol \t bestObj \t LNSObj \t itLNS \t cplexObj \t totalTime \t modelTime \t cplexTime \t Feasibility1 \t Feasibility2 \n";
		#ifdef NTestInstanceSet
		mirp::multiStartLNS(file.str(), output, timeLimit, numSolutions, probA, probRandomSelectCounterPartNode, nRoutes, nSolutions, ageMax, r, probRandomSelect2ndPort);
		break;
		#endif
		
		#ifndef NTestInstanceSet
		for(int i=0;i<instances.size();i++){
			mirp::multiStartLNS(instances[i], output, timeLimit, numSolutions, probA, probRandomSelectCounterPartNode, nRoutes, nSolutions, ageMax, r, probRandomSelect2ndPort);
		}
		break;	
		#endif
	default: 
		cout << "Error, no Model with index " << modelId << " was not found!"<< endl;
		break;
 } 
 output.close();
return 0;
}
