#pragma once
#include <vector>
#include <utility>
#include <ilcplex/ilocplex.h>
#include "random.h"
#include "util.h"

using namespace std;

namespace mirp{
	/*Parameters multistart:
    *timeLimit - in seconds
    *initMode - 0 - if vessels are forced to operate at maximium capacity as soon as possible when initializing a solution
    */
    void multiStart(std::string file, ofstream& output, const double& 
    timeLimit, const unsigned int& numSolutions, const float& 
    probSelectBestVessel, const float& probRandomSelectCounterPartNode, const unsigned int& nRoutes, const unsigned 
    int& nSolutions, const unsigned int& ageMax, const float& r, const float& probRandomSelect2ndPort);
    
    void multiStartLNS(std::string file, ofstream& output, const double& 
    timeLimit, const unsigned int& numSolutions, const float& 
    probSelectBestVessel, const float& probRandomSelectCounterPartNode, const unsigned int& nRoutes, const unsigned 
    int& nSolutions, const unsigned int& ageMax, const float& r, const float& probRandomSelect2ndPort);
	
    struct ComparePairHigherFirst{
		public:
		bool operator()(const pair<unsigned int, unsigned int>& pair1,const pair<unsigned int, unsigned int>& pair2){
			return (pair1.second < pair2.second);
		}
	};
    struct compareTupleHigherFirst{
		public:
		bool operator()(const tuple<unsigned int, unsigned int,float>& tuple1,const tuple<unsigned int, unsigned int,float>& tuple2){
			return (get<2>(tuple1) < get<2>(tuple2));
		}
	};
    struct compare4TupleMinorFirst{
		public:
		bool operator()(const tuple<unsigned int, unsigned int, unsigned int, float>& tuple1,const tuple<unsigned int, unsigned int, unsigned int,float>& tuple2){
			if(get<0>(tuple1) == get<0>(tuple2)){
				return (get<1>(tuple1) > get<1>(tuple2)); //First enum has priority over others (operate over wait/travel)
			}else{
				return (get<0>(tuple1) > get<0>(tuple2));
			}
		}
	};
	 struct compareSecondPortList{ //<1> has priority over <2> which have priority over <3> (1 is greater first, 2 and 3 minor first)
		public:
		bool operator()(const tuple<unsigned int, unsigned int, unsigned int, float>& tuple1,const tuple<unsigned int, unsigned int, unsigned int,float>& tuple2){
			if(get<1>(tuple1) == get<1>(tuple2)){
				if(get<2>(tuple1) == get<2>(tuple2)){
					return (get<3>(tuple1) > get<3>(tuple2)); 
				}else{
					return (get<2>(tuple1) > get<2>(tuple2)); 
				}
			}else{
				return (get<0>(tuple1) < get<0>(tuple2)); //Greater first
			}
		}
	};
    struct ComparePairMinorFirst{
		public:
		bool operator()(const pair<unsigned int, unsigned int>& pair1,const pair<unsigned int, unsigned int>& pair2){
			return (pair1.second > pair2.second);
		}
	};
    struct ComparePairMinorFloatFirst{
		public:
		bool operator()(const pair<unsigned int, float>& pair1,const pair<unsigned int, float>& pair2){
			return (pair1.second > pair2.second);
		}
	};
    struct ComparePairHighestFloatFirst{
		public:
		bool operator()(const pair<unsigned int, float>& pair1,const pair<unsigned int, float>& pair2){
			return (pair1.second < pair2.second);
		}
	};
    struct ComparePairHigherFloatFirst{
		bool operator()(const pair<unsigned int, float>& pair1,const pair<unsigned int, float>& pair2){
			return (pair1.second > pair2.second);
		}
	};  
    
    
	struct Solution{
        //For each vessel 
		
		/* 
		 * {v=<i,t,f,s>} 
		 * 	where f=0 when port not perform operation, and is always a positive value (either if load or unload)
		 * s is the load on vessel after operated in the corresponding node (i,t) 
		 * First id is for the initial position
		 */ 
		vector< vector<tuple<unsigned int, unsigned int,float, float> > > vesselRoute; 
		vector<float> vesselRouteCost; 	//Cost of the total route of each vessel (travelling + operations - revenue)
		vector<float> vesselRouteCostSpot; //vesselRouteCost + spot markets values used due the vessel route (used when sorting routes to add to the model)
                
        //For vessels - auxiliary structures for the iterations
        priority_queue< pair<unsigned int,unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst > vesselMinNumberOperations;
        priority_queue< pair<unsigned int,unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst > vesselEarliestArrivalPort; //Used to compare which vessel arrives before at the port -- New: combine the time to reach the loading and unloading port
        priority_queue< pair<unsigned int,float>, vector<pair<unsigned int, float>>, ComparePairMinorFloatFirst > vesselCostVoyage;
        priority_queue< pair<unsigned int,float>, vector<pair<unsigned int, float>>, ComparePairHighestFloatFirst > vesselHigherCostVoyage;        
        priority_queue< pair<unsigned int,unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst > vesselMinArrivalDifference;
        priority_queue< pair<unsigned int,unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst > vesselMinWaitTimes;
        priority_queue< pair<unsigned int,float>, vector<pair<unsigned int, float>>, ComparePairMinorFloatFirst > vesselCostQuantity;
        
        //For late vessels
        priority_queue< pair<unsigned int,unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst > lateVesselMinNumberOperations;
        priority_queue< pair<unsigned int,unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst > lateVesselEarliestArrivalPort;
        priority_queue< pair<unsigned int,float>, vector<pair<unsigned int, float>>, ComparePairMinorFloatFirst > lateVesselCostVoyage;
        priority_queue< pair<unsigned int,float>, vector<pair<unsigned int, float>>, ComparePairHighestFloatFirst > lateVesselHigherCostVoyage;
        priority_queue< pair<unsigned int,unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst > lateVesselMinArrivalDifference;
        priority_queue< pair<unsigned int,float>, vector<pair<unsigned int, float>>, ComparePairMinorFloatFirst > lateVesselMinimumAlphaBeta;
        priority_queue< pair<unsigned int,unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst > lateVesselMinWaitTimes;
        priority_queue< pair<unsigned int,float>, vector<pair<unsigned 
        int, float>>, ComparePairMinorFloatFirst > lateVesselCostQuantity;
        //ArrivalTimes (for each vessel)
        vector<pair<unsigned int, unsigned int> > lateVesselArrivalTimes; //[v]<Arrival at Loading, Arrival at Discharging>
                
        //For each port and time (i,t)
        vector<vector<unsigned int> > portBerthOcupation ;  //0 for no ocupation, >0 for the number of ocupations
        vector<vector<float> > portInventory;
        vector<vector<float> > portInventoryCopy;   //Copy of the port inventory for evaluation vessels - when using spot markets before a vessel operation
        vector<vector<float> > portAlpha;
        //Auxiliary variables
        vector<vector<float> > portBeta;
        vector<vector<float> > portTheta;
        priority_queue<tuple<unsigned int, unsigned int, float>,vector<tuple<unsigned int,unsigned int, float>>,compareTupleHigherFirst> infeasibilitiesBeta;
        priority_queue<tuple<unsigned int, unsigned int, float>,vector<tuple<unsigned int,unsigned int, float>>,compareTupleHigherFirst> infeasibilitiesTheta;
        
        //Aux structures to allow operations before other
        vector<vector<float> > maxF_it; //Maximum value that can be operated at time t in port i such that no inventory violation occurs in t' > t
        vector<vector<float> > maxF_itCopy; //For using in the scope of each vessel
        
        //vector<pair<unsigned int, float> > inventoryInvertedPeak; //For each port, store the time and value where inventory have a inverted peak, i.e. minimum inventory at loading ports, and minimum{sMax - s_it} at discharging ports
                
        //For each port
        vector< priority_queue< pair<unsigned int,unsigned int>,vector<pair<unsigned int,unsigned int>>, ComparePairHigherFirst > > portOperationTime; //For each port, <v,t> stores the vessels operations, where the most recent is on the top
        vector< vector< pair<unsigned int,unsigned int> > > portVisits; 
        //For each port, <v,t> stores the vessels visits, where the last item is the most recent
        vector<vector<float> > portOperations; //For each port and time, store the value operated at the port.
        vector<float> cummulativeAlpha;        
        vector<float> cummulativeBeta;
        vector<float> cummulativeTheta;
         //For init
        vector<int> initFirstVesselArriving; //Store the vessel which reaches the port earlier, considering the initial port of the vessels (-1 if no vessel arrives at port initially)
        vector<float> portCosts; //Store for each port the costs associated to the use alpha,beta and theta variables
                
        //Sequence of operations for the vessel
        /*
         * <t,op,i,value>
         * op = 0 operate at
         * op = 1 travel to (value = 0)
         * op = 2 wait at (value = 0)
         * op = 3 alpha
         * op = 4 beta
         * op = 5 theta
         * 
        */
        vector< priority_queue< tuple<unsigned int,unsigned int,unsigned int,float>, 
                vector<tuple<unsigned int,unsigned int,unsigned int,float>>, compare4TupleMinorFirst> > vesselOperationOrders;
        
        enum Actions{
            OPERATE,            
            TRAVEL,
            WAIT,
            ADDALPHA,
            ADDBETA,
            ADDTHETA           
        };
        ///For sotre the ports with the earliest inventory violation
        //For loading ports
        priority_queue<pair<unsigned int, unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst > pPortsOverflow;
        vector< pair<unsigned int, float> > lPortsHighCosumptionCapacity; //non increase order of D_it/s^Max
        //For discharging ports
        priority_queue<pair<unsigned int, unsigned int>, vector<pair<unsigned int, unsigned int>>, ComparePairMinorFirst > dPortsStockout;
        vector< pair<unsigned int, float> > dPortHighRevenue; //Sort the ports in non increase order of revenue
        vector< pair<unsigned int, float> > dPortsHighCosumptionCapacity; //non increase order of D_it/s^Max
        
        vector<bool> hasInventoryViolation;
        vector<unsigned int> inventoryViolation; //for each port, stores the time in which there is a violation on the port inventory
        //Values - objective function - for all vessels and ports
        float costTravel;
        float costOperation;
        float costAlpha;
        float profitRevenue;
        float costPenalization;
        float objectiveValue;        
        
        //Used for ranking the best vessel for performing a voyage
        float costVoyage; 
        unsigned int numOp;
        
        float totalAlphaBeta;
        
        //For LNS
        vector<bool> isVesselAvaiable; // True if vessel can be evaluated for
        
        ///Methods
        Solution(){};
        
        //Printing
        void print(Instance inst);
                
        void init(Instance inst, const vector<unsigned int>& avVessels);
        bool loadVessel(Instance inst, const unsigned int& v, const unsigned int& i, const unsigned int& t, const float f); //Load vessel v from port i in time t the quantity f.
        bool unloadVessel(Instance inst, const unsigned int& v, const unsigned int& i, const unsigned int& t, const float f); //Unload from vessel v in port i in time t the quantity f.
        void wait(Instance inst, const unsigned int& v, const unsigned int& i, const unsigned int& t);     //Make vessel v wait in port i one time period from t
        int getMinOperationTime(Instance inst, const unsigned int& v, const float& vLoad, const unsigned int& i, const unsigned int& t, unsigned int& numOp, bool isStored = false);
        bool vesselCanVisitNodes(Instance inst, unsigned int v, const vector<unsigned int>& avVessels, pair<unsigned int, unsigned int> urgentNode,  
			pair<unsigned int, unsigned int> counterPartNode, bool& cpNodeCanVisited, bool& uNodeCanVisited, const  float& probRandomSelect2ndPort, float fP=-1, float fD=-1);   //bools are passed by reference to indicate which port can (or cannot) be visited
        void performVoayge(Instance inst,unsigned int v, unsigned int i);
        void resetAuxliaryStructureForSelectingVessel(Instance inst);
        bool useSpotMarket(Instance inst, const pair<unsigned int, unsigned int>& node, const unsigned int& endTime, bool init=false);
        void useSpotMarketCopy(Instance inst, const int& v, const pair<unsigned int, unsigned int>& node, const unsigned int& endTime, vector<float>& portInventoryCP, vector<float>& maxF_itCP);
        void selectVesselRandomCriteria(Instance inst, int& bestVessel, int& bestDelayedVessel,float prob=1.0);
        void performActions(Instance inst, const unsigned int& v);
        void addAlphaBetaTheta(Instance inst,const unsigned int& i, const unsigned int& t,const float& alpha, const float& beta, const float& theta);
        unsigned int getNearestPortOtherType(Instance inst,const unsigned int& v); //Gets the nearest port of different type from the current position of vessel v
        void setObjectiveValue();
        void buildSolution(Instance inst, const bool& isPartial,  const vector<unsigned int>& avVessels, const float& probSelectBestVessel, const float& probRandomSelectCounterPartNode, const float& probRandomSelect2ndPort);
        //~ void LS_reduceSpotMarket(Instance inst);
        int getNextVisitingVessel(const int& j, const int& t, int& t_vj);
        pair<int,int> getPreviosNodeVisit(const unsigned int& v, const unsigned int& t);
        float getVesselRouteCost(Instance inst, const unsigned int& v, const pair<unsigned int,unsigned int>& nodeI);
		float addOperation(Instance inst,const unsigned int& v, const float& f, const unsigned int& i,const unsigned int& t,const unsigned int& idVesselOper);
        void setFirstVoygeVessel(Instance inst, const vector<unsigned int>& avVessels, const unsigned int& v);
        unsigned int get2ndRegionPort(Instance inst, const vector<unsigned int>& avVessels, const bool& isInit, const unsigned int& i, const unsigned int& t, const unsigned int& v, const float& f, float& fI, float& fJ, unsigned int& lessDelayedTime, float  probRandomSelect2ndPort=-0.0);
        bool vesselCanOperatePorts2(Instance inst,const unsigned int& v, const unsigned int& i,const unsigned int& j,const unsigned int& t,float& operateInI, float& operateInJ, const float& f);
        float getMaxOperationInOneTimePeriod(Instance inst,const unsigned int& i, const unsigned int& t);
        pair<unsigned int,unsigned int> getBestCounterpartNode(Instance inst,const unsigned int& urgentPort, float probRandomSelect=0.0);
        void removeAuxiliaryVariables(Instance inst, const unsigned int& v, const unsigned int& i, const unsigned int& t,vector<float>& portInvCP, const bool& isStored);
        void mergeActions(priority_queue< tuple<unsigned int,unsigned int,unsigned int,float>, 
                vector<tuple<unsigned int,unsigned int,unsigned int,float>>, compare4TupleMinorFirst>& vesselOperationOrders, 
                vector<tuple<unsigned int,unsigned int,unsigned int,float>>& actionsP1);
        
        //LNS
        vector<unsigned int> destroy(Instance inst, const unsigned int& type);	
        vector<unsigned int> removeVesselRoutes(Instance inst, const unsigned int& type);
        vector<unsigned int> getVesselsMajorWaitTimes(const unsigned int& numVessels);
        void rebuild(Instance inst, const unsigned int& type, const vector<unsigned int>& avVessels, const float& probSelectBestVessel, const float& probRandomSelectCounterPartNode, const float& probRandomSelect2ndPort);
        
        float getSumAlphaBetaUntilNextOperation(const unsigned int& i, const unsigned int& t);
        pair<unsigned int,unsigned int> getPortNextOperation(const unsigned int& i, const unsigned int& t);
        void updateMaxF(Instance inst, const unsigned int& i);
        void updateMaxFCopy(Instance inst, const unsigned int& i, const vector<float>& portInventoryCP, vector<float>& maxF_itCP);
        void updateAuxiliaryVariables(Instance inst,const bool& isStored, const unsigned int& v,const unsigned int& i, vector<pair<int, float>>& operations, const vector<float>& portInventoryCP);
	};
}
