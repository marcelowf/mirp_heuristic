#pragma once
#include <vector>
#include <utility>
#include <ilcplex/ilocplex.h>
#include <solution.h>
#include "util.h"

namespace mirp{		
	typedef IloArray<IloNumVarArray> NumVarMatrix;
	typedef IloArray<IloIntVarArray> IntVarMatrix;
	typedef IloArray<IloNumArray> NumNumMatrix;
	typedef IloArray<IloIntArray> IntMatrix;
	typedef IloArray<IloBoolVarArray> BoolMatrix; 
	
	void milp(std::string file, const double& timeLimit, std::string optStr);
	
	void fixAndRelax(std::string file, std::string optStr, const double& nIntervals, const double& gapFirst, const int& f, const double& overLap, const int& endBlockFirst,
const int& timePerIter, const double& mIntervals, const int& timePerIter2, const double& gapSecond, const double& overlap2,const double& timeLimit);
	
	void fixAndRelaxH(std:: string file, std::string optStr, const double& gapFirst, const int& outVessels, const int& timeLimitFirst, 
	const double& mIntervals, const int& timeLimitSecond, const double& gapSecond,const double& overlap2);
	
	void fixAndRelaxHV(std::string file, const double& nIntervals, const int& f, const double& overLap, const int& endBlock,
	const double& gapFirst, const int& outVessels, const int& timeLimitFirst, 
	const double& mIntervals, const int& timeLimitSecond, const double& gapSecond);
	
	
	struct CompareSFPairHighestFloatFirst{
		public:
		bool operator()(const std::pair<std::string, float>& pair1,const std::pair<std::string, float>& pair2){
			return (pair1.second < pair2.second);
		}
	};
	struct Model{
		IloObjective obj;
		IloModel model;
		IloCplex cplex;	
			
		/*Variables
		 * OBS: Arc variables has indexes for port 0 and port J+1, corresponding to the source and sink 'ports'.
		 * This not happen with 
		 */
		NumVarMatrix alpha; 								//amount of product purchasses from or sells to spot market in time time period	
		NumVarMatrix beta;	 								//Slack for amount of product purchasses from or sells to spot market in time time period	
		NumVarMatrix theta;	 								//Slack for amount of product purchasses from or sells to spot market in time time period	
		NumVarMatrix sP;									//Number of units of inventory at port j available at the end of time period t
		IloArray<NumVarMatrix> f;		 					//amount loaded/discharged by a vessel in a port-time node
		
		IloArray<IloArray<NumVarMatrix> >fX;				//Load on board vessel when travelling from port i to port j, leaving at time period t
		IloArray<NumVarMatrix> fO;							//Load on board vessel when starting to operate at port i in time period t
		IloArray<NumVarMatrix> fW;							//Load on board vessel while waiting at port i in time period t
		IloArray<NumVarMatrix> fWB;							//Load on board vessel while waiting at port i in time period t after operated in time t-1
		
		IloArray<IloArray<IloArray<IloBoolVarArray> > > x;	//Takes value 1 if vessel v travesses an arc departing from i and arriving at j, starting in time period t. Port 0 corresponds to the source node and port T+1 corresponds to the sink node
		IloArray<IloArray<IloBoolVarArray> > o;				//Takes value 1 if vessel v attmpts to load/discharge at port j in time period t
		IloArray<IloArray<IloBoolVarArray> > w;				//Takes value 1 if vessel v waits at port j in time period t
		IloArray<IloArray<IloBoolVarArray> > wB;			//Takes value 1 if vessel v waits at port j in time period t after operated in time period t-1
		
		/*Age of variables for the CSMA algorithm  
		 * Default value = -1 (not added to the model)
		*/
		std::vector<std::vector<std::vector<std::vector<int>>>> ageX; // x and fX
		std::vector<std::vector<std::vector<int>>> ageO;  // z, fOA and f
		std::vector<std::vector<std::vector<int>>> ageW;  // w and fW
		std::vector<std::vector<std::vector<int>>> ageWB; // wB and fWB
		
		//For storage the reduced costs
		IloArray<IloArray<IloArray<IloNumArray> > > xReducedCost;
		IloArray<IloArray<IloNumArray> > oReducedCost;
		IloArray<IloArray<IloNumArray> > wReducedCost;
		IloArray<IloArray<IloNumArray> > wBReducedCost;
		//For sorting the reduced costs - string storage the variable type and id ex ("x_1_2_1_5")
		priority_queue< pair<std::string,float>, vector<pair<std::string, float>>, CompareSFPairHighestFloatFirst > variablesReducedCost;				
		
		//Branching variables 
		IloArray<IloIntVarArray> sX;						//Number of times a port i is visited by vessel v during all planing horizon
		IloArray<IloIntVarArray> sOA;						//Number of times a vessel v starts to operate (or operate) at port i
		//~ IloArray<IloBoolVarArray> w;			//Used to force a vessel to depart from a region after it is empty(or full)
	
		//Information
		IloArray<IloArray<IloArray<IloIntArray> > > hasArc; 	//Takes value 1 if there is a travelling arc for vessel v travelling from i to j, starting at time t
		IloArray<IloArray<IloArray<IloNumArray> > > arcCost;	//Cost of travelling arc if hasArc == 1
		IloArray<IloArray<IloIntArray> > hasEnteringArc1st;		//Takes value 1 if node v,i,t has an entering arc in the first level(source,traveling,or waiting). i \in J
		
		//Conversion - For relax-and-fix 
		IloArray<IloArray<IloArray<IloArray<IloConversion> > > > convertX; //Convert x variables according [v][i][j][t]
		IloArray<IloArray<IloArray<IloConversion> > > convertO; //Convert z variables according to time ([v][i][t])
		IloArray<IloArray<IloArray<IloConversion> > > convertW; //Convert w variables according to time ([v][i][t])
		IloArray<IloArray<IloArray<IloConversion> > > convertWB; //Convert wB variables according to time ([v][i][t])
				
		//Store variables values 
		IloArray<IloArray<IloArray<IloNumArray> > > xValue;
		IloArray<IloArray<IloNumArray> > oValue;
		IloArray<IloArray<IloNumArray> > wValue;
		IloArray<IloArray<IloNumArray> > wBValue;
				
		IloArray<IloArray<IloNumArray> > fValue;
		IloArray<IloArray<IloArray<IloNumArray> > >  fXValue;
		IloArray<IloArray<IloNumArray> > fOValue;
		IloArray<IloArray<IloNumArray> > fWValue;
		IloArray<IloArray<IloNumArray> > fWBValue;
		IloArray<IloNumArray> sPValue;
		IloArray<IloNumArray> alphaValue;
		IloArray<IloNumArray> betaValue;
		IloArray<IloNumArray> thetaValue;

		///Constraints
		//Balance between nodes
		IloRangeArray sinkNodeBalance;
		IloRangeArray sourceNodeBalance;
		IloArray<IloArray<IloRangeArray> > firstLevelBalance;
		IloArray<IloArray<IloRangeArray> > secondLevelBalance;
				
		//Flow vessels
		IloArray<IloArray<IloRangeArray> > firstLevelFlow;
		IloArray<IloArray<IloRangeArray> > secondLevelFlow;

		IloArray<IloRangeArray> berthLimit;
		IloArray<IloArray<IloArray<IloRangeArray> > >  travelAtCapacity;
		IloArray<IloArray<IloArray<IloRangeArray> > >  travelEmpty;
		IloArray<IloRangeArray> portInventory;
		IloRangeArray cumSlack;
		IloArray<IloArray<IloRangeArray> > operationLowerLimit;
		IloArray<IloArray<IloRangeArray> > operationUpperLimit;
		
		IloArray<IloArray<IloArray<IloRangeArray> > >  flowCapacityX;
		IloArray<IloArray<IloRangeArray> >   flowCapacityO;
		IloArray<IloArray<IloRangeArray> >   flowCapacityW;
		IloArray<IloArray<IloRangeArray> >   flowCapacityWB;
		
		IloArray<IloArray<IloArray<IloRangeArray> > >  flowMinCapacityX;
		IloArray<IloArray<IloRangeArray> >   flowMinCapacityO;		
		IloArray<IloArray<IloRangeArray> >   flowMinCapacityW;
		IloArray<IloArray<IloRangeArray> >   flowMinCapacityWB;
		
		///Valid inequalities
		//For multiples denominators
		//~ //Loading ports
		//~ IloArray<IloArray<IloRangeArray> >knapsack_P_1; //Inequalities for the case where T_v = T (for each Q_v, j and it)
		//~ IloArray<IloRangeArray> knapsack_P_2; //Inequalities for the case where T_v = \emptyset
		//~ //Dischargin ports
		//~ IloArray<IloArray<IloRangeArray> > knapsack_D_1; //Case R²_v = T 
		//~ #ifndef WaitAfterOperate
		//~ IloArray<IloArray<IloRangeArray> > knapsack_D_2; //Case R¹_v = T
		//~ IloArray<IloRangeArray> knapsack_D_3; //Case R⁰_v = T
		//~ #endif
		//~ #ifdef WaitAfterOperate
		//~ IloArray<IloRangeArray>  knapsack_D_2; //Case R¹_v = T
		//~ #endif
		
		//For one denominator
		//Loading ports
		IloArray<IloRangeArray> knapsack_P_1; //Inequalities for the case where T_v = T (for each Q_v, j and it)
		IloArray<IloRangeArray> knapsack_P_2; //Inequalities for the case where T_v = \emptyset
		//Dischargin ports
		IloArray<IloRangeArray> knapsack_D_1; //Case R²_v = T
		IloArray<IloRangeArray> knapsack_D_2; //Case R¹_v = T
		IloArray<IloRangeArray> knapsack_D_3; //Case R⁰_v = T

		//Additional constraints;
		IloArray<IloRangeArray> branchingX;		//Branching rule where sX = sum{j\inJ}{t\inT} x[v][i][j][t]
		IloArray<IloRangeArray> branchingOA;	//Branching rule where sOA = sum{j\inJ}{t\inT} oA[v][i][t]
		IloRangeArray y_Sum; 					//For branching (total number visits in port j during all planing horizon)
		IloArray<IloRangeArray> minVisits;		//Valid inequality - minimum number of operations that must be done in each port from t=0 until time t' 
		IloArray<IloArray<IloRangeArray> > operateAndDepart;	//Ensure that a vessel must exit a region after operate when the vessel capacity is lesser than the maximum operation at port

		//Wagner-whitin e Lot-sizing with constant capacity (1 level)- variables and constraints
		IloArray<IloRangeArray> wwcc_relaxation;
		
		//Lot-sizing with start up relaxation
		IloArray<IloRangeArray> startup_sumStartIfOperate;  //(i,t)
		IloArray<IloRangeArray> startup_sumForceStartup; 	//(i,t)
		IloArray<IloRangeArray> startup_dlsccs;				//(i,t)
		IloArray<IloRangeArray> startup_validInequality;	//(i,x), where x is a combination in funcion of [k,l] and p
		
	
		///Branching rules
		IntVarMatrix sumX;
		IloArray<IloRangeArray> priorityX;
		IntVarMatrix sumOA;
		IloArray<IloRangeArray> priorityOA;
		
		//Others
		std::vector<std::pair<int,int> > ordV;
			
		Model(IloEnv& env) : obj(env), model(env), cplex(model){
			model.add(obj);		
		};	
		void buildModel(IloEnv& env, Instance inst); 
		void buildFixAndRelaxModel(IloEnv& env, Instance inst, const double& nIntervals, const int& endBlock, const bool& validIneq, const bool& addConstr, const bool& tightenInvConstr, const bool& proportionalAlpha, const bool& reduceArcs, const bool& tightenFlow); 
		void setParameters(IloEnv& env, const double& timeLimit, double gap=1e-04);
		void fixSolution(IloEnv& env, Instance inst, const int& t3S, const int& t3F, const int& p, const bool& fixSinkArc);
		void fixAllSolution(IloEnv& env, const Instance& inst);
		void getSolution(IloEnv& env, Instance inst);
		void getSolutionVesselPair(IloEnv& env, Instance inst, const unsigned int& v1, const unsigned int& v2);
		void improvementPhase_timeIntervals(IloEnv& env, Instance inst, const double& mIntervals, const double& timePerIter, const double& gap, const double& overlap, Timer<std::chrono::milliseconds>& timer_cplex,float& opt_time,const double& timeLimit, float& elapsed_time, double& incumbent);
		void unFixInterval(Instance inst, const int& tS, const int& tF);
		void improvementPhaseVND_vessels(IloEnv& env, Instance inst, const double& timePerIter, const double& gap, double& incumbent, Timer<chrono::milliseconds>& timer_cplex,float& opt_time, const double& timeLimit, float& elapsed_time, unsigned int& stopsByGap,unsigned int& stopsByTime);
		void fixVessel(IloEnv env, Instance inst, const int& v,const bool& getValues);
		void fixVesselPair(IloEnv env, Instance inst, const int& v,const int& v1,const bool& getValues);
		void fixVesselInterval(IloEnv env,Instance inst, const int& v,const int& tS, const int& tF);
		void unFixVessel(Instance inst, const int& v);
		void printSolution(IloEnv env, Instance inst, const int& tF);
		void getSolVals(IloEnv& env, const Instance& inst);
		void getSolValsW(IloEnv& env, Instance inst, const int& tS, const int& tF, const bool& fixSinkArc);
	
						
		void setRoute(Instance inst, std::vector< std::priority_queue< std::pair<unsigned int, float>, std::vector<std::pair<unsigned int,float>>, ComparePairMinorFloatFirst> >& vesselRoutes, const std::vector<Solution>& solutions, const unsigned int& n, const bool& addAdjacents);
		void setSolutions(Instance inst, const std::vector<Solution>& solutions, priority_queue< pair<unsigned int, float>, vector<pair<unsigned int,float>>, ComparePairMinorFloatFirst>& solutionsObj, const unsigned int& n, const bool& addAdjacents);
		void buildNetwork(IloEnv& env, Instance inst);
		void buildReducedModel(IloEnv& env, Instance inst);
		void addComponentsToModel(IloEnv& env, Instance inst);
		void adapt(IloEnv& env, Instance inst, const float& r,const unsigned int& ageMax);
		void setMIPStart(IloEnv& env, Instance inst, const Solution& solution);
		
	};
}
