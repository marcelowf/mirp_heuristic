#pragma once
#include "../include/solution.h"
#include "../include/util.h"
using namespace std;

namespace mirp{
	Solution LS_reduceBetaPenalization(Instance inst, const Solution& solution);

}
